﻿Imports System.IO
Imports System.Threading

Module modActAPSCalApp

  Public gstrAPSCalFileName As String
  Public gstrAPSSlopeDeltaTestResult As String = ""
  Public gstrAPSType As String
  Public gdblAPSCALTime As Double
  Public gstrAPSCALResult As String

  Public gDutPower As clsDutPower

  Public gstrBaseCalibrationTestResult As String = ""
  Public gdblBurnInElapsedTimeFaults As Double
  Public gdblBurnInTempFaults As Double
  Public gstrBurninTimesArray() As String
  Public gstrBurninTempsArray() As String
  Public gintBurnInTime As Integer
  Public gintBurnInCycles As Integer

  Public gstrCalDataIndexTestResult As String = ""
  Public gstrCalibrationWraparoundTestResult As String = ""

  Public gDIOStructure As DIOStructure

  Public gstrEEPROMReadDirectory As String
  Public gstrEEPROMArray() As String

  Public gFileChecksum As UInt32

  Public gintHWTemp As Integer

  Public gintMaxInternalTemperature As Integer
  Public gsngMinBatteryVoltage As Single
  Public gstrMonotonicityTestResult As String = ""

  Public gstrPostBurninDataFile As String

  Public gSoftwareFiles As SoftwareFilesStructure

  Public gstrTimeFileName As String

  Public gstrXLFileName As String

  'for frmNewRack
  Public gstrNewRackInstructions As String
  Public gstrCurrentCartID As String
  Public gintCartNumber As Int16 = 0
  Public gintReportCartNumber As Int16 = 9999 'to save cart number for last part in the cart
  Public gintCartInProcessID As Int16
  Public gintNumberOfFirstBurnInRack As Int16
  Public gintNumberOfBurnInRacks As Int16

  Public gstrCartID As String
  Public gstrStall As String
  Public gstrCartBom As String


  ' CRC polynomial
  Private Const CRC32_POLY As UInteger = &H4C11DB7


  Public Structure DIOStructure
    Dim StartButton As Integer
    Dim RedLight As Integer
    Dim YellowLight As Integer
    Dim GreenLight As Integer
    Dim StartButtonWithoutProx As Integer
    Dim Prox As Integer
  End Structure

  Public Structure SoftwareFilesStructure
    Dim NetworkVault As String
    Dim LocalVault As String
    Dim LocalExcel As String
  End Structure

  Public Function APSCalibrate() As Integer
    Dim strResult As String = ""
    Dim intResult As Integer
    Dim blnResult As Boolean
    Dim intDynamicWriteBytes(0) As Integer
    Dim lDelayStopwatch As New clsStopWatch
    Dim lAPSCalStopWatch As New clsStopWatch
    Dim intReturnValue As Integer
    Dim strErrorMessage As String
    Try

      intReturnValue = 2
      gstrAPSCALResult = ""
      gdblAPSCALTime = 0

      'Cycle Power to DUT
      'Call SetSupplyVoltage(0)
      'lDelayStopwatch.DelayTime(500)

      'Call SetSupplyVoltage(gElectricalOperationAndLoad.TestVoltage)
      'lDelayStopwatch.DelayTime(500)
      ResetPowerSupply()
      SetPowerSupply()

      'Prevents DUT from starting burnin profile
      Call gCanActuator.ReadSerialNumber(strResult)
      lDelayStopwatch.DelayTime(300)

      'Write APS Calibration Message
      If gElectricalOperationAndLoad.UsesAPSCal3 Then
        intDynamicWriteBytes(0) = 0
      Else
        intDynamicWriteBytes(0) = 255
      End If
      blnResult = gCanActuator.APSCalibrate(intDynamicWriteBytes, strResult)
      If gCanActuator.Anomaly IsNot Nothing Then
        gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
        gCanActuator.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      lDelayStopwatch.DelayTime(200)

      lAPSCalStopWatch.Reset()
      lAPSCalStopWatch.Start()
      Do
        'Read APS Calibration Progress. Result of 1 means still in progress
        blnResult = gCanActuator.ReadAPSCalibrateProgress(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gCanActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        intResult = CInt(strResult)

        If intResult > 1 Then ' APS Calibration Complete
          gstrAPSCALResult = strResult
          gdblAPSCALTime = lAPSCalStopWatch.ElapsedMilliseconds
          intReturnValue = intResult
          Exit Do
        End If

        If lAPSCalStopWatch.ElapsedMilliseconds > 60000 Then
          strErrorMessage = "APS Cal Timeout"
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
          Throw New TsopAnomalyException
        End If

        lDelayStopwatch.DelayTime(200)
      Loop

      lAPSCalStopWatch.Stop()

      Return intReturnValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return intReturnValue
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return intReturnValue
    End Try
  End Function

  Public Sub BuildCellArray(ByRef strCellArray() As String, ByVal intRowNumber As Integer)
    Dim strLetters(-1) As String
    Dim intLoop As Integer
    Dim intIndex As Integer
    Dim intLetter1 As Integer
    Dim intLetter2 As Integer
    Dim strLetter1 As String = ""
    Dim strCellName As String
    Dim intCell As Integer
    Dim blnDone As Integer
    Try
      intCell = 0

      'Get Array of Alpha Letters
      For intLoop = 65 To 90
        intIndex = intLoop - 65
        ReDim Preserve strLetters(intIndex)
        strLetters(intIndex) = Chr(intLoop)
      Next

      For intLetter1 = -1 To 25
        If blnDone Then Exit For
        If intLetter1 = -1 Then
          strLetter1 = ""
        Else
          strLetter1 = strLetters(intLetter1)
        End If
        For intLetter2 = 0 To 25
          If intCell >= strCellArray.Length Then
            blnDone = True
            Exit For
          End If
          strCellName = strLetter1 & strLetters(intLetter2) & CStr(intRowNumber)
          'Console.WriteLine(strCellName)
          strCellArray(intCell) = strCellName
          intCell += 1
        Next
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function CRC_32(ByVal buffer As List(Of IntelDataRecord), ByVal endAddress As UShort, ByRef fileChksum As UInteger) As Boolean
    ' Function to generate IAR 32 bit CRC
    Dim CRC As UInteger = &H0
    Dim Address As UShort = &H0
    Dim returnValue As Boolean = False

    Try
      ' Loop through all bytes in buffer
      For Each record As IntelDataRecord In buffer
        For Each value As Byte In record.Data
          CRC = CRC Xor (CUInt(value) << 24)
          For j As Byte = 0 To 7
            If (CRC And &H80000000) Then
              CRC = (CRC << 1) Xor CRC32_POLY
            Else
              CRC = (CRC << 1)
            End If
          Next
          If (Address = endAddress) Then
            fileChksum = CRC
            returnValue = True
            Exit For
          End If
          Address += 1
        Next
      Next

      Return returnValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return False
    End Try
  End Function

  Public Sub CreateBurninArraysFromEEProm()

    Dim blnEEPromStartFound As Boolean = False
    Dim strBurninTimes As String = ""
    Dim strBurninTemps As String = ""
    Dim bytFastTime1 As Byte
    Dim bytChecksum1 As Byte
    Dim bytCalcChecksum1 As Byte
    Dim dblSlowTime1 As Double
    Dim bytFastTime2 As Byte
    Dim bytChecksum2 As Byte
    Dim bytCalcChecksum2 As Byte
    Dim dblSlowTime2 As Double
    Dim bytCount As Byte = 0
    Dim bytComplement As Byte
    Dim dblBurninElapsedTime1 As Double
    Dim dblBurninElapsedTime2 As Double
    'Dim BurninStartTimes = New System.Collections.Concurrent.ConcurrentQueue(Of Double)
    Dim BurninStartTimes = New System.Collections.ArrayList 'New List(Of Double)
    Dim dblBurninElapsedTime As Double
    'Dim BurninTemperatures = New System.Collections.Concurrent.ConcurrentQueue(Of Integer)
    Dim BurninTemperatures = New System.Collections.ArrayList 'New List(Of Integer)
    Dim DataRecord As IntelDataRecord
    Dim idx As Byte
    Dim dblBurninTime As Double
    Dim intRecordCount As Integer
    Try

      ReDim gstrBurninTimesArray(-1)
      ReDim gstrBurninTempsArray(-1)
      BurninStartTimes.clear
      BurninTemperatures.clear
      strBurninTimes = ""
      strBurninTemps = ""
      intRecordCount = -1

      'For Each DataRecord As IntelDataRecord In IntelHexFile
      For Each DataRecord In gCanActuator.IntelHexFile
        intRecordCount += 1
        If (blnEEPromStartFound = False) Then
          If (DataRecord.StartAddress = 0) Then
            blnEEPromStartFound = True
          Else
            Continue For
          End If
        End If

        ' Find Burn-in cycle count.
        If ((DataRecord.StartAddress = &H80) AndAlso (DataRecord.DataLength >= 4)) Then
          strBurninTimes = "," & (&HFA - DataRecord.Data(3)).ToString()
          'Console.WriteLine("strBurninTimes = " & strBurninTimes)
        End If
        ' Search snapshots for burn-in snapshots.
        If ((DataRecord.StartAddress >= &H180) AndAlso (DataRecord.StartAddress <= &H6F0)) Then
          ' Extract and store the burn-in start times nd temperatures.
          'If ((DataRecord.DataLength >= 8) AndAlso (DataRecord.Data(0) = 83)) Then
          If ((DataRecord.DataLength >= 8) AndAlso (DataRecord.Data(0) = &H53)) Then
            'BurninStartTimes.Enqueue(((CDbl(DataRecord.Data(3)) + (CDbl(DataRecord.Data(4)) * 256) + (CDbl(DataRecord.Data(5)) * 65536)) / 10.0))
            'BurninTemperatures.Enqueue(CInt(DataRecord.Data(1)) - 55)
            BurninStartTimes.add(((CDbl(DataRecord.Data(3)) + (CDbl(DataRecord.Data(4)) * 256) + (CDbl(DataRecord.Data(5)) * 65536)) / 10.0))
            BurninTemperatures.add(CInt(DataRecord.Data(1)) - 55)
          End If
          'If ((DataRecord.DataLength = 16) AndAlso (DataRecord.Data(8) = 83)) Then
          If ((DataRecord.DataLength = 16) AndAlso (DataRecord.Data(8) = &H53)) Then
            'BurninStartTimes.Enqueue(((CDbl(DataRecord.Data(11)) + (CDbl(DataRecord.Data(12)) * 256) + (CDbl(DataRecord.Data(13)) * 65536)) / 10.0))
            'BurninTemperatures.Enqueue(CInt(DataRecord.Data(9)) - 55)
            BurninStartTimes.add(((CDbl(DataRecord.Data(11)) + (CDbl(DataRecord.Data(12)) * 256) + (CDbl(DataRecord.Data(13)) * 65536)) / 10.0))
            BurninTemperatures.add(CInt(DataRecord.Data(9)) - 55)
          End If
        End If

        ' Extract the overall burn-in elapsed time.
        If ((DataRecord.StartAddress = &H7C0) AndAlso (DataRecord.DataLength = 16)) Then
          bytFastTime1 = DataRecord.Data(8)
          bytCalcChecksum1 = bytFastTime1 Xor (DataRecord.Data(9) Xor DataRecord.Data(10))
          bytChecksum1 = DataRecord.Data(11)
          dblSlowTime1 = CDbl(65535 - (DataRecord.Data(9) + (DataRecord.Data(10) * 256.0))) * 8.0
          bytCount = 0
          bytComplement = Not bytFastTime1
          While bytComplement > 0
            bytComplement >>= 1
            bytCount += 1
          End While
          dblBurninElapsedTime1 = (dblSlowTime1 + bytCount) / 10.0
          bytFastTime2 = DataRecord.Data(12)
          bytCalcChecksum2 = bytFastTime2 Xor (DataRecord.Data(13) Xor DataRecord.Data(14))
          bytChecksum2 = DataRecord.Data(15)
          dblSlowTime2 = CDbl(65535 - (DataRecord.Data(13) + (DataRecord.Data(14) * 256.0))) * 8.0
          bytCount = 0
          bytComplement = Not bytFastTime2
          While bytComplement > 0
            bytComplement >>= 1
            bytCount += 1
          End While
          dblBurninElapsedTime2 = (dblSlowTime2 + bytCount) / 10.0
          If ((bytChecksum1 = bytCalcChecksum1) AndAlso (bytChecksum2 = bytCalcChecksum2)) Then
            If (dblBurninElapsedTime1 > dblBurninElapsedTime2) Then
              dblBurninElapsedTime = dblBurninElapsedTime1
            Else
              dblBurninElapsedTime = dblBurninElapsedTime2
            End If
          ElseIf (bytChecksum1 = bytCalcChecksum1) Then
            dblBurninElapsedTime = dblBurninElapsedTime1
          ElseIf (bytChecksum2 = bytCalcChecksum2) Then
            dblBurninElapsedTime = dblBurninElapsedTime2
          End If
        End If
      Next

      If (String.IsNullOrEmpty(dblBurninElapsedTime) = False) Then
        ' Insert serial number and burn-in elapsed time.
        strBurninTimes = gDeviceInProcess.EncodedSerialNumber & "," & dblBurninElapsedTime.ToString(".0") & strBurninTimes
      End If

      If (BurninStartTimes.Count > 0) Then
        ' Add individual burn-in cycle elapsed times.
        For idx = 0 To BurninStartTimes.Count - 1
          If (idx < (BurninStartTimes.Count - 1)) Then
            dblBurninTime = BurninStartTimes(idx + 1) - BurninStartTimes(idx)
            'strBurninTimes &= "," & (BurninStartTimes(idx + 1) - BurninStartTimes(idx)).ToString(".0")
            strBurninTimes &= "," & (dblBurninTime).ToString(".0")
            'Console.WriteLine("strBurninTimes = " & strBurninTimes)
          Else
            dblBurninTime = dblBurninElapsedTime - BurninStartTimes(idx)
            'strBurninTimes &= "," & (dblBurninElapsedTime - BurninStartTimes(idx)).ToString(".0")
            strBurninTimes &= "," & (dblBurninTime).ToString(".0")
            'Console.WriteLine("strBurninTimes = " & strBurninTimes)
          End If
        Next
      End If

      strBurninTemps = gDeviceInProcess.EncodedSerialNumber & ","
      If (BurninTemperatures.Count > 0) Then
        ' Add burn-in interval starting temperatures.
        For idx = 0 To BurninTemperatures.Count - 1
          If (idx < (BurninTemperatures.Count - 1)) Then
            strBurninTemps &= BurninTemperatures(idx) & ","
          Else
            strBurninTemps &= BurninTemperatures(idx)
          End If
        Next
      End If

      'Create  Burnin Arrays
      gstrBurninTimesArray = strBurninTimes.Split(",")
      'Console.WriteLine("Burnin Times Array Size = " & gstrBurninTimesArray.Length)

      gstrBurninTempsArray = strBurninTemps.Split(",")
      'Console.WriteLine("Burnin Temps Array Size = " & gstrBurninTempsArray.Length)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcAPSCalWorksheet()
    Dim xlsname As String
    Dim lDate As Date
    Dim lstrYear As String
    Dim lstrMonth As String
    Dim lstrDay As String
    Dim strDate As String = ""
    Dim strEEPROMToSave As String = ""

    Dim oXL As New Microsoft.Office.Interop.Excel.Application
    Dim oWB As Microsoft.Office.Interop.Excel.Workbook
    Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet

    Dim strFilename As String
    Dim intLoop As Integer
    Dim strCell As String = ""

    Try
      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      gstrMonotonicityTestResult = "FAILED"
      gstrBaseCalibrationTestResult = "FAILED"
      gstrCalibrationWraparoundTestResult = "FAILED"
      gstrCalDataIndexTestResult = "FAILED"

      xlsname = gSoftwareFiles.LocalExcel & gControlAndInterface.APSCALExcelFilename

      oXL.Visible = False

      ' Get a new workbook.
      oWB = oXL.Workbooks.Open(Filename:=xlsname)

      oSheet = oWB.Sheets(gControlAndInterface.ExcelOverviewSheet)

      oSheet.Range(gControlAndInterface.ExcelOverviewCheck).Value = "X"

      oSheet = oWB.Sheets(gControlAndInterface.ExcelAPSCALVerifySheet)

      For intLoop = 0 To 32
        strCell = "A" & CStr(intLoop + 4)
        oSheet.Range(strCell).Value = gstrEEPROMArray(intLoop)
      Next

      'oSheet.Range("A4").Value = gstrEEPROMArray(0) '":10071000FFFF0210660842081508E407C107940700"
      'oSheet.Range("A5").Value = gstrEEPROMArray(1) '":1007200062073D071107E006B90690065D06360600"
      'oSheet.Range("A6").Value = gstrEEPROMArray(2) '":100730000B06D905B005880557052D050505D50400"
      'oSheet.Range("A7").Value = gstrEEPROMArray(3) '":10074000A904810454042604FF03D203A3037C0300"
      'oSheet.Range("A8").Value = gstrEEPROMArray(4) '":1007500050031F03F902CD029C0274024A02170200"
      'oSheet.Range("A9").Value = gstrEEPROMArray(5) '":10076000F001C6019201690141010E01E600BF0000"
      'oSheet.Range("A10").Value = gstrEEPROMArray(6) '":100770008B0062003B000800DF0FB80F870F5D0F00"
      'oSheet.Range("A11").Value = gstrEEPROMArray(7) '":10078000360F040FD80EB10E800E540E2E0EFE0D00"
      'oSheet.Range("A12").Value = gstrEEPROMArray(8) '":10079000CF0DA90D7B0D4C0D240DF80CCA0CA10C00"
      'oSheet.Range("A13").Value = gstrEEPROMArray(9) '":1007A000750C460C1D0CF40BC50B990B720B420B00"
      'oSheet.Range("A14").Value = gstrEEPROMArray(10) '":1007B000150BF00ABF0A910A6C0A3C0A0C0AE70900"
      'oSheet.Range("A15").Value = gstrEEPROMArray(11) '":1007C000B7098609630933090209DE08AF087F0800"
      'oSheet.Range("A16").Value = gstrEEPROMArray(12) '":1007D0005B082D08FC07D907AC077B07035B321500"
      'oSheet.Range("A17").Value = gstrEEPROMArray(13) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A18").Value = gstrEEPROMArray(14) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A19").Value = gstrEEPROMArray(15) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A20").Value = gstrEEPROMArray(16) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A21").Value = gstrEEPROMArray(17) ' ":10008000FFFFFFFF91019101FFFFFFFF1F031F0300"
      'oSheet.Range("A22").Value = gstrEEPROMArray(18) ' ":10009000FFFFFFFFFFFFFFFF91019101FFFFFFFF00"
      'oSheet.Range("A23").Value = gstrEEPROMArray(19) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A24").Value = gstrEEPROMArray(20) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A25").Value = gstrEEPROMArray(21) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A26").Value = gstrEEPROMArray(22) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A27").Value = gstrEEPROMArray(23) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A28").Value = gstrEEPROMArray(24) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A29").Value = gstrEEPROMArray(25) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A30").Value = gstrEEPROMArray(26) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A31").Value = gstrEEPROMArray(27) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A32").Value = gstrEEPROMArray(28) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A33").Value = gstrEEPROMArray(29) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A34").Value = gstrEEPROMArray(30) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A35").Value = gstrEEPROMArray(31) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A36").Value = gstrEEPROMArray(32) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"

      oSheet.Calculate()

      strFilename = gDeviceInProcess.EncodedSerialNumber & "_" & gstrTimeFileName ' gstrTesterNumber & Format(Now, "yyyyMMddHHmmss")
      gstrAPSCalFileName = strFilename
      gstrXLFileName = gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay & "\" & strFilename & ".xlsm"

      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If Directory.Exists(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay) = False Then
        Directory.CreateDirectory(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay)
      End If

      oXL.ActiveWorkbook.SaveAs(gstrXLFileName)

      gstrMonotonicityTestResult = CStr(oSheet.Range("E31").Value)
      gstrBaseCalibrationTestResult = CStr(oSheet.Range("E33").Value)
      gstrCalibrationWraparoundTestResult = CStr(oSheet.Range("E35").Value)
      gstrCalDataIndexTestResult = CStr(oSheet.Range("E37").Value)
      gstrAPSSlopeDeltaTestResult = CStr(oSheet.Range("P240").Value)

      Call LogActivity("MonotonictyPassed= " & gstrMonotonicityTestResult)
      Call LogActivity("BaseCalibrationPassed= " & gstrBaseCalibrationTestResult)
      Call LogActivity("CalibrationWraparoundPassed= " & gstrCalibrationWraparoundTestResult)
      Call LogActivity("DataIndexCheckPassed= " & gstrCalDataIndexTestResult)
      If gstrAPSSlopeDeltaTestResult <> "" Then
        Call LogActivity("APSSlopeDelta= " & gstrAPSSlopeDeltaTestResult)
      End If

      oXL.Quit()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub EvaluateBurnInTemp()
    Dim lDate As Date

    Dim lstrYear As String
    Dim lstrMonth As String
    Dim lstrDay As String

    Dim oXL As New Microsoft.Office.Interop.Excel.Application
    Dim oWB As Microsoft.Office.Interop.Excel.Workbook
    Dim oSheet2 As Microsoft.Office.Interop.Excel.Worksheet

    Dim strErrorMessage As String
    Dim strCellArray() As String
    Dim intLoop As Integer

    Try

      oXL.Visible = False

      ' Get a new workbook.
      oWB = oXL.Workbooks.Open(Filename:=gstrXLFileName)

      oSheet2 = oWB.Sheets(gControlAndInterface.BurnInTempSheet)

      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If gstrBurninTempsArray.Length = 0 Then
        strErrorMessage = "BurnIn Temp Array Has No Data"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      ReDim strCellArray(gstrBurninTempsArray.Length - 1)
      Call BuildCellArray(strCellArray, 6)
      For intLoop = 0 To gstrBurninTempsArray.Length - 1
        oSheet2.Range(strCellArray(intLoop)).Value = gstrBurninTempsArray(intLoop)
      Next

      'oSheet2.Range("A6").Value = gstrBurninTempsArray(0)
      'oSheet2.Range("B6").Value = gstrBurninTempsArray(1)
      'oSheet2.Range("C6").Value = gstrBurninTempsArray(2)
      'oSheet2.Range("D6").Value = gstrBurninTempsArray(3)
      'oSheet2.Range("E6").Value = gstrBurninTempsArray(4)
      'oSheet2.Range("F6").Value = gstrBurninTempsArray(5)
      'oSheet2.Range("G6").Value = gstrBurninTempsArray(6)
      'oSheet2.Range("H6").Value = gstrBurninTempsArray(7)
      'oSheet2.Range("I6").Value = gstrBurninTempsArray(8)
      'oSheet2.Range("J6").Value = gstrBurninTempsArray(9)
      'oSheet2.Range("K6").Value = gstrBurninTempsArray(10)
      'oSheet2.Range("L6").Value = gstrBurninTempsArray(11)
      'oSheet2.Range("M6").Value = gstrBurninTempsArray(12)
      'oSheet2.Range("N6").Value = gstrBurninTempsArray(13)
      'oSheet2.Range("O6").Value = gstrBurninTempsArray(14)
      'oSheet2.Range("P6").Value = gstrBurninTempsArray(15)
      'oSheet2.Range("Q6").Value = gstrBurninTempsArray(16)
      'oSheet2.Range("R6").Value = gstrBurninTempsArray(17)
      'oSheet2.Range("S6").Value = gstrBurninTempsArray(18)
      'oSheet2.Range("T6").Value = gstrBurninTempsArray(19)
      'oSheet2.Range("U6").Value = gstrBurninTempsArray(20)
      'oSheet2.Range("V6").Value = gstrBurninTempsArray(21)
      'oSheet2.Range("W6").Value = gstrBurninTempsArray(22)
      'oSheet2.Range("X6").Value = gstrBurninTempsArray(23)
      'oSheet2.Range("Y6").Value = gstrBurninTempsArray(24)
      'oSheet2.Range("Z6").Value = gstrBurninTempsArray(25)

      'oSheet2.Range("AA6").Value = gstrBurninTempsArray(26)
      'oSheet2.Range("AB6").Value = gstrBurninTempsArray(27)
      'oSheet2.Range("AC6").Value = gstrBurninTempsArray(28)
      'oSheet2.Range("AD6").Value = gstrBurninTempsArray(29)
      'oSheet2.Range("AE6").Value = gstrBurninTempsArray(30)
      'oSheet2.Range("AF6").Value = gstrBurninTempsArray(31)
      'oSheet2.Range("AG6").Value = gstrBurninTempsArray(32)
      'oSheet2.Range("AH6").Value = gstrBurninTempsArray(33)
      'oSheet2.Range("AI6").Value = gstrBurninTempsArray(34)
      'oSheet2.Range("AJ6").Value = gstrBurninTempsArray(35)
      'oSheet2.Range("AK6").Value = gstrBurninTempsArray(36)
      'oSheet2.Range("AL6").Value = gstrBurninTempsArray(37)
      'oSheet2.Range("AM6").Value = gstrBurninTempsArray(38)
      'oSheet2.Range("AN6").Value = gstrBurninTempsArray(39)
      'oSheet2.Range("AO6").Value = gstrBurninTempsArray(40)
      'oSheet2.Range("AP6").Value = gstrBurninTempsArray(41)
      'oSheet2.Range("AQ6").Value = gstrBurninTempsArray(42)
      'oSheet2.Range("AR6").Value = gstrBurninTempsArray(43)
      'oSheet2.Range("AS6").Value = gstrBurninTempsArray(44)
      'oSheet2.Range("AT6").Value = gstrBurninTempsArray(45)
      'oSheet2.Range("AU6").Value = gstrBurninTempsArray(46)
      'oSheet2.Range("AV6").Value = gstrBurninTempsArray(47)
      'oSheet2.Range("AW6").Value = gstrBurninTempsArray(48)
      'oSheet2.Range("AX6").Value = gstrBurninTempsArray(49)
      'oSheet2.Range("AY6").Value = gstrBurninTempsArray(50)
      'oSheet2.Range("AZ6").Value = gstrBurninTempsArray(51)

      'oSheet2.Range("BA6").Value = gstrBurninTempsArray(52)
      'oSheet2.Range("BB6").Value = gstrBurninTempsArray(53)
      'oSheet2.Range("BC6").Value = gstrBurninTempsArray(54)
      'oSheet2.Range("BD6").Value = gstrBurninTempsArray(55)
      'oSheet2.Range("BE6").Value = gstrBurninTempsArray(56)
      'oSheet2.Range("BF6").Value = gstrBurninTempsArray(57)
      'oSheet2.Range("BG6").Value = gstrBurninTempsArray(58)
      'oSheet2.Range("BH6").Value = gstrBurninTempsArray(59)
      'oSheet2.Range("BI6").Value = gstrBurninTempsArray(60)
      'oSheet2.Range("BJ6").Value = gstrBurninTempsArray(61)
      'oSheet2.Range("BK6").Value = gstrBurninTempsArray(62)
      'oSheet2.Range("BL6").Value = gstrBurninTempsArray(63)
      'oSheet2.Range("BM6").Value = gstrBurninTempsArray(64)
      'oSheet2.Range("BN6").Value = gstrBurninTempsArray(65)
      'oSheet2.Range("BO6").Value = gstrBurninTempsArray(66)
      'oSheet2.Range("BP6").Value = gstrBurninTempsArray(67)
      'oSheet2.Range("BQ6").Value = gstrBurninTempsArray(68)
      'oSheet2.Range("BR6").Value = gstrBurninTempsArray(69)
      'oSheet2.Range("BS6").Value = gstrBurninTempsArray(70)
      'oSheet2.Range("BT6").Value = gstrBurninTempsArray(71)
      'oSheet2.Range("BU6").Value = gstrBurninTempsArray(72)
      'oSheet2.Range("BV6").Value = gstrBurninTempsArray(73)
      'oSheet2.Range("BW6").Value = gstrBurninTempsArray(74)
      'oSheet2.Range("BX6").Value = gstrBurninTempsArray(75)
      'oSheet2.Range("BY6").Value = gstrBurninTempsArray(76)
      'oSheet2.Range("BZ6").Value = gstrBurninTempsArray(77)

      'oSheet2.Range("CA6").Value = gstrBurninTempsArray(78)
      'oSheet2.Range("CB6").Value = gstrBurninTempsArray(79)
      'oSheet2.Range("CC6").Value = gstrBurninTempsArray(80)
      'oSheet2.Range("CD6").Value = gstrBurninTempsArray(81)
      'oSheet2.Range("CE6").Value = gstrBurninTempsArray(82)
      'oSheet2.Range("CF6").Value = gstrBurninTempsArray(83)
      'oSheet2.Range("CG6").Value = gstrBurninTempsArray(84)
      'oSheet2.Range("CH6").Value = gstrBurninTempsArray(85)
      'oSheet2.Range("CI6").Value = gstrBurninTempsArray(86)
      'oSheet2.Range("CJ6").Value = gstrBurninTempsArray(87)
      'oSheet2.Range("CK6").Value = gstrBurninTempsArray(88)
      'oSheet2.Range("CL6").Value = gstrBurninTempsArray(89)
      'oSheet2.Range("CM6").Value = gstrBurninTempsArray(90)
      'oSheet2.Range("CN6").Value = gstrBurninTempsArray(91)
      'oSheet2.Range("CO6").Value = gstrBurninTempsArray(92)
      'oSheet2.Range("CP6").Value = gstrBurninTempsArray(93)
      'oSheet2.Range("CQ6").Value = gstrBurninTempsArray(94)
      'oSheet2.Range("CR6").Value = gstrBurninTempsArray(95)
      'oSheet2.Range("CS6").Value = gstrBurninTempsArray(96)
      'oSheet2.Range("CT6").Value = gstrBurninTempsArray(97)
      'oSheet2.Range("CU6").Value = gstrBurninTempsArray(98)
      'oSheet2.Range("CV6").Value = gstrBurninTempsArray(99)
      'oSheet2.Range("CW6").Value = gstrBurninTempsArray(100)
      'oSheet2.Range("CX6").Value = gstrBurninTempsArray(101)
      'oSheet2.Range("CY6").Value = gstrBurninTempsArray(102)
      'oSheet2.Range("CZ6").Value = gstrBurninTempsArray(103)

      'oSheet2.Range("DA6").Value = gstrBurninTempsArray(104)
      'oSheet2.Range("DB6").Value = gstrBurninTempsArray(105)
      'oSheet2.Range("DC6").Value = gstrBurninTempsArray(106)
      'oSheet2.Range("DD6").Value = gstrBurninTempsArray(107)
      'oSheet2.Range("DE6").Value = gstrBurninTempsArray(108)

      oSheet2.Calculate()

      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If Directory.Exists(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay) = False Then
        Directory.CreateDirectory(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay)
      End If

      oXL.ActiveWorkbook.Save()

      gdblBurnInTempFaults = CStr(oSheet2.Range("b12").Value)

      oXL.Quit() 'Nothing

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub EvaluateBurnInTime()
    Dim lDate As Date
    Dim lstrYear As String
    Dim lstrMonth As String
    Dim lstrDay As String

    Dim oXL As New Microsoft.Office.Interop.Excel.Application
    Dim oWB As Microsoft.Office.Interop.Excel.Workbook
    Dim oSheet2 As Microsoft.Office.Interop.Excel.Worksheet

    Dim strErrorMessage As String
    Dim strCellArray() As String
    Dim intLoop As Integer
    Try

      oXL.Visible = False

      ' Get a new workbook.
      oWB = oXL.Workbooks.Open(Filename:=gstrXLFileName)

      oSheet2 = oWB.Sheets(gControlAndInterface.BurnInTimeSheet)

      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If gstrBurninTimesArray.Length = 0 Then
        strErrorMessage = "BurnIn Time Array Has No Data"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      ReDim strCellArray(gstrBurninTimesArray.Length - 1)
      Call BuildCellArray(strCellArray, 6)
      For intLoop = 0 To gstrBurninTimesArray.Length - 1
        oSheet2.Range(strCellArray(intLoop)).Value = gstrBurninTimesArray(intLoop)
      Next

      'oSheet2.Range("A6").Value = gstrBurninTimesArray(0)
      'oSheet2.Range("B6").Value = gstrBurninTimesArray(1)
      'oSheet2.Range("C6").Value = gstrBurninTimesArray(2)
      'oSheet2.Range("D6").Value = gstrBurninTimesArray(3)
      'oSheet2.Range("E6").Value = gstrBurninTimesArray(4)
      'oSheet2.Range("F6").Value = gstrBurninTimesArray(5)
      'oSheet2.Range("G6").Value = gstrBurninTimesArray(6)
      'oSheet2.Range("H6").Value = gstrBurninTimesArray(7)
      'oSheet2.Range("I6").Value = gstrBurninTimesArray(8)
      'oSheet2.Range("J6").Value = gstrBurninTimesArray(9)
      'oSheet2.Range("K6").Value = gstrBurninTimesArray(10)
      'oSheet2.Range("L6").Value = gstrBurninTimesArray(11)
      'oSheet2.Range("M6").Value = gstrBurninTimesArray(12)
      'oSheet2.Range("N6").Value = gstrBurninTimesArray(13)
      'oSheet2.Range("O6").Value = gstrBurninTimesArray(14)
      'oSheet2.Range("P6").Value = gstrBurninTimesArray(15)
      'oSheet2.Range("Q6").Value = gstrBurninTimesArray(16)
      'oSheet2.Range("R6").Value = gstrBurninTimesArray(17)
      'oSheet2.Range("S6").Value = gstrBurninTimesArray(18)
      'oSheet2.Range("T6").Value = gstrBurninTimesArray(19)
      'oSheet2.Range("U6").Value = gstrBurninTimesArray(20)
      'oSheet2.Range("V6").Value = gstrBurninTimesArray(21)
      'oSheet2.Range("W6").Value = gstrBurninTimesArray(22)
      'oSheet2.Range("X6").Value = gstrBurninTimesArray(23)
      'oSheet2.Range("Y6").Value = gstrBurninTimesArray(24)
      'oSheet2.Range("Z6").Value = gstrBurninTimesArray(25)

      'oSheet2.Range("AA6").Value = gstrBurninTimesArray(26)
      'oSheet2.Range("AB6").Value = gstrBurninTimesArray(27)
      'oSheet2.Range("AC6").Value = gstrBurninTimesArray(28)
      'oSheet2.Range("AD6").Value = gstrBurninTimesArray(29)
      'oSheet2.Range("AE6").Value = gstrBurninTimesArray(30)
      'oSheet2.Range("AF6").Value = gstrBurninTimesArray(31)
      'oSheet2.Range("AG6").Value = gstrBurninTimesArray(32)
      'oSheet2.Range("AH6").Value = gstrBurninTimesArray(33)
      'oSheet2.Range("AI6").Value = gstrBurninTimesArray(34)
      'oSheet2.Range("AJ6").Value = gstrBurninTimesArray(35)
      'oSheet2.Range("AK6").Value = gstrBurninTimesArray(36)
      'oSheet2.Range("AL6").Value = gstrBurninTimesArray(37)
      'oSheet2.Range("AM6").Value = gstrBurninTimesArray(38)
      'oSheet2.Range("AN6").Value = gstrBurninTimesArray(39)
      'oSheet2.Range("AO6").Value = gstrBurninTimesArray(40)
      'oSheet2.Range("AP6").Value = gstrBurninTimesArray(41)
      'oSheet2.Range("AQ6").Value = gstrBurninTimesArray(42)
      'oSheet2.Range("AR6").Value = gstrBurninTimesArray(43)
      'oSheet2.Range("AS6").Value = gstrBurninTimesArray(44)
      'oSheet2.Range("AT6").Value = gstrBurninTimesArray(45)
      'oSheet2.Range("AU6").Value = gstrBurninTimesArray(46)
      'oSheet2.Range("AV6").Value = gstrBurninTimesArray(47)
      'oSheet2.Range("AW6").Value = gstrBurninTimesArray(48)
      'oSheet2.Range("AX6").Value = gstrBurninTimesArray(49)
      'oSheet2.Range("AY6").Value = gstrBurninTimesArray(50)
      'oSheet2.Range("AZ6").Value = gstrBurninTimesArray(51)

      'oSheet2.Range("BA6").Value = gstrBurninTimesArray(52)
      'oSheet2.Range("BB6").Value = gstrBurninTimesArray(53)
      'oSheet2.Range("BC6").Value = gstrBurninTimesArray(54)
      'oSheet2.Range("BD6").Value = gstrBurninTimesArray(55)
      'oSheet2.Range("BE6").Value = gstrBurninTimesArray(56)
      'oSheet2.Range("BF6").Value = gstrBurninTimesArray(57)
      'oSheet2.Range("BG6").Value = gstrBurninTimesArray(58)
      'oSheet2.Range("BH6").Value = gstrBurninTimesArray(59)
      'oSheet2.Range("BI6").Value = gstrBurninTimesArray(60)
      'oSheet2.Range("BJ6").Value = gstrBurninTimesArray(61)
      'oSheet2.Range("BK6").Value = gstrBurninTimesArray(62)
      'oSheet2.Range("BL6").Value = gstrBurninTimesArray(63)
      'oSheet2.Range("BM6").Value = gstrBurninTimesArray(64)
      'oSheet2.Range("BN6").Value = gstrBurninTimesArray(65)
      'oSheet2.Range("BO6").Value = gstrBurninTimesArray(66)
      'oSheet2.Range("BP6").Value = gstrBurninTimesArray(67)
      'oSheet2.Range("BQ6").Value = gstrBurninTimesArray(68)
      'oSheet2.Range("BR6").Value = gstrBurninTimesArray(69)
      'oSheet2.Range("BS6").Value = gstrBurninTimesArray(70)
      'oSheet2.Range("BT6").Value = gstrBurninTimesArray(71)
      'oSheet2.Range("BU6").Value = gstrBurninTimesArray(72)
      'oSheet2.Range("BV6").Value = gstrBurninTimesArray(73)
      'oSheet2.Range("BW6").Value = gstrBurninTimesArray(74)
      'oSheet2.Range("BX6").Value = gstrBurninTimesArray(75)
      'oSheet2.Range("BY6").Value = gstrBurninTimesArray(76)
      'oSheet2.Range("BZ6").Value = gstrBurninTimesArray(77)

      'oSheet2.Range("CA6").Value = gstrBurninTimesArray(78)
      'oSheet2.Range("CB6").Value = gstrBurninTimesArray(79)
      'oSheet2.Range("CC6").Value = gstrBurninTimesArray(80)
      'oSheet2.Range("CD6").Value = gstrBurninTimesArray(81)
      'oSheet2.Range("CE6").Value = gstrBurninTimesArray(82)
      'oSheet2.Range("CF6").Value = gstrBurninTimesArray(83)
      'oSheet2.Range("CG6").Value = gstrBurninTimesArray(84)
      'oSheet2.Range("CH6").Value = gstrBurninTimesArray(85)
      'oSheet2.Range("CI6").Value = gstrBurninTimesArray(86)
      'oSheet2.Range("CJ6").Value = gstrBurninTimesArray(87)
      'oSheet2.Range("CK6").Value = gstrBurninTimesArray(88)
      'oSheet2.Range("CL6").Value = gstrBurninTimesArray(89)
      'oSheet2.Range("CM6").Value = gstrBurninTimesArray(90)
      'oSheet2.Range("CN6").Value = gstrBurninTimesArray(91)
      'oSheet2.Range("CO6").Value = gstrBurninTimesArray(92)
      'oSheet2.Range("CP6").Value = gstrBurninTimesArray(93)
      'oSheet2.Range("CQ6").Value = gstrBurninTimesArray(94)
      'oSheet2.Range("CR6").Value = gstrBurninTimesArray(95)
      'oSheet2.Range("CS6").Value = gstrBurninTimesArray(96)
      'oSheet2.Range("CT6").Value = gstrBurninTimesArray(97)
      'oSheet2.Range("CU6").Value = gstrBurninTimesArray(98)
      'oSheet2.Range("CV6").Value = gstrBurninTimesArray(99)
      'oSheet2.Range("CW6").Value = gstrBurninTimesArray(100)
      'oSheet2.Range("CX6").Value = gstrBurninTimesArray(101)
      'oSheet2.Range("CY6").Value = gstrBurninTimesArray(102)
      'oSheet2.Range("CZ6").Value = gstrBurninTimesArray(103)

      'oSheet2.Range("DA6").Value = gstrBurninTimesArray(104)
      'oSheet2.Range("DB6").Value = gstrBurninTimesArray(105)
      'oSheet2.Range("DC6").Value = gstrBurninTimesArray(106)
      'oSheet2.Range("DD6").Value = gstrBurninTimesArray(107)
      'oSheet2.Range("DE6").Value = gstrBurninTimesArray(108)
      'oSheet2.Range("DF6").Value = gstrBurninTimesArray(109)
      'oSheet2.Range("DG6").Value = gstrBurninTimesArray(110)

      oSheet2.Calculate()

      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If Directory.Exists(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay) = False Then
        Directory.CreateDirectory(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay)
      End If

      oXL.ActiveWorkbook.Save()

      gdblBurnInElapsedTimeFaults = CStr(oSheet2.Range("b9").Value)

      oXL.Quit() 'Nothing

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetPostBurnInData(ByRef strCartID As String, ByRef strCartNumber As String, ByRef strStall As String, ByRef strBom As String)
    Dim lstrFilePath As String
    Dim fs As FileStream
    Dim br As StreamReader
    Dim strTemp As String
    Dim strTemp1 As String = ""
    Dim intloop As Integer

    Try
      'lstrFilePath = "c:\ee's\PostBurnInData.dat"
      lstrFilePath = gstrPostBurninDataFile

      fs = New FileStream(lstrFilePath, FileMode.Open, FileAccess.Read, FileShare.None)
      br = New StreamReader(fs)

      strTemp = br.ReadLine()

      For intloop = 1 To strTemp.Length
        If Mid(strTemp, intloop, 1) = "," Then
          strTemp1 = Mid(strTemp, intloop + 1, strTemp.Length)
          Exit For
        End If
      Next
      strCartID = strTemp1

      strTemp = br.ReadLine()
      For intloop = 1 To strTemp.Length
        If Mid(strTemp, intloop, 1) = "," Then
          strTemp1 = Mid(strTemp, intloop + 1, strTemp.Length)
          Exit For
        End If
      Next
      strCartNumber = strTemp1

      strTemp = br.ReadLine()
      For intloop = 1 To strTemp.Length
        If Mid(strTemp, intloop, 1) = "," Then
          strTemp1 = Mid(strTemp, intloop + 1, strTemp.Length)
          Exit For
        End If
      Next
      strBom = strTemp1

      strTemp = br.ReadLine()
      For intloop = 1 To strTemp.Length
        If Mid(strTemp, intloop, 1) = "," Then
          strTemp1 = Mid(strTemp, intloop + 1, strTemp.Length)
          Exit For
        End If
      Next
      strStall = strTemp1

      br.Close()
      fs.Close()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ReadAppSoftwareFile()
    Dim strFilename As String = ""
    Dim dataRecord As IntelDataRecord
    Dim strErrorMessage As String
    Dim strActuatorVariant As String
    Dim strEmbeddedVoltage As String
    Try
      strFilename = gSoftwareFiles.LocalVault & gProductProgramming.AppSoftwareFilename

      gCanActuator.IntelHexFileDownloadTarget = clsCANActuator.TargetMemoryAreaEnum.ProgramMemory

      ReadHexFile(strFilename)

      'Get Record for Software Version
      dataRecord = gCanActuator.IntelHexFileDownload(8)

      'Verify Actuator Variant in Download File
      strActuatorVariant = Mid(gProductProgramming.AppSoftwareVersion, 1, 1)
      'strActuatorVariant = Mid("L2D204", 1, 1)
      If strActuatorVariant <> ChrW(dataRecord.Data(6)) Then
        strErrorMessage = "Actuator Variant in Download File is Incorrect"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Verify Embedded Voltage in Download File
      strEmbeddedVoltage = Mid(gProductProgramming.AppSoftwareVersion, 3, 1)
      'strEmbeddedVoltage = Mid("L2D204", 3, 1)
      If strEmbeddedVoltage <> ChrW(dataRecord.Data(8)) Then
        strErrorMessage = "Actuator Embedded Voltage in Download File is Incorrect"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ReadEEPromFile()
    Dim strFilename As String = ""

    strFilename = gSoftwareFiles.LocalVault & gProductProgramming.EEPromHexFileName

    gCanActuator.IntelHexFileDownloadTarget = clsCANActuator.TargetMemoryAreaEnum.EEProm

    ReadHexFile(strFilename)
  End Sub

  Private Sub ReadHexFile(strFilename As String)
    Dim dataRecord As IntelDataRecord
    Dim strRawDataRecord As String
    Dim strDataRecordArray() As String
    Dim intLineChecksum As UInt32 = 0
    Dim intEmbeddedChecksum As UInt32 = 0
    Dim intDataRecordType As Integer = 0
    Dim intDataRecordStartIndex As Integer = 0
    Dim intIndex As Integer = 0
    Dim blnReturnValue As Boolean = False
    Dim intChecksumAddress As UShort = 0
    Dim strHexFile() As String

    'Dim intelHexFileAppSoftware As New List(Of IntelDataRecord)
    Dim strErrorMessage As String
    'Dim strActuatorVariant As String
    'Dim strEmbeddedVoltage As String

    Try

      gFileChecksum = 0

      If (My.Computer.FileSystem.GetFileInfo(strFilename).Length <> 0) Then
        strHexFile = File.ReadAllLines(strFilename)
        gCanActuator.IntelHexFileDownload.Clear()
        ' First two lines in array contain the version string and checksum respectively.
        For recordIdx As UShort = 0 To strHexFile.Length - 1
          Application.DoEvents()
          strRawDataRecord = strHexFile(recordIdx)
          ' Initialize Variables
          dataRecord = New IntelDataRecord
          intLineChecksum = 0
          intDataRecordType = 0
          'remove whitespace
          Trim(strRawDataRecord)
          ReDim strDataRecordArray((strRawDataRecord.Length / 2) - 1)
          ' Split raw data record string into bytes
          intDataRecordStartIndex = 2
          For intIndex = 0 To strDataRecordArray.Length - 1
            strDataRecordArray(intIndex) = Mid(strRawDataRecord, intDataRecordStartIndex, 2)
            intDataRecordStartIndex += 2
            If intIndex < (strDataRecordArray.Length - 1) Then
              intLineChecksum = intLineChecksum + CUInt("&H" & strDataRecordArray(intIndex))
            End If
          Next
          ' Save data record length
          dataRecord.DataLength = CType("&H" & strDataRecordArray(0), UInt16)
          ' Save data record starting address
          dataRecord.StartAddress = CType("&H" & strDataRecordArray(1) & strDataRecordArray(2), UInt16)
          ' Save data record type
          intDataRecordType = CType("&H" & strDataRecordArray(3), UInt16)
          Select Case intDataRecordType
            Case IntelDataRecord.HEXRecordType.DataRecord
              If (recordIdx <> (strHexFile.Length - 1)) Then
                dataRecord.RecordType = IntelDataRecord.HEXRecordType.DataRecord
              Else
                'finalRslt = FinalResult.NoEofRecordFoundInFile
                'Exit Try
                strErrorMessage = "No EOF record found in file."
                gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
                Throw New TsopAnomalyException
              End If
            Case IntelDataRecord.HEXRecordType.EOFRecord
              ' End of file found
              Exit For
            Case Else
              ' Run-time error
              ' "Invalid record type found in file."
              'finalRslt = FinalResult.InvalidRecordType
              'Exit Try
              strErrorMessage = "Invalid record type found in file."
              gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
              Throw New TsopAnomalyException
          End Select
          'Data
          ReDim dataRecord.Data(dataRecord.DataLength - 1)
          For intIndex = 0 To dataRecord.DataLength - 1
            ' Add byte values for line checksum calculation.
            dataRecord.Data(intIndex) = CByte("&H" & strDataRecordArray(intIndex + 4))
          Next
          dataRecord.CheckSum = CType("&H" & strDataRecordArray(intIndex + 4), UInt16)
          ' Calculate line end address.
          dataRecord.EndAddress = dataRecord.StartAddress + (dataRecord.DataLength - 1)
          ' Calculate the line checksum.
          intLineChecksum = intLineChecksum And &HFF
          If intLineChecksum <> 0 Then
            intLineChecksum = &H100 - intLineChecksum
          End If
          ' Verify the line checksum
          If intLineChecksum <> dataRecord.CheckSum Then
            '"Calculated line checksum doesn't match embedded line checksum.
            'finalRslt = FinalResult.LineChksumMismatch
            'Exit Try
            strErrorMessage = "Calculated line checksum doesn't match embedded line checksum."
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            Throw New TsopAnomalyException
          End If
          ' Add latest data record to list.
          gCanActuator.IntelHexFileDownload.Add(dataRecord)
          '_fileSize += dataRecord.DataLength
        Next
        If gCanActuator.IntelHexFileDownloadTarget = clsCANActuator.TargetMemoryAreaEnum.ProgramMemory Then
          ' Generate checksum for entire file.
          dataRecord = gCanActuator.IntelHexFileDownload(8)
          If (dataRecord.StartAddress = &H80) Then
            intChecksumAddress = dataRecord.Data(4)
            intChecksumAddress += CUShort(dataRecord.Data(5)) << 8
            If (CRC_32(gCanActuator.IntelHexFileDownload, intChecksumAddress - 1, gFileChecksum) = True) Then
              For intIndex = 0 To gCanActuator.IntelHexFileDownload.Count - 1
                Application.DoEvents()
                dataRecord = gCanActuator.IntelHexFileDownload.Item(intIndex)
                If ((dataRecord.StartAddress <= intChecksumAddress) AndAlso
                  (dataRecord.EndAddress >= intChecksumAddress)) Then

                  ' Extract checksum from hex file records.
                  intEmbeddedChecksum = CUInt(dataRecord.Data(intChecksumAddress - dataRecord.StartAddress))
                  intEmbeddedChecksum += CUInt(dataRecord.Data((intChecksumAddress - dataRecord.StartAddress) + 1)) << 8
                  intEmbeddedChecksum += CUInt(dataRecord.Data((intChecksumAddress - dataRecord.StartAddress) + 2)) << 16
                  intEmbeddedChecksum += CUInt(dataRecord.Data((intChecksumAddress - dataRecord.StartAddress) + 3)) << 24
                  Exit For
                End If
              Next
              If (gFileChecksum = intEmbeddedChecksum) Then
                ' Read of Intel hex file successful
                blnReturnValue = True
              Else
                ' Hex file contains bad checksum.
                'finalRslt = FinalResult.FileChksumMismatch
                strErrorMessage = "Hex file contains bad checksum."
                gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
                Throw New TsopAnomalyException
              End If
            Else
              ' Run-time error - unable to generate hex file checksum.
              'finalRslt = FinalResult.ChksumCalcFailed
              strErrorMessage = "Unable to generate hex file checksum."
              gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
              Throw New TsopAnomalyException
            End If
          Else
            ' The specified file does not appear to be an application hex file.
            'finalRslt = FinalResult.ApplicationVectorNotFound
            strErrorMessage = "The specified file does not appear to be an application hex file."
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            Throw New TsopAnomalyException
          End If

        Else
          blnReturnValue = True
        End If
      Else
        ' Hex file is empty
        'finalRslt = FinalResult.HexFileEmpty
        strErrorMessage = "Hex file is empty"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  'Public Sub gDutPower.SetSupplyVoltage(ByVal Voltage As Double)
  '  Dim lstrVout As String = ""
  '  Dim lStopwatch As New clsStopWatch

  '  Try

  '    'frmTsopMain.comPowerSupply.DiscardInBuffer()
  '    gDUTPowerSupplySerialPort.DiscardInBuffer()
  '    frmTsopMain.lblVoltageValue.Text = Voltage & "V"

  '    If gdblDUTPowerSupplyUsed = "BK" Then
  '      If Voltage = 0 Then
  '        gDUTPowerSupplySerialPort.Write("OUTP OFF" & vbCrLf)
  '      Else
  '        gDUTPowerSupplySerialPort.Write("SOUR:CURR " & gstrDUTPowerSupplyMaxCurrent & vbCrLf) 'Max current of supply 
  '        gDUTPowerSupplySerialPort.Write("SOUR:VOLT " & Voltage & vbCrLf)
  '        gDUTPowerSupplySerialPort.Write("OUTP ON " & vbCrLf)
  '      End If

  '    Else
  '      If Voltage = 0 Then
  '        gDUTPowerSupplySerialPort.Write("OUT OFF" & vbCr)

  '      Else
  '        Voltage = Voltage '+ 0.1
  '        gDUTPowerSupplySerialPort.Write("ISET " & 10 & vbCr) 'Max current of supply 
  '        gDUTPowerSupplySerialPort.Write("VSET " & Voltage & vbCr)
  '        gDUTPowerSupplySerialPort.Write("OUT ON" & vbCr)
  '      End If

  '      'check that supply was set properly
  '      lStopwatch.DelayTime(100)
  '      gDUTPowerSupplySerialPort.DiscardInBuffer()

  '      Do
  '        gDUTPowerSupplySerialPort.Write("VOUT?" & vbCr)
  '        lstrVout = gDUTPowerSupplySerialPort.ReadTo(vbCr)
  '        lstrVout = Mid(lstrVout, 6, lstrVout.Length)
  '        lstrVout = Math.Round(CSng(lstrVout), 1)
  '      Loop Until lstrVout = Voltage Or Voltage < 0.5

  '    End If

  '    'frmTsopMain.comPowerSupply.Close()

  '    lStopwatch.DelayTime(250)

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message)
  '  End Try
  'End Sub
  'Public Sub gDutPower.SetSupplyVoltage(ByVal Voltage As Double)
  '  Dim lstrVout As String
  '  Dim lStopwatch As New clsStopWatch
  '  Try

  '    'If Voltage <> 0 Then
  '    '  If blnTestVoltagesOverridden And Not blnFindStopsRoutine Then
  '    '    Voltage = dblOverriddenTestVoltage
  '    '  End If
  '    'End If

  '    If Not (gDUTPowerSupplySerialPort.IsOpen) Then

  '      gDUTPowerSupplySerialPort.PortName = gstrDUTPowerSupplyComPortName  '"COM5"
  '      gDUTPowerSupplySerialPort.BaudRate = gstrDUTPowerSupplyBaudRate '9600
  '      gDUTPowerSupplySerialPort.Parity = IO.Ports.Parity.None
  '      gDUTPowerSupplySerialPort.StopBits = IO.Ports.StopBits.One
  '      gDUTPowerSupplySerialPort.Open()
  '    End If

  '    gDUTPowerSupplySerialPort.DiscardInBuffer()
  '    frmTsopMain.lblVoltageValue.Text = Voltage & "V"


  '    Select Case gdblDUTPowerSupplyUsed
  '      Case "Sorenson"
  '        Voltage = Voltage + 0.1
  '        ' frmTsopMain.comPowerSupply.Write("*CLS" & vbCr)
  '        ' frmTsopMain.comPowerSupply.Write("*RST" & vbCr)
  '        gDUTPowerSupplySerialPort.Write("SOUR:CURR " & gstrDUTPowerSupplyMaxCurrent & vbCr) 'Max current of supply '1.1.0.2 DLG
  '        gDUTPowerSupplySerialPort.Write("SOUR:VOLT " & Voltage & vbCr)

  '      Case "BK"
  '        If Voltage = 0 Then
  '          gDUTPowerSupplySerialPort.Write("OUTP OFF" & vbCrLf)
  '        Else
  '          gDUTPowerSupplySerialPort.Write("SOUR:CURR " & gstrDUTPowerSupplyMaxCurrent & vbCrLf) 'Max current of supply 
  '          gDUTPowerSupplySerialPort.Write("SOUR:VOLT " & Voltage & vbCrLf)
  '          gDUTPowerSupplySerialPort.Write("OUTP ON " & vbCrLf)
  '        End If

  '      Case "Xantrex"
  '        If Voltage = 0 Then
  '          gDUTPowerSupplySerialPort.Write("OUT OFF" & vbCr)

  '        Else Voltage = Voltage + 0.1  '1.1.0.2 DLG
  '          gDUTPowerSupplySerialPort.Write("ISET " & gstrDUTPowerSupplyMaxCurrent & vbCrLf) 'Max current of supply '1.1.0.2 DLG
  '          gDUTPowerSupplySerialPort.Write("VSET " & Voltage & vbCrLf)
  '          gDUTPowerSupplySerialPort.Write("OUT ON" & vbCr)
  '        End If

  '        'check that supply was set properly
  '        lStopwatch.DelayTime(100)
  '        gDUTPowerSupplySerialPort.DiscardInBuffer()

  '        Do
  '          gDUTPowerSupplySerialPort.Write("VOUT?" & vbCr)
  '          lstrVout = gDUTPowerSupplySerialPort.ReadTo(vbCr)
  '          lstrVout = Mid(lstrVout, 6, lstrVout.Length)
  '          lstrVout = Math.Round(CSng(lstrVout), 1)
  '        Loop Until (lstrVout >= (Voltage - 0.5)) And (lstrVout <= (Voltage + 0.5))

  '    End Select


  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message)
  '  End Try


  'End Sub
  Public Sub OperatorTouchStartTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim intResult As Integer = 0
    Dim portData As Byte = 0
    Dim lStopwatch As New clsStopWatch
    Try

      If gControlFlags.UsesExternalDevices Then

        gtmrOperatorTouchStartTimer.Enabled = False

        intResult = gDigitalInput.Read(0, portData)

        lStopwatch.DelayTime(25)
        intResult = 0

        intResult = gDigitalInput.Read(0, portData)

        If portData = 0 Then
          Call ResetLight()
        End If

        Console.WriteLine(portData)

        If portData = gDIOStructure.StartButton Then
          'button has been pressed
          RestartCANActuator()

          gControlFlags.StartScan = True
          If gControlFlags.UsesExternalDevices Then
            intResult = gDigitalOutput.Write(0, gDIOStructure.YellowLight)
          End If

          Call ExecuteTaskList(TaskListFilterEnum.Test)

          If glstAnomaly.Count > 0 Or gControlFlags.AbortTest Then
            If gControlAndInterface.UsesProgramming And (gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.Programming.ToString)) Then
              gControlFlags.ProgError = True
              Call UpdateProgLotCounts()
            Else
              gControlFlags.TestError = True
              Call UpdateTestLotCounts()
              Call UpdateLotDataDisplay()
            End If

            Call UpdatePassFailStatus()
            Call PrintRejectLabel()
            Call LogAnomaly()
          End If

          ResetPowerSupply()
          gControlFlags.AbortTest = False
          gControlFlags.TestError = False
          gControlFlags.TestFailure = False
          gControlFlags.StartScan = False
          gControlFlags.TestInProgress = False
          glstAnomaly.Clear()
          gtmrOperatorTouchStartTimer.Enabled = True

        Else
          If Not gControlFlags.TestInProgress Then
            gtmrOperatorTouchStartTimer.Enabled = True
          End If
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Public Function WaitForApplicationChecksumCalculation() As Boolean
    Dim lStopwatch As New clsStopWatch
    Dim blnResult As Boolean
    Dim strResult As String = ""
    Dim strErrorMessage As String = ""
    Dim intElapsedTime As Integer
    Dim intTimeout As Integer
    Dim uintChecksum As UInteger = &H7FFFFFFF
    Dim blnReturnValue As Boolean = False
    Try

      intTimeout = 5000

      lStopwatch.Reset()
      lStopwatch.Start()
      Do
        Application.DoEvents()
        blnResult = gCanActuator.ReadProgramChecksumInteger(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gCanActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        intElapsedTime = lStopwatch.ElapsedMilliseconds

        If strResult <> "" Then
          uintChecksum = CUInt(strResult)
        End If

        If (uintChecksum <> 0) Then
          If (uintChecksum = gFileChecksum) Then
            ' The actuator update has completed successfully.
            blnReturnValue = True
            Exit Do
          Else
            ' Actuator calculated application checksum doesn't match
            ' the checksum calculated on the application hex file.
            Exit Do
          End If
        End If
      Loop Until intElapsedTime >= intTimeout
      If (uintChecksum = &H7FFFFFFF) Then
        ' The actuator failed to respond to a request for calculated application checksum.
        strErrorMessage = "Actuator Failed to Respond to Request for Calculated Application Checksum"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      Return blnReturnValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return False
    End Try
  End Function

  Public Sub WaitForApplicationDownloadComplete()
    Dim intPercentMarks() As Integer
    Dim intNextMark As Integer
    Dim strStatusMessage As String
    Try

      intPercentMarks = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110}
      intNextMark = 0
      strStatusMessage = frmTsopMain.StatusLabel2.Text
      Do
        'Application.DoEvents()
        Thread.Sleep(0)
        If gCanActuator.ActuatorDownloadPercentComplete >= intPercentMarks(intNextMark) Then
          'Console.WriteLine("Actuator Download " & gCanActuator.ActuatorDownloadPercentComplete & " Percent Complete")
          frmTsopMain.StatusLabel2.Text = strStatusMessage & " " & gCanActuator.ActuatorDownloadPercentComplete & "%"
          Call LogActivity(frmTsopMain.StatusLabel2.Text)
          If intNextMark < intPercentMarks.Length - 1 Then
            intNextMark += 1
          End If
        End If
      Loop Until gCanActuator.ActuatorDownloadComplete = True

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub WaitForResetPowerCycles()
    Dim lStopwatch As New clsStopWatch
    Dim blnResult As Boolean
    Dim strResult As String = ""
    Dim strErrorMessage As String = ""
    Dim intElapsedTime As Integer
    Dim intTimeout As Integer
    Dim blnWriteComplete As Boolean = False
    Try

      intTimeout = 1500

      lStopwatch.Reset()
      lStopwatch.Start()
      Do
        Application.DoEvents()
        blnResult = gCanActuator.ReadPowerCycleCounts(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gCanActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        intElapsedTime = lStopwatch.ElapsedMilliseconds

        If CInt(strResult) = 0 Then
          blnWriteComplete = True
        End If

      Loop Until blnWriteComplete Or intElapsedTime >= intTimeout

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

End Module
