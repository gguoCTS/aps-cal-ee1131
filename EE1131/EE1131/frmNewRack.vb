﻿Public Class frmNewRack
  Dim gblnOverride As Boolean


  Private Sub frmNewRack_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    rtfInstructions.Text = gstrNewRackInstructions
    rtfResults.Text = ""
    txtRackScanned.Text = ""
    txtRackScanned.Focus()
  End Sub

  'Dim cLocalOPC As frmTsopMain.clsOPC
  Private Sub frmNewRack_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    btnAcceptRack.Enabled = False
    gstrCurrentCartID = 0

    'gstrCartNumber = ""
    'gintCartNumber = 0
    gintCartInProcessID = -1
    gblnOverride = False
  End Sub

  Private Sub tmrLoad_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrLoad.Tick
    tmrLoad.Enabled = False
    txtRackScanned.Focus()
  End Sub

  Private Sub txtRackScanned_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRackScanned.KeyDown

    Dim intCartBurnInStatus As Integer
    Dim strStatus As String = ""
    Dim blnValidCartNumber As Boolean
    Dim BurnInDB As New BurnInDatabase

    If e.KeyValue = Keys.Enter Then
      'Validate Cart Number
      If Not IsNumeric(txtRackScanned.Text) Then
        MsgBox("Invalid Cart Scan, Must Be " & gintNumberOfFirstBurnInRack & " - " & gintNumberOfBurnInRacks + gintNumberOfFirstBurnInRack - 1 & ", Please Try Again")

        txtRackScanned.Text = ""
        txtRackScanned.Focus()
        Exit Sub
      Else
        gintCartNumber = txtRackScanned.Text
        gintReportCartNumber = gintCartNumber

        blnValidCartNumber = CheckIfValidCartNumber()

        If Not blnValidCartNumber Then
          MsgBox("Invalid Cart Scan, Must Be " & gintNumberOfFirstBurnInRack & " - " & gintNumberOfBurnInRacks + gintNumberOfFirstBurnInRack - 1 & ", Please Try Again")
          txtRackScanned.Text = ""
          txtRackScanned.Focus()
          Exit Sub

        End If
      End If

      ' need cart status from burn in database
      intCartBurnInStatus = BurnInDB.GetCartStatus(gintCartNumber)
      If BurnInDB.IsCartInProcess(gintCartNumber) Then


        If intCartBurnInStatus = BurnInDatabase.Status.InAPSCal Or intCartBurnInStatus = BurnInDatabase.Status.ToAPSCal Then

          Dim CurrentCartInfo As Tuple(Of BurnInDatabase.Status, String, Integer) =
      New Tuple(Of BurnInDatabase.Status, String, Integer)(BurnInDatabase.Status.Available, "00000", 0)

          gstrStall = BurnInDB.GetBurnInStall(gintCartNumber)
          CurrentCartInfo = BurnInDB.GetCartStatus2(gintCartNumber)
          gstrCartBom = CurrentCartInfo.Item2
          gstrCartID = BurnInDB.GetCartUniqueID(gintCartNumber)

          rtfResults.Text = "Please Ensure the Cart contains parts"
          rtfResults.ForeColor = Color.Green

          btnAcceptRack.Enabled = True
          System.Windows.Forms.Application.DoEvents()

        Else

          If intCartBurnInStatus = BurnInDatabase.Status.Available Then strStatus = "Cart " & txtRackScanned.Text & " Available for Leak and should be empty"
          If intCartBurnInStatus = BurnInDatabase.Status.InLeak Then strStatus = "Cart " & txtRackScanned.Text & " In Leak, Not ended properly"
          If intCartBurnInStatus = BurnInDatabase.Status.ToBurnIn Then strStatus = "Cart " & txtRackScanned.Text & " Needs to Go to Burn In"
          If intCartBurnInStatus = BurnInDatabase.Status.InBurnIn Then strStatus = "Cart " & txtRackScanned.Text & " Didn't Finish Burn In"
          'BurnInDatabase.Status.ToAPSCal acceptable
          'BurnInDatabase.Status.InAPSCal acceptable
          If intCartBurnInStatus = BurnInDatabase.Status.Finished Then strStatus = "Cart " & txtRackScanned.Text & " is complete and should be empty"

          rtfResults.Text = strStatus & vbCrLf & " . Please Process Cart Accordingly."
          rtfResults.ForeColor = Color.Red

          'gintCartNumber = 0        commented out to allow override without reentering the number
          'txtRackScanned.Text = ""       commented out to allow override without reentering the number
          System.Windows.Forms.Application.DoEvents()
          txtRackScanned.Focus()
          btnAcceptRack.Enabled = False

        End If
      Else
        strStatus = "Cart " & txtRackScanned.Text & " complete in database, Select another cart."
        rtfResults.Text = strStatus & vbCrLf
        btnAcceptRack.Enabled = False
        rtfResults.ForeColor = Color.Red
        gintCartNumber = 0
        txtRackScanned.Text = ""
        System.Windows.Forms.Application.DoEvents()
        txtRackScanned.Focus()
      End If
    End If

  End Sub

  'Private Function CheckBurnInPLCRackStatus() As String

  '  'Dim strResult As String
  '  Dim strStatus As String = ""
  '  Dim strBOM As String = ""
  '  Dim strQty As String = ""
  '  Dim strID As String = ""
  '  'Dim blnResult As Boolean

  '  'Update Cart Number Write

  '  'blnResult = lclsOPC.SetOutput(lclsOPC.PLCDataItemPosition.ReadCartNumberStatus, gstrCartNumber, "")

  '  ''Find ReadReqStatus
  '  'blnResult = lclsOPC.SetOutput(lclsOPC.PLCDataItemPosition.ReadReqStatus, 1, "")

  '  'strResult = ""
  '  ''Do
  '  'strResult = lclsopc.GetInput(lclsopc.PLCDataItemPosition.ReadReqStatus)
  '  ''Loop Until strResult = "2"

  '  ''Get Status, BOM, QTY, ID
  '  'strStatus = lclsopc.GetInput(lclsopc.PLCDataItemPosition.ReadStatusOnly)
  '  'strBOM = lclsopc.GetInput(lclsopc.PLCDataItemPosition.LeakPcReadBom)
  '  'strQty = lclsopc.GetInput(lclsopc.PLCDataItemPosition.LeakPcReadQty)
  '  'strID = lclsopc.GetInput(lclsopc.PLCDataItemPosition.LeakPcReadId)

  '  ''Send Clear Info
  '  'blnResult = lclsOPC.SetOutput(lclsOPC.PLCDataItemPosition.ReadReqStatus, 1, "")

  '  Return strStatus


  'End Function

  Private Sub btnAcceptRack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAcceptRack.Click
    Dim blnValidCartNumber As Boolean
    Dim BurnInDB As New BurnInDatabase

    'gstrCurrentCartID = GetNewRackID()
    'If Val(gstrCurrentCartID) = 0 Then
    '  'MsgBox("Invalid Rack ID, Must be 1-14, Please Rescan")
    '  MsgBox("Invalid Cart Scan, Must Be " & gintNumberOfFirstBurnInRack & " - " & gintNumberOfBurnInRacks + gintNumberOfFirstBurnInRack - 1 & ", Please Rescan")
    '  txtRackScanned.Text = ""
    '  txtRackScanned.Focus()
    '  Exit Sub
    'End If
    'gintCartNumber = txtRackScanned.Text
    'Validate Cart Number
    blnValidCartNumber = CheckIfValidCartNumber() Or gblnOverride

    If blnValidCartNumber Then
      'gstrCurrentCartID = BuildCartUniqueID(gintCartNumber)
      'Insert New Cart here
      'ANM fix 
      '######
      'gintCartInProcessID = gDatabase.InsertNewCartInProcess(gstrCurrentCartID, gintCartNumber, gDeviceInProcess.BOM)
      'If gDatabase.Anomaly IsNot Nothing Then
      '  gAnomaly = gDatabase.Anomaly
      '  gDatabase.Anomaly = Nothing
      '  Throw New TsopAnomalyException
      'End If

      'If BurnInDB.IsCartInProcess(gintCartNumber) Then
      '  BurnInDB.SetCartComplete(gintCartNumber)
      '  BurnInDB.SetCartStatus(CShort(gintCartNumber), BurnInDatabase.Status.Available)
      'End If
      'BurnInDB.AddNewCart(gintCartNumber, gDeviceInProcess.BOM, gstrCurrentCartID)

      'frmBurninCartTest.txtCartNumber.Text = txtRackScanned.Text
      'frmTsopMain.txtCartNumber.Text = gintCartNumber
      'gstrCartNumber = txtRackScanned.Text
      'gintCartNumber = txtRackScanned.Text
      'txtRackScanned.Text = ""

      Me.Close()
    Else
      MsgBox("Invalid Cart Scan, Must Be " & gintNumberOfFirstBurnInRack & " - " & gintNumberOfBurnInRacks + gintNumberOfFirstBurnInRack - 1 & ", Please Try Again")
      'MsgBox("Invalid Cart Scan, Must Be 1-" & gintNumberOfBurnInRacks & ", Please Try Again")
      txtRackScanned.Text = ""
      txtRackScanned.Focus()
    End If
  End Sub

  Private Function CheckIfValidCartNumber() As Boolean
    Dim blnValidCartNumber As Boolean

    If Not IsNumeric(txtRackScanned.Text) Then
      blnValidCartNumber = False
      '    ElseIf CInt(txtRackScanned.Text) < 1 Or CInt(txtRackScanned.Text) > gintNumberOfBurnInRacks Then
    ElseIf CInt(txtRackScanned.Text < gintNumberOfFirstBurnInRack Or CInt(txtRackScanned.Text) > gintNumberOfBurnInRacks + gintNumberOfFirstBurnInRack - 1) Then

      blnValidCartNumber = False
    Else
      blnValidCartNumber = True
    End If

    Return blnValidCartNumber

  End Function

  Public Function BuildCartUniqueID(intCartNumber As Integer) As String
    Try
      Dim strMinuteOfDay As String
      Dim strDayOfYear As String
      Dim strYear As String
      Dim strCart As String
      Dim DateMidnight As Date
      Dim DateNow As DateTime
      Dim strCartUniqueID As String

      'New Format -(4) Minute of the day (3) day of year (4) year (2) cart #

      DateNow = Now

      strDayOfYear = DateNow.DayOfYear
      strYear = Format(DateNow, "yyyy")

      DateMidnight = Today
      strMinuteOfDay = Format(DateDiff(DateInterval.Second, DateMidnight, DateNow), "0000#")
      strCart = Format(intCartNumber, "0#")
      strCartUniqueID = strMinuteOfDay & strDayOfYear & strYear & strCart

      Return strCartUniqueID

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try

  End Function

  'Public Function GetNewRackID() As String
  '  Dim strResult As String = ""
  '  Dim strMinuteOfDay As String
  '  Dim strDayOfYear As String
  '  Dim strYear As String
  '  Dim strCart As String
  '  Dim DateMidnight As Date
  '  Dim strTemp As String


  '  If Val(txtRackScanned.Text) > 0 And Val(txtRackScanned.Text) <= gintNumberOfBurnInRacks Then
  '    strTemp = gConverter.Date2Julian(Now)

  '    New Format -(4) Minute of the day (3) day of year (1) year (2) cart #

  '    DateMidnight = Today
  '    strMinuteOfDay = Format(DateDiff(DateInterval.Minute, DateMidnight, Now), "000#")
  '    strDayOfYear = Mid(strTemp, 3, 3)
  '    strYear = Mid(strTemp, 2, 1)
  '    strCart = Format(Val(txtRackScanned.Text), "0#")
  '    strResult = strMinuteOfDay & strDayOfYear & strYear & strCart
  '    Call LogActivity("Rack= " & strCart)
  '    Call LogActivity("New Rack ID= " & strResult)
  '  End If



  '  Return strResult


  'End Function

  'Public Function GetNewRackID_TERNew() As String
  Public Function GetNewRackID() As String
    Dim strResult As String = ""
    Dim strMinuteOfDay As String
    Dim strDayOfYear As String
    Dim strYear As String
    Dim strCart As String
    Dim DateMidnight As Date
    Dim strTemp As String

    Dim DateNow As DateTime

    If CheckIfValidCartNumber() Or gblnOverride Then


      strTemp = gConverter.Date2Julian(Now)

      'New Format (4)Minute of the day (3)day of year (4)year (2)cart #

      DateNow = Now
      strDayOfYear = DateNow.DayOfYear
      strYear = Format(DateNow, "yyyy")
      strCart = Format(Val(txtRackScanned.Text), "0#")
      DateMidnight = Today
      strMinuteOfDay = Format(DateDiff(DateInterval.Minute, DateMidnight, DateNow), "000#")

      strResult = strMinuteOfDay & strDayOfYear & strYear & strCart

      Call LogActivity("Rack= " & strCart)
      Call LogActivity("New Rack ID= " & strResult)
    End If

    Return strResult

  End Function

  Private Sub frmNewRack_VisibleChanged(sender As Object, e As EventArgs) Handles Me.VisibleChanged
    txtRackScanned.Focus()

  End Sub

  Private Sub btnPickNewRack_Click(sender As Object, e As EventArgs) Handles btnPickNewRack.Click
    txtRackScanned.Text = ""
    txtRackScanned.Focus()
  End Sub

  Private Sub btnNewBOM_Click(sender As Object, e As EventArgs) Handles btnNewBOM.Click
    txtRackScanned.Text = ""
    frmTsopMain.cboParameterSets.Enabled = True
    'gControlFlags.StartScanEnabled = False
    gControlFlags.AbortTaskList = True

    gintCartNumber = 0 ' force a new cart selection


    Me.Hide()
    'frmTsopMain.cboParameterSets.Focus()
    'gControlFlags.AbortTaskList = True
  End Sub

  Private Sub btnOverrideRack_Click(sender As Object, e As EventArgs) Handles btnOverrideRack.Click
    Dim intresult As Integer
    Dim result As DialogResult
    Dim BurnInDB As New BurnInDatabase

    txtRackScanned.Text = "None"
    gintCartNumber = gintNumberOfBurnInRacks + gintNumberOfFirstBurnInRack 'one above valid rack numbers
    gintReportCartNumber = 9999 ' very invalid cart number for reject label and email report

    If BurnInDB.GetCartStatus(gintCartNumber) <> BurnInDatabase.Status.Available Then
      btnAcceptRack.Enabled = True
      gblnOverride = True
      rtfResults.Text = ""
      System.Windows.Forms.Application.DoEvents()
      Exit Sub
    End If

    intresult = MsgBox("Ensure these parts are not from a Burn-In cart. This will generate a virtual Cart with 5 parts. Is this OK?", vbYesNo)

    If intresult <> 6 Then
      Exit Sub
    End If



    Call LogActivity("Rack Override!! Virtual Rack # " & gintCartNumber)

    result = frmPasswordDialog.ShowDialog()
    If result <> Windows.Forms.DialogResult.OK Then
      MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
      ' ActuatorMenuToolIgnoreTestBitsClick.checked = gControlFlags.IgnoreTestBits
      ' miMenuItem.Checked = gControlFlags.IgnoreTestBits
      Exit Sub
    Else
      If BurnInDB.GetCartStatus(gintCartNumber) = BurnInDatabase.Status.Available Then ' check again
        BurnInDB.AddNewCart(gintCartNumber, gDeviceInProcess.BOM, BuildCartUniqueID(gintCartNumber))
        ' override so set status for InAPSCal
        ' need to update PartQuantity (and APSCalPartQuantity), set it to 40
        BurnInDB.SetCartStatus(gintCartNumber, BurnInDatabase.Status.InAPSCal, 5)

        rtfResults.Text = "Virtual cart created with 5 parts, repeat the No Rack operation for more parts not on a Burn-In cart"
        rtfResults.ForeColor = Color.Green
      Else
        rtfResults.Text = ""
      End If
      System.Windows.Forms.Application.DoEvents()
      btnAcceptRack.Enabled = True
      gblnOverride = True
    End If
  End Sub
End Class