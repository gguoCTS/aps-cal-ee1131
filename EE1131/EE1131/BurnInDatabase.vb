﻿Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks


Public Class BurnInDatabase
  Public Enum Status
    Available = -1
    InLeak = 0
    ToBurnIn = 1
    InBurnIn = 2
    ToAPSCal = 3
    InAPSCal = 4
    Finished = 5
  End Enum
#Region "Members"
  Private daDontCare As SqlDataAdapter 'for when you need to grab a table but dont need to update it
#End Region
#Region "Constructors"

  Public Sub New()
    MyBase.New
    'load in appsettings
    Dim appSettings = ConfigurationManager.AppSettings
    ConnectionString = appSettings("DatabaseConnectionString")
  End Sub
#End Region
#Region "Properties"

  Public Property ConnectionString As String

  ''' <summary>
  ''' Adds a new entry to the database. This is used before filling up the cart at leak.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <param name="BOM">The BOM.</param>
  ''' <param name="uniqueID">The unique identifier assigned at leak.</param>
  Public Sub AddNewCart(cartNum As Short, BOM As String, uniqueID As String)
    Dim command As SqlCommand = New SqlCommand("pspAddNewCart")
    command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
    command.Parameters.Add(New SqlParameter("@BOM", BOM))
    command.Parameters.Add(New SqlParameter("@uniqueID", uniqueID))
    Me.DoStoredProcedure(command)
  End Sub
  ''' <summary>
  ''' Gets the cart status that is currently in process from the entry created by AddNewCart.
  ''' A cart that has been completed will return as Available.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <returns></returns>
  Public Function GetCartStatus(cartNum As Short) As Status
    Dim command As SqlCommand = New SqlCommand("pspGetCartStatus")
    command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
    Dim temp As Short = CShort(DoScalarStoredProcedure(command))
    Return CType([Enum].Parse(GetType(Status), temp), Status)
    'Return CType([Enum].Parse(GetType(Status), Me.DoScalarStoredProcedure(command)), Status)
  End Function
  ''' <summary>
  ''' Gets the cart status that is currently in process from the entry created by AddNewCart.
  ''' A cart that has been completed will return as Available.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <returns>status, bom, aps cal part quantity</returns>
  Public Function GetCartStatus2(cartNum As Short) As Tuple(Of Status, String, Integer)
    Try
      Dim command As SqlCommand = New SqlCommand("pspGetCartStatus2")
      command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
      Dim dt As DataTable = DoStoredProcedure(command)
      Dim status As Status = CType([Enum].Parse(GetType(Status), dt.Rows(0).Item("CartStatus")), Status)
      Dim BOM As String = dt.Rows(0).Item("BOM")
      Dim partQuantity As Integer = If(dt.Rows(0).Item("APSCalPartQuantity").Equals(String.Empty), 0, Integer.Parse(dt.Rows(0).Item("APSCalPartQuantity").ToString()))
      Return New Tuple(Of Status, String, Integer)(status, BOM, partQuantity)
    Catch e As Exception
      Throw New Exception()
    End Try
  End Function

  ''' <summary>
  ''' Gets the Burn-In stall (bay) that is currently associated with a cart in process.
  ''' A cart that has been completed will return as -1.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <returns></returns>
  Public Function GetBurnInStall(cartNum As Short) As Short
    Dim command As SqlCommand = New SqlCommand("pspGetCartBay")
    command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
    Dim temp As Short = CShort(DoScalarStoredProcedure(command))
    Return temp
  End Function


  ''' <summary>
  ''' Gets the Burn-In stall (bay) that is currently associated with a cart in process.
  ''' A cart that has been completed will return as -1.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <returns></returns>
  Public Function GetCartUniqueID(cartNum As Short) As String
    Dim command As SqlCommand = New SqlCommand("pspGetUniqueID")
    command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
    Dim temp As String = DoScalarStoredProcedure(command)
    Return temp
  End Function

  '  Public Function GetStringRepresentation(DataTable As DataTable) As String

  '    If (DataTable = null) Then
  '      Throw New ArgumentNullException("'dataTable' cannot be null.");

  '    StringWriter representationWriter = New StringWriter();

  '    ' First, set the width of every column to the length of its largest element.
  '      Int()[] columnWidths = New int[dataTable.Columns.Count];
  '    For (int columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
  '    {
  '        int headerWidth = DataTable.Columns[columnIndex].ColumnName.Length;
  '        int longestElementWidth = DataTable.AsEnumerable()
  '  .Select((row) >= row[columnIndex].ToString().Length)
  '            .Max();
  '        columnWidths[columnIndex] = Math.Max(headerWidth, longestElementWidth);
  '    }

  '    // Next, write the table
  '    // Write a horizontal line.
  '    representationWriter.Write("+-");
  '    For (int columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
  '    {
  '        For (int i = 0; i < columnWidths[columnIndex]; i++)
  '          representationWriter.Write("-");
  '        representationWriter.Write("-+");
  '        If (columnIndex!= dataTable.Columns.Count - 1)
  '            representationWriter.Write("-");
  '    }
  '    representationWriter.WriteLine(" ");
  '    // Print the headers
  '    representationWriter.Write("| ");
  '    For (int columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
  '    {
  '        String header = dataTable.Columns[columnIndex].ColumnName;
  '        representationWriter.Write(header);
  '        For (int blanks = columnWidths[columnIndex] - header.Length; blanks > 0; blanks--)
  '            representationWriter.Write(" ");
  '        representationWriter.Write(" | ");
  '    }
  '    representationWriter.WriteLine();
  '    // Print another horizontal line.
  '    representationWriter.Write("+-");
  '    For (int columnIndex = 0; columnIndex < dataTable.Columns.Count; columnIndex++)
  '    {
  '        For (int i = 0; i < columnWidths[columnIndex]; i++)
  '            representationWriter.Write("-");
  '        representationWriter.Write("-+");
  '        If (columnIndex!= dataTable.Columns.Count - 1)
  '            representationWriter.Write("-");
  '    }
  '  representationWriter.WriteLine(" ");

  '    // Print the contents of the table.
  '    For (int row = 0; row < dataTable.Rows.Count; row++)
  '    {
  '        representationWriter.Write("| ");
  '        For (int column = 0; column < dataTable.Columns.Count; column++)
  '        {
  '            representationWriter.Write(dataTable.Rows[row][column]);
  '            For (int blanks = columnWidths[column] - dataTable.Rows[row][column].ToString().Length;
  '                blanks > 0; blanks--)
  '                representationWriter.Write(" ");
  '            representationWriter.Write(" | ");
  '        }
  '        representationWriter.WriteLine();
  '    }

  '    // Print a final horizontal line.
  '    representationWriter.Write("+-");
  '    For (int column = 0; column < dataTable.Columns.Count; column++)
  '  {
  '        For (int i = 0; i < columnWidths[column]; i++)
  '            representationWriter.Write("-");
  '        representationWriter.Write("-+");
  '        If (column!= dataTable.Columns.Count - 1)
  '            representationWriter.Write("-");
  '    }
  '    representationWriter.WriteLine(" ");

  '    Return representationWriter.ToString();
  '}









  ''' <summary>
  ''' Determines whether if the cart is currently in process somewhere along the line. (status of anything other than available)
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <returns></returns>
  Public Function IsCartInProcess(cartNum As Short) As Boolean
    Dim command As SqlCommand = New SqlCommand("pspIsCartInProcess")
    command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
    Return CBool(Me.DoScalarStoredProcedure(command))
  End Function
  ''' <summary>
  ''' Sets the entry for the cart in the database as complete. This cart will now be available. Should be called when cart status is Finished.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  Public Sub SetCartComplete(cartNum As Short)
    Dim command As SqlCommand = New SqlCommand("pspSetCartComplete")
    command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
    Me.DoStoredProcedure(command)
  End Sub
  ''' <summary>
  ''' Sets the entry for the cart in the database to a specified status.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <param name="status">The status.</param>
  ''' <exception cref="Exception"></exception>
  Public Sub SetCartStatus(cartNum As Short, status As Status)
    Try
      Dim command As SqlCommand = New SqlCommand("pspSetCartStatus")
      command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
      command.Parameters.Add(New SqlParameter("@status", Convert.ToInt16(status)))
      Me.DoStoredProcedure(command)
    Catch ex As Exception
      Throw New Exception(ex.ToString())
    End Try
  End Sub
  ''' <summary>
  ''' Sets the entry for the cart in the database to a specified status.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <param name="status">The status.</param>
  ''' <param name="partQuantity">The part quantity.</param>
  ''' <exception cref="Exception"></exception>
  Public Sub SetCartStatus(cartNum As Short, status As Status, partQuantity As Short)
    Try
      Dim command As SqlCommand = New SqlCommand("pspSetCartStatus2")
      command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
      command.Parameters.Add(New SqlParameter("@partQuantity", partQuantity))
      command.Parameters.Add(New SqlParameter("@status", Convert.ToInt16(status)))
      Me.DoScalarStoredProcedure(command)
    Catch ex As Exception
      Throw New Exception(ex.ToString())
    End Try
  End Sub
  ''' <summary>
  ''' Decrements the aps cal part quantity by one. Returns a bool, true if there are parts left in the cart.
  ''' </summary>
  ''' <param name="cartNum">The cart number.</param>
  ''' <returns></returns>
  ''' <exception cref="Exception"></exception>
  Public Function DecAPSCalPartQuantity(cartNum As Short) As Boolean
    Try
      Dim command As SqlCommand = New SqlCommand("pspDecrementAPSCalPartQuantity")
      command.Parameters.Add(New SqlParameter("@cartNumber", cartNum))
      'Dim temp As Boolean = CBool(Me.DoScalarStoredProcedure(command))
      Return CBool(Me.DoScalarStoredProcedure(command))
    Catch ex As Exception
      Throw New Exception(ex.ToString())
    End Try
  End Function


#End Region
#Region "Methods"
  Private Function DoStoredProcedure(ByVal command As SqlCommand) As DataTable
    Return DoStoredProcedure(command, daDontCare)
  End Function
  Private Function DoStoredProcedure(ByVal command As SqlCommand, ByRef da As SqlDataAdapter) As DataTable
    Dim returnedTable As DataTable = New DataTable
    Dim conn = New SqlConnection(ConnectionString)
    Try
      conn.Open()
    Catch connectionError As Exception
      Throw New Exception(("Error connecting to database: " + connectionError.ToString))
    End Try

    Dim transaction As SqlTransaction = conn.BeginTransaction
    ' Attempt to insert data using stored procedure
    Try
      command.Connection = conn
      command.Transaction = transaction
      command.CommandType = CommandType.StoredProcedure
      da = New SqlDataAdapter(command)
      da.Fill(returnedTable)
      'no error so commit
      command.Transaction.Commit()
    Catch sqlerror As Exception
      'attempt to rollback the transaction
      Try
        transaction.Rollback()
      Catch rollbackError As Exception
        Throw New Exception("Error Attempting to rollback transaction after failed query: " _
                        + rollbackError.ToString + "" + vbLf + "Execute query error: " + sqlerror.ToString)
      End Try

      'stored procedure error
      Throw New Exception(("Error Attempting to execute query: " + sqlerror.ToString))
    End Try
    If (da.Equals(daDontCare)) Then
      conn.Close()
    End If
    Return returnedTable
  End Function
  Private Function DoScalarStoredProcedure(ByVal command As SqlCommand) As Object
    Dim returnedVal As Object
    Dim conn = New SqlConnection(ConnectionString)
    Try
      conn.Open()
    Catch connectionError As Exception
      Throw New Exception(("Error connecting to database: " + connectionError.ToString))
    End Try

    Dim transaction As SqlTransaction = conn.BeginTransaction
    ' Attempt to insert data using stored procedure
    Try
      command.Connection = conn
      command.Transaction = transaction
      command.CommandType = CommandType.StoredProcedure
      daDontCare = New SqlDataAdapter(command)
      returnedVal = command.ExecuteScalar()
      'no error so commit
      command.Transaction.Commit()
    Catch sqlerror As Exception
      'attempt to rollback the transaction
      Try
        transaction.Rollback()
      Catch rollbackError As Exception
        Throw New Exception("Error Attempting to rollback transaction after failed query: " _
                                + rollbackError.ToString + "" + vbLf + "Execute query error: " + sqlerror.ToString)
      End Try

      'stored procedure error
      Throw New Exception(("Error Attempting to execute query: " + sqlerror.ToString))
    End Try
    conn.Close()
    Return returnedVal
  End Function

  Private Sub UpdateDataTable(ByVal da As SqlDataAdapter, ByVal dt As DataTable)
    Dim objCommandBuilder As SqlCommandBuilder = New SqlCommandBuilder(da)
    da.Update(dt)
  End Sub
#End Region
End Class
