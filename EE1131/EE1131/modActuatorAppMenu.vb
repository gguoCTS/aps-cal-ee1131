﻿Imports System.Reflection
Imports System.Configuration
Module modActuatorAppMenu

  Public Sub ActuatorAppInitMenus()
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim miMenuSeperator As System.Windows.Forms.ToolStripSeparator
    Dim miParentMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim miToolStripItem As System.Windows.Forms.ToolStripItem
    Dim lfrmTsopMain As frmTsopMain = My.Application.OpenForms("frmTsopMain")
    Dim blnMenuItemFound As Boolean
    Dim strMenuItemName As String
    Dim strShortCutKey As String
    Dim strErrorMessage As String

    Dim tForm As Type 'Type variable for main form
    Dim tMod As Type 'Type variable for this module
    Dim evMenu As EventInfo 'Event Info variable for menu event to handle
    Dim tDelegate As Type 'Type variable to help define event handler delegate
    Dim miHandler As MethodInfo 'Method Info variable to help define event handler method
    Dim delHandler As System.Delegate 'Delegate variable used to setup event handler programmatically
    Dim strKey As String '

    Dim drMenu As DataRow

    Try
      'Establish type variable for main form
      tForm = Type.GetType(gstrAssemblyName & "." & "frmTsopMain")

      'Loop through menu defs here
      For Each drMenu In gdtMenuDefinitions.Rows
        'Find parent menu item
        miParentMenuItem = FindParentMenuItem(drMenu)

        'See if menu item has been added to menu strip control
        blnMenuItemFound = False
        strMenuItemName = drMenu.Item("MenuItemName")
        For Each miToolStripItem In miParentMenuItem.DropDownItems
          If miToolStripItem.Name = strMenuItemName Then
            blnMenuItemFound = True
            Exit For
          End If
        Next
        If blnMenuItemFound = False Then
          If drMenu.Item("MenuItemText") = "Separator" Then
            miMenuSeperator = New System.Windows.Forms.ToolStripSeparator
            miParentMenuItem.DropDownItems.Add(miMenuSeperator)
            Continue For
          Else
            miMenuItem = New System.Windows.Forms.ToolStripMenuItem
          End If

        Else
          miMenuItem = miParentMenuItem.DropDownItems(strMenuItemName)
        End If

        'Set menu properties
        miMenuItem.Enabled = drMenu.Item("MenuItemEnabled")
        miMenuItem.Name = strMenuItemName
        miMenuItem.Text = drMenu.Item("MenuItemText")
        If drMenu.Item("ShortcutKeys").ToString <> "" Then
          strShortCutKey = Mid(drMenu.Item("ShortcutKeys"), 5, Len(drMenu.Item("ShortcutKeys")))
          miMenuItem.ShortcutKeys = DirectCast([Enum].Parse(GetType(Keys), strShortCutKey), Integer)
        End If

        If drMenu.Item("CheckState") <> "0" Then
          miMenuItem.CheckState = CheckState.Checked
        Else
          miMenuItem.CheckState = CheckState.Unchecked
        End If

        If drMenu.Item("CheckOnClick") <> "0" Then
          miMenuItem.CheckOnClick = True
        Else
          miMenuItem.CheckOnClick = False
        End If

        If drMenu.Item("Visible") <> "0" Then
          miMenuItem.Visible = True
        Else
          miMenuItem.Visible = False
        End If

        'Hook up event handler if needed
        If drMenu.Item("EventName") <> "None" Then
          'Establish type variable for module containing menu event handler
          tMod = Type.GetType(gstrAssemblyName & "." & drMenu.Item("EventHandlerModuleName"))
          'Type of Event to handle
          evMenu = tForm.GetEvent(drMenu.Item("EventName"))
          'Type variable for event handler delegate
          tDelegate = evMenu.EventHandlerType
          'Method Info variable for event handler delegate
          miHandler = tMod.GetMethod(drMenu.Item("EventHandlerName"))
          If miHandler IsNot Nothing Then
            'Delegate variable to hook up event handler
            delHandler = System.Delegate.CreateDelegate(tDelegate, miHandler)
          Else
            strErrorMessage = "Event Handler Method: " & drMenu.Item("EventHandlerName") & " Doesn't Exist"
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            Throw New TsopAnomalyException
          End If

          'Remove previous event handlers
          For Each strKey In gMenuEvents.Keys
            If strKey = strMenuItemName Then
              RemoveHandler miMenuItem.Click, gMenuEvents(strKey)
            End If
          Next

          'Remove menu item from events collection
          If gMenuEvents.ContainsKey(strMenuItemName) Then
            gMenuEvents.Remove(strMenuItemName)
          End If

          'Add event handler to menu item
          Select Case drMenu.Item("EventName")
            Case "Click"
              AddHandler miMenuItem.Click, delHandler
          End Select

          'Add menu item and its handler to collection
          gMenuEvents.Add(miMenuItem.Name, delHandler)
        End If

        'Add menu item to menu if it doesn't already exist
        If blnMenuItemFound = False Then
          miParentMenuItem.DropDownItems.Add(miMenuItem)
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionTestClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Dim lfrmUpdate As frmTsopMain
    Dim intResult As Integer
    Try
      'lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      'Call lfrmUpdate.mnuFunctionTest_Click()

      gControlFlags.StartScan = True
      gControlFlags.ManualScan = True
      gtmrOperatorTouchStartTimer.Enabled = False

      If gControlFlags.UsesExternalDevices Then
        Call ResetLight()
        intResult = gDigitalOutput.Write(0, gDIOStructure.YellowLight)
      End If

      Call ExecuteTaskList(TaskListFilterEnum.Test)

      If glstAnomaly.Count > 0 Or gControlFlags.AbortTest Then
        gControlFlags.TestError = True
        ResetPowerSupply()
        Call UpdateTestLotCounts()
        Call UpdateLotDataDisplay()
        Call UpdatePassFailStatus()
        Call PrintRejectLabel()
      End If

      If glstAnomaly.Count > 0 Then
        LogAnomaly()
      End If

      gControlFlags.AbortTest = False
      gControlFlags.TestError = False
      gControlFlags.TestFailure = False
      gControlFlags.StartScan = False
      gControlFlags.ManualScan = False
      gtmrOperatorTouchStartTimer.Enabled = True
      glstAnomaly.Clear()


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintParameterSetClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintParameterSet_Click()
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintTestResultsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintTestResults_Click()
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintTestStatisticsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintTestStatistics_Click()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintPreviousLotsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintPreviousLot_Click()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintPreviewTestResultsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintPreviewTestResults_Click()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintPreviewTestStatisticsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintPreviewTestStatistics_Click()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuFunctionPrintPreviewParameterSetClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      Call lfrmUpdate.mnuFunctionPrintPreviewParameterSet_Click()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolOPCClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Dim lfrmUpdate As frmTsopMain
    Try
      'lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      'Call lfrmUpdate.mnuToolOPC_Click()

      Dim result As DialogResult
      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        Exit Sub
      End If

      'frmOPCTest.Show()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolOperatorIOClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Try

      frmOperatorIO.Show()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolCANClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    'Dim lfrmUpdate As frmTsopMain
    Try
      'lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      'Call lfrmUpdate.mnuToolOPC_Click()

      Dim result As DialogResult
      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        Exit Sub
      End If

      frmCANTestPanel.Show()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolGetLastTestResultsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim intLoop As Integer = 0
    Dim intLoop2 As Integer = 0
    Dim dt As DataTable
    Dim strData As String = ""
    Try

      dt = gDatabase.LookupTestData(gblnUseUnreleasedSets)

      strData = ""
      'For intLoop = 0 To dt.Rows.Count - 1
      For intLoop2 = 0 To dt.Columns.Count - 1
        strData = strData & dt.Columns(intLoop2).ColumnName.ToString & " : " & dt.Rows(0).Item(intLoop2).ToString & vbCrLf
        'strData = strData & dt.Rows(0).Item(intLoop2).ToString & vbCrLf
      Next
      'Next
      MsgBox(strData)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolSetPowerSupplyManuallyClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Dim strVoltage As String
    Try


      'Call StartPowerSupplyCommunication()
      If gDutPower Is Nothing Then
        gDutPower = New clsDutPower(gstrClassConfigFilePath, ConfigurationManager.AppSettings("PowerSupplyUsed"))
      End If
      strVoltage = InputBox("Enter the Voltage to Set the Power Supply For")
      Call gDutPower.SetSupplyVoltage(strVoltage)


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolCANActivityLoggingClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain = Nothing
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim miParentMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim result As DialogResult

    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      miParentMenuItem = lfrmUpdate.MenuStripMain.Items("mnuTool")
      miMenuItem = miParentMenuItem.DropDownItems("mnuToolEnableCANActuatorActivityLogging")

      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        miMenuItem.Checked = gControlFlags.MasterMode
        Exit Sub
      Else
        gCanActuator.CanActuatorActivityLoggingEnabled = Not gCanActuator.CanActuatorActivityLoggingEnabled
        miMenuItem.Checked = gCanActuator.CanActuatorActivityLoggingEnabled

      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub ActuatorMenuToolTestWrongStationSoundClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Try


      Call PlayWrongStationSound()



    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolTestRejectLabelClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Try

      frmTestPrintRejectLabels.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolGetLastShiftCountsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Try

      MsgBox("Previous Shift Data:" & vbCrLf &
            "                     Passed: " & gintRTYPreviousShiftTotalPassed & vbCrLf &
            "                     Failed: " & gintRTYShiftTotalFailed)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolIgnoreTestBitsClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim result As DialogResult
    Dim lfrmUpdate As frmTsopMain = Nothing
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim miParentMenuItem As System.Windows.Forms.ToolStripMenuItem

    Try

      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      miParentMenuItem = lfrmUpdate.MenuStripMain.Items("mnuTool")
      miMenuItem = miParentMenuItem.DropDownItems("mnuToolIgnoreTestBits")

      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        ' ActuatorMenuToolIgnoreTestBitsClick.checked = gControlFlags.IgnoreTestBits
        miMenuItem.Checked = gControlFlags.IgnoreTestBits
        Exit Sub
      Else
        gControlFlags.IgnoreTestBits = Not gControlFlags.IgnoreTestBits
        '  ActuatorMenuToolIgnoreTestBitsClick.checked = gControlFlags.IgnoreTestBits
        miMenuItem.Checked = gControlFlags.IgnoreTestBits

        Call frmTsopMain.SetBackgroundColor()

      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuToolMasterModeClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain = Nothing
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim miParentMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim result As DialogResult

    Try
      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      miParentMenuItem = lfrmUpdate.MenuStripMain.Items("mnuTool")
      miMenuItem = miParentMenuItem.DropDownItems("mnuToolMasterMode")

      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        miMenuItem.Checked = gControlFlags.MasterMode
        Exit Sub
      Else
        gControlFlags.MasterMode = Not gControlFlags.MasterMode
        miMenuItem.Checked = gControlFlags.MasterMode
        Call frmTsopMain.SetBackgroundColor()

      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ActuatorMenuOptionsOnPlcStartTestClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim lfrmUpdate As frmTsopMain
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim miParentMenuItem As System.Windows.Forms.ToolStripMenuItem
    Try

      lfrmUpdate = My.Application.OpenForms("frmTsopMain")
      miParentMenuItem = lfrmUpdate.MenuStripMain.Items("mnuOptions")
      miParentMenuItem = miParentMenuItem.DropDownItems("mnuOptionsOnPlcStart")
      miMenuItem = miParentMenuItem.DropDownItems("mnuOptionsOnPlcStartTest")

      'gControlFlags.OnPlcStartTest = miMenuItem.Checked

      If miMenuItem.Checked = True Then
        'If gblnProductionMode Then
        If gControlFlags.ProductionMode = True Then
          'Password required to uncheck
          Dim result As DialogResult
          result = frmPasswordDialog.ShowDialog()
          If result <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("Password was not entered correctly" & vbCrLf & "'Test' still checked.", "Password ERROR")
            Exit Sub
          End If
        End If
        miMenuItem.Checked = False
        gControlFlags.OnPlcStartTest = False
      Else
        miMenuItem.Checked = True
        gControlFlags.OnPlcStartTest = True
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub
  Public Sub RestartCANActuator()
    Try
      gCanActuator.ShutDown()
      If gCanActuator.Anomaly IsNot Nothing Then
        gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
        gCanActuator.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gCanActuator = Nothing

      gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)
      If gCanActuator.Anomaly IsNot Nothing Then
        gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
        gCanActuator.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Finally
      frmTsopMain.Cursor = Cursors.Default
    End Try
  End Sub
  Public Sub ActuatorMenuRestartCANActuatorClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Try

      frmTsopMain.Cursor = Cursors.WaitCursor

      RestartCANActuator()
      'gCanActuator.ShutDown()
      'If gCanActuator.Anomaly IsNot Nothing Then
      '  gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
      '  gCanActuator.Anomaly = Nothing
      '  Throw New TsopAnomalyException
      'End If

      'gCanActuator = Nothing

      'gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)
      'If gCanActuator.Anomaly IsNot Nothing Then
      '  gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
      '  gCanActuator.Anomaly = Nothing
      '  Throw New TsopAnomalyException
      'End If

      MsgBox("CAN Actuator Component Has Been Restarted Successfully")

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Finally
      frmTsopMain.Cursor = Cursors.Default
    End Try

  End Sub

End Module
