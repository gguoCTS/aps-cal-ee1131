﻿Imports System.Windows.Forms

Public Class frmRouting

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmRouting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    RichTextBox1.Text = gstrRoutingMessage
    End Sub

  Private Sub btnViewAnomalyMessage_Click(sender As System.Object, e As System.EventArgs) Handles btnViewAnomalyMessage.Click
    Dim strMessage As String
    If gAnomaly IsNot Nothing Then
      strMessage = "Anomaly " & gAnomaly.AnomalyNumber & " Occured in Procedure:" & vbCrLf & gAnomaly.ModuleName & "." & gAnomaly.ProcedureName

      If gAnomaly.AnomalyExceptionMessage <> String.Empty Then
        strMessage = strMessage & vbCrLf & vbCrLf & gAnomaly.AnomalyExceptionMessage
      End If

      If gAnomaly.AnomalyMessageText <> String.Empty Then
        strMessage = strMessage & vbCrLf & vbCrLf & gAnomaly.AnomalyMessageText
      End If

      If gAnomaly.CallerList <> String.Empty Then
        strMessage = strMessage & vbCrLf & vbCrLf & "Caller List:" & vbCrLf & gAnomaly.CallerList
      End If

      MessageBox.Show(strMessage, gAnomaly.AnomalyMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)
    End If

  End Sub
End Class
