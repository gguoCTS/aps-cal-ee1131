﻿Public Class clsActuatorProductionStations

    Public Enum Names
        Rework = 0
        Open1 = 1
        SoftwareDownload = 2
        Open3 = 3
        LeakTest = 4
        APSCalibration = 5
        Open6 = 6
        FunctionalTest = 7
        Open8 = 8
        LASERMarker = 9
        Open10 = 10
        Open11 = 11
        Open12 = 12
        Open13 = 13
        Open14 = 14
        Open15 = 15

    End Enum


    Public Sub HWLineCodeStatus(ByVal HWLine1Upper As Byte, _
                                     ByVal HWLine1Lower As Byte, _
                                     ByVal HWLine2Upper As Byte, _
                                     ByVal HWLine2Lower As Byte, _
                                     ByRef ReadyForStation As String, _
                                     ByRef StartedStation As String, _
                                     ByRef FailedAtStation As String, _
                                     ByRef AllStationsPass As Boolean, _
                                     ByRef WithRework As Boolean)

        Dim strStation As String
        Dim Started As Boolean = False
        Dim Passed As Boolean = False
        Dim enumValues As Array = System.[Enum].GetValues(GetType(Names))

        For Each Station As Names In enumValues 'Check each station against the supplied code
            Select Case Station
                Case 1 To 7 'Lower Bytes
                    If Not Station.ToString.Contains("Open") Then
                        strStation = Station.ToString
                        Started = (Not HWLine1Lower And 2 ^ Station) > 0
                        Console.WriteLine(strStation & " " & Started)
                        If Not Started Then
                            ReadyForStation = strStation
                            Exit For
                        Else 'Start Bit is set for this station
                            Passed = (Not HWLine2Lower And 2 ^ Station) > 0
                            If Not Passed Then
                                FailedAtStation = strStation
                                Exit For
                            End If
                        End If
                    End If

                Case 8 To 15 'Upper Bytes 
                    If Not Station.ToString.Contains("Open") Then
                        strStation = Station.ToString
                        Started = (Not HWLine1Upper And 2 ^ (Station - 8)) > 0
                        Console.WriteLine(strStation & " " & Started)
                        If Not Started Then
                            ReadyForStation = strStation
                            Exit For
                        Else 'Start Bit is set for this station
                            Passed = (Not HWLine2Upper And 2 ^ (Station - 8)) > 0
                            If Not Passed Then
                                FailedAtStation = strStation
                                Exit For
                            End If
                        End If
                        'No more station Left
                        AllStationsPass = True
                    End If


            End Select


        Next


        WithRework = Not HWLine1Lower And (2 ^ Names.Rework) Or _
                      Not HWLine2Lower And (2 ^ Names.Rework)

    End Sub
End Class

