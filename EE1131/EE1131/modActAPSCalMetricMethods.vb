﻿Module modActAPSCalMetricMethods

  Public Sub SetAPSTypeMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gstrAPSType

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSBaseCalDataMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrBaseCalibrationTestResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSBaseCalMonotonicityMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrMonotonicityTestResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSCalDataIndexMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrCalDataIndexTestResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSCalExcelFileNameMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gControlAndInterface.APSCALExcelFilename

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSCalFileNameMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrAPSCalFileName

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSCalResultMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gstrAPSCALResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSCalTimeMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gdblAPSCALTime

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSCalWraparoundMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrCalibrationWraparoundTestResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetAPSSlopeDeltaMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gstrAPSSlopeDeltaTestResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub SetBurnInElapsedTempFaultsMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gdblBurnInTempFaults

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetBurnInElapsedTimeFaultsMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gdblBurnInElapsedTimeFaults

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetBurnInPowerUpCountMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gintBurnInCycles

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetBurnInTimeMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gintBurnInTime

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetFaultCountMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gintActuatorFaultCount

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetPostAPSCalFaultCountMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gintPostDownloadActuatorFaultCount

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetHWTempMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gintHWTemp

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetOptionIgnoreTestStatusBitsMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gControlFlags.IgnoreTestBits

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetProcessPCPassedMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrProcessPCPassed


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetReadyForStationMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)

      If gControlFlags.IgnoreTestBits Or gControlFlags.MasterMode Then
        metMetric.AlphaMetricValue = "BITS BYPASSED"
      Else
        metMetric.AlphaMetricValue = gstrReadyForStation
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetReworkMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      If gControlFlags.IgnoreTestBits Or gControlFlags.MasterMode Then
        metMetric.AlphaMetricValue = "BITS BYPASSED"
      Else
        metMetric.AlphaMetricValue = gstrRework
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetTestStartedStationMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      If gControlFlags.IgnoreTestBits Or gControlFlags.MasterMode Then
        metMetric.AlphaMetricValue = "BITS BYPASSED"
      Else
        metMetric.AlphaMetricValue = gstrTestStartedStation
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetTestPassedStationMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)

      If gControlFlags.IgnoreTestBits Or gControlFlags.MasterMode Then
        metMetric.AlphaMetricValue = "BITS BYPASSED"
      Else
        metMetric.AlphaMetricValue = gstrTestPassedStation
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetEntrySoftwareVersionMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrActuatorAppSoftwareVersion

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetExitSoftwareVersionMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gstrPostDownloadActuatorAppSoftwareVersion

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

End Module
