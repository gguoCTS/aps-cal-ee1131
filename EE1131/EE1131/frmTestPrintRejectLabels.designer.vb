<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestPrintRejectLabels
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.btnCell4 = New System.Windows.Forms.Button()
    Me.lblBOM = New System.Windows.Forms.Label()
    Me.lblVoltage = New System.Windows.Forms.Label()
    Me.txtBoxBOM = New System.Windows.Forms.TextBox()
    Me.txtVoltage = New System.Windows.Forms.TextBox()
    Me.txtSerial = New System.Windows.Forms.TextBox()
    Me.txtDateTime = New System.Windows.Forms.TextBox()
    Me.lblSerial = New System.Windows.Forms.Label()
    Me.lblDateTime = New System.Windows.Forms.Label()
    Me.lblProcess = New System.Windows.Forms.Label()
    Me.txtProcess = New System.Windows.Forms.TextBox()
    Me.lblDescription = New System.Windows.Forms.Label()
    Me.txtDescription = New System.Windows.Forms.TextBox()
    Me.SuspendLayout()
    '
    'btnCell4
    '
    Me.btnCell4.Location = New System.Drawing.Point(207, 270)
    Me.btnCell4.Name = "btnCell4"
    Me.btnCell4.Size = New System.Drawing.Size(180, 34)
    Me.btnCell4.TabIndex = 3
    Me.btnCell4.Text = "Print Reject Label"
    Me.btnCell4.UseVisualStyleBackColor = True
    '
    'lblBOM
    '
    Me.lblBOM.AutoSize = True
    Me.lblBOM.Location = New System.Drawing.Point(76, 16)
    Me.lblBOM.Name = "lblBOM"
    Me.lblBOM.Size = New System.Drawing.Size(31, 13)
    Me.lblBOM.TabIndex = 4
    Me.lblBOM.Text = "BOM"
    '
    'lblVoltage
    '
    Me.lblVoltage.AutoSize = True
    Me.lblVoltage.Location = New System.Drawing.Point(76, 44)
    Me.lblVoltage.Name = "lblVoltage"
    Me.lblVoltage.Size = New System.Drawing.Size(43, 13)
    Me.lblVoltage.TabIndex = 5
    Me.lblVoltage.Text = "Voltage"
    '
    'txtBoxBOM
    '
    Me.txtBoxBOM.Location = New System.Drawing.Point(153, 12)
    Me.txtBoxBOM.Name = "txtBoxBOM"
    Me.txtBoxBOM.Size = New System.Drawing.Size(103, 20)
    Me.txtBoxBOM.TabIndex = 6
    Me.txtBoxBOM.Text = "07547"
    '
    'txtVoltage
    '
    Me.txtVoltage.Location = New System.Drawing.Point(153, 41)
    Me.txtVoltage.Name = "txtVoltage"
    Me.txtVoltage.Size = New System.Drawing.Size(103, 20)
    Me.txtVoltage.TabIndex = 7
    Me.txtVoltage.Text = "12V"
    '
    'txtSerial
    '
    Me.txtSerial.Location = New System.Drawing.Point(153, 67)
    Me.txtSerial.Name = "txtSerial"
    Me.txtSerial.Size = New System.Drawing.Size(103, 20)
    Me.txtSerial.TabIndex = 8
    Me.txtSerial.Text = "FE12DAE1234"
    '
    'txtDateTime
    '
    Me.txtDateTime.Location = New System.Drawing.Point(153, 93)
    Me.txtDateTime.Name = "txtDateTime"
    Me.txtDateTime.Size = New System.Drawing.Size(103, 20)
    Me.txtDateTime.TabIndex = 9
    Me.txtDateTime.Text = "08JAN14 19:02"
    '
    'lblSerial
    '
    Me.lblSerial.AutoSize = True
    Me.lblSerial.Location = New System.Drawing.Point(76, 72)
    Me.lblSerial.Name = "lblSerial"
    Me.lblSerial.Size = New System.Drawing.Size(33, 13)
    Me.lblSerial.TabIndex = 10
    Me.lblSerial.Text = "Serial"
    '
    'lblDateTime
    '
    Me.lblDateTime.AutoSize = True
    Me.lblDateTime.Location = New System.Drawing.Point(76, 96)
    Me.lblDateTime.Name = "lblDateTime"
    Me.lblDateTime.Size = New System.Drawing.Size(56, 13)
    Me.lblDateTime.TabIndex = 11
    Me.lblDateTime.Text = "Date Time"
    '
    'lblProcess
    '
    Me.lblProcess.AutoSize = True
    Me.lblProcess.Location = New System.Drawing.Point(76, 122)
    Me.lblProcess.Name = "lblProcess"
    Me.lblProcess.Size = New System.Drawing.Size(45, 13)
    Me.lblProcess.TabIndex = 13
    Me.lblProcess.Text = "Process"
    '
    'txtProcess
    '
    Me.txtProcess.Location = New System.Drawing.Point(153, 119)
    Me.txtProcess.Name = "txtProcess"
    Me.txtProcess.Size = New System.Drawing.Size(103, 20)
    Me.txtProcess.TabIndex = 12
    Me.txtProcess.Text = "APSCAL3"
    '
    'lblDescription
    '
    Me.lblDescription.AutoSize = True
    Me.lblDescription.Location = New System.Drawing.Point(76, 148)
    Me.lblDescription.Name = "lblDescription"
    Me.lblDescription.Size = New System.Drawing.Size(60, 13)
    Me.lblDescription.TabIndex = 15
    Me.lblDescription.Text = "Description"
    '
    'txtDescription
    '
    Me.txtDescription.Location = New System.Drawing.Point(153, 145)
    Me.txtDescription.Multiline = True
    Me.txtDescription.Name = "txtDescription"
    Me.txtDescription.Size = New System.Drawing.Size(398, 45)
    Me.txtDescription.TabIndex = 14
    Me.txtDescription.Text = "26; The actuator failed to respond to a request for calculated application checks" &
    "um.; ; ; ;"
    '
    'frmTestPrintRejectLabels
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(586, 316)
    Me.Controls.Add(Me.lblDescription)
    Me.Controls.Add(Me.txtDescription)
    Me.Controls.Add(Me.lblProcess)
    Me.Controls.Add(Me.txtProcess)
    Me.Controls.Add(Me.lblDateTime)
    Me.Controls.Add(Me.lblSerial)
    Me.Controls.Add(Me.txtDateTime)
    Me.Controls.Add(Me.txtSerial)
    Me.Controls.Add(Me.txtVoltage)
    Me.Controls.Add(Me.txtBoxBOM)
    Me.Controls.Add(Me.lblVoltage)
    Me.Controls.Add(Me.lblBOM)
    Me.Controls.Add(Me.btnCell4)
    Me.Name = "frmTestPrintRejectLabels"
    Me.Text = "Test Print Reject Labels"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnCell4 As System.Windows.Forms.Button
    Friend WithEvents lblBOM As System.Windows.Forms.Label
    Friend WithEvents lblVoltage As System.Windows.Forms.Label
    Friend WithEvents txtBoxBOM As System.Windows.Forms.TextBox
    Friend WithEvents txtVoltage As System.Windows.Forms.TextBox
    Friend WithEvents txtSerial As System.Windows.Forms.TextBox
    Friend WithEvents txtDateTime As System.Windows.Forms.TextBox
    Friend WithEvents lblSerial As System.Windows.Forms.Label
    Friend WithEvents lblDateTime As System.Windows.Forms.Label
    Friend WithEvents lblProcess As System.Windows.Forms.Label
    Friend WithEvents txtProcess As System.Windows.Forms.TextBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As System.Windows.Forms.TextBox

End Class
