﻿Imports System.Configuration

Module modActuatorApp

  Public gActuatorStations As New clsActuatorProductionStations
  Public gstrActuatorAppSoftwareVersion As String
  Public gstrPostDownloadActuatorAppSoftwareVersion As String
  Public gintActuatorFaultCount As Integer
  Public gintPostDownloadActuatorFaultCount As Integer

  Public gCanActuator As clsCANActuator

  Public gstrDUTPowerSupplyBaudRate As String
  Public gstrDUTPowerSupplyComPortName As String
  Public gstrDUTPowerSupplyMaxCurrent As String
  Public gDUTPowerSupplySerialPort As New System.IO.Ports.SerialPort
  Public gdblDUTPowerSupplyUsed As String

  Public gMediaPlayer As New System.Media.SoundPlayer

  Public gtblParamArrayMap As DataTable
  Public gstrProcessPCPassed As String

  Public gstrReadyForStation As String
  Public gRejectLabel As clsIntermecRejectLabel
  Public gstrRework As String

  Public gstrStatusBits As String

  Public gstrTestPassedByteLower As String
  Public gstrTestPassedByteUpper As String
  Public gstrTestPassedStation As String
  Public gstrTestStartedByteLower As String
  Public gstrTestStartedByteUpper As String
  Public gstrTestStartedStation As String
  'Public gblTestStatusBitsEnabled As Boolean

  Public gstrWrongStationSoundPath As String


  Public Function CheckStatus(ByRef strHWLine1Upper As String, _
              ByRef strHWLine1Lower As String, _
              ByRef strHWLine2Upper As String, _
              ByRef strHWLine2Lower As String) As String

    Dim ReadyForStation As String = ""
    Dim StartedStation As String = ""
    Dim FailedAtStation As String = ""
    Dim AllStationsPass As Boolean = False
    Dim WithRework As Boolean = False
    Dim strRework As String = ""
    Dim ReturnValue As String = ""
    gActuatorStations.HWLineCodeStatus(strHWLine1Upper, strHWLine1Lower, _
                                   strHWLine2Upper, strHWLine2Lower, _
                                   ReadyForStation, StartedStation, FailedAtStation, _
                                   AllStationsPass, WithRework)
    If WithRework Then
      strRework = " with Rework"
      gstrRework = "True"
    Else
      gstrRework = "False"
    End If

    If ReadyForStation <> "" Then ReturnValue = "Ready for " & ReadyForStation & strRework
    If FailedAtStation <> "" Then ReturnValue = "Started " & FailedAtStation & strRework
    If AllStationsPass Then ReturnValue = "All Stations Pass" & strRework

    Return ReturnValue

  End Function

  Public Sub PlayWrongStationSound()

    gMediaPlayer.SoundLocation = gstrWrongStationSoundPath
    gMediaPlayer.Load()
    gMediaPlayer.Play()

  End Sub



End Module
