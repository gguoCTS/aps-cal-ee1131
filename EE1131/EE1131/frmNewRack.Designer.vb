﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewRack
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.rtfInstructions = New System.Windows.Forms.RichTextBox()
    Me.rtfResults = New System.Windows.Forms.RichTextBox()
    Me.txtRackScanned = New System.Windows.Forms.TextBox()
    Me.lblRackScan = New System.Windows.Forms.Label()
    Me.btnAcceptRack = New System.Windows.Forms.Button()
    Me.btnPickNewRack = New System.Windows.Forms.Button()
    Me.tmrLoad = New System.Windows.Forms.Timer(Me.components)
    Me.btnNewBOM = New System.Windows.Forms.Button()
    Me.btnOverrideRack = New System.Windows.Forms.Button()
    Me.SuspendLayout()
    '
    'rtfInstructions
    '
    Me.rtfInstructions.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rtfInstructions.ForeColor = System.Drawing.Color.Blue
    Me.rtfInstructions.Location = New System.Drawing.Point(12, 23)
    Me.rtfInstructions.Name = "rtfInstructions"
    Me.rtfInstructions.Size = New System.Drawing.Size(424, 106)
    Me.rtfInstructions.TabIndex = 0
    Me.rtfInstructions.Text = ""
    '
    'rtfResults
    '
    Me.rtfResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.rtfResults.Location = New System.Drawing.Point(12, 195)
    Me.rtfResults.Name = "rtfResults"
    Me.rtfResults.Size = New System.Drawing.Size(424, 106)
    Me.rtfResults.TabIndex = 1
    Me.rtfResults.Text = ""
    '
    'txtRackScanned
    '
    Me.txtRackScanned.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.txtRackScanned.Location = New System.Drawing.Point(91, 148)
    Me.txtRackScanned.Name = "txtRackScanned"
    Me.txtRackScanned.Size = New System.Drawing.Size(83, 29)
    Me.txtRackScanned.TabIndex = 2
    '
    'lblRackScan
    '
    Me.lblRackScan.AutoSize = True
    Me.lblRackScan.Location = New System.Drawing.Point(12, 159)
    Me.lblRackScan.Name = "lblRackScan"
    Me.lblRackScan.Size = New System.Drawing.Size(61, 13)
    Me.lblRackScan.TabIndex = 3
    Me.lblRackScan.Text = "Scan Rack"
    '
    'btnAcceptRack
    '
    Me.btnAcceptRack.Enabled = False
    Me.btnAcceptRack.Location = New System.Drawing.Point(351, 316)
    Me.btnAcceptRack.Name = "btnAcceptRack"
    Me.btnAcceptRack.Size = New System.Drawing.Size(84, 27)
    Me.btnAcceptRack.TabIndex = 4
    Me.btnAcceptRack.Text = "Accept Rack"
    Me.btnAcceptRack.UseVisualStyleBackColor = True
    '
    'btnPickNewRack
    '
    Me.btnPickNewRack.Location = New System.Drawing.Point(128, 316)
    Me.btnPickNewRack.Name = "btnPickNewRack"
    Me.btnPickNewRack.Size = New System.Drawing.Size(92, 27)
    Me.btnPickNewRack.TabIndex = 5
    Me.btnPickNewRack.Text = "Pick New Rack"
    Me.btnPickNewRack.UseVisualStyleBackColor = True
    '
    'tmrLoad
    '
    Me.tmrLoad.Enabled = True
    Me.tmrLoad.Interval = 1000
    '
    'btnNewBOM
    '
    Me.btnNewBOM.Location = New System.Drawing.Point(15, 316)
    Me.btnNewBOM.Name = "btnNewBOM"
    Me.btnNewBOM.Size = New System.Drawing.Size(92, 27)
    Me.btnNewBOM.TabIndex = 6
    Me.btnNewBOM.Text = "Pick New BOM"
    Me.btnNewBOM.UseVisualStyleBackColor = True
    '
    'btnOverrideRack
    '
    Me.btnOverrideRack.Enabled = False
    Me.btnOverrideRack.Location = New System.Drawing.Point(248, 316)
    Me.btnOverrideRack.Name = "btnOverrideRack"
    Me.btnOverrideRack.Size = New System.Drawing.Size(84, 27)
    Me.btnOverrideRack.TabIndex = 7
    Me.btnOverrideRack.Text = "No Rack"
    Me.btnOverrideRack.UseVisualStyleBackColor = True
    '
    'frmNewRack
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(463, 355)
    Me.ControlBox = False
    Me.Controls.Add(Me.btnOverrideRack)
    Me.Controls.Add(Me.btnNewBOM)
    Me.Controls.Add(Me.btnPickNewRack)
    Me.Controls.Add(Me.btnAcceptRack)
    Me.Controls.Add(Me.lblRackScan)
    Me.Controls.Add(Me.txtRackScanned)
    Me.Controls.Add(Me.rtfResults)
    Me.Controls.Add(Me.rtfInstructions)
    Me.Name = "frmNewRack"
    Me.Text = "frmNewRack"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents rtfInstructions As System.Windows.Forms.RichTextBox
    Friend WithEvents rtfResults As System.Windows.Forms.RichTextBox
    Friend WithEvents txtRackScanned As System.Windows.Forms.TextBox
    Friend WithEvents lblRackScan As System.Windows.Forms.Label
    Friend WithEvents btnAcceptRack As System.Windows.Forms.Button
    Friend WithEvents btnPickNewRack As System.Windows.Forms.Button
    Friend WithEvents tmrLoad As System.Windows.Forms.Timer
  Friend WithEvents btnNewBOM As Button
  Friend WithEvents btnOverrideRack As Button
End Class
