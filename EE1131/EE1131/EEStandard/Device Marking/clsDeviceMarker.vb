﻿
Imports System.Xml
Imports VB = Microsoft.VisualBasic

Public Class clsDeviceMarker

#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

#End Region

#Region "Constructors =================================================="

  Public Sub New(ByVal configFilePath As String)
    Try

      Me.mstrClassConfigFilePath = configFilePath
      Me.mSerialPort = New System.IO.Ports.SerialPort

      'Read configuration settings from file
      GetConfigurationSettings()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      'Initialize Communication Settings
      Call InitializeCommunication()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub


#End Region

#Region "Private Member Variables =================================================="

  Private mAnomaly As clsAnomaly

  Private mstrClassConfigFilePath As String

  Private mSerialPort As System.IO.Ports.SerialPort
  Private mintSerialPortBaudRate As Integer
  Private mintSerialPortDataBits As Integer
  Private mintSerialPortHandshakeProtocol As IO.Ports.Handshake
  Private mstrSerialPortName As String
  Private mintSerialPortParity As IO.Ports.Parity
  Private mintSerialPortReadTimeout As Integer
  Private mintSerialPortStopBits As IO.Ports.StopBits
  Private mintSerialPortWriteTimeout As Integer
  Private mStackTrace As New clsStackTrace
  Private mStopwatch As New clsStopWatch

#End Region

#Region "Public Constants, Structures, Enums =================================================="

#End Region

#Region "Properties =================================================="

  Public Property Anomaly() As clsAnomaly
    Get
      Return Me.mAnomaly
    End Get
    Set(ByVal value As clsAnomaly)
      Me.mAnomaly = value
    End Set
  End Property

  Public Property SerialPort() As System.IO.Ports.SerialPort
    Get
      Return mSerialPort
    End Get
    Set(ByVal value As System.IO.Ports.SerialPort)
      mSerialPort = value
    End Set
  End Property

  Public Property SerialPortBaudRate() As Integer
    Get
      Return Me.mintSerialPortBaudRate
    End Get
    Set(ByVal value As Integer)
      Me.mintSerialPortBaudRate = value
    End Set
  End Property

  Public Property SerialPortDataBits() As Integer
    Get
      Return Me.mintSerialPortDataBits
    End Get
    Set(ByVal value As Integer)
      Me.mintSerialPortDataBits = value
    End Set
  End Property

  Public Property SerialPortHandshakeProtocol() As IO.Ports.Handshake
    Get
      Return Me.mintSerialPortHandshakeProtocol
    End Get
    Set(ByVal value As IO.Ports.Handshake)
      Me.mintSerialPortHandshakeProtocol = value
    End Set
  End Property

  Public Property SerialPortParity() As IO.Ports.Parity
    Get
      Return Me.mintSerialPortParity
    End Get
    Set(ByVal value As IO.Ports.Parity)
      Me.mintSerialPortParity = value
    End Set
  End Property

  Public Property SerialPortReadTimeout() As Integer
    Get
      Return Me.mintSerialPortReadTimeout
    End Get
    Set(ByVal value As Integer)
      Me.mintSerialPortReadTimeout = value
    End Set
  End Property

  Public Property SerialPortStopBits() As IO.Ports.StopBits
    Get
      Return Me.mintSerialPortStopBits
    End Get
    Set(ByVal value As IO.Ports.StopBits)
      Me.mintSerialPortStopBits = value
    End Set
  End Property

  Public Property SerialPortWriteTimeout() As Integer
    Get
      Return Me.mintSerialPortWriteTimeout
    End Get
    Set(ByVal value As Integer)
      Me.mintSerialPortWriteTimeout = value
    End Set
  End Property

#End Region

#Region "Public Methods =================================================="

  Public Sub MarkDateCodeSerialNumberAndRev(ByVal strFileName As String, ByVal strDateCode As String, ByVal strSerialNumber As String, ByVal strRev As String)
    Dim strRead As String
    Dim strAsc As String
    Try
      Me.mSerialPort.Write("LOADFILE " & strFileName & vbLf)
      Me.mStopwatch.DelayTime(100)
      Me.mSerialPort.Write("SETVAR DC " & strDateCode & vbLf)
      Me.mStopwatch.DelayTime(100)
      Me.mSerialPort.Write("SETVAR SN " & strSerialNumber & vbLf)
      Me.mStopwatch.DelayTime(100)
      Me.mSerialPort.Write("SETVAR REV " & strRev & vbLf)
      Me.mStopwatch.DelayTime(100)
      Me.mSerialPort.Write("RUN " & vbLf)
      Me.mStopwatch.DelayTime(6500)

      strRead = Me.mSerialPort.ReadExisting
      strAsc = Asc(Mid(strRead, strRead.Length - 1, 1))
      If strAsc = "4" Then
        'Good Mark
        '.Close()
      Else
        'Error
        Me.mSerialPort.Write("RESETERROR " & vbLf)
        '.Close()
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Pin Stamper Returned this message: " & strRead)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub


#End Region

#Region "Private Methods =================================================="

  Private Sub GetConfigurationSettings()
    Try

      Dim configReader As XmlTextReader
      Dim configSet As New DataSet
      Dim configTable As DataTable
      Dim configRow As DataRow
      Dim strClassName As String

      strClassName = Me.GetType.Name

      'Create the XML Reader
      configReader = New XmlTextReader(Me.mstrClassConfigFilePath & strClassName & ".cfg")

      'Clear data set that holds xml file contents
      configSet.Clear()
      'Read the file
      configSet.ReadXml(configReader)

      'Config File not hierarchical, only 1 table and 1 row, with many fields
      configTable = configSet.Tables(0)
      configRow = configTable.Rows(0)

      'Assign field values to variables
      Me.mstrSerialPortName = configRow("SerialPortName")
      Me.mintSerialPortBaudRate = configRow("SerialPortBaudRate")
      Me.mintSerialPortParity = DirectCast([Enum].Parse(GetType(IO.Ports.Parity), configRow("SerialPortParity")), Integer)
      Me.mintSerialPortDataBits = configRow("SerialPortDataBits")
      Me.mintSerialPortStopBits = DirectCast([Enum].Parse(GetType(IO.Ports.StopBits), configRow("SerialPortStopBits")), Integer)
      Me.mintSerialPortWriteTimeout = configRow("SerialPortWriteTimeoutMs")
      Me.mintSerialPortReadTimeout = configRow("SerialPortReadTimeoutMs")
      Me.mintSerialPortHandshakeProtocol = DirectCast([Enum].Parse(GetType(IO.Ports.Handshake), configRow("SerialPortHandshakeProtocol")), Integer)

      'Close reader, releases file
      configReader.Close()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Public Sub InitializeCommunication()
    '
    '   PURPOSE: To establish RS232 Communication with marker
    '
    '  INPUT(S): 
    '
    ' OUTPUT(S):

    Try

      'Make sure the port is closed
      If Me.SerialPort.IsOpen = True Then Me.SerialPort.Close()

      Me.SerialPort.PortName = Me.mstrSerialPortName 'Set the serial port #
      Me.SerialPort.BaudRate = Me.mintSerialPortBaudRate
      Me.SerialPort.Parity = Me.mintSerialPortParity
      Me.SerialPort.DataBits = Me.mintSerialPortDataBits
      Me.SerialPort.StopBits = Me.mintSerialPortStopBits
      Me.SerialPort.WriteTimeout = Me.mintSerialPortWriteTimeout
      Me.SerialPort.ReadTimeout = Me.mintSerialPortReadTimeout
      Me.SerialPort.Handshake = Me.mintSerialPortHandshakeProtocol 'Set flow control

      Me.SerialPort.Open()  'Open COM Port


    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub


#End Region


End Class
