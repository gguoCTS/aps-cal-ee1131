Public Class KepwareOPC

#Region "Delegates =================================================="

#End Region

#Region "Events ====================================================="
  Public Event DataChanged(ByVal intArrayPosition As Integer, ByVal strNewValue As String, ByVal strQuality As String)

  ' Server and group related data
  ' The OPCServer objects must be declared here due to the use of WithEvents
  Dim WithEvents AnOPCServer As OPCAutomation.OPCServer
  Dim WithEvents ConnectedOPCServer As OPCAutomation.OPCServer
  Dim WithEvents ConnectedGroup As OPCAutomation.OPCGroup

#End Region

#Region "Constructors ==============================================="
  Public Sub New()

  End Sub
#End Region

#Region "Private Member Variables ==================================="

#End Region

#Region "Public Constants, Structures, Enums ========================"

  ' Also note what the actual array size is.( NUMITEMS SPECIFIES Upper Bound of Array )
  ' To summarize then:
  '	1) Array size = 11
  '	2) We use indexes 1 thru 10
  '	3) Index 0 is not used at all
  Const ACTUAL_ARRAY_SIZE As Integer = NUMITEMS + 1

  Enum CanonicalDataTypes As Short
    CanonDtByte = 17
    CanonDtChar = 16
    CanonDtWord = 18
    CanonDtShort = 2
    CanonDtDWord = 19
    CanonDtLong = 3
    CanonDtFloat = 4
    CanonDtDouble = 5
    CanonDtBool = 11
    CanonDtString = 8
  End Enum

  Const NUMITEMS As Integer = 100

  ' OPC Item related data
  Dim OPCItemIDs(NUMITEMS) As String
  Dim ClientHandles(NUMITEMS) As Int32
  Dim ItemServerHandles As System.Array

  ' Arrays are used to provide iterative access to sets of controls
  Dim OPCItemName(NUMITEMS) As Object
  Dim OPCItemValue(NUMITEMS) As Object
  Dim OPCItemValueToWrite(NUMITEMS) As Object
  Dim OPCItemWriteButton(NUMITEMS) As Object
  Dim OPCItemActiveState(NUMITEMS) As Object
  Dim OPCItemSyncReadButton(NUMITEMS) As Object
  Dim OPCItemQuality(NUMITEMS) As Object
  Dim OPCItemIsArray(NUMITEMS) As Integer

#End Region

#Region "Properties ================================================="

#End Region

#Region "Public Methods ============================================="



  ' This sub handles adding the group to the OPC server and establishing the
  ' group interface.  When adding a group you can preset some of the group
  ' parameters using the properties '.DefaultGroupIsActive'
  ' and '.DefaultGroupDeadband'.  Set these before adding the group. Once the
  ' group has been successfully added you can change these same settings
  ' along with the group update rate on the fly using the properties on the
  ' resulting OPCGroup object.
  Public Function AddOPCGroup(ByRef strOPCGroupName As String, ByRef strUpdateRate As String, ByRef strErrorMess As String) As Integer
    Dim intResult As Integer

    Try
      ' Set the desire active state for the group
      intResult = 0
      ConnectedOPCServer.OPCGroups.DefaultGroupIsActive = True

      'Set the desired percent deadband
      ConnectedOPCServer.OPCGroups.DefaultGroupDeadband = 0

      ' Add the group and set its update rate
      ConnectedGroup = ConnectedOPCServer.OPCGroups.Add(strOPCGroupName)

      ' Set the update rate for the group
      ConnectedGroup.UpdateRate = 300 ' Val(strUpdateRate)

      ' ****************************************************************
      ' Mark this group to receive asynchronous updates via the DataChange event.
      ' This setting is IMPORTANT. Without setting '.IsSubcribed' to True your
      ' VB application will not receive DataChange notifications.  This will
      ' make it appear that you have not properly connected to the server.
      ConnectedGroup.IsSubscribed = True

      '*****************************************************************


    Catch ex As Exception
      ' Error handling
      strErrorMess = "OPC server add group failed with exception: " + ex.Message
      intResult = 99
    End Try

    Return intResult

  End Function

  Public Function AddItemstoGroup(ByRef strOPCGroupName As String, ByRef strArray() As String, ByRef intArrayLength As Integer, ByRef strErrorMess As String) As Integer
    Dim intResult As Integer
    Dim AddItemServerErrors As System.Array = Nothing
    Dim intLoop As Integer

    intResult = 0



    If Not ConnectedGroup Is Nothing Then
      Try
        For intLoop = 0 To intArrayLength - 1
          OPCItemIDs(intLoop) = strArray(intLoop)
          OPCItemIsArray(intLoop) = 0
          ClientHandles(intLoop) = intLoop
        Next
        ' Make the Items active start control Active, for the demo I want all items to start active
        ' Your application may need to start the items as inactive.
        ' OPCItemActiveState(intItemPosition).CheckState = 1

        ConnectedGroup.OPCItems.DefaultIsActive = True
        ConnectedGroup.OPCItems.AddItems(intArrayLength, OPCItemIDs, ClientHandles, ItemServerHandles, AddItemServerErrors)


        ' This next step checks the error return on each item we attempted to
        ' register.  If an item is in error it's associated controls will be
        ' disabled.  If all items are in error then the Add Item button will
        ' remain active.
        Dim AnItemIsGood As Boolean
        AnItemIsGood = False


      Catch ex As Exception
        intResult = 99
        strErrorMess = "OPC Add Item Fail " & ex.Message
      End Try
    End If

    Return intResult

  End Function

  Public Sub ChangeIsActive(ByRef strOPCGroupName As String, ByRef intArrayPosition As Integer, ByRef blnIsActive As Boolean, ByRef strErrorMess As String) 'As Integer
    Dim intResult As Integer

    intResult = 0

    If Not ConnectedGroup Is Nothing Then

      Try
        ' Change only 1 item
        Dim ItemCount As Short = 1

        ' Dim local arrays to pass desired item for state change
        Dim ActiveItemServerHandles(1) As Integer

        Dim ActiveItemErrors As System.Array = Nothing

        ' Get the desired state from the control.


        ' Get the Servers handle for the desired item.  The server handles
        ' were returned in add item subroutine.
        ActiveItemServerHandles(1) = ItemServerHandles(intArrayPosition)

        ' Invoke the SetActive operation on the OPC item collection interface
        ConnectedGroup.OPCItems.SetActive(ItemCount, ActiveItemServerHandles, blnIsActive, ActiveItemErrors)


      Catch ex As Exception
        ' Error handling
        strErrorMess = "OPC server set active state failed with exception: " + ex.Message
      End Try
    End If

  End Sub


  ' This sub handles disconnecting from the OPC Server.  The OPCServer Object
  ' provides the method 'Disconnect'.  Calling this on an active OPCSerer
  ' object will release the OPC Server interface with your application.  When
  ' this occurs you should see the OPC server application shut down if it started
  ' automatically on the OPC connect. This step should not occur until the group
  ' and items have been removed
  Public Function CloseOPCServer(ByVal OPCServerName As String, ByVal OPCNodeName As String, ByRef sErrorMess As String) As Integer
    ' Test to see if the OPC Server connection is currently available
    Dim intResult As Integer

    intResult = 0

    If Not ConnectedOPCServer Is Nothing Then
      Try
        'Disconnect from the server, This should only occur after the items and group
        ' have been removed
        ConnectedOPCServer.Disconnect()
      Catch ex As Exception
        ' Error handling
        sErrorMess = "OPC server disconnect failed with exception: " + ex.Message
        intResult = 99
      Finally
        ' Release the old instance of the OPC Server object and allow the resources
        ' to be freed
        ConnectedOPCServer = Nothing

      End Try

    End If

    Return intResult

  End Function

  Public Function GetValue(ByRef strOPCGroupName As String, ByRef intArrayPosition As Integer, ByRef strValue As String, ByRef strErrorMess As String) As Integer
    Dim intResult As Integer

    intResult = 0

    If Not ConnectedGroup Is Nothing Then
      Try
        ' Read only 1 item
        Dim ItemCount As Short = 1

        ' Provide storage the arrays returned by the 'SyncRead' method
        Dim SyncItemServerHandles(1) As Integer
        Dim SyncItemValues As System.Array = Nothing
        Dim SyncItemServerErrors As System.Array = Nothing

        ' Get the Servers handle for the desired item.  The server handles were
        ' returned in add item subroutine.
        SyncItemServerHandles(1) = ItemServerHandles(intArrayPosition)

        ' Invoke the SyncRead operation.  Remember this call will wait until
        ' completion. The source flag in this case, 'OPCDevice' , is set to
        ' read from device which may take some time.
        ConnectedGroup.AsyncRead(ItemCount, SyncItemServerHandles, SyncItemValues, 1, 2)
        ConnectedGroup.SyncRead(OPCAutomation.OPCDataSource.OPCDevice, ItemCount, SyncItemServerHandles, SyncItemValues, SyncItemServerErrors)

        ',OPCAutomation.OPCDataSource.OPCDevice, ItemCount, SyncItemServerHandles, SyncItemValues, SyncItemServerErrors)

        ' Save off the value returned after checking for error
        If SyncItemServerErrors(1) = 0 Then
          If IsArray(SyncItemValues(1)) Then
            Dim ItsAnArray As Array
            Dim x As Integer
            Dim Suffix As String

            ItsAnArray = SyncItemValues(1)

            strValue = ""
            For x = ItsAnArray.GetLowerBound(0) To ItsAnArray.GetUpperBound(0)
              If x = ItsAnArray.GetUpperBound(0) Then
                Suffix = ""
              Else
                Suffix = ", "
              End If
              strValue = strValue & ItsAnArray(x) & Suffix
            Next x
          Else
            strValue = SyncItemValues(1)
          End If
        Else
          strErrorMess = "SyncItemServerError: " & SyncItemServerErrors(1)
        End If

      Catch ex As Exception
        ' Error handling
        strErrorMess = "OPC server read item failed with exception: " + ex.Message
      End Try
    End If





  End Function

  Public Function OpenOPCServer(ByVal OPCServerName As String, ByVal OPCNodeName As String, ByRef sErrorMess As String) As Integer
    Dim intResult As Integer

    Try
      intResult = 0
      ConnectedOPCServer = New OPCAutomation.OPCServer

      'Attempt to connect with the server
      ConnectedOPCServer.Connect(OPCServerName, OPCNodeName)

    Catch ex As Exception
      ' Error handling
      ConnectedOPCServer = Nothing
      sErrorMess = ("OPC server connect failed with exception: " + ex.Message)
      intResult = 99
    End Try


    Return intResult


  End Function

  ' This sub handles removing a group from the OPC server, this must be done after
  ' items have been removed.  The 'Remove' method allows a group to be removed
  ' by name from the OPC Server.  If your application will maintains more than
  ' one group you will need to keep a list of the group names for use in the
  ' 'Remove' method.  In this demo there is only one group.  The name is maintained
  ' in the OPCGroupName TextBox but it can not be changed once the group is added.
  Public Function RemoveOPCGroup(ByRef strOPCGroupName As String, ByRef strErrorMess As String) As Integer
    Dim intResult As Integer
    ' Test to see if the OPC Group object is currently available
    intResult = 0
    If Not ConnectedGroup Is Nothing Then
      Try
        ' Remove the group from the server
        ConnectedOPCServer.OPCGroups.Remove(strOPCGroupName)
      Catch ex As Exception
        ' Error handling
        strErrorMess = "OPC server remove group failed with exception: " + ex.Message
        intResult = 99
      Finally
        ConnectedGroup = Nothing


      End Try
    End If

    Return intResult

  End Function

  Public Function SetValue(ByRef strOPCGroupName As String, ByRef intArrayPosition As Integer, ByRef strValue As String, ByRef strErrorMess As String) As Integer
    Dim intResult As Integer

    intResult = 0
    If Not ConnectedGroup Is Nothing Then
      ' Get control index from name

      Try
        ' Write only 1 item
        Dim ItemCount As Short = 1

        ' Create some local scope variables to hold the value to be sent.
        ' These arrays could just as easily contain all of the item we have added.
        Dim SyncItemServerHandles(1) As Integer
        Dim SyncItemValues(1) As Object
        Dim SyncItemServerErrors As System.Array = Nothing
        Dim AnOpcItem As OPCAutomation.OPCItem

        ' Get the Servers handle for the desired item.  The server handles
        ' were returned in add item subroutine.
        SyncItemServerHandles(1) = ItemServerHandles(intArrayPosition)
        AnOpcItem = ConnectedGroup.OPCItems.GetOPCItem(ItemServerHandles(intArrayPosition))

        ' Load the value to be written using Item's Canonical Data Type to
        ' convert to correct type. 
        ' See Kepware Application note on Canonical Data Types
        Dim ItsAnArray As Array
        Dim CanonDT As Short
        CanonDT = AnOpcItem.CanonicalDataType()

        ' If it is an array, figure out the base type
        If CanonDT > vbArray Then
          CanonDT -= vbArray
        End If

        Select Case CanonDT
          Case CanonicalDataTypes.CanonDtByte
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(Byte), OPCItemIsArray(intArrayPosition))
              If LoadArray(ItsAnArray, CanonDT, OPCItemValueToWrite(intArrayPosition).Text, strErrorMess) <> 0 Then

                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToByte(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtChar
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType([SByte]), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToSByte(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtWord
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(UInt16), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToUInt16(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtShort
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(Int16), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToInt16(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtDWord
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(UInt32), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToUInt32(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtLong
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(Int32), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToInt32(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtFloat
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(Single), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToSingle(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtDouble
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(Double), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              SyncItemValues(1) = Convert.ToDouble(strValue)
            End If
            ' End case

          Case CanonicalDataTypes.CanonDtBool
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(Boolean), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              'SyncItemValues(1) = Convert.ToBoolean(OPCItemValueToWrite(intArrayPosition).Text)
              '                            SyncItemValues(1) = Convert.ToBoolean(strValue)
              If strValue = "1" Then
                SyncItemValues(1) = "True"
              Else
                SyncItemValues(1) = "False"
              End If

            End If
            ' End case

          Case CanonicalDataTypes.CanonDtString
            If OPCItemIsArray(intArrayPosition) > 0 Then
              ItsAnArray = Array.CreateInstance(GetType(String), OPCItemIsArray(intArrayPosition))
              If Not LoadArray(ItsAnArray, CanonDT, strValue, strErrorMess) Then
                Return 99
              End If
              SyncItemValues(1) = CObj(ItsAnArray)
            Else
              'SyncItemValues(1) = Convert.ToString(OPCItemValueToWrite(intArrayPosition).Text)
              If strValue = "1" Then
                SyncItemValues(1) = "True"
              Else
                SyncItemValues(1) = "False"
              End If

            End If
            ' End case

          Case Else
            strErrorMess = "OPCItemWriteButton Unknown data type"
            Return 99
            ' End case
        End Select

        ' Invoke the SyncWrite operation.  Remember this call will wait until completion
        ConnectedGroup.SyncWrite(ItemCount, SyncItemServerHandles, SyncItemValues, SyncItemServerErrors)

        If SyncItemServerErrors(1) <> 0 Then
          strErrorMess = "SyncItemServerError: " & SyncItemServerErrors(1)
          intResult = 99
          Throw New Exception(strErrorMess & " in KepwareOPC SetValue")
        End If
      Catch ex As Exception


        ' Error handling
        strErrorMess = "OPC server write item failed with exception: " + ex.Message
        intResult = 99
        Return intResult 'TER6Gen
      End Try
    End If
  End Function
#End Region

#Region "Private Methods ============================================"

  ' This sub handles the 'DataChange' call back event which returns data that has
  ' been detected as changed within the OPC Server.  This call back should be
  ' used primarily to receive the data.  Do not make any other calls back into
  ' the OPC server from this call back.  The other item related functions covered
  ' in this example have shown how the ItemServerHandle is used to control and
  ' manipulate individual items in the OPC server.  The 'DataChange' event allows
  ' us to see how the 'ClientHandles we gave the OPC Server when adding items are
  ' used.  As you can see here the server returns the 'ClientHandles' as an array.
  ' The number of item returned in this event can change from trigger to trigger
  ' so don't count on always getting a 1 to 1 match with the number of items
  ' you have registered.  That where the 'ClientHandles' come into play.  Using
  ' the 'ClientHandles' returned here you can determine what data has changed and
  ' where in your application the data should go.  In this example the
  ' 'ClientHandles' were the Index number of each item we added to the group.
  ' Using this returned index number the 'DataChange' handler shown here knows
  ' what controls need to be updated with new data.  In your application you can
  ' make the client handles anything you like as long as they allow you to
  ' uniquely identify each item as it returned in this event.
  Private Sub ConnectedGroup_DataChange(ByVal TransactionID As Integer, ByVal NumItems As Integer, ByRef ClientHandles As System.Array, ByRef ItemValues As System.Array, ByRef Qualities As System.Array, ByRef TimeStamps As System.Array) Handles ConnectedGroup.DataChange
    ' We don't have error handling here since this is an event called from the OPC interface

    Try
      Dim i As Short
      For i = 1 To NumItems
        ' Use the 'Clienthandles' array returned by the server to pull out the
        ' index number of the control to update and load the value.
        If IsArray(ItemValues(i)) Then
          Dim ItsAnArray As Array
          Dim x As Integer
          Dim Suffix As String

          ItsAnArray = ItemValues(i)

          ' Store the size of array for use by sync write
          OPCItemIsArray(ClientHandles(i)) = ItsAnArray.GetUpperBound(0) + 1

          OPCItemValue(ClientHandles(i)).Text = ""
          For x = ItsAnArray.GetLowerBound(0) To ItsAnArray.GetUpperBound(0)
            If x = ItsAnArray.GetUpperBound(0) Then
              Suffix = ""
            Else
              Suffix = ", "
            End If
            OPCItemValue(ClientHandles(i)).Text = _
             OPCItemValue(ClientHandles(i)).Text & ItsAnArray(x) & Suffix
          Next x
        Else
          'OPCItemValue(ClientHandles(i)).Text = ItemValues(i)
          ' MsgBox(ClientHandles(i) & " =" & ItemValues(i))
          RaiseEvent DataChanged(ClientHandles(i), ItemValues(i), Qualities(i))
        End If

      Next i
    Catch ex As Exception
      ' Error handling
      MessageBox.Show("OPC DataChange failed with exception: " + ex.Message, "SimpleOPCInterface Exception", MessageBoxButtons.OK)
    End Try
  End Sub


  Private Function LoadArray(ByRef AnArray As System.Array, ByVal CanonDT As Short, ByRef wrTxt As String, ByRef strErrorMess As String) As Integer
    Dim ii As Integer
    Dim loc As Integer
    Dim Wlen As Integer
    Dim start As Integer

    Try
      start = 1
      Wlen = Len(wrTxt)
      For ii = AnArray.GetLowerBound(0) To AnArray.GetUpperBound(0)
        loc = InStr(start, wrTxt, ",")
        If ii < AnArray.GetUpperBound(0) Then
          If loc = 0 Then
            strErrorMess = "Write Value: Incorrect Number of Items for Array Size?"
            Return 99
          End If
        Else
          loc = Wlen + 1
        End If

        Select Case CanonDT
          Case CanonicalDataTypes.CanonDtByte
            AnArray(ii) = Convert.ToByte(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtChar
            AnArray(ii) = Convert.ToSByte(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtWord
            AnArray(ii) = Convert.ToUInt16(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtShort
            AnArray(ii) = Convert.ToInt16(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtDWord
            AnArray(ii) = Convert.ToUInt32(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtLong
            AnArray(ii) = Convert.ToInt32(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtFloat
            AnArray(ii) = Convert.ToSingle(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtDouble
            AnArray(ii) = Convert.ToDouble(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtBool
            AnArray(ii) = Convert.ToBoolean(Mid(wrTxt, start, loc - start))
            ' End case

          Case CanonicalDataTypes.CanonDtString
            AnArray(ii) = Convert.ToString(Mid(wrTxt, start, loc - start))
            ' End case

          Case Else
            strErrorMess = "Write Value Unknown data type"
            Return 99
        End Select

        start = loc + 1
      Next ii

      Return True
    Catch ex As Exception
      strErrorMess = "Write Value generated Exception: " & ex.Message
      Return 99
    End Try
    Return 0
  End Function


#End Region









End Class
