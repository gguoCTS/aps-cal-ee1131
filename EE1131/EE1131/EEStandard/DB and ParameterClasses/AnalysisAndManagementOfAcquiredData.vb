#Region "Imports"
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class AnalysisAndManagementOfAcquiredData

  'Private Const mStackTrace.CurrentClassName As String = "AnalysisAndManagementOfAcquiredData" 'TER6g

#Region "Private Member Variables"

  Private mAnomaly As clsDbAnomaly 'TER6g

  Private mstrADRStructureVersion As String

  Private mstrClassConfigFilePath As String

  Private mDatabase As Database 'TER6g
  Private mintDataPointsBeforeStart As Integer


  Private mENREnable As Boolean
  Private mENRPeakLocation As Double
  Private mEvaluationInterval As Double
  Private mEvaluationStart As Double
  Private mEvaluationStop As Double

  Private msngForceCorrectionOffset As Single
  Private msngForceCosineMultiplier As Single
  Private mForceOffset As Double '1.0.1.1DLG
  Private mForceSlopeOffset As Double '1.0.1.1DLG
  Private mForwardAveragingRangeHigh As Double
  Private mForwardAveragingRangeLow As Double

  Private msngHighEndSearchStopForce As Single

  Private mIdealIndex1Location As Double
  Private mdblIdealIndex1Output1 As Double
  Private mdblIdealIndex1Output2 As Double
  Private mdblIdealIndex1Output1Location As Double
  Private mdblIdealIndex1Output2Location As Double
  Private mIdealIndex1Value As Double
  Private mIdealIndex2Location As Double
  Private msngIdealIndex2Output1 As Single
  Private msngIdealIndex2Output2 As Double
  Private mIdealIndex2Value As Double
  Private mIdealIndex3Location As Double
  Private mIdealSlope As Double
  Private mdblIdealSlopeOutput1 As Double
  Private mdblIdealSlopeOutput2 As Double
  Private mIndex As Double
  Private mIndexLocation As Double
  Private mIndexOffset As Double '1.0.1.1DLG
  Private mInvalidData As Boolean

  Private msngLowEndSearchStopForce As Single

  Private mdblKneePositionHigh As Double

  Private mdblMeasuredOverallResOutput1 As Double
  Private mdblMeasuredOverallResOutput2 As Double
  Private mintMeasurementNumberMax As Integer
  Private mdblMeasurementTimeWindow As Double
  Private mMicrogradientOffset As Double '1.0.1.1DLG
  Private mdblMicrogradientIncrement As Double
  Private msngMLXWIddHighLimit As Single
  Private msngMLXWIddLowLimit As Single

  Private msngOutputAtEvaluationStart As Single
  Private msngOutputAtEvaluationStop As Single
  Private msngOutputCorrelationOffsetIdeal As Single
  Private msngOutputCorrelationRatioIdeal As Single
  Private mOverallResistance As Double

  Private mdblPeakForce As Double

  Private mReverseAveragingRangeHigh As Double
  Private mReverseAveragingRangeLow As Double
  Private msngRisePointStartLoc As Single
  Private msngRisePointStopLoc As Single
  Private msngRisePointStartPercentVRef As Single
  Private msngRisePointStopPercentVRef As Single
  Private msngRisePointTargetPosition As Single

  Private mSeriesResistance As Double
  Private mSinglePointOffset As Double = 0 '1.0.1.1DLG
  Private mintSlopeDevIncrement As Integer
  Private mintSlopeDevInterval As Integer
  Private mSlopeOffset As Double '1.0.1.1DLG
  Private mStackTrace As New clsStackTrace
  Private mdblStaticIndex1PercentVRef As Double
  Private mdblStaticIndex2PercentVRef As Double
  Private mdblStaticFPTOutput1PercentVRef As Double
  Private mdblStaticFPTOutput2PercentVRef As Double

  Private mintSteadyStateMeasurementAverages As Integer
  Private mdblSteadyStateThresholdHigh As Double
  Private mdblSteadyStateThresholdLow As Double

  Private mTorqueEvaluationInterval As Double
  Private mTorqueEvaluationStart As Double
  Private mTorqueEvaluationStop As Double
  Private mTorqueEnable As Boolean


#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)
    Try 'TER6g

      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow

      Me.mDatabase = db 'TER6g
      strClassName = Me.GetType.Name
      classType = GetType(AnalysisAndManagementOfAcquiredData)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)
        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class", Me.mDatabase)
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          'Select Case strPropDataType
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me.mDatabase)
    End Try
  End Sub


#End Region

#Region "Properties"
  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set
  End Property

  Public Property ADRStructureVersion() As String
    Get
      Return Me.mstrADRStructureVersion
    End Get
    Set(ByVal value As String)
      Me.mstrADRStructureVersion = value
    End Set
  End Property

  Public Property DataPointsBeforeStart() As Integer
    Get
      Return Me.mintDataPointsBeforeStart
    End Get
    Set(ByVal value As Integer)
      Me.mintDataPointsBeforeStart = value
    End Set
  End Property

  Public Property ENREnable() As Boolean
    Get
      Return mENREnable
    End Get
    Set(ByVal value As Boolean)
      mENREnable = value
    End Set
  End Property

  Public Property EvaluationInterval() As Double
    Get
      Return mEvaluationInterval
    End Get
    Set(ByVal value As Double)
      mEvaluationInterval = value
    End Set
  End Property

  Public Property EvaluationStart() As Double
    Get
      Return mEvaluationStart
    End Get
    Set(ByVal value As Double)
      mEvaluationStart = value
    End Set
  End Property

  Public Property EvaluationStop() As Double
    Get
      Return mEvaluationStop
    End Get
    Set(ByVal value As Double)
      mEvaluationStop = value
    End Set
  End Property

  Public Property ForceCorrectionOffset As Single
    Get
      Return Me.msngForceCorrectionOffset
    End Get
    Set(ByVal value As Single)
      Me.msngForceCorrectionOffset = value
    End Set
  End Property

  Public Property ForceCosineMultiplier As Single
    Get
      Return Me.msngForceCosineMultiplier
    End Get
    Set(ByVal value As Single)
      Me.msngForceCosineMultiplier = value
    End Set
  End Property

  Public Property ForceOffset() As Double
    Get
      Return mForceOffset
    End Get
    Set(ByVal value As Double)
      mForceOffset = value
    End Set
  End Property

  Public Property ForceSlopeOffset() As Double
    Get
      Return mForceSlopeOffset
    End Get
    Set(ByVal value As Double)
      mForceSlopeOffset = value
    End Set
  End Property

  Public Property ForwardAveragingRangeLow() As Double
    Get
      Return mForwardAveragingRangeLow
    End Get
    Set(ByVal value As Double)
      mForwardAveragingRangeLow = value
    End Set
  End Property

  Public Property ForwardAveragingRangeHigh() As Double
    Get
      Return mForwardAveragingRangeHigh
    End Get
    Set(ByVal value As Double)
      mForwardAveragingRangeHigh = value
    End Set
  End Property

  Public Property HighEndSearchStopForce As Single
    Get
      Return Me.msngHighEndSearchStopForce
    End Get
    Set(ByVal value As Single)
      Me.msngHighEndSearchStopForce = value
    End Set
  End Property

  Public Property IdealIndex1Location() As Double
    Get
      Return mIdealIndex1Location
    End Get
    Set(ByVal value As Double)
      mIdealIndex1Location = value
    End Set
  End Property

  Public Property IdealIndex1Output1() As Double
    Get
      Return Me.mdblIdealIndex1Output1
    End Get
    Set(ByVal value As Double)
      Me.mdblIdealIndex1Output1 = value
    End Set
  End Property

  Public Property IdealIndex1Output2() As Double
    Get
      Return Me.mdblIdealIndex1Output2
    End Get
    Set(ByVal value As Double)
      Me.mdblIdealIndex1Output2 = value
    End Set
  End Property

  Public Property IdealIndex1Output1Location() As Double
    Get
      Return Me.mdblIdealIndex1Output1Location
    End Get
    Set(ByVal value As Double)
      Me.mdblIdealIndex1Output1Location = value
    End Set
  End Property

  Public Property IdealIndex1Output2Location() As Double
    Get
      Return Me.mdblIdealIndex1Output2Location
    End Get
    Set(ByVal value As Double)
      Me.mdblIdealIndex1Output2Location = value
    End Set
  End Property

  Public Property IdealIndex1Value() As Double
    Get
      Return mIdealIndex1Value
    End Get
    Set(ByVal value As Double)
      mIdealIndex1Value = value
    End Set
  End Property

  Public Property IdealIndex2Location() As Double
    Get
      Return mIdealIndex2Location
    End Get
    Set(ByVal value As Double)
      mIdealIndex2Location = value
    End Set
  End Property

  Public Property IdealIndex2Output1 As Single
    Get
      Return Me.msngIdealIndex2Output1
    End Get
    Set(ByVal value As Single)
      Me.msngIdealIndex2Output1 = value
    End Set
  End Property

  Public Property IdealIndex2Output2 As Single
    Get
      Return Me.msngIdealIndex2Output2
    End Get
    Set(ByVal value As Single)
      Me.msngIdealIndex2Output2 = value
    End Set
  End Property

  Public Property Index3Location() As Double
    Get
      Return mIdealIndex3Location
    End Get
    Set(ByVal value As Double)
      mIdealIndex3Location = value
    End Set
  End Property

  Public Property IdealIndex2Value() As Double
    Get
      Return mIdealIndex2Value
    End Get
    Set(ByVal value As Double)
      mIdealIndex2Value = value
    End Set
  End Property

  Public Property IdealSlope() As Double
    Get
      Return mIdealSlope
    End Get
    Set(ByVal value As Double)
      mIdealSlope = value
    End Set
  End Property

  Public Property IdealSlopeOutput1 As Double
    Get
      Return Me.mdblIdealSlopeOutput1
    End Get
    Set(ByVal value As Double)
      Me.mdblIdealSlopeOutput1 = value
    End Set
  End Property

  Public Property IdealSlopeOutput2 As Double
    Get
      Return Me.mdblIdealSlopeOutput2
    End Get
    Set(ByVal value As Double)
      Me.mdblIdealSlopeOutput2 = value
    End Set
  End Property

  Public Property IndexOffset() As Double
    Get
      Return mIndexOffset
    End Get
    Set(ByVal value As Double)
      mIndexOffset = value
    End Set
  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property KneePositionHigh() As Double
    Get
      Return Me.mdblKneePositionHigh
    End Get
    Set(ByVal value As Double)
      Me.mdblKneePositionHigh = value
    End Set
  End Property

  Public Property LowEndSearchStopForce As Single
    Get
      Return Me.msngLowEndSearchStopForce
    End Get
    Set(ByVal value As Single)
      Me.msngLowEndSearchStopForce = value
    End Set
  End Property

  Public Property MeasurementNumberMax() As Integer
    Get
      Return Me.mintMeasurementNumberMax
    End Get
    Set(ByVal value As Integer)
      Me.mintMeasurementNumberMax = value
    End Set
  End Property

  Public Property MeasurementTimeWindow() As Double
    Get
      Return Me.mdblMeasurementTimeWindow
    End Get
    Set(ByVal value As Double)
      Me.mdblMeasurementTimeWindow = value
    End Set
  End Property

  Public Property MicrogradientOffset() As Double '1.0.1.1DLG 
    Get
      Return mMicrogradientOffset
    End Get
    Set(ByVal value As Double)
      mMicrogradientOffset = value
    End Set
  End Property

  Public Property MicrogradientIncrement() As Double
    Get
      Return Me.mdblMicrogradientIncrement
    End Get
    Set(ByVal value As Double)
      Me.mdblMicrogradientIncrement = value
    End Set
  End Property

  Public Property MLXWIddHighLimit As Single
    Get
      Return Me.msngMLXWIddHighLimit
    End Get
    Set(ByVal value As Single)
      Me.msngMLXWIddHighLimit = value
    End Set
  End Property

  Public Property MLXWIddLowLimit As Single
    Get
      Return Me.msngMLXWIddLowLimit
    End Get
    Set(ByVal value As Single)
      Me.msngMLXWIddLowLimit = value
    End Set
  End Property

  Public Property OutputAtEvaluationStart() As Single
    Get
      Return Me.msngOutputAtEvaluationStart
    End Get
    Set(ByVal value As Single)
      Me.msngOutputAtEvaluationStart = value
    End Set
  End Property

  Public Property OutputAtEvaluationStop() As Single
    Get
      Return Me.msngOutputAtEvaluationStop
    End Get
    Set(ByVal value As Single)
      Me.msngOutputAtEvaluationStop = value
    End Set
  End Property

  Public Property OutputCorrelationOffsetIdeal As Single
    Get
      Return Me.msngOutputCorrelationOffsetIdeal
    End Get
    Set(ByVal value As Single)
      Me.msngOutputCorrelationOffsetIdeal = value
    End Set
  End Property

  Public Property OutputCorrelationRatioIdeal() As Single
    Get
      Return msngOutputCorrelationRatioIdeal
    End Get
    Set(ByVal value As Single)
      msngOutputCorrelationRatioIdeal = value
    End Set
  End Property

  Public Property OverallResistance() As Double
    Get
      Return mOverallResistance
    End Get
    Set(ByVal value As Double)
      mOverallResistance = value
    End Set
  End Property

  Public Property MeasuredOverallResOutput1() As Double
    Get
      Return Me.mdblMeasuredOverallResOutput1
    End Get
    Set(ByVal value As Double)
      Me.mdblMeasuredOverallResOutput1 = value
    End Set
  End Property

  Public Property MeasuredOverallResOutput2() As Double
    Get
      Return mdblMeasuredOverallResOutput2
    End Get
    Set(ByVal value As Double)
      mdblMeasuredOverallResOutput2 = value
    End Set
  End Property

  Public Property PeakForce As Double
    Get
      Return Me.mdblPeakForce
    End Get
    Set(ByVal value As Double)
      Me.mdblPeakForce = value
    End Set
  End Property

  Public Property ReverseAveragingRangeHigh() As Double
    Get
      Return mReverseAveragingRangeHigh
    End Get
    Set(ByVal value As Double)
      mReverseAveragingRangeHigh = value
    End Set
  End Property

  Public Property ReverseAveragingRangeLow() As Double
    Get
      Return mReverseAveragingRangeLow
    End Get
    Set(ByVal value As Double)
      mReverseAveragingRangeLow = value
    End Set
  End Property

  Public Property RisePointStartLoc As Single
    Get
      Return Me.msngRisePointStartLoc
    End Get
    Set(ByVal value As Single)
      Me.msngRisePointStartLoc = value
    End Set
  End Property

  Public Property RisePointStopLoc As Single
    Get
      Return Me.msngRisePointStopLoc
    End Get
    Set(ByVal value As Single)
      Me.msngRisePointStopLoc = value
    End Set
  End Property

  Public Property RisePointStartPercentVRef As Single
    Get
      Return Me.msngRisePointStartPercentVRef
    End Get
    Set(ByVal value As Single)
      Me.msngRisePointStartPercentVRef = value
    End Set
  End Property

  Public Property RisePointStopPercentVRef As Single
    Get
      Return Me.msngRisePointStopPercentVRef
    End Get
    Set(ByVal value As Single)
      Me.msngRisePointStopPercentVRef = value
    End Set
  End Property

  Public Property RisePointTargetPosition As Single
    Get
      Return Me.msngRisePointTargetPosition
    End Get
    Set(ByVal value As Single)
      Me.msngRisePointTargetPosition = value
    End Set
  End Property

  Public Property SeriesRes() As Double
    Get
      Return mSeriesResistance
    End Get
    Set(ByVal value As Double)
      mSeriesResistance = value
    End Set
  End Property

  Public Property SinglePointOffset() As Double '1.0.1.1DLG 
    Get
      Return mSinglePointOffset
    End Get
    Set(ByVal value As Double)
      mSinglePointOffset = value
    End Set
  End Property

  Public Property SlopeDevIncrement() As Integer
    Get
      Return Me.mintSlopeDevIncrement
    End Get
    Set(ByVal value As Integer)
      Me.mintSlopeDevIncrement = value
    End Set
  End Property

  Public Property SlopeDevInterval() As Integer
    Get
      Return Me.mintSlopeDevInterval
    End Get
    Set(ByVal value As Integer)
      Me.mintSlopeDevInterval = value
    End Set
  End Property

  Public Property SlopeOffset() As Double
    Get
      Return mSlopeOffset
    End Get
    Set(ByVal value As Double)
      mSlopeOffset = value
    End Set
  End Property

  Public Property StaticIndex1PercentVRef As Double
    Get
      Return Me.mdblStaticIndex1PercentVRef
    End Get
    Set(ByVal value As Double)
      Me.mdblStaticIndex1PercentVRef = value
    End Set
  End Property

  Public Property StaticIndex2PercentVRef As Double
    Get
      Return Me.mdblStaticIndex2PercentVRef
    End Get
    Set(ByVal value As Double)
      Me.mdblStaticIndex2PercentVRef = value
    End Set
  End Property

  Public Property StaticFPTOutput1PercentVRef As Double
    Get
      Return Me.mdblStaticFPTOutput1PercentVRef
    End Get
    Set(ByVal value As Double)
      Me.mdblStaticFPTOutput1PercentVRef = value
    End Set
  End Property

  Public Property StaticFPTOutput2PercentVRef As Double
    Get
      Return Me.mdblStaticFPTOutput2PercentVRef
    End Get
    Set(ByVal value As Double)
      Me.mdblStaticFPTOutput2PercentVRef = value
    End Set
  End Property

  Public Property SteadyStateMeasurementAverages() As Integer
    Get
      Return Me.mintSteadyStateMeasurementAverages
    End Get
    Set(ByVal value As Integer)
      Me.mintSteadyStateMeasurementAverages = value
    End Set
  End Property

  Public Property SteadyStateThresholdHigh() As Double
    Get
      Return (Me.mdblSteadyStateThresholdHigh)
    End Get
    Set(ByVal value As Double)
      Me.mdblSteadyStateThresholdHigh = value
    End Set
  End Property

  Public Property SteadyStateThresholdLow() As Double
    Get
      Return Me.mdblSteadyStateThresholdLow
    End Get
    Set(ByVal value As Double)
      Me.mdblSteadyStateThresholdLow = value
    End Set
  End Property

  Public Property TorqueEnable() As Boolean
    Get
      Return mTorqueEnable
    End Get
    Set(ByVal value As Boolean)
      mTorqueEnable = value
    End Set
  End Property

  Public Property TorqueEvaluationInterval() As Double
    Get
      Return mTorqueEvaluationInterval
    End Get
    Set(ByVal value As Double)
      mTorqueEvaluationInterval = value
    End Set
  End Property

  Public Property TorqueEvaluationStart() As Double
    Get
      Return mTorqueEvaluationStart
    End Get
    Set(ByVal value As Double)
      mTorqueEvaluationStart = value
    End Set
  End Property

  Public Property TorqueEvaluationStop() As Double
    Get
      Return mTorqueEvaluationStop
    End Get
    Set(ByVal value As Double)
      mTorqueEvaluationStop = value
    End Set
  End Property

#End Region

#Region "Public Methods =================================================="




#End Region
End Class

