#Region "Imports =================================================="
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class StationMonitorAndAlarm

#Region "Private Member Variables"
  Private mInvalidData As Boolean
  Private mAnomaly As clsDbAnomaly 'TER6g
  Private mDatabase As Database 'TER6g
  Private mCurrentYieldCountLimit As Integer
  Private mCurrentProgTotalCount As Integer
  Private mCurrentProgPassCount As Integer
  Private mCurrentTestTotalCount As Integer
  Private mCurrentTestPassCount As Integer
  Private mConsecutiveAnomalyCount As Integer = 0
  Private mGreenYieldLimit As Double
  Private mStackTrace As New clsStackTrace 'WTG
  Private mYellowYieldLimit As Double
#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)

    Try 'TER6g
      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow

      Me.mDatabase = db 'TER6g
      strClassName = Me.GetType.Name
      classType = GetType(StationMonitorAndAlarm)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)
        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class")
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          'Select Case strPropDataType
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Properties"
  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set

  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property CurrentYieldCountLimit() As Integer
    Get
      Return mCurrentYieldCountLimit
    End Get
    Set(ByVal value As Integer)
      mCurrentYieldCountLimit = value
    End Set
  End Property

  Public Property CurrentProgTotalCount() As Integer
    Get
      Return mCurrentProgTotalCount
    End Get
    Set(ByVal value As Integer)
      mCurrentProgTotalCount = value
    End Set
  End Property

  Public Property CurrentProgPassCount() As Integer
    Get
      Return mCurrentProgPassCount
    End Get
    Set(ByVal value As Integer)
      mCurrentProgPassCount = value
    End Set
  End Property

  Public Property CurrentTestTotalCount() As Integer
    Get
      Return mCurrentTestTotalCount
    End Get
    Set(ByVal value As Integer)
      mCurrentTestTotalCount = value
    End Set
  End Property

  Public Property CurrentTestPassCount() As Integer
    Get
      Return mCurrentTestPassCount
    End Get
    Set(ByVal value As Integer)
      mCurrentTestPassCount = value
    End Set
  End Property

  Public Property ConsecutiveAnomalyCount() As Integer
    Get
      Return mConsecutiveAnomalyCount
    End Get
    Set(ByVal value As Integer)
      mConsecutiveAnomalyCount = value
    End Set
  End Property

  Public Property GreenYieldLimit() As Double
    Get
      Return mGreenYieldLimit
    End Get
    Set(ByVal value As Double)
      mGreenYieldLimit = value
    End Set
  End Property

  Public Property YellowYieldLimit() As Double
    Get
      Return mYellowYieldLimit
    End Get
    Set(ByVal value As Double)
      mYellowYieldLimit = value
    End Set
  End Property
#End Region
End Class