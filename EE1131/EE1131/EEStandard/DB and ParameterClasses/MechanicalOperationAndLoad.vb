
#Region "Imports =================================================="
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class MechanicalOperationAndLoad

#Region "Private Member Variables"
  Private mAnomaly As clsDbAnomaly

  Private mstrClassConfigFilePath As String

  Private mdblDUTCalibrationPosition As Double
  Private mDatabase As Database
  Private mintDriveArmSetupCode As Integer

  Private mintEncoderMultiplier As Integer
  Private mdblEncoderOffset As Double
  Private mEncoderResolution As Double

  Private mForceCheckSlopeIncrement As Double
  Private mForceCheckSlopeInterval As Double
  Private msngForcePoint1Location As Single
  Private msngForcePoint2Location As Single
  Private msngForcePoint3Location As Single
  Private mForwardRotationDirection As String

  Private msngHighEndForceThreshold As Single
  Private msngHighEndPosition As Single
  Private msngHighEndSearchStartPosition As Single
  Private mdblHomeAcceleration As Double
  Private mHomeBlockOffset As Double
  Private mdblHomeFinalVelocity As Double
  Private mdblHomeFinalAcceleration As Double
  Private mHomeOffsetToDUTFace As Double
  Private mdblHomeVelocity As Double

  Private mstrInitialHomingDirection As String
  Private mInvalidData As Boolean

  Private mblnKickdownEnable As Boolean
  Private msngKickdownOnLocation As Single
  Private msngKickDownSearchStartLoc As Single
  Private msngKickDownSearchStopLoc As Single
  Private msngKickDownStartTransitionPercentage As Single
  Private msngKickDownStartTransitionSlope As Single
  Private msngKickDownStartTransitionWindow As Single
    Private msngKDStartAdj As Single

  Private msngLoadLocation As Single
  Private mdblLoadPositionMoveVelocity As Double
  Private mdblLoadPositionMoveAcceleration As Double
  Private mLinearMeasurementBase As String
  Private mLinearSlidePitch As Double
  Private msngLowEndForceThreshold As Single
  Private msngLowEndPosition As Single
  Private msngLowEndSearchStartPosition As Single
  Private msngLowEndSearchStopPosition As Single

  Private msngMechanicalSearchAcceleration As Single
  Private msngMechanicalSearchVelocity As Single
  Private mdblMotorGearRatio As Double
  Private mMotorStepsPerThou As Double
  Private mMotorStepsPerMillimeter As Double
  Private msngMotorStepsPerRev As Single

  Private mOffsetToStartScanFromHomeProxInmm As Double
  Private mOverTravel As Double

  Private mstrParameterSetLinearDistanceUnits As String
  Private mdblProgrammingVelocity As Double
  Private mdblProgrammingAcceleration As Double

  Private mScanLength As Double
  Private mScanLengthInMillimeters As Double
  Private mScanLengthInThou As Double
  'Private mServoMotor As clsServoMotor
  Private mStackTrace As New clsStackTrace
  Private mStartScanLocation As Double
  Private mStartScanLocationInThou As Double

  Private mdblTestAcceleration As Double
  Private mdblTestVelocity As Double

#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)
    Try 'TER6g
      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow

      'Me.mServoMotor = servoMotor

      Me.mDatabase = db 'TER6g
      strClassName = Me.GetType.Name
      classType = GetType(MechanicalOperationAndLoad)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)

        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class")
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Properties"

  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set
  End Property

  Public Property DriveArmSetupCode As Integer
    Get
      Return Me.mintDriveArmSetupCode
    End Get
    Set(ByVal value As Integer)
      Me.mintDriveArmSetupCode = value
    End Set
  End Property

  Public Property DUTCalibrationPosition() As Double
    Get
      Return Me.mdblDUTCalibrationPosition
    End Get
    Set(ByVal value As Double)
      Me.mdblDUTCalibrationPosition = value
    End Set
  End Property

  Public Property EncoderMultiplier() As Integer
    Get
      Return Me.mintEncoderMultiplier
    End Get
    Set(ByVal value As Integer)
      Me.mintEncoderMultiplier = value
    End Set
  End Property

  Public Property EncoderOffset() As Double
    Get
      Return Me.mdblEncoderOffset
    End Get
    Set(ByVal value As Double)
      Me.mdblEncoderOffset = value
    End Set
  End Property

  Public Property EncoderResolution() As Double
    Get
      Return mEncoderResolution
    End Get
    Set(ByVal value As Double)
      mEncoderResolution = value
    End Set
  End Property


  Public Property ForceCheckSlopeIncrement() As Double
    Get
      Return mForceCheckSlopeIncrement
    End Get
    Set(ByVal value As Double)
      mForceCheckSlopeIncrement = value
    End Set
  End Property

  Public Property ForceCheckSlopeInterval() As Double
    Get
      Return mForceCheckSlopeInterval
    End Get
    Set(ByVal value As Double)
      mForceCheckSlopeInterval = value
    End Set
  End Property

  Public Property ForcePoint1Location As Single
    Get
      Return Me.msngForcePoint1Location
    End Get
    Set(ByVal value As Single)
      Me.msngForcePoint1Location = value
    End Set
  End Property

  Public Property ForcePoint2Location As Single
    Get
      Return Me.msngForcePoint2Location
    End Get
    Set(ByVal value As Single)
      Me.msngForcePoint2Location = value
    End Set
  End Property

  Public Property ForcePoint3Location As Single
    Get
      Return Me.msngForcePoint3Location
    End Get
    Set(ByVal value As Single)
      Me.msngForcePoint3Location = value
    End Set
  End Property

  Public Property ForwardRotationDirection() As String
    Get
      Return mForwardRotationDirection
    End Get
    Set(ByVal value As String)
      mForwardRotationDirection = value
    End Set
  End Property


  Public Property HighEndForceThreshold As Single
    Get
      Return Me.msngHighEndForceThreshold
    End Get
    Set(ByVal value As Single)
      Me.msngHighEndForceThreshold = value
    End Set
  End Property

  Public Property HighEndPosition As Single
    Get
      Return Me.msngHighEndPosition
    End Get
    Set(ByVal value As Single)
      Me.msngHighEndPosition = value
    End Set
  End Property

  Public Property HighEndSearchStartPosition As Single
    Get
      Return Me.msngHighEndSearchStartPosition
    End Get
    Set(ByVal value As Single)
      Me.msngHighEndSearchStartPosition = value
    End Set
  End Property

  Public Property HomeAcceleration() As Double
    Get
      Return Me.mdblHomeAcceleration
    End Get
    Set(ByVal value As Double)
      Me.mdblHomeAcceleration = value
      'Me.mServoMotor.HomeAcceleration = Me.mdblHomeAcceleration
    End Set
  End Property

  Public Property HomeBlockOffset() As Double
    Get
      Return mHomeBlockOffset
    End Get
    Set(ByVal value As Double)
      mHomeBlockOffset = value
    End Set
  End Property

  Public Property HomeVelocity() As Double
    Get
      Return Me.mdblHomeVelocity
    End Get
    Set(ByVal value As Double)
      Me.mdblHomeVelocity = value
      'Me.mServoMotor.HomeVelocity = Me.mdblHomeVelocity
    End Set
  End Property

  Public Property HomeFinalAcceleration() As Double
    Get
      Return Me.mdblHomeFinalAcceleration
    End Get
    Set(ByVal value As Double)
      Me.mdblHomeFinalAcceleration = value
      'Me.mServoMotor.HomeFinalAcceleration = Me.mdblHomeFinalAcceleration
    End Set
  End Property

  Public Property HomeFinalVelocity() As Double
    Get
      Return Me.mdblHomeFinalVelocity
    End Get
    Set(ByVal value As Double)
      Me.mdblHomeFinalVelocity = value
      'Me.mServoMotor.HomeFinalVelocity = Me.mdblHomeFinalVelocity
    End Set
  End Property

  Public Property HomeOffsetToDUTFace() As Double
    Get
      Return mHomeOffsetToDUTFace
    End Get
    Set(ByVal value As Double)
      mHomeOffsetToDUTFace = value
    End Set
  End Property


  Public Property InitialHomingDirection() As String
    Get
      Return Me.mstrInitialHomingDirection
    End Get
    Set(ByVal value As String)
      Me.mstrInitialHomingDirection = value
    End Set
  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property KickdownEnable As Boolean
    Get
      Return Me.mblnKickdownEnable
    End Get
    Set(ByVal value As Boolean)
      Me.mblnKickdownEnable = value
    End Set
  End Property

  Public Property KickDownOnLocation As Single
    Get
      Return Me.msngKickdownOnLocation
    End Get
    Set(ByVal value As Single)
      Me.msngKickdownOnLocation = value
    End Set
  End Property

  Public Property KickDownSearchStartLoc As Single
    Get
      Return Me.msngKickDownSearchStartLoc
    End Get
    Set(ByVal value As Single)
      Me.msngKickDownSearchStartLoc = value
    End Set
  End Property

  Public Property KickDownSearchStopLoc As Single
    Get
      Return Me.msngKickDownSearchStopLoc
    End Get
    Set(ByVal value As Single)
      Me.msngKickDownSearchStopLoc = value
    End Set
  End Property

  Public Property KickDownStartTransitionPercentage As Single
    Get
      Return Me.msngKickDownStartTransitionPercentage
    End Get
    Set(ByVal value As Single)
      Me.msngKickDownStartTransitionPercentage = value
    End Set
  End Property

  Public Property KickDownStartTransitionSlope As Single
    Get
      Return Me.msngKickDownStartTransitionSlope
    End Get
    Set(ByVal value As Single)
      Me.msngKickDownStartTransitionSlope = value
    End Set
  End Property

  Public Property KickDownStartTransitionWindow As Single
    Get
      Return Me.msngKickDownStartTransitionWindow
    End Get
    Set(ByVal value As Single)
      Me.msngKickDownStartTransitionWindow = value
    End Set
  End Property

    Public Property KDStartAdj As Single
        Get
            Return Me.msngKDStartAdj
        End Get
        Set(ByVal value As Single)
            Me.msngKDStartAdj = value
        End Set
    End Property

  Public Property LinearMeasurementBase() As String
    Get
      Return mLinearMeasurementBase
    End Get
    Set(ByVal value As String)
      mLinearMeasurementBase = value
    End Set
  End Property

  Public Property LinearSlidePitch() As Double
    Get
      Return mLinearSlidePitch
    End Get
    Set(ByVal value As Double)
      mLinearSlidePitch = value
    End Set
  End Property

  Public Property LoadLocation() As Single
    Get
      Return Me.msngLoadLocation
    End Get
    Set(ByVal value As Single)
      Me.msngLoadLocation = value
    End Set
  End Property

  Public Property LoadPositionMoveAcceleration() As Double
    Get
      Return Me.mdblLoadPositionMoveAcceleration
    End Get
    Set(ByVal value As Double)
      Me.mdblLoadPositionMoveAcceleration = value
      'Me.mServoMotor.LoadPositionMoveAcceleration = Me.mdblLoadPositionMoveAcceleration
    End Set
  End Property

  Public Property LoadPositionMoveVelocity() As Double
    Get
      Return Me.mdblLoadPositionMoveVelocity
    End Get
    Set(ByVal value As Double)
      Me.mdblLoadPositionMoveVelocity = value
      'Me.mServoMotor.LoadPositionMoveVelocity = Me.mdblLoadPositionMoveVelocity
    End Set
  End Property

  Public Property LowEndForceThreshold As Single
    Get
      Return Me.msngLowEndForceThreshold
    End Get
    Set(ByVal value As Single)
      Me.msngLowEndForceThreshold = value
    End Set
  End Property

  Public Property LowEndPosition As Single
    Get
      Return Me.msngLowEndPosition
    End Get
    Set(ByVal value As Single)
      Me.msngLowEndPosition = value
    End Set
  End Property

  Public Property LowEndSearchStartPosition As Single
    Get
      Return Me.msngLowEndSearchStartPosition
    End Get
    Set(ByVal value As Single)
      Me.msngLowEndSearchStartPosition = value
    End Set
  End Property

  Public Property LowEndSearchStopPosition As Single
    Get
      Return Me.msngLowEndSearchStopPosition
    End Get
    Set(ByVal value As Single)
      Me.msngLowEndSearchStopPosition = value
    End Set
  End Property


  Public Property MechanicalSearchAcceleration As Single
    Get
      Return Me.msngMechanicalSearchAcceleration
    End Get
    Set(ByVal value As Single)
      Me.msngMechanicalSearchAcceleration = value
    End Set
  End Property

  Public Property MechanicalSearchVelocity As Single
    Get
      Return Me.msngMechanicalSearchVelocity
    End Get
    Set(ByVal value As Single)
      Me.msngMechanicalSearchVelocity = value
    End Set
  End Property

  Public Property MotorGearRatio() As Double
    Get
      Return mdblMotorGearRatio
    End Get
    Set(ByVal value As Double)
      mdblMotorGearRatio = value
      'Me.mServoMotor.GearRatio = Me.mdblMotorGearRatio
    End Set
  End Property

  Public Property MotorStepsPerMillimeter() As Double
    Get
      Return mMotorStepsPerMillimeter
    End Get
    Set(ByVal value As Double)
      mMotorStepsPerMillimeter = value
    End Set
  End Property

  Public Property MotorStepsPerRev() As Single
    Get
      Return Me.msngMotorStepsPerRev
    End Get
    Set(ByVal value As Single)
      Me.msngMotorStepsPerRev = value
      'Me.mServoMotor.StepsPerRev = Me.msngMotorStepsPerRev
    End Set
  End Property

  Public Property MotorStepsPerThou() As Double
    Get
      Return mMotorStepsPerThou
    End Get
    Set(ByVal value As Double)
      mMotorStepsPerThou = value
    End Set
  End Property


  Public Property OffsetToStartScanFromHomeProxInmm() As Double
    Get
      Return mOffsetToStartScanFromHomeProxInmm
    End Get
    Set(ByVal value As Double)
      mOffsetToStartScanFromHomeProxInmm = value
    End Set
  End Property

  Public Property OverTravel() As Double
    Get
      Return mOverTravel
    End Get
    Set(ByVal value As Double)
      mOverTravel = value
    End Set
  End Property


  Public Property ParameterSetLinearDistanceUnits()
    Get
      Return Me.mstrParameterSetLinearDistanceUnits
    End Get
    Set(ByVal value)
      Me.mstrParameterSetLinearDistanceUnits = value
    End Set
  End Property

  Public Property ProgrammingAcceleration() As Double
    Get
      Return Me.mdblProgrammingAcceleration
    End Get
    Set(ByVal value As Double)
      Me.mdblProgrammingAcceleration = value
    End Set
  End Property

  Public Property ProgrammingVelocity() As Double
    Get
      Return Me.mdblProgrammingVelocity
    End Get
    Set(ByVal value As Double)
      Me.mdblProgrammingVelocity = value
    End Set
  End Property


  Public Property ScanLength() As Double
    Get
      Return mScanLength
    End Get
    Set(ByVal value As Double)
      mScanLength = value
    End Set
  End Property

  Public Property ScanLengthInMillimeters() As Double
    Get
      Return mScanLengthInMillimeters
    End Get
    Set(ByVal value As Double)
      mScanLengthInMillimeters = value
    End Set
  End Property

  Public Property ScanLengthInThou() As Double
    Get
      Return mScanLengthInThou
    End Get
    Set(ByVal value As Double)
      mScanLengthInThou = value
    End Set
  End Property

  Public Property StartScanLocation() As Double
    Get
      Return mStartScanLocation
    End Get
    Set(ByVal value As Double)
      mStartScanLocation = value
    End Set
  End Property

  Public Property StartScanLocationInThou() As Double
    Get
      Return mStartScanLocationInThou
    End Get
    Set(ByVal value As Double)
      mStartScanLocationInThou = value
    End Set
  End Property


  Public Property TestAcceleration() As Double
    Get
      Return mdblTestAcceleration
    End Get
    Set(ByVal value As Double)
      mdblTestAcceleration = value
      'Me.mServoMotor.TestAcceleration = Me.mdblTestAcceleration
    End Set
  End Property

  Public Property TestVelocity() As Double
    Get
      Return mdblTestVelocity
    End Get
    Set(ByVal value As Double)
      mdblTestVelocity = value
      'Me.mServoMotor.TestVelocity = Me.mdblTestVelocity
    End Set
  End Property


#End Region
End Class
