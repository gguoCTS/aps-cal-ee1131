
#Region "Imports"
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class ControlandInterface
  'Private Const mStackTrace.CurrentClassName As String = "ControlandInterface" 'TER6g

#Region "Private Member Variables"

  Private mAnomaly As clsDbAnomaly 'TER6g
  Private mstrAPSCALExcelFilename As String
  Private mintAutoRetestTaskStep As Integer

  Private mstrBurnInTimeSheet As String
  Private mstrBurnInTempSheet As String

  Private mstrClassConfigFilePath As String 'WTG
  Private mstrCustomerDrawingRev As String

  Private mDatabase As Database 'TER6g
  Private mstrDeviceMarkingFileName As String

  Private mstrExcelOverviewSheet As String
  Private mstrExcelOverviewCheck As String
  Private mstrExcelAPSCALVerifySheet As String

  Private mintFilter1Location As Integer
  Private mintFilter2Location As Integer
  Private mintFilter3Location As Integer
  Private mintFilter4Location As Integer
  Private mblnForceScanEnabled As Boolean

  Private mstrGraphGroup As String

  Private mblnHasMotionControl As Boolean

  Private mstrInitializationMethodName As String
  Private mstrInitializationModuleName As String
  Private mInvalidData As Boolean
  Private mblnIsLinearPart As Boolean

  Private mLinearPart As String = "False"

  Private mstrMenuGroup As String

  Private mintNumberOfAutoRetests As Integer
  Private mintNumberOfFailureAutoReTests As Integer
  Private mintNumberOfPassedAutoReTests As Integer
  Private mintNumberOfScans As Integer

  Private mstrOnEntryReadyForStationValue As String

  Private mRackCapactity As Integer

  Private mStackTrace As New clsStackTrace
  Private mseriesID As String
  Private mstrSensingTechnology As String

  Private mstrTestPassedStationValue As String
  Private mstrTestType As String
  Private mstrTestStartedStationValue As String

  Private mUsesAdditionalIO As String
  Private mblnUsesADRDatabase As Boolean
  Private mblnUsesBurnIn As Boolean
  Private mblnUsesDcMotorControl As Boolean
  Private mblnUsesMotionControl As Boolean
  Private mUsesOptimizedMetricMethods As String
  Private mblnUsesOperatorTouchStart As Boolean
  Private mblnUsesOverviewScreen As Boolean
  Private mblnUsesProgramming As Boolean


#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)

    Try 'TER6g
      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow

      'Set Defaults
      Me.mblnHasMotionControl = True
      Me.mblnUsesMotionControl = True
      Me.mintNumberOfScans = 1

      'Read values from database
      Me.mDatabase = db 'TER6g
      strClassName = Me.GetType.Name
      classType = GetType(ControlandInterface)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)

        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class")
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Properties"
  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set
  End Property

  Public Property APSCALExcelFilename As String
    Get
      Return mstrAPSCALExcelFilename
    End Get
    Set(value As String)
      mstrAPSCALExcelFilename = value
    End Set
  End Property

  Public Property BurnInTempSheet As String
    Get
      Return mstrBurnInTempSheet
    End Get
    Set(value As String)
      mstrBurnInTempSheet = value
    End Set
  End Property

  Public Property BurnInTimeSheet As String
    Get
      Return mstrBurnInTimeSheet
    End Get
    Set(value As String)
      mstrBurnInTimeSheet = value
    End Set
  End Property

  Public Property CustomerDrawingRev As String
    Get
      Return Me.mstrCustomerDrawingRev
    End Get
    Set(ByVal value As String)
      Me.mstrCustomerDrawingRev = value
    End Set
  End Property

  Public Property ExcelOverviewSheet As String
    Get
      Return mstrExcelOverviewSheet
    End Get
    Set(value As String)
      mstrExcelOverviewSheet = value
    End Set
  End Property

  Public Property ExcelOverviewCheck As String
    Get
      Return mstrExcelOverviewCheck
    End Get
    Set(value As String)
      mstrExcelOverviewCheck = value
    End Set
  End Property

  Public Property ExcelAPSCALVerifySheet As String
    Get
      Return mstrExcelAPSCALVerifySheet
    End Get
    Set(value As String)
      mstrExcelAPSCALVerifySheet = value
    End Set
  End Property


  Public Property NumberOfFailureAutoReTests() As Integer
    Get
      Return Me.mintNumberOfFailureAutoReTests
    End Get
    Set(ByVal value As Integer)
      Me.mintNumberOfFailureAutoReTests = value
    End Set
  End Property

  Public Property NumberOfPassedAutoReTests() As Integer
    Get
      Return Me.mintNumberOfPassedAutoReTests
    End Get
    Set(ByVal value As Integer)
      Me.mintNumberOfPassedAutoReTests = value
    End Set
  End Property


  Public Property AutoRetestTaskStep() As Integer
    Get
      Return Me.mintAutoRetestTaskStep
    End Get
    Set(ByVal value As Integer)
      Me.mintAutoRetestTaskStep = value
    End Set
  End Property

  Public Property DeviceMarkingFileName As String
    Get
      Return Me.mstrDeviceMarkingFileName
    End Get
    Set(ByVal value As String)
      Me.mstrDeviceMarkingFileName = value
    End Set
  End Property

  Public Property ForceScanEnabled() As Boolean
    Get
      Return Me.mblnForceScanEnabled
    End Get
    Set(ByVal value As Boolean)
      Me.mblnForceScanEnabled = value
    End Set
  End Property

  Public Property GraphGroup() As String
    Get
      Return Me.mstrGraphGroup
    End Get
    Set(ByVal value As String)
      Me.mstrGraphGroup = value
    End Set
  End Property

  Public Property HasMotionControl() As Boolean
    Get
      Return Me.mblnHasMotionControl
    End Get
    Set(ByVal value As Boolean)
      Me.mblnHasMotionControl = value
    End Set
  End Property

  Public Property IsLinearPart() As Boolean
    Get
      Return Me.mblnIsLinearPart
    End Get
    Set(ByVal value As Boolean)
      Me.mblnIsLinearPart = value
    End Set
  End Property

  Public Property InitializationMethodName() As String
    Get
      Return Me.mstrInitializationMethodName
    End Get
    Set(ByVal value As String)
      Me.mstrInitializationMethodName = value
    End Set
  End Property

  Public Property InitializationModuleName() As String
    Get
      Return Me.mstrInitializationModuleName
    End Get
    Set(ByVal value As String)
      Me.mstrInitializationModuleName = value
    End Set
  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property LinearPart() As String
    Get
      Return mLinearPart
    End Get
    Set(ByVal value As String)
      mLinearPart = value
      Me.mblnIsLinearPart = Convert.ToBoolean(mLinearPart)
    End Set
  End Property

  Public Property MenuGroup As String
    Get
      Return Me.mstrMenuGroup
    End Get
    Set(ByVal value As String)
      Me.mstrMenuGroup = value
    End Set
  End Property

  Public Property NumberOfAutoRetests() As Integer
    Get
      Return Me.mintNumberOfAutoRetests
    End Get
    Set(ByVal value As Integer)
      Me.mintNumberOfAutoRetests = value
    End Set
  End Property

  Public Property NumberOfScans() As Integer
    Get
      Return Me.mintNumberOfScans
    End Get
    Set(ByVal value As Integer)
      Me.mintNumberOfScans = value
    End Set
  End Property

  Public Property OnEntryReadyForStationValue As String
    Get
      Return mstrOnEntryReadyForStationValue
    End Get
    Set(value As String)
      mstrOnEntryReadyForStationValue = value
    End Set
  End Property

  Public Property RackCapacity() As Integer
    Get
      Return Me.mRackCapactity
    End Get
    Set(ByVal value As Integer)
      Me.mRackCapactity = value
    End Set
  End Property


  Public Property SensingTechnology() As String
    Get
      Return Me.mstrSensingTechnology
    End Get
    Set(ByVal value As String)
      Me.mstrSensingTechnology = value
    End Set
  End Property

  Public Property SeriesID() As String
    Get
      Return mseriesID
    End Get
    Set(ByVal value As String)
      mseriesID = value
    End Set
  End Property

  Public Property TestPassedStationValue As String
    Get
      Return mstrTestPassedStationValue
    End Get
    Set(value As String)
      mstrTestPassedStationValue = value
    End Set
  End Property

  Public Property TestType() As String
    Get
      Return Me.mstrTestType
    End Get
    Set(ByVal value As String)
      Me.mstrTestType = value
    End Set
  End Property

  Public Property TestStartedStationValue As String
    Get
      Return mstrTestStartedStationValue
    End Get
    Set(value As String)
      mstrTestStartedStationValue = value
    End Set
  End Property

  Public Property UsesAdditionalIO() As String
    Get
      Return mUsesAdditionalIO
    End Get
    Set(ByVal value As String)
      mUsesAdditionalIO = value
    End Set
  End Property

  Public Property UsesADRDatabase() As Boolean
    Get
      Return Me.mblnUsesADRDatabase
    End Get
    Set(ByVal value As Boolean)
      Me.mblnUsesADRDatabase = value
    End Set
  End Property

  Public Property UsesBurnIn As Boolean
    Get
      Return mblnUsesBurnIn
    End Get
    Set(value As Boolean)
      mblnUsesBurnIn = value
    End Set
  End Property

  Public Property UsesDCMotorControl() As Boolean
    Get
      Return Me.mblnUsesDcMotorControl
    End Get
    Set(ByVal value As Boolean)
      Me.mblnUsesDcMotorControl = value
    End Set
  End Property

  Public Property UsesMotionControl() As Boolean
    Get
      Return Me.mblnUsesMotionControl
    End Get
    Set(ByVal value As Boolean)
      Me.mblnUsesMotionControl = value
    End Set
  End Property

  Public Property UsesOperatorTouchStart() As Boolean
    Get
      Return Me.mblnUsesOperatorTouchStart
    End Get
    Set(ByVal value As Boolean)
      Me.mblnUsesOperatorTouchStart = value
    End Set
  End Property

  Public Property UsesOptimizedMetricMethods() As String
    Get
      Return mUsesOptimizedMetricMethods
    End Get
    Set(ByVal value As String)
      mUsesOptimizedMetricMethods = value
    End Set
  End Property

  Public Property UsesOverviewScreen As Boolean
    Get
      Return Me.mblnUsesOverviewScreen
    End Get
    Set(ByVal value As Boolean)
      Me.mblnUsesOverviewScreen = value
    End Set
  End Property

  Public Property UsesProgramming() As Boolean
    Get
      Return Me.mblnUsesProgramming
    End Get
    Set(ByVal value As Boolean)
      Me.mblnUsesProgramming = value
    End Set
  End Property

#End Region
End Class
