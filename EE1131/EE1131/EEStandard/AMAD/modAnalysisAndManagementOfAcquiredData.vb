Imports System.Reflection
Imports System.IO

Module modAnalysisAndManagementOfAcquiredData

#Region "Public Global Variables =================================================="
  'Public AMAD As AnalysisAndManagementOfAcquiredData
  Public gdtAlphaTestResultsTable As New DataTable ' 2.0.1.0 TER
  Public gAmad As AnalysisAndManagementOfAcquiredData
  Public gintAdrArraySize As Integer
  Public gAdrChannels As New List(Of AdrChannel)
  Public gAdrArray() As Int32
  Public gDataArrays As DataArrays
  Public gDataGrids As New Dictionary(Of String, DataGridView)
  Public gdblForceAmpGain As Double
  Public gintForceAmpGainUnits As ForceAmpGainUnitsEnum
  Public gdblForceAmpOffset As Double
  Public gPedalReturnProperties As PedalReturnProperties()
  Public gdtMetricProperties As New DataTable
  Public gMetrics As New Dictionary(Of Guid, Metric)
  Public gdblMotorPerParamSetUnitConversion As Double
  Public gProgLot As New Lot
  Public gdtProgResultsTable As New DataTable
  Public gdtTestFailuresTable As New DataTable
  Public gdtTestResultsTable As New DataTable
  Public gTestLot As New Lot
  Public gPreviousTestLot As Lot

  'Public gdblXArrayOffsetServo As Double
  'Public gdblXArrayOffsetPara As Double
  Public gdblPositionResolution As Double

#End Region

#Region "Public Constants, Structures, Enums =================================================="

  Public Structure AdrChannelMeasurement
    Public MeasurementName As String
    Public DataArrayName As String
  End Structure

  Public Structure AdrChannel
    Public BitsPerChannel As Integer
    Public ChannelName As String
    Public Range As Double
    Public Measurements As List(Of AdrChannelMeasurement)
  End Structure

  Public Enum ForceAmpGainUnitsEnum
    lbfPerVolt = 0
    NewtonPerVolt = 1
  End Enum

  Public Enum MPCTypeEnum
    Ideal = 0
    High = 1
    Low = 2
  End Enum

  Public Structure PedalReturnProperties
    Public ArrayOffset As Integer
    Public ArrayLength As Integer
    Public StartTime As Double
    Public EndTime As Double
    Public ReturnTime As Double
  End Structure

#Region "Bit Constants"
  Public Const BIT0 = &H1                 'Bit mask (xxxx xxxx xxxx xxxx xxxx xxx1)
  Public Const BIT1 = &H2                 'Bit mask (xxxx xxxx xxxx xxxx xxxx xx1x)
  Public Const BIT2 = &H4                 'Bit mask (xxxx xxxx xxxx xxxx xxxx x1xx)
  Public Const BIT3 = &H8                 'Bit mask (xxxx xxxx xxxx xxxx xxxx 1xxx)
  Public Const BIT4 = &H10                'Bit mask (xxxx xxxx xxxx xxxx xxx1 xxxx)
  Public Const BIT5 = &H20                'Bit mask (xxxx xxxx xxxx xxxx xx1x xxxx)
  Public Const BIT6 = &H40                'Bit mask (xxxx xxxx xxxx xxxx x1xx xxxx)
  Public Const BIT7 = &H80                'Bit mask (xxxx xxxx xxxx xxxx 1xxx xxxx)
  Public Const BIT8 = &H100               'Bit mask (xxxx xxxx xxxx xxx1 xxxx xxxx)
  Public Const BIT9 = &H200               'Bit mask (xxxx xxxx xxxx xx1x xxxx xxxx)
  Public Const BIT10 = &H400              'Bit mask (xxxx xxxx xxxx x1xx xxxx xxxx)
  Public Const BIT11 = &H800              'Bit mask (xxxx xxxx xxxx 1xxx xxxx xxxx)
  Public Const BIT12 = &H1000             'Bit mask (xxxx xxxx xxx1 xxxx xxxx xxxx)
  Public Const BIT13 = &H2000             'Bit mask (xxxx xxxx xx1x xxxx xxxx xxxx)
  Public Const BIT14 = &H4000             'Bit mask (xxxx xxxx x1xx xxxx xxxx xxxx)
  Public Const BIT15 = &H8000             'Bit mask (xxxx xxxx 1xxx xxxx xxxx xxxx)
  Public Const BIT16 = &H10000            'Bit mask (xxxx xxx1 xxxx xxxx xxxx xxxx)
  Public Const BIT17 = &H20000            'Bit mask (xxxx xx1x xxxx xxxx xxxx xxxx)
  Public Const BIT18 = &H40000            'Bit mask (xxxx x1xx xxxx xxxx xxxx xxxx)
  Public Const BIT19 = &H80000            'Bit mask (xxxx 1xxx xxxx xxxx xxxx xxxx)
  Public Const BIT20 = &H100000           'Bit mask (xxx1 xxxx xxxx xxxx xxxx xxxx)
  Public Const BIT21 = &H200000           'Bit mask (xx1x xxxx xxxx xxxx xxxx xxxx)
  Public Const BIT22 = &H400000           'Bit mask (x1xx xxxx xxxx xxxx xxxx xxxx)
  Public Const BIT23 = &H800000           'Bit mask (1xxx xxxx xxxx xxxx xxxx xxxx)
#End Region

#End Region


#Region "Public Functions =================================================="


  Public Function BuildMethodArgs(ByVal tsopMetric As Metric) As Object()
    Dim argArray() As Object
    Dim myType As Type
    Dim myProperty As PropertyInfo
    Dim sClassName As String
    Dim sPropertyName As String
    Dim sArgType As String
    Dim sArgDataType As String
    Dim i As Int16
    Dim dt As DataTable = tsopMetric.MethodArgsDataTable
    Dim row As DataRow
    Try

      ReDim argArray(dt.Rows.Count - 1)
      i = 0
      For Each row In dt.Rows
        sArgType = row("ArgType")
        Select Case sArgType
          Case "Value"
            sArgDataType = row("ArgDataType")
            Select Case sArgDataType
              Case "Boolean"
                argArray(i) = CType(row("ArgValue"), Boolean)
              Case "Byte"
                argArray(i) = CType(row("ArgValue"), Byte)
              Case "Char"
                argArray(i) = CType(row("ArgValue"), Char)
              Case "Double"
                argArray(i) = CType(row("ArgValue"), Double)
              Case "Int16"
                argArray(i) = CType(row("ArgValue"), Int16)
              Case "Int32"
                argArray(i) = CType(row("ArgValue"), Int32)
              Case "Int64"
                argArray(i) = CType(row("ArgValue"), Int64)
              Case "SByte"
                argArray(i) = CType(row("ArgValue"), SByte)
              Case "Single"
                argArray(i) = CType(row("ArgValue"), Single)
              Case "String"
                argArray(i) = CType(row("ArgValue"), String)
              Case "UInt16"
                argArray(i) = CType(row("ArgValue"), UInt16)
              Case "UInt32"
                argArray(i) = CType(row("ArgValue"), UInt32)
              Case "UInt64"
                argArray(i) = CType(row("ArgValue"), UInt64)
            End Select
          Case "Parameter"
            sClassName = row("ParamClassName")
            sPropertyName = row("ParamArgValue")
            Select Case sClassName
              Case "ControlAndInterface"
                myType = GetType(ControlandInterface)
                myProperty = myType.GetProperty(sPropertyName)
                argArray(i) = myProperty.GetValue(gControlAndInterface, Nothing)
              Case "DataArrays"
                myType = GetType(DataArrays)
                myProperty = myType.GetProperty(sPropertyName)
                argArray(i) = myProperty.GetValue(gDataArrays, Nothing)
              Case "MechanicalOperationAndLoad"
                myType = GetType(MechanicalOperationAndLoad)
                myProperty = myType.GetProperty(sPropertyName)
                argArray(i) = myProperty.GetValue(gMechanicalOperationAndLoad, Nothing)
              Case "AnalysisAndManagementOfAcquiredData"
                myType = GetType(AnalysisAndManagementOfAcquiredData)
                myProperty = myType.GetProperty(sPropertyName)
                argArray(i) = myProperty.GetValue(gAmad, Nothing)
                'Case "ProductProgramming"
                '  myType = GetType(ProductProgramming)
                '  myProperty = myType.GetProperty(sPropertyName)
                '  argArray(i) = myProperty.GetValue(gProductProgramming, Nothing)
            End Select
          Case "Metric"
            argArray(i) = New Guid(row("MetricArgValue").ToString)
        End Select
        i += 1
      Next

      Return argArray

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Sub CalcAddOffsetToArray(ByVal DataArray() As Double, ByVal Offset As Double)
    Dim i As Integer
    Try
      For i = 0 To DataArray.Length - 1
        DataArray(i) = DataArray(i) + Offset
      Next
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresis(ByVal ScaledForwardDataArray() As Double, ByVal ScaledReverseDataArray() As Double, ByRef calcDataArray() As Double)
    Dim i As Integer
    Try
      ReDim calcDataArray(ScaledForwardDataArray.Length - 1)
      For i = 0 To ScaledForwardDataArray.Length - 1
        calcDataArray(i) = ScaledReverseDataArray(i) - ScaledForwardDataArray(i)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub CalcIndependentLinearity(ByVal rawDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByVal resolution As Double, ByVal RunSkewMethod As Boolean, ByRef calcDataArray() As Double)

  '  '
  '  '   PURPOSE:    To calculate the independent linearity deviation.  The independent
  '  '               linearity is based on the Least Squares Approximation (BEST FIT LINE)
  '  '               for slope and either the the Least Squares Approximation for y-intercept
  '  '               or the Skew Method for y-intercept.
  '  '
  '  '     NOTES:    For the linear portion of the curve, the ideal value follows
  '  '               the equation of a line:  y(x) = m * (x - n) + b, where:
  '  '
  '  '               y(x) =  ideal value @ point x
  '  '                 m  =  ideal slope
  '  '                 x  =  location of ideal value point
  '  '                 n  =  location of index point
  '  '                 b  =  output at index point
  '  '
  '  '               The linearity checks are typically performed on only the forward data.
  '  '
  '  '  INPUT(S):    rawDataArray  : Raw data array for the current channel
  '  '               evaluateStart : Start point of data for current channel
  '  '               evaluateStop  : End point of data for current channel
  '  '               resolution    : Scan resolution
  '  '               RunSkewMethod : TRUE  = Select y-intercept via skew method
  '  '                             : FALSE = Select y-intercept via least squares method
  '  '
  '  ' OUTPUT(S):    calcDataArray : Output array of absolute linearity deviation
  '  '                               values.
  '  '

  '  Dim i As Integer                        'loop counter
  '  Dim n As Integer                        'count of sums
  '  Dim yFor As Single                      'temp variable used in skew calculation
  '  Dim lsngSigmaX As Single                'sum of X's
  '  Dim lsngSigmaY As Single                'sum of Y's
  '  Dim lsngSigmaX2 As Single               'sum of X^2's
  '  Dim lsngSigmaXY As Single               'sum of Y^2's
  '  Dim lsngXBar As Single                  'average of X's
  '  Dim lsngYBar As Single                  'average of Y's
  '  Dim lsngNumerator As Single             'numerator for Least Squares slope calculation
  '  Dim lsngDenominator As Single           'denominator for Least Squares slope calculation
  '  Dim lsngSkewLinMax As Single            'maximum deviation for skew calculation
  '  Dim lsngSkewLinMin As Single            'minimum deviation for skew calculation
  '  Dim lsng_b As Single                    'y-intercept for Independent Linearity
  '  Dim lsng_m As Single                    'slope for Independent Linearity
  '  Dim lsngIdealVoltage As Single          'ideal voltage gradient
  '  Dim lsngVoltArray() As Single           'actual voltage gradient

  '  Try
  '    ''*** Initialize variables for arrays ***
  '    'evaluateStart = evaluateStart * resolution
  '    'evaluateStop = evaluateStop * resolution

  '    'Set array size after variable initialization
  '    ReDim lsngVoltArray(evaluateStop - evaluateStart)
  '    ReDim calcDataArray(evaluateStop - evaluateStart)

  '    'Calculate summations required for Least Squares Approximation of Best Fit Line
  '    For i = 0 To evaluateStop - evaluateStart
  '      lsngVoltArray(i) = rawDataArray(i)
  '      lsngSigmaX = lsngSigmaX + i
  '      lsngSigmaX2 = lsngSigmaX2 + (i ^ 2)
  '      lsngSigmaY = lsngSigmaY + lsngVoltArray(i)
  '      lsngSigmaXY = lsngSigmaXY + (i * lsngVoltArray(i))
  '      n = n + 1
  '    Next

  '    ' LEAST SQUARES APPROXIMATION
  '    '
  '    ' Given the equation of a line is:   y(x) = mx + b
  '    '
  '    ' Calculate m & b as follows:
  '    '
  '    '                            SigmaXY - n * xBar * yBar
  '    '                       m = ___________________________
  '    '
  '    '                             SigmaX^2 - n * xBar^2
  '    '
  '    '
  '    '                       b =  yBar - m * xBar
  '    '

  '    'Calculate averages of X & Y
  '    lsngXBar = lsngSigmaX / n
  '    lsngYBar = lsngSigmaY / n

  '    'Calculate LEAST SQUARES APPROXIMATION SLOPE:
  '    lsngNumerator = lsngSigmaXY - n * lsngXBar * lsngYBar
  '    lsngDenominator = lsngSigmaX2 - n * lsngXBar ^ 2
  '    lsng_m = lsngNumerator / lsngDenominator

  '    'Check if skew method is used for y-intercept value:
  '    If RunSkewMethod Then

  '      'Initialize peak values
  '      lsngSkewLinMax = -100
  '      lsngSkewLinMin = 100

  '      'Calculate max & min deviation values for skew
  '      For i = 0 To evaluateStop - evaluateStart
  '        yFor = lsngVoltArray(i) - (lsng_m * (i))      'Note: Y-intercept (b) assumed to be 0

  '        If yFor > lsngSkewLinMax Then
  '          lsngSkewLinMax = yFor
  '        ElseIf yFor < lsngSkewLinMin Then
  '          lsngSkewLinMin = yFor
  '        End If
  '      Next

  '      'Calculate SKEW METHOD Y-INTERCEPT:
  '      lsng_b = (lsngSkewLinMax + lsngSkewLinMin) / 2

  '    Else
  '      'Calculate LEAST SQUARES APPROXIMATION Y-INTERCEPT
  '      lsng_b = lsngYBar - (lsng_m * lsngXBar)
  '    End If

  '    'Now calculate the BEST-FIT linearity deviation, using the slope and
  '    'y-intercept determined above:
  '    For i = 0 To evaluateStop - evaluateStart
  '      lsngIdealVoltage = (lsng_m * i) + lsng_b
  '      calcDataArray(i) = lsngVoltArray(i) - lsngIdealVoltage
  '    Next

  '    'Erase lsngVoltArray    'free up the memory since we're done with it

  '    ''Convert value(s) back to degrees
  '    'evaluateStart = evaluateStart / resolution
  '    'evaluateStop = evaluateStop / resolution

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcIndependentLinearity(ByVal rawDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByVal leastSquaresRegionStart As Double, ByVal leastSquaresRegionStop As Double, ByVal resolution As Double, ByVal RunSkewMethod As Boolean, ByRef calcDataArray() As Double)
    ' 3.0.0.0 TER
    '
    '   PURPOSE:    To calculate the independent linearity deviation.  The independent
    '               linearity is based on the Least Squares Approximation (BEST FIT LINE)
    '               for slope and either the the Least Squares Approximation for y-intercept
    '               or the Skew Method for y-intercept.
    '
    '     NOTES:    For the linear portion of the curve, the ideal value follows
    '               the equation of a line:  y(x) = m * (x - n) + b, where:
    '
    '               y(x) =  ideal value @ point x
    '                 m  =  ideal slope
    '                 x  =  location of ideal value point
    '                 n  =  location of index point
    '                 b  =  output at index point
    '
    '               The linearity checks are typically performed on only the forward data.
    '
    '  INPUT(S):    rawDataArray  : Raw data array for the current channel
    '               evaluateStart : Start point of data for current channel
    '               evaluateStop  : End point of data for current channel
    '               resolution    : Scan resolution
    '               RunSkewMethod : TRUE  = Select y-intercept via skew method
    '                             : FALSE = Select y-intercept via least squares method
    '
    ' OUTPUT(S):    calcDataArray : Output array of absolute linearity deviation
    '                               values.
    '

    Dim i As Integer                        'loop counter
    Dim n As Integer                        'count of sums
    Dim yFor As Single                      'temp variable used in skew calculation
    Dim lsngSigmaX As Single                'sum of X's
    Dim lsngSigmaY As Single                'sum of Y's
    Dim lsngSigmaX2 As Single               'sum of X^2's
    Dim lsngSigmaXY As Single               'sum of Y^2's
    Dim lsngXBar As Single                  'average of X's
    Dim lsngYBar As Single                  'average of Y's
    Dim lsngNumerator As Single             'numerator for Least Squares slope calculation
    Dim lsngDenominator As Single           'denominator for Least Squares slope calculation
    Dim lsngSkewLinMax As Single            'maximum deviation for skew calculation
    Dim lsngSkewLinMin As Single            'minimum deviation for skew calculation
    Dim lsng_b As Single                    'y-intercept for Independent Linearity
    Dim lsng_m As Single                    'slope for Independent Linearity
    Dim lsngIdealVoltage As Single          'ideal voltage gradient
    Dim lsngVoltArray() As Single           'actual voltage gradient

    Try

      'Set array size after variable initialization
      ReDim lsngVoltArray(evaluateStop - evaluateStart)
      ReDim calcDataArray(evaluateStop - evaluateStart)

      'Calculate summations required for Least Squares Approximation of Best Fit Line
      For i = 0 To (evaluateStop - evaluateStart)
        lsngVoltArray(i) = rawDataArray(i)
      Next
      For i = (leastSquaresRegionStart) To (leastSquaresRegionStop)
        lsngSigmaX = lsngSigmaX + i
        lsngSigmaX2 = lsngSigmaX2 + (i ^ 2)
        lsngSigmaY = lsngSigmaY + lsngVoltArray(i)
        lsngSigmaXY = lsngSigmaXY + (i * lsngVoltArray(i))
        n = n + 1
      Next

      ' LEAST SQUARES APPROXIMATION
      '
      ' Given the equation of a line is:   y(x) = mx + b
      '
      ' Calculate m & b as follows:
      '
      '                            SigmaXY - n * xBar * yBar
      '                       m = ___________________________
      '
      '                             SigmaX^2 - n * xBar^2
      '
      '
      '                       b =  yBar - m * xBar
      '

      'Calculate averages of X & Y
      lsngXBar = lsngSigmaX / n
      lsngYBar = lsngSigmaY / n

      'Calculate LEAST SQUARES APPROXIMATION SLOPE:
      lsngNumerator = lsngSigmaXY - n * lsngXBar * lsngYBar
      lsngDenominator = lsngSigmaX2 - n * lsngXBar ^ 2
      lsng_m = lsngNumerator / lsngDenominator

      'Check if skew method is used for y-intercept value:
      If RunSkewMethod Then

        'Initialize peak values
        lsngSkewLinMax = -100
        lsngSkewLinMin = 100

        'Calculate max & min deviation values for skew
        For i = 0 To evaluateStop - evaluateStart
          yFor = lsngVoltArray(i) - (lsng_m * (i))      'Note: Y-intercept (b) assumed to be 0

          If yFor > lsngSkewLinMax Then
            lsngSkewLinMax = yFor
          ElseIf yFor < lsngSkewLinMin Then
            lsngSkewLinMin = yFor
          End If
        Next

        'Calculate SKEW METHOD Y-INTERCEPT:
        lsng_b = (lsngSkewLinMax + lsngSkewLinMin) / 2

      Else
        'Calculate LEAST SQUARES APPROXIMATION Y-INTERCEPT
        lsng_b = lsngYBar - (lsng_m * lsngXBar)
      End If

      'Now calculate the BEST-FIT linearity deviation, using the slope and
      'y-intercept determined above:
      For i = 0 To (evaluateStop - evaluateStart)
        lsngIdealVoltage = (lsng_m * i) + lsng_b
        calcDataArray(i) = lsngVoltArray(i) - lsngIdealVoltage
      Next

      'Erase lsngVoltArray    'free up the memory since we're done with it

      ''Convert value(s) back to degrees
      'evaluateStart = evaluateStart / resolution
      'evaluateStop = evaluateStop / resolution

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub



  Public Sub CalcLimitLinesFromMpc(ByVal dtLimitTable As DataTable, ByRef arrX() As Double, ByRef arrY() As Double)
    Dim arlLimitX As New ArrayList
    Dim arlLimitY As New ArrayList
    Dim drLimit As DataRow

    Try
      arrX = Nothing
      arrY = Nothing
      For Each drLimit In dtLimitTable.Rows
        'arlLimitX.Add(CDbl(drLimit("Abscissa1") * gdblMotorPerParamSetUnitConversion) + gdblXArrayOffsetServo)
        'arlLimitX.Add(CDbl(drLimit("Abscissa2") * gdblMotorPerParamSetUnitConversion) + gdblXArrayOffsetServo)
        arlLimitX.Add(CDbl(drLimit("Abscissa1") * gdblMotorPerParamSetUnitConversion))
        arlLimitX.Add(CDbl(drLimit("Abscissa2") * gdblMotorPerParamSetUnitConversion))
        arlLimitY.Add(CDbl(drLimit("Ordinate1")))
        arlLimitY.Add(CDbl(drLimit("Ordinate2")))
      Next
      arrX = CType(arlLimitX.ToArray(GetType(Double)), Double())
      arrY = CType(arlLimitY.ToArray(GetType(Double)), Double())

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub CalcLimitLines(ByVal guidMetricID As Guid, ByRef LowLimitXarray() As Double, ByRef LowLimitYarray() As Double, ByRef HighLimitXarray() As Double, ByRef HighLimitYarray() As Double)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim TsopMetric As Metric = gMetrics(guidMetricID)
    Dim intRow As Int16
    Dim rowHigh As DataRow
    Dim rowLow As DataRow
    Dim intLimitCounter As Int16 = 0

    dtHighLimit = TsopMetric.MPC.HighLimitRegionsTable
    dtLowLimit = TsopMetric.MPC.LowLimitRegionsTable
    If dtHighLimit.Rows.Count > 1 Then
      ReDim HighLimitXarray(dtHighLimit.Rows.Count * 2 - 1)
      ReDim HighLimitYarray(dtHighLimit.Rows.Count * 2 - 1)
      ReDim LowLimitXarray(dtHighLimit.Rows.Count * 2 - 1)
      ReDim LowLimitYarray(dtHighLimit.Rows.Count * 2 - 1)
    Else
      ReDim HighLimitXarray(dtHighLimit.Rows.Count)
      ReDim HighLimitYarray(dtHighLimit.Rows.Count)
      ReDim LowLimitXarray(dtHighLimit.Rows.Count)
      ReDim LowLimitYarray(dtHighLimit.Rows.Count)
    End If

    'calculate limit lines
    For intRow = 0 To dtHighLimit.Rows.Count - 1
      rowHigh = dtHighLimit.Rows(intRow)
      rowLow = dtLowLimit.Rows(intRow)
      HighLimitXarray(intLimitCounter) = rowHigh("Abscissa1")
      HighLimitXarray(intLimitCounter + 1) = rowHigh("Abscissa2")
      HighLimitYarray(intLimitCounter) = rowHigh("Ordinate1")
      HighLimitYarray(intLimitCounter + 1) = rowHigh("Ordinate2")
      LowLimitXarray(intLimitCounter) = rowLow("Abscissa1")
      LowLimitXarray(intLimitCounter + 1) = rowLow("Abscissa2")
      LowLimitYarray(intLimitCounter) = rowLow("Ordinate1")
      LowLimitYarray(intLimitCounter + 1) = rowLow("Ordinate2")
      intLimitCounter += 2
    Next

  End Sub

  Public Sub CalcLinAbsolute(ByRef ScaledDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByRef indexIdeal As Double, ByVal indexLoc As Double, ByVal mIdeal As Double, ByVal Interval As Double, ByRef calcDataArray() As Double)
    '
    '   PURPOSE:    To calculate the absolute linearity deviation.  The absolute
    '               linearity is based on the ideal index output and the ideal slope
    '               of the part.
    '
    '     NOTES:    For the linear portion of the curve, the ideal value follows
    '               the equation of a line:  y(x) = m * (x - n) + b, where:
    '
    '               y(x) =  ideal value @ point x
    '                 m  =  ideal slope
    '                 x  =  location of ideal value point
    '                 n  =  location of index point
    '                 b  =  output at index point
    '
    '               The linearity checks are typically performed on only the forward data.
    '
    '  INPUT(S):    scaledDataArray : Scaled data array
    '               evaluateStart   : Start point of data
    '               evaluateStop    : End point of data
    '               indexIdeal      : Ideal index value
    '               indexLoc        : Index location
    '               mIdeal          : Ideal slope
    '               resolution      : Scan resolution
    '
    ' OUTPUT(S):    calcDataArray   : Output array of absolute linearity deviation
    '                                 values.
    '
    Dim ldblPointNum As Double
    Dim lsngIdealOutput As Double
    Dim DataIndex As Integer

    Try
      ReDim calcDataArray(CInt(Math.Abs(evaluateStop - evaluateStart) / Interval))
      'Calculate the Absolute Linearity Deviation array
      DataIndex = 0
      ldblPointNum = Math.Round(evaluateStart, 3, MidpointRounding.AwayFromZero)
      Do
        lsngIdealOutput = indexIdeal + (mIdeal * (ldblPointNum - indexLoc))
        calcDataArray(DataIndex) = (ScaledDataArray(DataIndex) - lsngIdealOutput)
        'Console.WriteLine(ldblPointNum & ", " & ScaledDataArray(DataIndex) & ", " & lsngIdealOutput & ", " & calcDataArray(DataIndex))
        DataIndex = DataIndex + 1
        ldblPointNum = Math.Round(ldblPointNum + Interval, 3, MidpointRounding.AwayFromZero)
      Loop Until ldblPointNum > evaluateStop

      'For ldblPointNum = evaluateStart To evaluateStop Step Interval
      'Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcLinSngPoint(ByVal ScaledDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByVal index1Val As Double, ByVal index1Loc As Double, ByVal Interval As Double, ByVal Slope As Double, ByRef calcDataArray() As Double)
    '
    '   PURPOSE:    To calculate Single-point linearity deviation.  The Single-point
    '               linearity is based on the Ideal Slope and Index1 value
    '
    '     NOTES:    For the linear portion of the curve, the ideal value follows
    '               the equation of a line:  y(x) = m * (x - n) + b, where:
    '
    '               y(x) =  ideal value @ point x
    '                 m  =  ideal slope
    '                 x  =  location of ideal value point
    '                 n  =  location of index point
    '                 b  =  output at index point
    '
    '               The ideal line is defined by the two reference values passed into the routine.
    '               The linearity checks are typically performed on only the forward data.
    '
    '  INPUT(S):    scaledDataArray : Scaled data array
    '               evaluateStart   : Start point of data
    '               evaluateStop    : End point of data
    '               index1Val       : Index one value
    '               index1Loc       : Index one location
    '               interval        : Scan resolution
    '               Slope           : Ideal Slope
    '
    ' OUTPUT(S):    calcDataArray   : Output array of absolute linearity deviation
    '                                 values.
    '
    Dim ldblPointNum As Double
    Dim lsngIdealM As Double
    Dim lsngIdealB As Double
    Dim lsngIdealOutput As Double
    Dim DataIndex As Integer

    Try
      ''Define the ideal slope (in %/data resolution)
      'If index2Loc <> index1Loc Then
      '  lsngIdealM = (index2Val - index1Val) / (index2Loc - index1Loc) 'y = mx + b
      'Else
      '  lsngIdealM = 0
      'End If

      lsngIdealM = Slope

      'Define the ideal Y-Intercept
      lsngIdealB = index1Val - (index1Loc * lsngIdealM) 'b = y - mx


      ReDim calcDataArray((evaluateStop - evaluateStart) / Interval)
      'Calculate the Absolute Linearity Deviation Array
      For ldblPointNum = evaluateStart To evaluateStop Step Interval
        lsngIdealOutput = (lsngIdealM * ldblPointNum) + lsngIdealB
        calcDataArray(DataIndex) = (ScaledDataArray(DataIndex)) - lsngIdealOutput
        DataIndex = DataIndex + 1
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcLinTwoPoint(ByVal ScaledDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByVal index1Val As Double, ByVal index1Loc As Double, ByVal index2Val As Double, ByVal index2Loc As Double, ByVal Interval As Double, ByRef calcDataArray() As Double)
    '
    '   PURPOSE:    To calculate two-point linearity deviation.  The two-point
    '               linearity is based on the two points passed in to the routine
    '
    '     NOTES:    For the linear portion of the curve, the ideal value follows
    '               the equation of a line:  y(x) = m * (x - n) + b, where:
    '
    '               y(x) =  ideal value @ point x
    '                 m  =  ideal slope
    '                 x  =  location of ideal value point
    '                 n  =  location of index point
    '                 b  =  output at index point
    '
    '               The ideal line is defined by the two reference points passed into the routine.
    '               The linearity checks are typically performed on only the forward data.
    '
    '  INPUT(S):    scaledDataArray : Scaled data array
    '               evaluateStart   : Start point of data
    '               evaluateStop    : End point of data
    '               index1Val       : Index one value
    '               index1Loc       : Index one location
    '               index2Val       : Index two value
    '               index2Loc       : Index two location
    '               resolution      : Scan resolution
    '
    ' OUTPUT(S):    IdealSlope      : Calculated ideal slope
    '               calcDataArray   : Output array of absolute linearity deviation
    '                                 values.
    '
    Dim ldblPointNum As Double
    Dim lsngIdealM As Double
    Dim lsngIdealB As Double
    Dim lsngIdealOutput As Double
    Dim DataIndex As Integer
    Dim DataShift As Integer

    Try
      'Define the ideal slope (in %/data resolution)
      If index2Loc <> index1Loc Then
        lsngIdealM = (index2Val - index1Val) / (index2Loc - index1Loc) 'y = mx + b
      Else
        lsngIdealM = 0
      End If
      'Define the ideal Y-Intercept
      lsngIdealB = index1Val - (index1Loc * lsngIdealM) 'b = y - mx
      DataShift = evaluateStart * (1 / gAmad.EvaluationInterval)

      ReDim calcDataArray((evaluateStop - evaluateStart) / Interval)
      'Calculate the Absolute Linearity Deviation Array
      For ldblPointNum = evaluateStart To evaluateStop Step Interval
        lsngIdealOutput = (lsngIdealM * ldblPointNum) + lsngIdealB
        calcDataArray(DataIndex) = (ScaledDataArray(DataIndex + DataShift)) - lsngIdealOutput
        DataIndex = DataIndex + 1
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMultGainToArray(ByVal DataArray() As Double, ByVal Gain As Double)
    Dim i As Integer
    Try
      For i = 0 To DataArray.Length - 1
        DataArray(i) = DataArray(i) * Gain
      Next
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMicrogradient(ByRef ScaledDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByVal mIdeal As Double, ByVal Interval As Double, ByRef calcDataArray() As Double)
    '2.0.1.0 TER
    Dim intCount As Integer
    Dim intLoop As Integer
    Dim dblOutputChange As Double = 0
    Dim dblIdealOutputChange As Double = 0
    Dim intPositionsToSkip As Integer

    Try

      intPositionsToSkip = gAmad.MicrogradientIncrement / gAmad.EvaluationInterval
      'How many positions in array?
      intCount = ((evaluateStop - evaluateStart) / Interval) - intPositionsToSkip

      'resize arrays to proper positions
      ReDim calcDataArray(intCount)
      ReDim gDataArrays.MicrogradientPositionArray(intCount)

      For intLoop = 0 To intCount
        dblOutputChange = ScaledDataArray(intLoop + intPositionsToSkip) - ScaledDataArray(intLoop)
        dblIdealOutputChange = mIdeal * gAmad.MicrogradientIncrement
        calcDataArray(intLoop) = dblOutputChange - dblIdealOutputChange
        'gDataArrays.MicrogradientPositionArray(intLoop) = intLoop * gAmad.EvaluationInterval + gAmad.EvaluationStart
        gDataArrays.MicrogradientPositionArray(intLoop) = Math.Round(intLoop * gAmad.EvaluationInterval + gAmad.EvaluationStart, 3, MidpointRounding.AwayFromZero)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CalcOutputCorrelation(ByRef ScaledDataArray() As Double, ByRef ScaledDataArray2() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByRef calcDataArray() As Double)
    '
    '   PURPOSE:    To calculate the output correlation.
    '
    '     NOTES:    For the linear portion of the curve, the ideal value follows
    '               the equation of a line:  y(x) = m * (x - n) + b, where:
    '
    '  INPUT(S):    scaledDataArray  : Scaled data array output 1
    '               scaledDataArray2 : Scaled data array output 2
    '               evaluateStart    : Start point of data
    '               evaluateStop     : End point of data
    '
    ' OUTPUT(S):    calcDataArray    : Output array of absolute linearity deviation
    '                                  values.
    '

    Dim x As Single
    Dim DataIndex As Integer

    Try
      ReDim calcDataArray((evaluateStop - evaluateStart) / gAmad.EvaluationInterval)
      DataIndex = 0
      'Calculate the Absolute Linearity Deviation array
      For x = evaluateStart To evaluateStop Step gAmad.EvaluationInterval
        calcDataArray(DataIndex) = ScaledDataArray(DataIndex) - (gAmad.OutputCorrelationRatioIdeal * ScaledDataArray2(DataIndex) + gAmad.OutputCorrelationOffsetIdeal)
        DataIndex = DataIndex + 1
      Next x

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub CalcPedalArmReturn(ByRef timeArray As Double(), ByRef voltageArray As Double())
    ' 2.0.1.0 TER

    Dim intLoop As Integer
    Dim dblStartTime As Double
    Dim intStartIndex As Integer
    Dim dblEndTime As Double
    Dim intEndIndex As Integer
    Dim dblPedalReturnTime As Double
    Dim intCount As Integer
    Dim intSteadyStateMeasurementAverages As Integer
    Dim dblVoltageSum As Double
    Dim dblVoltageHigh As Double
    Dim dblVoltageLow As Double
    Dim dblSteadyStateHighThresholdPercent As Double
    Dim dblSteadyStateLowThresholdPercent As Double
    Dim dblVoltageHighThresh As Double
    Dim dblVoltageLowThresh As Double
    Dim strErrorMessage As String
    'Dim intArrayOffset As Integer
    Dim intDataPointsBeforeStartTime As Integer
    Dim dblMeasurementTimeWindow As Double
    Dim dblTimeOffset As Double
    Dim intNewArrayLength As Integer
    Dim intNewArrayOffset As Integer

    Try

      'make these parameters
      'intSteadyStateMeasurementAverages = 20
      'dblSteadyStateHighThresholdPercent = 0.5
      'dblSteadyStateLowThresholdPercent = 0.5
      'intDataPointsBeforeStartTime = 500
      'dblMeasurementTimeWindow = 0.5

      intSteadyStateMeasurementAverages = gAmad.SteadyStateMeasurementAverages
      dblSteadyStateHighThresholdPercent = gAmad.SteadyStateThresholdHigh
      dblSteadyStateLowThresholdPercent = gAmad.SteadyStateThresholdLow
      intDataPointsBeforeStartTime = gAmad.DataPointsBeforeStart
      dblMeasurementTimeWindow = gAmad.MeasurementTimeWindow

      dblStartTime = -1
      dblEndTime = -1

      'Find Starting Voltage Value
      dblVoltageSum = 0
      intCount = 0
      For intLoop = 0 To intSteadyStateMeasurementAverages - 1
        intCount += 1
        dblVoltageSum = dblVoltageSum + voltageArray(intLoop)
      Next
      dblVoltageHigh = dblVoltageSum / intCount

      'Find Ending Voltage Value
      dblVoltageSum = 0
      intCount = 0
      For intLoop = (voltageArray.Length - 1) To (voltageArray.Length - intSteadyStateMeasurementAverages) Step -1
        intCount += 1
        dblVoltageSum = dblVoltageSum + voltageArray(intLoop)
      Next
      dblVoltageLow = dblVoltageSum / intCount

      'Determine voltage tolerances
      dblVoltageHighThresh = dblVoltageHigh * dblSteadyStateHighThresholdPercent / 100
      dblVoltageLowThresh = dblVoltageLow * dblSteadyStateLowThresholdPercent / 100

      'Find Start Time
      For intLoop = 0 To voltageArray.Length - 1
        If voltageArray(intLoop) < dblVoltageHigh - dblVoltageHighThresh Then
          dblStartTime = timeArray(intLoop)
          intStartIndex = intLoop
          Exit For
        End If
      Next
      If dblStartTime = -1 Then
        strErrorMessage = "Unable to find Start Time"
        gAnomaly = New clsDbAnomaly(9001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Find End Time
      For intLoop = voltageArray.Length - 1 To 0 Step -1
        If voltageArray(intLoop) > dblVoltageLow + dblVoltageLowThresh _
        Or voltageArray(intLoop) < dblVoltageLow - dblVoltageLowThresh Then
          If intLoop = voltageArray.Length - 1 Then
            dblEndTime = timeArray(intLoop)
            intEndIndex = intLoop
          Else
            dblEndTime = timeArray(intLoop + 1)
            intEndIndex = intLoop + 1
          End If
          Exit For
        End If
      Next
      If dblEndTime = -1 Then
        strErrorMessage = "Unable to find End Time"
        gAnomaly = New clsDbAnomaly(9002, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Number of points before start, can't be greater than start index
      If intDataPointsBeforeStartTime > intStartIndex Then
        intDataPointsBeforeStartTime = intStartIndex
      End If

      'Offset to shift arrays by
      intNewArrayOffset = intStartIndex - intDataPointsBeforeStartTime

      'Trimmed Array Length
      intNewArrayLength = (dblMeasurementTimeWindow * gDataAcquisitionSystem.SamplingRate) + 1

      If intNewArrayLength < intEndIndex + intNewArrayOffset + 1 Then
        intNewArrayLength = intEndIndex + intNewArrayOffset + 1
      End If

      gPedalReturnProperties(gintMeasurementNumber - 1).ArrayOffset = intNewArrayOffset
      gPedalReturnProperties(gintMeasurementNumber - 1).ArrayLength = intNewArrayLength

      'Array offset in terms of time
      dblTimeOffset = (gPedalReturnProperties(gintMeasurementNumber - 1).ArrayOffset) / gDataAcquisitionSystem.SamplingRate

      'Determine Return Time and other measurement properties
      dblStartTime = dblStartTime - dblTimeOffset
      dblEndTime = dblEndTime - dblTimeOffset
      dblPedalReturnTime = dblEndTime - dblStartTime
      'If dblPedalReturnTime > dblTimeWindow Then
      '  dblTimeWindow = dblPedalReturnTime
      'End If

      gPedalReturnProperties(gintMeasurementNumber - 1).EndTime = dblEndTime
      gPedalReturnProperties(gintMeasurementNumber - 1).StartTime = dblStartTime
      gPedalReturnProperties(gintMeasurementNumber - 1).ReturnTime = dblPedalReturnTime

      'Console.WriteLine("Start Time: " & CStr(dblStartTime) & ", End Time: " & CStr(dblEndTime))
      'Console.WriteLine("PedalReturnTime: " & CStr(dblPedalReturnTime))


      If gPedalReturnProperties(gintMeasurementNumber - 1).ArrayLength > voltageArray.Length - gPedalReturnProperties(gintMeasurementNumber - 1).ArrayOffset Then
        gPedalReturnProperties(gintMeasurementNumber - 1).ArrayLength = voltageArray.Length - gPedalReturnProperties(gintMeasurementNumber - 1).ArrayOffset
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPercentVrefArray(ByVal Output() As Double, ByVal Vref() As Double, ByRef PercentValArray() As Double)
    Dim i As Integer
    Try
      ReDim PercentValArray(Output.Length - 1)
      For i = 0 To Output.Length - 1
        PercentValArray(i) = (Output(i) / Vref(i)) * 100
      Next
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcRefPointByVal(ByVal ScaledDataArray() As Double, ByVal refPointIdeal As Single, ByRef refPointValue As Single, ByRef refPointLoc As Single)
    '
    '   PURPOSE:    To calculate the value of a reference point based on its
    '               ideal value.
    '
    '  INPUT(S):    scaledDataArray : Scaled data array for current channel
    '               refPointIdeal   : Ideal reference point value for the current channel
    '
    ' OUTPUT(S):    refPointLoc     : Reference point location for current channel
    '               refPointValue   : Reference point value for the current channel

    Dim i As Integer

    Try
      'Calculate the value and location of the reference point
      For i = 1 To ScaledDataArray.Length - 1
        'Find the first point above the ideal reference
        If ScaledDataArray(i) > refPointIdeal Then
          'Check if the previous point is closer to the ideal reference
          If (refPointIdeal - ScaledDataArray(i - 1)) < (ScaledDataArray(i) - refPointIdeal) Then
            'refPointLoc = (i - 1) / gMechanicalOperationAndLoad.PedalResolution
            refPointLoc = (i - 1) * gAmad.EvaluationInterval + gAmad.EvaluationStart
            refPointValue = ScaledDataArray(i - 1)
          Else
            'refPointLoc = i / gMechanicalOperationAndLoad.PedalResolution
            refPointLoc = i * gAmad.EvaluationInterval + gAmad.EvaluationStart
            refPointValue = ScaledDataArray(i)
          End If
          Exit For
        End If
      Next i
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcRepeatability(ByVal ScaledDataArray() As Double _
      , ByVal ScaledDataArray2() As Double, ByRef calcDataArray() As Double) ' 2.0.1.0 TER

    Dim i As Integer
    Try

      ReDim calcDataArray(ScaledDataArray.Length - 1)
      For i = 0 To ScaledDataArray.Length - 1
        calcDataArray(i) = ScaledDataArray2(i) - ScaledDataArray(i)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcRisingPoint(ByVal ScaledDataArray() As Double, ByVal idleOutput As Single, ByVal LineStartVal As Single, ByVal LineStartLoc As Single, ByVal LineStopVal As Single, ByVal LineStopLoc As Single, ByRef risingPointVal As Single, ByRef risingPointLoc As Single)
    '
    '   PURPOSE:    To calculate the rising point.
    '
    '  INPUT(S):    scaledDataArray : Scaled data array, binary counts
    '               idleOutput      : Output at Idle, defines first line
    '               LineStartVal    : Start Value for Line representing linear pedal output
    '               LineStartLoc    : Start Location for Line representing linear pedal output
    '               LineStopVal     : Stop Value  for Line representing linear pedal output
    '               LineStopLoc     : Stop Location for Line representing linear pedal output
    '
    ' OUTPUT(S):    risingPointVal  : Value of Rising Point
    '               risingPointLoc  : Location of Rising Point
    '

    Dim lsngM As Single
    Dim lsngB As Single

    Try
      'Initialize the variables to zeroes
      risingPointLoc = 0
      risingPointVal = 0

      'Calculate the best-fit slope of line 2
      lsngM = (LineStopVal - LineStartVal) / (LineStopLoc - LineStartLoc)

      'Calculate the intercept of line 2
      lsngB = LineStartVal - (LineStartLoc * lsngM)

      If lsngM <> 0 Then
        'Calculate the Rising Point using Line 2 and the Idle Output
        risingPointLoc = (idleOutput - lsngB) / lsngM
        'If the Rising Point is less than zero, or the rising point is beyond the
        'start of Line 2, the part's output is BAD
        If risingPointLoc < 0 Or risingPointLoc > LineStartLoc Then
          risingPointLoc = 0
        Else
          'Determine the output at the Rising Point Location
          'risingPointVal = ScaledDataArray(CInt(risingPointLoc * gMechanicalOperationAndLoad.PedalResolution))
          risingPointVal = ScaledDataArray(CInt((risingPointLoc - gAmad.EvaluationStart) / gAmad.EvaluationInterval))
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub CalcSlopeDeviation(ByVal rawDataArray() As Double, ByVal evaluateStart As Double, ByVal evaluateStop As Double, ByVal slopeInterval As Double, ByVal slopeIncrement As Double, ByVal mIdeal As Double, ByVal RunRatioMethod As Boolean, ByRef calcDataArray() As Double)
    '
    '   PURPOSE:    To calculate the slope deviation.  The slope deviation is specified
    '               by Slope Interval and Slope Increment.  The Slope Interval represents
    '               the number of data points between the two points used for the slope
    '               calculation.  The Slope Increment represents the step size between
    '               the slope checks.
    '
    '     NOTES:    The slope deviation is typically performed on only the forward data.
    '
    '  INPUT(S):    rawDataArray  : Raw data array for the current channel
    '               evaluateStart : Start point of data for current channel
    '               evaluateStop  : End point of data for current channel
    '               slopeInterval : Number of data points between slope points
    '               slopeIncrement: Step size between slope checks
    '               mIdeal        : Ideal slope for current channel
    '               RunRatioMethod: TRUE  = Calculate slope deviation via ratio method
    '                             : FALSE = Calculate slope deviation via subtraction method
    '
    ' OUTPUT(S):    calcDataArray : Output array of slope deviation values.
    '
    Dim i As Integer
    Try
      'Set array size after variable initialization
      ReDim calcDataArray(evaluateStop - evaluateStart)

      '*** Calculate the Slope Deviation array based on method selected ***
      For i = 0 To (evaluateStop - slopeInterval - evaluateStart) Step slopeIncrement
        If (RunRatioMethod) Then            'Slope dev via ratio method
          calcDataArray(i) = ((rawDataArray(i + slopeInterval)) - (rawDataArray(i))) / (mIdeal * slopeInterval)
        Else                                'Slope dev via subtraction method
          calcDataArray(i) = ((rawDataArray(i + slopeInterval)) - (rawDataArray(i))) - (mIdeal * slopeInterval)
        End If

      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub CombineArraysForADR(ByVal intMeasurementNumber As Integer)
    Dim intLoop As Integer
    Dim myType As Type
    Dim myProperty As PropertyInfo
    Dim dblChannelRange As Double
    Dim intBitsPerChannel As Int32
    Dim intStartingIndex As Int32
    Dim DataArray() As Double
    Dim strArrayName As String

    Try

      ReDim gAdrArray(gintAdrArraySize * gAdrChannels.Count - 1)

      myType = GetType(DataArrays)

      For intLoop = 0 To gAdrChannels.Count - 1
        'Channel Conversion Attributes
        dblChannelRange = gAdrChannels(intLoop).Range
        intBitsPerChannel = gAdrChannels(intLoop).BitsPerChannel
        intStartingIndex = gintAdrArraySize * intLoop

        'Get Channel Data Array
        strArrayName = gAdrChannels(intLoop).Measurements(intMeasurementNumber - 1).DataArrayName
        myProperty = myType.GetProperty(strArrayName)
        DataArray = myProperty.GetValue(gDataArrays, Nothing)

        'Resize array if necessary to make all arrays the same size
        If DataArray.Length > gintAdrArraySize Then
          Array.Resize(DataArray, gintAdrArraySize)
        End If

        'Convert Data Array and Append to ADR Array
        Call ConvertAndAppendAdrArray(DataArray, dblChannelRange, intBitsPerChannel _
                                      , intStartingIndex)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ConvertAndAppendAdrArray(ByVal DataArray() As Double, _
                                        ByVal ChannelRange As Double, _
                                        ByVal BitsPerChannel As Int32, _
                                        ByVal StartingIndex As Int32)
    'Takes the Data Array(), converts each value to an integer for maximized memory usage 
    'and appends it to the AdrArray(), which is converted to a BLOB and inserted into database
    Try

      Dim K32 As Int32 = 2 ^ BitsPerChannel / ChannelRange

      'Data * K32 before storing, Data / K32 when retrieved. 
      For i As Int32 = StartingIndex To StartingIndex + DataArray.Length - 1
        gAdrArray(i) = DataArray(i - StartingIndex) * K32
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CreateLinearForwardPositionBasedForceDataArray()
    Dim intLoop As Integer = 0
    Dim dblForwardPosition() As Double = Nothing
    Dim dblForwardForce() As Double = Nothing
    Dim intSize As Integer = 0
    Dim dblLastValue As Double = 0
    Dim intValue As Integer = 0
    Dim dblThisValue As Double = 0
    Dim intCountOfThisValue As Integer = 0

    For intLoop = 0 To gDataArrays.ForwardEncoderPositionArray.Length - 1
      intValue = gDataArrays.ForwardEncoderPositionArray(intLoop) * 100
      dblThisValue = intValue / 100
      If dblThisValue <> dblLastValue Then
        If Not dblForwardForce Is Nothing Then
          dblForwardForce(intSize) = dblForwardForce(intSize) / intCountOfThisValue
        End If
        dblLastValue = dblThisValue
        intSize = intSize + 1
        ReDim Preserve dblForwardPosition(intSize)
        ReDim Preserve dblForwardForce(intSize)
        dblForwardPosition(intSize) = dblLastValue
        dblForwardForce(intSize) = gDataArrays.ForwardTorqueValueArray(intLoop)
        intCountOfThisValue = 1
      Else
        dblForwardForce(intSize) = dblForwardForce(intSize) + gDataArrays.ForwardTorqueValueArray(intLoop)
        intCountOfThisValue = intCountOfThisValue + 1
      End If

    Next

    gDataArrays.ForwardPositionBasedForceArray = dblForwardForce

  End Sub

  Public Sub CreateLinearForwardPositionBasedDataArray()
    Dim intLoop As Integer = 0
    Dim dblForwardPosition() As Double = Nothing
    Dim dblForwardPercentVref() As Double = Nothing
    Dim intSize As Integer = 0
    Dim dblLastValue As Double = 0
    Dim intValue As Integer = 0
    Dim dblThisValue As Double = 0
    Dim intCountOfThisValue As Integer = 0

    ReDim Preserve dblForwardPosition(intSize)
    ReDim Preserve dblForwardPercentVref(intSize)
    For intLoop = 0 To gDataArrays.ForwardEncoderPositionArray.Length - 1
      intValue = gDataArrays.ForwardEncoderPositionArray(intLoop) * 100
      dblThisValue = intValue / 100
      If dblThisValue <> dblLastValue Then
        If Not dblForwardPercentVref Is Nothing Then
          dblForwardPercentVref(intSize) = dblForwardPercentVref(intSize) / intCountOfThisValue
        End If
        dblLastValue = dblThisValue
        dblForwardPosition(intSize) = dblLastValue
        dblForwardPercentVref(intSize) = gDataArrays.ForwardDUTOut1PercentVrefArray(intLoop)
        intSize = intSize + 1
        ReDim Preserve dblForwardPosition(intSize)
        ReDim Preserve dblForwardPercentVref(intSize)
        intCountOfThisValue = 1
      Else
        dblForwardPercentVref(intSize) = dblForwardPercentVref(intSize) + gDataArrays.ForwardDUTOut1PercentVrefArray(intLoop)
        intCountOfThisValue = intCountOfThisValue + 1
      End If

    Next
    dblForwardPercentVref(intSize) = dblForwardPercentVref(intSize) / intCountOfThisValue

    gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray = dblForwardPercentVref
    gDataArrays.HysteresisPositionArray = dblForwardPosition

  End Sub

  Public Sub CreateLinearReversePositionBasedDataArray()
    Dim intLoop As Integer = 0
    Dim dblReversePosition() As Double = Nothing
    Dim dblReversePercentVref() As Double = Nothing
    Dim intSize As Integer = 0
    Dim dblLastValue As Double = 0
    Dim intValue As Integer = 0
    Dim dblThisValue As Double = 0
    Dim intCountOfThisValue As Integer = 0

    ReDim Preserve dblReversePosition(intSize)
    ReDim Preserve dblReversePercentVref(intSize)
    For intLoop = 0 To gDataArrays.ReverseEncoderPositionArray.Length - 1
      intValue = gDataArrays.ReverseEncoderPositionArray(intLoop) * 100
      dblThisValue = intValue / 100
      If dblThisValue <> dblLastValue Then
        If Not dblReversePercentVref Is Nothing Then
          dblReversePercentVref(intSize) = dblReversePercentVref(intSize) / intCountOfThisValue
        End If
        dblLastValue = dblThisValue
        dblReversePosition(intSize) = dblLastValue
        dblReversePercentVref(intSize) = gDataArrays.ReverseDUTOut1PercentVrefArray(intLoop)
        intSize = intSize + 1
        ReDim Preserve dblReversePosition(intSize)
        ReDim Preserve dblReversePercentVref(intSize)
        intCountOfThisValue = 1
      Else
        dblReversePercentVref(intSize) = dblReversePercentVref(intSize) + gDataArrays.ReverseDUTOut1PercentVrefArray(intLoop)
        intCountOfThisValue = intCountOfThisValue + 1
      End If

    Next
    dblReversePercentVref(intSize) = dblReversePercentVref(intSize) / intCountOfThisValue

    gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray = dblReversePercentVref
    gDataArrays.ReversePositionArray = dblReversePosition


  End Sub

  Public Sub CreatePositionBasedDataArray(ByVal ForwardScan As Boolean, ByVal Xarray() As Double, ByVal Yarray() As Double, ByVal EvaluateStartLocation As Double, ByVal EvaluateStopLocation As Double, ByVal Interval As Double, ByVal AveragingRangeHigh As Double, ByVal AveragingRangeLow As Double, ByRef calcDataArray() As Double)
    Dim i As Integer
    Dim x As Double
    Dim NumValues As Integer
    Dim DataIndex As Integer
    Dim ldblSum As Double
    Dim lblnValueFound As Boolean
    Dim lintStartSearch As Integer
    Dim lintStopSearch As Integer
    'Dim lblnAscending As Boolean
    Dim StartSearchIndex As Integer
    Dim strErrorMessage As String
    Dim dblXValue As Double
    Dim y As Integer

    Try

      ReDim calcDataArray(CInt(Math.Abs((EvaluateStopLocation - EvaluateStartLocation) / Interval)))

      DataIndex = 0

      'If Xarray(0) > Xarray(Xarray.Length - 1) Then
      '  lblnAscending = False
      'Else
      '  lblnAscending = True
      'End If

      lintStartSearch = 0
      lintStopSearch = 0


      If ForwardScan Then
        x = EvaluateStartLocation
        y = x * Math.Abs(1 / Interval)
        y = Int(y)
        x = y / Math.Abs(1 / Interval)
        Do Until x > EvaluateStopLocation
          'For x = EvaluateStartLocation To EvaluateStopLocation Step Interval
          'y = 10 * x
          'y = Int(y)
          'x = y / 10
          'y = x * Math.Abs(1 / Interval)
          'y = Int(y)
          'x = y / Math.Abs(1 / Interval)

          NumValues = 0
          ldblSum = 0
          lblnValueFound = False
          StartSearchIndex = lintStartSearch
          For i = StartSearchIndex To Xarray.Length - 1
            'Shift to StartScan, Eval Start and Stop are relative to StartScan
            dblXValue = (Xarray(i) / gdblMotorPerParamSetUnitConversion) ' - gdblXArrayOffsetPara
            'Console.WriteLine(Xarray(i))
            'calculate averages for each position
            'If (Math.Round(Xarray(i), 2, MidpointRounding.AwayFromZero) <= Math.Round((x + AveragingRangeHigh), 2, MidpointRounding.AwayFromZero)) And (Math.Round(Xarray(i), 2, MidpointRounding.AwayFromZero) >= Math.Round((x - AveragingRangeLow), 2, MidpointRounding.AwayFromZero)) Then
            If (Math.Round(dblXValue, 3, MidpointRounding.AwayFromZero) <= Math.Round((x + AveragingRangeHigh), 3, MidpointRounding.AwayFromZero)) And (Math.Round(dblXValue, 3, MidpointRounding.AwayFromZero) >= Math.Round((x - AveragingRangeLow), 3, MidpointRounding.AwayFromZero)) Then
              NumValues = NumValues + 1
              ldblSum = (ldblSum + Yarray(i))
              calcDataArray(DataIndex) = ldblSum / NumValues
              If lblnValueFound = False Then
                lintStartSearch = i
              End If
              lblnValueFound = True
            ElseIf lblnValueFound And (i > (lintStartSearch + 10)) Then
              Exit For
            End If
          Next

          'check if we skipped a degree point
          If lblnValueFound = False Then
            'gintAnomaly = 20
            'Call ErrorLogFile("Unable to find specified X Axis location", True)
            'Log the error to the error log and display the error message
            strErrorMessage = "Unable to find specified X Axis location"
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            Throw New TsopAnomalyException
          End If
          DataIndex = DataIndex + 1
          'Next
          x = x + Interval
          y = x * Math.Abs(1 / Interval)
          y = Int(y)
          x = y / Math.Abs(1 / Interval)
        Loop
      Else
        x = EvaluateStopLocation
        y = x * Math.Abs(1 / Interval)
        y = Int(y)
        x = y / Math.Abs(1 / Interval)
        Do Until x < EvaluateStartLocation
          'For x = EvaluateStopLocation To EvaluateStartLocation Step -Interval
          'y = 10 * x
          'y = Int(y)
          'x = y / 10
          'y = x * Math.Abs(1 / Interval)
          'y = Int(y)
          'x = y / Math.Abs(1 / Interval)
          NumValues = 0
          ldblSum = 0
          lblnValueFound = False
          StartSearchIndex = lintStartSearch
          For i = StartSearchIndex To Xarray.Length - 1
            dblXValue = (Xarray(i) / gdblMotorPerParamSetUnitConversion) '- gdblXArrayOffsetPara
            'calculate averages for each position
            'If (Math.Round(Xarray(i), 2, MidpointRounding.AwayFromZero) <= Math.Round((x + AveragingRangeHigh), 2, MidpointRounding.AwayFromZero)) And (Math.Round(Xarray(i), 2, MidpointRounding.AwayFromZero) >= Math.Round((x - AveragingRangeLow), 2, MidpointRounding.AwayFromZero)) Then
            If (Math.Round(dblXValue, 3, MidpointRounding.AwayFromZero) <= Math.Round((x + AveragingRangeHigh), 3, MidpointRounding.AwayFromZero)) And (Math.Round(dblXValue, 3, MidpointRounding.AwayFromZero) >= Math.Round((x - AveragingRangeLow), 3, MidpointRounding.AwayFromZero)) Then
              NumValues = NumValues + 1
              ldblSum = (ldblSum + Yarray(i))
              calcDataArray(DataIndex) = ldblSum / NumValues
              If lblnValueFound = False Then
                lintStartSearch = i
              End If
              lblnValueFound = True
            ElseIf lblnValueFound And (i > (lintStartSearch + 10)) Then
              Exit For
            End If
          Next

          'check if we skipped a degree point
          If lblnValueFound = False Then
            'gintAnomaly = 20
            'Call ErrorLogFile("Unable to find specified X Axis location", True)
            'Log the error to the error log and display the error message
            strErrorMessage = "Unable to find specified X Axis location"
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            Throw New TsopAnomalyException
          End If
          DataIndex = DataIndex + 1
          'Next
          x = x - Interval
          y = x * Math.Abs(1 / Interval)
          y = Int(y)
          x = y / Math.Abs(1 / Interval)
        Loop
        'reverse array
        Array.Reverse(calcDataArray)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CreateAdrPositionBasedDataArray(ByVal Xarray() As Double, ByVal Yarray() As Double, ByRef PositionArray() As Double, ByRef DutOutArray() As Double)
    Dim alPosition As New ArrayList
    Dim alDut As New ArrayList
    Dim intIndex As Integer
    Dim intNextIndex As Integer
    Dim dblSum As Double
    Dim intCount As Integer
    Dim dblValue As Double
    Dim blnReverseArray As Boolean

    Try
      intNextIndex = 0
      intCount = 0
      dblSum = 0

      If Xarray(0) > Xarray(Xarray.Length - 1) Then
        blnReverseArray = True
      End If

      For intIndex = 0 To Xarray.Length - 1
        intNextIndex = intIndex + 1
        intCount = intCount + 1
        dblSum = dblSum + Yarray(intIndex)
        dblValue = dblSum / intCount
        If intIndex = Xarray.Length - 1 Then
          intNextIndex = 0
        End If
        If Xarray(intIndex) <> Xarray(intNextIndex) Then
          dblSum = 0
          intCount = 0
          alPosition.Add(Xarray(intIndex))
          alDut.Add(dblValue)
        End If
      Next

      Stop

      PositionArray = CType(alPosition.ToArray(GetType(Double)), Double())
      DutOutArray = CType(alDut.ToArray(GetType(Double)), Double())

      If blnReverseArray Then
        Array.Reverse(PositionArray)
        Array.Reverse(DutOutArray)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CreateResultDataTables()
    Try
      gdtTestResultsTable.Columns.Add("MetricID", GetType(Guid))
      gdtTestResultsTable.Columns.Add("MetricValue", GetType(Double))

      gdtProgResultsTable.Columns.Add("MetricID", GetType(Guid))
      gdtProgResultsTable.Columns.Add("MetricValue", GetType(Double))

      gdtTestFailuresTable.Columns.Add("MetricID", GetType(Guid))

      gdtAlphaTestResultsTable.Columns.Add("MetricID", GetType(Guid)) ' 2.0.1.0 TER
      gdtAlphaTestResultsTable.Columns.Add("MetricValue", GetType(String)) ' 2.0.1.0 TER

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub DetermineMetricPassFailSingleRegion(ByVal metMetric As Metric)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim rowHigh As DataRow
    Dim rowLow As DataRow
    Dim HighLimit As Double
    Dim LowLimit As Double

    Try
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable

      rowHigh = dtHighLimit.Rows(0)
      rowLow = dtLowLimit.Rows(0)
      HighLimit = rowHigh("Slope") * metMetric.MetricLocation + rowHigh("YIntercept")
      LowLimit = rowLow("Slope") * metMetric.MetricLocation + rowLow("YIntercept")

      If metMetric.MetricValue > HighLimit Then
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
        metMetric.Statistics.FailHighCount = metMetric.Statistics.FailHighCount + 1
      ElseIf metMetric.MetricValue < LowLimit Then
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
        metMetric.Statistics.FailLowCount = metMetric.Statistics.FailLowCount + 1
      Else
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub DetermineMetricPassFailSingleRegionHigh(ByVal metMetric As Metric)
    Dim dtHighLimit As New DataTable
    Dim rowHigh As DataRow
    Dim HighLimit As Double

    Try
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable

      rowHigh = dtHighLimit.Rows(0)

      HighLimit = rowHigh("Slope") * metMetric.MetricLocation + rowHigh("YIntercept")

      If metMetric.MetricValue > HighLimit Then
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
        metMetric.Statistics.FailHighCount = metMetric.Statistics.FailHighCount + 1
      Else
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub DetermineMetricPassFailSingleRegionLow(ByVal metMetric As Metric)
    Dim dtLowLimit As New DataTable
    Dim rowLow As DataRow
    Dim LowLimit As Double

    Try
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable

      rowLow = dtLowLimit.Rows(0)

      LowLimit = rowLow("Slope") * metMetric.MetricLocation + rowLow("YIntercept")

      If metMetric.MetricValue < LowLimit Then
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
        metMetric.Statistics.FailLowCount = metMetric.Statistics.FailLowCount + 1
      Else
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub DetermineMetricPassFailSingleRegionGreaterThanLowLimit(ByVal metMetric As Metric)
    Dim dtLowLimit As New DataTable
    Dim rowLow As DataRow
    Dim LowLimit As Double

    Try
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable

      rowLow = dtLowLimit.Rows(0)

      LowLimit = rowLow("Slope") * metMetric.MetricLocation + rowLow("YIntercept")

      If metMetric.MetricValue <= LowLimit Then
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
        metMetric.Statistics.FailLowCount = metMetric.Statistics.FailLowCount + 1
      Else
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub DetermineMetricPassFailCompareAlphaMPC(ByVal metMetric As Metric)
    Dim intloop As Integer
    Dim strAlphaMPC As String

    Try

      metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
      If metMetric.MPC.AlphaMPCValue.Rows.Count > 0 Then
        For intloop = 0 To metMetric.MPC.AlphaMPCValue.Rows.Count - 1
          strAlphaMPC = metMetric.MPC.AlphaMPCValue.Rows(intloop).Item("Value")
          If metMetric.AlphaMetricValue = strAlphaMPC Then
            metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
            Exit For
          End If
        Next
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub DetermineMetricPassFailPercentTol(ByVal metMetric As Metric)
    Dim dtIdeal As New DataTable
    Dim ideal As Double
    Dim rowideal As DataRow
    Dim intRow As Int16
    Dim intLimitCounter As Int16 = 0
    Dim sngMetricLocation As Single

    Try
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      sngMetricLocation = Math.Round(metMetric.MetricLocation, 2)

      'set failure and adjust counts
      For intRow = 0 To dtIdeal.Rows.Count - 1
        rowideal = dtIdeal.Rows(intRow)
        If (sngMetricLocation >= rowideal("Abscissa1")) And (sngMetricLocation <= rowideal("Abscissa2")) Then
          ideal = rowideal("Slope") * metMetric.MetricLocation + rowideal("YIntercept")
          If metMetric.MetricValue > 100 Then
            metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject
            If metMetric.MetricValue > ideal Then
              metMetric.Statistics.FailHighCount = metMetric.Statistics.FailHighCount + 1
            Else
              metMetric.Statistics.FailLowCount = metMetric.Statistics.FailLowCount + 1
            End If
          Else
            metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
          End If
          Exit For
        Else
          metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ExtractAndConvertAdrArray(ByVal ChannelRange As Double, _
                                       ByVal BitsPerChannel As Int32, _
                                       ByVal StartingIndex As Int32, _
                                       ByRef DataArray() As Double)

    Dim K32 As Int32
    Dim intLoop As Integer
    Dim intEndIndex As Int32

    Try

      K32 = 2 ^ BitsPerChannel / ChannelRange

      intEndIndex = StartingIndex + DataArray.Length - 1

      For intLoop = StartingIndex To intEndIndex
        DataArray(intLoop - StartingIndex) = gAdrArray(intLoop) / K32
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindAbsoluteMaxWithMPC(ByVal DataArray() As Double _
      , ByRef MaxIndex As Integer, ByVal guidMetricID As Guid) '2.0.1.0 TER

    Dim i As Integer
    Dim MaxValue As Double = Double.MinValue
    Dim rowIdeal As DataRow
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Try

      metMetric = gMetrics(guidMetricID)
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      rowIdeal = dtIdeal.Rows(0)

      'look for max of values
      For i = 0 To DataArray.Length - 1
        If (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) >= rowIdeal("Abscissa1") And (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) <= rowIdeal("Abscissa2") Then
          If Math.Abs(DataArray(i)) > MaxValue Then
            MaxValue = Math.Abs(DataArray(i))
            MaxIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function FindEvaluationLocation(ByVal sngTargetOutput As Single) As Double
    Dim intLoop As Integer
    Dim dblEvalLocation As Double

    Try
      For intLoop = 0 To gDataArrays.ForwardDUTOut1PercentVrefArray.Length - 1
        If gDataArrays.ForwardDUTOut1PercentVrefArray(intLoop) > sngTargetOutput Then
          dblEvalLocation = gDataArrays.ForwardEncoderPositionArray(intLoop)
          If intLoop > 0 Then
            If Math.Abs(gDataArrays.ForwardDUTOut1PercentVrefArray(intLoop - 1) - sngTargetOutput) < Math.Abs(sngTargetOutput - gDataArrays.ForwardDUTOut1PercentVrefArray(intLoop)) Then
              dblEvalLocation = gDataArrays.ForwardEncoderPositionArray(intLoop - 1)
            End If
          End If
          Exit For
        End If
      Next

      Return dblEvalLocation

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Sub FindLowEndForceThreshold(ByVal forceVoltageArray() As Double, ByVal positionArray() As Double, ByVal threshold As Single, ByRef thresholdPosition As Single)
    Dim intLoop As Integer
        Dim intLoop2 As Integer
        Dim intCount As Integer
    Dim sngForce As Single
    Dim dblMultiplier As Double
    Dim dblOffset As Double
    Try
      dblMultiplier = gdblForceAmpGain
      If gintForceAmpGainUnits = ForceAmpGainUnitsEnum.lbfPerVolt Then
        dblMultiplier = dblMultiplier * gConverter.NewtonsPerLbf
      End If

      dblMultiplier = dblMultiplier * gAmad.ForceCosineMultiplier
      dblOffset = gdblForceAmpOffset + gAmad.ForceCorrectionOffset

      thresholdPosition = 999
      For intLoop = 0 To forceVoltageArray.Length - 1
        sngForce = (CSng(forceVoltageArray(intLoop)) * dblMultiplier) + dblOffset

                'If sngForce > threshold Then
                '    thresholdPosition = positionArray(intLoop)
                '    Exit For
                'End If

                If sngForce > threshold Then
                    intCount = 0
                    For intLoop2 = 1 To 5
                        sngForce = (CSng(forceVoltageArray(intLoop + intLoop2)) * dblMultiplier) + dblOffset
                        If sngForce > threshold Then
                            intCount = intCount + 1
                        End If
                    Next
                    If intCount >= 3 Then
                        thresholdPosition = positionArray(intLoop)
                        Exit For
                    End If
                End If
      Next

      If thresholdPosition = 999 Then
        Call LogActivity("FLEFT: TP = " & CStr(thresholdPosition) & " fva(0) = " & CStr(forceVoltageArray(0)) & " pa(0) = " & CStr(positionArray(0)))
                Call modTaskList.SaveForceDataToFile(forceVoltageArray, positionArray)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Unable to Find Force Threshold", gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function FindKickdownPeakLocation(ByVal forceVoltageArray() As Double, ByVal positionArray() As Double, ByRef kickdownPeakLocation As Single) As Single
    Dim intLoop As Integer
    Dim sngMaxForce As Single
    Dim sngMaxForceLocation As Single
    Dim sngForce As Single
    Dim dblMultiplier As Double
    Dim dblOffset As Double

    'Dim sngForcePoint2High As Single
    'Dim sngForcePoint2Low As Single
    'Dim sngKickdownForceSpanHigh As Single
    'Dim sngKickdownForceSpanLow As Single
    'Dim sngHighForceLimit As Single            'Highest Allowable Kickdown Peak Force
    'Dim sngLowForceLimit As Single             'Lowest Allowable Kickdown Peak Force
    'Dim metMetric As Metric
    'Dim strErrorMessage As String
    Try

      'For intLoop = 0 To positionArray.Length - 1
      '  If positionArray(intLoop) = startSearchPosition Then
      '    Exit For
      '  End If
      'Next

      'intStartIndex = intLoop

      dblMultiplier = gdblForceAmpGain
      If gintForceAmpGainUnits = ForceAmpGainUnitsEnum.lbfPerVolt Then
        dblMultiplier = dblMultiplier * gConverter.NewtonsPerLbf
      End If

      dblMultiplier = dblMultiplier * gAmad.ForceCosineMultiplier
      dblOffset = gdblForceAmpOffset + gAmad.ForceCorrectionOffset

      sngMaxForce = Single.MinValue

      For intLoop = 0 To forceVoltageArray.Length - 1
        sngForce = (CSng(forceVoltageArray(intLoop)) * dblMultiplier) + dblOffset

        If sngForce > sngMaxForce Then
          sngMaxForce = sngForce
          sngMaxForceLocation = positionArray(intLoop)
        End If
      Next

            ''Find Metric that is associated with parameter(property)
            'metMetric = GetMetricFromAssiciatedParameter("ForcePoint2Location")
            ''Lookup High MPC value
            'sngForcePoint2High = GetSinglePointMPCFromMetric(metMetric, 1, 1, MPCTypeEnum.High)
            ''Lookup Low MPC value
            'sngForcePoint2Low = GetSinglePointMPCFromMetric(metMetric, 1, 1, MPCTypeEnum.Low)

            ''Find Metric that is associated with parameter(property)
            '      metMetric = GetMetricFromAssiciatedParameter("KDForceSpan")
            ''Lookup High MPC value
            'sngKickdownForceSpanHigh = GetSinglePointMPCFromMetric(metMetric, 1, 1, MPCTypeEnum.High)
            ''Lookup Low MPC value
            'sngKickdownForceSpanLow = GetSinglePointMPCFromMetric(metMetric, 1, 1, MPCTypeEnum.Low)

            ''Highest allowable kickdown Force
            'sngHighForceLimit = sngForcePoint2High + sngKickdownForceSpanHigh

            ''Lowest allowable kickdown Force
            'sngLowForceLimit = sngForcePoint2Low + sngKickdownForceSpanLow

            ''Check force against limits here and error if out
            'If (sngMaxForce > sngHighForceLimit) Or (sngMaxForce < sngLowForceLimit) Then
            '  strErrorMessage = "Measured Kickdown Peak Force Out of Range." & vbCrLf & _
            '                    "Verify Kickdown Module Installed Correctly" & vbCrLf & _
            '                    "and Check Force Sensing Equipment."
            '  gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            '  Throw New TsopAnomalyException
            'End If
            kickdownPeakLocation = sngMaxForceLocation
      Return sngMaxForceLocation

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Sub FindArrayMaxBetweenPositions(ByVal dataArray() As Double, ByVal startPosition As Double, ByVal stopPosition As Double, ByRef maxIndex As Integer)
    Dim dblMaxValue As Double
    Dim intLoop As Integer
    Dim dblPosition As Double
    Try
      dblMaxValue = Double.MinValue
      For intLoop = 0 To dataArray.Length - 1
        dblPosition = intLoop * gAmad.EvaluationInterval + gAmad.EvaluationStart
        If dblPosition >= startPosition And dblPosition <= stopPosition Then
          If dblMaxValue > dataArray(intLoop) Then
            dblMaxValue = dataArray(intLoop)
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindDutShaftTravel(ByVal forceArray() As Double, ByVal positionArray() As Double, ByRef shaftTravel As Double)
    Dim intLoop As Integer
    Dim sngShaftTravelStart As Single
    Dim sngShaftTravelStop As Single
    Dim intLastElement As Integer
    Try

      For intLoop = 0 To forceArray.Length - 1
        If forceArray(intLoop) > gMechanicalOperationAndLoad.LowEndForceThreshold Then
          sngShaftTravelStart = positionArray(intLoop)
          Exit For
        End If
        If intLoop + 1 = forceArray.Length Then
          sngShaftTravelStart = positionArray(intLoop)
        End If
      Next

      intLastElement = gDataArrays.ForwardPositionBasedForceArray.Length - 1
      sngShaftTravelStop = intLastElement * gAmad.EvaluationInterval + gAmad.EvaluationStart

      shaftTravel = sngShaftTravelStop - sngShaftTravelStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FlipArray(ByRef InputArray() As Double, ByRef OutputArray() As Double)
    '
    '   PURPOSE:    To invert a data array.        '
    '
    '  INPUT(S):    InputArray  : Input data array
    '
    ' OUTPUT(S):    OutputArray : Inverted output array 
    '
    'New sub 1.0.1.1DLG  
    Dim intLoop As Integer
    Dim intLength As Integer
    Try
      intLength = InputArray.Length - 1

      For intLoop = 0 To intLength - 1
        OutputArray(intLoop) = InputArray(intLength - intLoop)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetAdrArraySize(ByVal intMeasurementNumber As Integer) '2.0.1.0 TER
    Dim intMinSize As Integer
    Dim intLoop As Integer
    Dim intArrayLength As Integer
    Dim myType As Type
    Dim myProperty As PropertyInfo
    Dim DataArray() As Double
    Dim strArrayName As String

    Try

      myType = GetType(DataArrays)

      intMinSize = Integer.MaxValue

      For intLoop = 0 To gAdrChannels.Count - 1
        strArrayName = gAdrChannels(intLoop).Measurements(intMeasurementNumber - 1).DataArrayName
        myProperty = myType.GetProperty(strArrayName)
        DataArray = myProperty.GetValue(gDataArrays, Nothing)

        intArrayLength = DataArray.Length
        If intArrayLength < intMinSize Then
          intMinSize = intArrayLength
        End If
      Next

      gintAdrArraySize = intMinSize

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetADRChannelProperties()
    Dim dtAdrConversionMethods As DataTable
    Dim drRow As DataRow
    Dim AdrCh As New AdrChannel
    Dim dtAdrChannelMeasurements As DataTable
    Dim drChannel As DataRow
    Dim AdrChMeas As New AdrChannelMeasurement
    Try

      'Get Conversion Methods
      dtAdrConversionMethods = gDatabase.GetAdrConversionMethodsDataTable()
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      'Assign Conversion method agrs to Adr Channel object properties, used later to package data
      For Each drRow In dtAdrConversionMethods.Rows
        AdrCh.ChannelName = drRow.Item("ADR_ChannelName")
        AdrCh.Range = drRow.Item("ChannelRange")
        AdrCh.BitsPerChannel = drRow.Item("BitsPerChannel")
        AdrCh.Measurements = New List(Of AdrChannelMeasurement)

        'Get Channel Measurements
        dtAdrChannelMeasurements = gDatabase.GetAdrChannelMeasurementsDataTable(AdrCh.ChannelName)
        For Each drChannel In dtAdrChannelMeasurements.Rows
          AdrChMeas.MeasurementName = drChannel.Item("MeasurementName")
          AdrChMeas.DataArrayName = drChannel.Item("DataArrayName")
          AdrCh.Measurements.Add(AdrChMeas)
        Next
        'AdrCh.DataArrayName = drRow.Item("ADR_DataArrayName")

        'Add channel to collection
        gAdrChannels.Add(AdrCh)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetADRChannelProperties(ByVal guidAdrStructureID As Guid, ByRef AdrChannels As List(Of AdrChannel))
    Dim dtAdrConversionMethods As DataTable
    Dim drRow As DataRow
    Dim AdrCh As New AdrChannel
    Dim dtAdrChannelMeasurements As DataTable
    Dim drChannel As DataRow
    Dim AdrChMeas As New AdrChannelMeasurement
    Try

      AdrChannels.Clear()

      'Get Conversion Methods
      dtAdrConversionMethods = gDatabase.GetAdrConversionMethodsDataTable(guidAdrStructureID)
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      'Assign Conversion method agrs to Adr Channel object properties, used later to package data
      For Each drRow In dtAdrConversionMethods.Rows
        AdrCh.ChannelName = drRow.Item("ADR_ChannelName")
        AdrCh.Range = drRow.Item("ChannelRange")
        AdrCh.BitsPerChannel = drRow.Item("BitsPerChannel")
        AdrCh.Measurements = New List(Of AdrChannelMeasurement)

        'Get Channel Measurements
        dtAdrChannelMeasurements = gDatabase.GetAdrChannelMeasurementsDataTable(AdrCh.ChannelName)
        For Each drChannel In dtAdrChannelMeasurements.Rows
          AdrChMeas.MeasurementName = drChannel.Item("MeasurementName")
          AdrChMeas.DataArrayName = drChannel.Item("DataArrayName")
          AdrCh.Measurements.Add(AdrChMeas)
        Next

        'Add channel to collection
        AdrChannels.Add(AdrCh)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetDataArraysFromADR(ByRef AdrChannels As List(Of AdrChannel), ByVal intMeasurementNumber As Integer)
    Dim intLoop As Integer
    Dim myType As Type
    Dim myProperty As PropertyInfo
    Dim dblChannelRange As Double
    Dim intBitsPerChannel As Int32
    Dim intStartingIndex As Int32
    Dim DataArray() As Double
    Dim intArrayLength As Int32
    Dim strArrayName As String

    Try

      myType = GetType(DataArrays)

      intArrayLength = (gAdrArray.Length - 1) / AdrChannels.Count

      For intLoop = 0 To AdrChannels.Count - 1
        dblChannelRange = AdrChannels(intLoop).Range
        intBitsPerChannel = AdrChannels(intLoop).BitsPerChannel
        intStartingIndex = intArrayLength * intLoop

        ReDim DataArray(intArrayLength - 1)

        Call ExtractAndConvertAdrArray(dblChannelRange, intBitsPerChannel, intStartingIndex, DataArray)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If

        strArrayName = AdrChannels(intLoop).Measurements(intMeasurementNumber - 1).DataArrayName
        myProperty = myType.GetProperty(strArrayName)
        myProperty.SetValue(gDataArrays, DataArray, Nothing)

      Next


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function GetDataArrayName(ByVal strDaqSessionName As String, ByVal strChannelName As String) As String
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String
    Dim strDataArrayName As String
    Try
      strFilterExpression = "DaqSessionName = '" & strDaqSessionName & "'" _
                            & " And ChannelName = '" & strChannelName & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtDaqDataArrayConfig.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        strDataArrayName = rowsFound(0).Item("ArrayName")
        Return strDataArrayName
      Else
        strErrorMessage = "DaqSessionName '" & strDaqSessionName & "' And ChannelName '" & strChannelName & "'" _
                          & " Not Found In Daq Data Array Definition Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetMetricFromAssiciatedParameter(ByVal strParameterName) As Metric
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String
    Dim metMetric As Metric
    Try
      strFilterExpression = "ParameterName = '" & strParameterName & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtMetricProperties.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        metMetric = gMetrics(rowsFound(0).Item("FunctionMetricID"))
        Return metMetric
      Else
        strErrorMessage = "ParameterName '" & strParameterName & "' Not Found In Metric Properties Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetSinglePointMPCFromMetric(ByVal metMetric As Metric, ByVal intRegion As Integer, ByVal intOrdinateNumber As Integer, ByVal mpcType As MPCTypeEnum) As Double
    Dim dtLimit As New DataTable
    Dim rowLimit As DataRow
    Dim dblLimit As Double

    Try

      Select Case mpcType
        Case MPCTypeEnum.Ideal
          dtLimit = metMetric.MPC.IdealValueRegionsTable
        Case MPCTypeEnum.High
                    dtLimit = metMetric.MPC.HighLimitRegionsTable
        Case MPCTypeEnum.Low
                    dtLimit = metMetric.MPC.LowLimitRegionsTable
      End Select

      rowLimit = dtLimit.Rows(intRegion - 1)

      dblLimit = rowLimit("Ordinate" & CStr(intOrdinateNumber))

      Return dblLimit

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Sub InvertArray(ByRef InputArray() As Double, ByRef OutputArray() As Double)
    '
    '   PURPOSE:    To invert a data array.        '
    '
    '  INPUT(S):    InputArray  : Input data array
    '
    ' OUTPUT(S):    OutputArray : Inverted output array 
    '
    Dim i As Integer
    Dim l As Integer
    Try
      l = InputArray.Length
      ReDim OutputArray(l - 1)
      For i = 0 To l - 1
        OutputArray(i) = -1 * InputArray(i)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InvokeMetricMethod(ByVal functionMetricID As Guid, ByVal methodName As String, ByVal argArray() As Object)
    ' 3.1.0.2 TER
    Dim strModuleName As String
    Dim strMethodName As String = Nothing
    'Dim mf As New MetricMethods
    Dim mfType As Type
    Dim mfMethod As MethodInfo
    Try

      If InStr(methodName, ".") <> 0 Then
        strModuleName = methodName.Split(".")(0)
        strMethodName = methodName.Split(".")(1)
        strModuleName = gstrAssemblyName & "." & strModuleName
      Else
        strModuleName = gstrAssemblyName & "." & "modMetricMethods"
        strMethodName = methodName
      End If

      mfType = Type.GetType(strModuleName)
      mfMethod = mfType.GetMethod(strMethodName)

      mfMethod.Invoke(Nothing, argArray)

      If gAnomaly IsNot Nothing Then
        'test this
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName & ": " & strMethodName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName & ": " & strMethodName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadLotMetricStatsFromDb()
    Dim dt As DataTable
    Dim row As DataRow
    Dim myMetric As Metric
    Dim MetricID As Guid
    Try
      dt = gDatabase.GetLotMetricStatisticsDataTable
      For Each row In dt.Rows
        MetricID = row("FunctionMetricID")
        If gMetrics.ContainsKey(MetricID) Then
          myMetric = gMetrics(MetricID)
          myMetric.Statistics.Avg = IIf(row("MetricAvg") Is DBNull.Value, Double.NaN, row("MetricAvg"))
          myMetric.Statistics.StDev = IIf(row("MetricStDev") Is DBNull.Value, Double.NaN, row("MetricStDev"))
          myMetric.Statistics.Cp = IIf(row("MetricCp") Is DBNull.Value, Double.NaN, row("MetricCp"))
          myMetric.Statistics.Cpk = IIf(row("MetricCpk") Is DBNull.Value, Double.NaN, row("MetricCpk"))
          myMetric.Statistics.Max = IIf(row("MetricMax") Is DBNull.Value, Double.NaN, row("MetricMax"))
          myMetric.Statistics.Min = IIf(row("MetricMin") Is DBNull.Value, Double.NaN, row("MetricMin"))
          myMetric.Statistics.FailHighCount = row("FailHighCount")
          myMetric.Statistics.FailLowCount = row("FailLowCount")
          myMetric.Statistics.SampleSize = row("SampleSize")
          myMetric.Statistics.SumMetricVal = IIf(row("SumMetricValues") Is DBNull.Value, Double.NaN, row("SumMetricValues"))
          myMetric.Statistics.SumMetricValSqrd = IIf(row("SumMetricValuesSquared") Is DBNull.Value, Double.NaN, row("SumMetricValuesSquared"))
        End If
      Next
    Catch ex As Exception
      MessageBox.Show(ex.Message, "LoadLotMetricStatsFromDb Error")
    End Try
  End Sub

  Public Sub LoadPreviousLotMetricStatsFromDb(ByVal lotID As Guid, ByRef grid As DataGridView)
    Dim dtStats As DataTable
    Dim rowStats As DataRow
    Dim myMetric As Metric = Nothing
    Dim MetricID As Guid

    Dim rowGrid As DataRow = Nothing
    Dim sGridName As String = ""
    Dim dtRowInfo As DataTable = Nothing
    Dim dtColInfo As DataTable = Nothing
    Dim rowRowInfo As DataRow
    Dim rowColInfo As DataRow
    Dim intRowNumber As Int16
    Dim intColNumber As Int16
    Dim NumRowsRemoved As Int16


    Try
      For Each rowGrid In gdtDisplayGrids.Rows
        sGridName = rowGrid("DisplayGridName")
        If sGridName = "FTStats" Then
          dtRowInfo = GetDisplayGridRows(rowGrid.Item("DisplayGridID"))
          dtColInfo = GetDisplayGridColumns(rowGrid.Item("DisplayGridID"))
          Exit For
        End If
      Next

      NumRowsRemoved = 0
      dtStats = gDatabase.GetPreviousLotMetricStatisticsDataTable(lotID)
      For Each rowStats In dtStats.Rows
        For Each rowRowInfo In dtRowInfo.Rows
          intRowNumber = rowRowInfo("RowNumber") - NumRowsRemoved
          If rowRowInfo("RowType") = "Metric" Then
            MetricID = New Guid(rowRowInfo("FunctionMetricID").ToString)
            If MetricID = rowStats.Item("FunctionMetricID") Then
              If gMetrics(MetricID).Enabled Then
                For Each rowColInfo In dtColInfo.Rows
                  intColNumber = rowColInfo("ColumnNumber")
                  Select Case rowColInfo("ColumnType")
                    Case "FailHighCount"
                      grid(intColNumber, intRowNumber).Value = rowStats("FailHighCount")
                    Case "FailLowCount"
                      grid(intColNumber, intRowNumber).Value = rowStats("FailLowCount")
                    Case "AVGValue"
                      grid(intColNumber, intRowNumber).Value = Math.Round(rowStats("MetricAvg"), 2)
                    Case "STDValue"
                      grid(intColNumber, intRowNumber).Value = Math.Round(rowStats("MetricStDev"), 5)
                    Case "MaxValue"
                      grid(intColNumber, intRowNumber).Value = Math.Round(rowStats("MetricMax"), 2)
                    Case "MinValue"
                      grid(intColNumber, intRowNumber).Value = Math.Round(rowStats("MetricMin"), 2)
                  End Select
                Next
              Else
                NumRowsRemoved += 1
              End If ' metric enabled
            End If 'stats metric = rowinfo metric
          End If 'is a metric
        Next
        NumRowsRemoved = 0
      Next 'Metric

    Catch ex As Exception
      MessageBox.Show(ex.Message, "LoadLotMetricStatsFromDb Error")
    End Try
  End Sub

  Public Sub ModifyMpcAbscissa(ByVal guidMetricID As Guid, ByVal abscissaRegionNumber As Integer, ByVal abscissaNumber As Integer, ByVal abscissaValue As Double)
    ' 3.1.0.2 TER, new routine
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim strAbscissa As String
    Dim intRegion As Integer
    Dim rowModified As DataRow

    Try

      metMetric = gMetrics(guidMetricID)
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      strAbscissa = "Abscissa" & abscissaNumber
      intRegion = abscissaRegionNumber - 1

      'Modify High Limit with measured Abscissa
      rowModified = dtHighLimit.Rows(intRegion)
      rowModified(strAbscissa) = abscissaValue
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

      'Modify Low Limit with measured Abscissa
      rowModified = dtLowLimit.Rows(intRegion)
      rowModified(strAbscissa) = abscissaValue
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

      'Modify Ideal with measured Abscissa
      rowModified = dtIdeal.Rows(intRegion)
      rowModified(strAbscissa) = abscissaValue
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ModifyMpcAbscissasWithEvalRange(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    'Dim strAbscissa As String
    Dim intRegion As Integer
    Dim rowModified As DataRow

    Try

      metMetric = gMetrics(guidMetricID)
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      'strAbscissa = "Abscissa" & abscissaNumber
      intRegion = 0

      'Modify High Limit with measured Abscissa
      rowModified = dtHighLimit.Rows(intRegion)
      rowModified("Abscissa1") = gAmad.EvaluationStart
      rowModified("Abscissa2") = gAmad.EvaluationStop
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

      'Modify Low Limit with measured Abscissa
      rowModified = dtLowLimit.Rows(intRegion)
      rowModified("Abscissa1") = gAmad.EvaluationStart
      rowModified("Abscissa2") = gAmad.EvaluationStop
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

      'Modify Ideal with measured Abscissa
      rowModified = dtIdeal.Rows(intRegion)
      rowModified("Abscissa1") = gAmad.EvaluationStart
      rowModified("Abscissa2") = gAmad.EvaluationStop
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ModifyMpcIdealAbscissasWithEvalRange(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    'Dim strAbscissa As String
    Dim intRegion As Integer
    Dim rowModified As DataRow

    Try

      metMetric = gMetrics(guidMetricID)
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      'strAbscissa = "Abscissa" & abscissaNumber
      intRegion = 0

      'Modify Ideal with measured Abscissa
      rowModified = dtIdeal.Rows(intRegion)
      rowModified("Abscissa1") = gAmad.EvaluationStart
      rowModified("Abscissa2") = gAmad.EvaluationStop
      rowModified("Slope") = (rowModified("Ordinate2") - rowModified("Ordinate1")) _
                              / (rowModified("Abscissa2") - rowModified("Abscissa1"))
      rowModified("YIntercept") = rowModified("Ordinate1") - (rowModified("Slope") * rowModified("Abscissa1"))

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FilterMetricsOnTaskListItem(ByRef metMetrics As Dictionary(Of Guid, Metric), ByVal intTaskIndex As Integer)
    Dim row As DataRow
    'Dim mpaMapTable As DataTable
    Dim intLoopCount As Integer = 0
    Try
      'mpaMapTable = Nothing
      'KeyCollection.Clear()
      metMetrics.Clear()
      'mpaMapTable = gtblParamArrayMap

      For Each row In gtblParamArrayMap.Rows
        If (Not row("TaskList_TaskList").ToString = "") Then
          If (row("TaskList_TaskList") = Str(intTaskIndex)) Then
            'KeyCollection.Add(row("FunctionMetricID"))
            metMetrics.Add(gMetrics(row("FunctionMetricID")).FunctionMetricID, gMetrics(row("FunctionMetricID")))
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub ProcessMetrics(ByVal metMetrics As Dictionary(Of Guid, Metric))
    Dim metMetric As Metric
    Dim argArray() As Object
    Dim arlSerializedStatistics As New ArrayList

    Try
      'Get Metric Values
      For Each metMetric In metMetrics.Values
        If metMetric.Enabled And Not metMetric.Processed Then
          If Not String.IsNullOrEmpty(metMetric.MethodName) Then
            'Build array of method arguments
            argArray = BuildMethodArgs(metMetric)
            If gAnomaly IsNot Nothing Then
              Throw New TsopAnomalyException
            End If
            'Invoke method
            Call InvokeMetricMethod(metMetric.FunctionMetricID, metMetric.MethodName, argArray)
            If gAnomaly IsNot Nothing Then
              Throw New TsopAnomalyException
            End If
          End If

          'Console.WriteLine(metMetric.MetricName & " = " & CStr(metMetric.MetricValue))

          'Check Pass/Fail if required
          If metMetric.ReportOnly Then
            'Metric is ReportOnly
            metMetric.PassFailStatus = Metric.PassFailStatusEnum.ReportOnly
          Else
            'Check Pass/Fail, and adjust counts
            Select Case metMetric.PassFailDeterminationType
              Case Metric.PassFailDeterminationTypeEnum.SingleRegion
                'Single Region Type
                Call DetermineMetricPassFailSingleRegion(metMetric)
              Case Metric.PassFailDeterminationTypeEnum.PercentTolerance
                'Percent Tolerance Type
                Call DetermineMetricPassFailPercentTol(metMetric)
              Case Metric.PassFailDeterminationTypeEnum.SingleRegionHigh
                'Single Region Check High Limit Only
                Call DetermineMetricPassFailSingleRegionHigh(metMetric)
              Case Metric.PassFailDeterminationTypeEnum.SingleRegionLow
                'Single Region Check Low Limit Only
                Call DetermineMetricPassFailSingleRegionLow(metMetric)
              'Case Metric.PassFailDeterminationTypeEnum.ProgSingleRegionLookupPropertyMPC
              '  'Single Region Type
              '  Call DetermineProgMetricPassFailSingleRegionLookupPropertyMPC(metMetric)
              Case Metric.PassFailDeterminationTypeEnum.CompareAlphaMPC
                Call DetermineMetricPassFailCompareAlphaMPC(metMetric)
              Case Metric.PassFailDeterminationTypeEnum.SingleRegionGreaterThanLowLimit
                Call DetermineMetricPassFailSingleRegionGreaterThanLowLimit(metMetric)
            End Select
            'metMetric.PassFailStatus = Metric.PassFailStatusEnum.Good
          End If
          If gAnomaly IsNot Nothing Then
            Throw New TsopAnomalyException
          End If

          'Update TestFailures
          If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
            Call UpdateTestFailuresDataTable(metMetric)
            If metMetric.MetricType = Metric.MetricTypeEnum.Alpha Then
              If gstrRoutingMessage = "" Then
                gstrRoutingMessage = "Part Failed " & metMetric.MetricName & " Value: " & metMetric.AlphaMetricValue
              Else
                gstrRoutingMessage = gstrRoutingMessage & metMetric.MetricName & " Value: " & metMetric.AlphaMetricValue
              End If
            Else
              If gstrRoutingMessage = "" Then
                gstrRoutingMessage = "Part Failed " & metMetric.MetricName & " Value: " & metMetric.MetricValue
              Else
                gstrRoutingMessage = gstrRoutingMessage & metMetric.MetricName & " Value: " & metMetric.MetricValue
              End If
            End If
          End If

          'Update stats
          Call UpdateMetricStatistics(metMetric)

          'Save to datatable
          Select Case metMetric.MetricType
            Case Metric.MetricTypeEnum.FunctionalTest
              Call UpdateTestResultsDataTable(metMetric)
            Case Metric.MetricTypeEnum.Programming
              Call UpdateProgResultsDataTable(metMetric)
            Case Metric.MetricTypeEnum.Alpha ' 2.0.1.0 TER
              Call UpdateAlphaTestResultsDataTable(metMetric)
          End Select
          'If metMetric.MetricType = Metric.MetricTypeEnum.FunctionalTest Then
          '  Call UpdateTestResultsDataTable(metMetric)
          'Else
          '  Call UpdateProgResultsDataTable(metMetric)
          'End If

          If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
            If metMetric.MetricType = Metric.MetricTypeEnum.FunctionalTest Then
              gControlFlags.TestFailure = True
            ElseIf metMetric.MetricType = Metric.MetricTypeEnum.Alpha
              gControlFlags.TestFailure = True
            Else
              gControlFlags.ProgFailure = True
            End If
          End If

          'Serialize statistics class to an xml string and concate strings from all metrics
          arlSerializedStatistics.Add(metMetric.Statistics.SerializeMetricStatsToString(gDatabase))

          If gAnomaly IsNot Nothing Then
            Throw New TsopAnomalyException
          End If

          metMetric.Processed = True

        End If
      Next

      'Need function to parse serialized string into chunks less than 8k chars
      ', then update database for alllot metric statistic values
      If arlSerializedStatistics.Count > 0 Then
        Call SaveLotMetricStatsToDatabase(arlSerializedStatistics)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ReadAcquiredDataRecord_ExampleCode()
    'Dim TestID As Guid
    'Dim AdrStructureID As Guid
    'Dim AdrChannels As New List(Of AdrChannel)
    'Dim intMeasurement As Integer
    'Try

    '  'Get ADRStructureID
    '  Call gDatabase.GetAdrStructureID(gAmad.ADRStructureVersion, AdrStructureID)
    '  If gDatabase.Anomaly IsNot Nothing Then
    '    gAnomaly = gDatabase.Anomaly
    '    gDatabase.Anomaly = Nothing
    '    Throw New TsopAnomalyException
    '  End If

    '  'Get ADR Conversion Method Arguments
    '  Call GetADRChannelProperties(AdrStructureID, AdrChannels)
    '  If gAnomaly IsNot Nothing Then
    '    Throw New TsopAnomalyException
    '  End If

    '  'TestID = d431c2e9-8b13-4ee7-99ad-ae09b6e7a6b1
    '  'TestID = 542cf6b3-f166-412e-8e16-6c9a6aef7922
    '  'TestID = 649e63b8-de28-429a-8511-ac9eb6eb3aa3
    '  'TestID = cd3f0cc8-d378-4a31-978f-97d4ec561215
    '  'TestID = 9429221b-1012-4ab8-8c2a-e782c6512414
    '  'TestID = 4279b5db-59f9-46c3-92a9-37c47fdd5212
    '  'TestID = New Guid("4279b5db-59f9-46c3-92a9-37c47fdd5212")
    '  TestID = gDatabase.TestID

    '  'gDataArrays.PedalReturnTimeArrayMax = Nothing
    '  'gDataArrays.PedalReturnDut1VoltageArrayMax = Nothing
    '  'gDataArrays.PedalReturnDut2VoltageArrayMax = Nothing

    '  Dim byteArray() As Byte = Nothing

    '  'For intMeasurement = 1 To gControlAndInterface.NumberOfScans
    '  For intMeasurement = 1 To gAdrChannels(0).Measurements.Count
    '    Call gDatabase.GetAcquiredData(TestID, byteArray, intMeasurement)
    '    If gDatabase.Anomaly IsNot Nothing Then
    '      gAnomaly = gDatabase.Anomaly
    '      gDatabase.Anomaly = Nothing
    '      Throw New TsopAnomalyException
    '    End If

    '    ReDim gAdrArray((byteArray.Length / 4) - 1)
    '    System.Buffer.BlockCopy(byteArray, 0, gAdrArray, 0, byteArray.Length)

    '    Call GetDataArraysFromADR(AdrChannels, intMeasurement)
    '    If gAnomaly IsNot Nothing Then
    '      Throw New TsopAnomalyException
    '    End If
    '  Next

    '  'For intMeasurement = 1 To gControlAndInterface.NumberOfScans
    '  For intMeasurement = 1 To gAdrChannels(0).Measurements.Count
    '    'Release CPU to other processes and threads
    '    System.Windows.Forms.Application.DoEvents()
    '    Threading.Thread.Sleep(1)

    '    'Pause task until user determines if retry is needed
    '    gControlFlags.PauseTask = True

    '    gintMeasurementNumber = intMeasurement

    '    'Graph Data in separate form and let user decide if measurement retry is needed
    '    frmGraphADR.GraphName = "VoltageGradient"
    '    'Call frmVerifyPedalArmReturn.Show()
    '    Call frmGraphADR.Show()

    '    Do
    '      'Release CPU to other processes and threads
    '      System.Windows.Forms.Application.DoEvents()
    '      Threading.Thread.Sleep(1)
    '    Loop Until gControlFlags.PauseTask = False
    '    If gAnomaly IsNot Nothing Then
    '      Throw New TsopAnomalyException
    '    End If

    '  Next

    'Catch ex As TsopAnomalyException
    '  gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    'Catch ex As Exception
    '  gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    'End Try
  End Sub

  'Public Sub RedrawLimitLines(ByVal guidMetricID As Guid)
  '  ' 3.1.0.2 TER, new routine
  '  Dim dtHighLimit As New DataTable
  '  Dim dtLowLimit As New DataTable
  '  Dim dtIdeal As New DataTable
  '  Dim metMetric As Metric

  '  Dim rowMetricProperties As DataRow
  '  Dim strGraphName As String
  '  Dim LimitArrayX() As Double
  '  Dim LimitArrayY() As Double
  '  Dim intPlotIndex As Integer
  '  Try
  '    'Redraw Limit Lines
  '    LimitArrayX = Nothing
  '    LimitArrayY = Nothing

  '    metMetric = gMetrics(guidMetricID)
  '    dtHighLimit = metMetric.MPC.HighLimitRegionsTable
  '    dtLowLimit = metMetric.MPC.LowLimitRegionsTable
  '    dtIdeal = metMetric.MPC.IdealValueRegionsTable

  '    rowMetricProperties = GetMetricPropertiesRow(metMetric.FunctionMetricID)
  '    strGraphName = rowMetricProperties.Item("GraphName")

  '    'Redraw Low Limit Lines using passed in Abscissa Value
  '    CalcLimitLinesFromMpc(dtLowLimit, LimitArrayX, LimitArrayY)
  '    'Lookup Low Limit plot index
  '    intPlotIndex = GetPlotIndex(strGraphName, PlotNameEnum.LowLimit.ToString)
  '    'Plot Low Limit data points
  '    gGraphsCollection(strGraphName).Plots(intPlotIndex).PlotXY(LimitArrayX, LimitArrayY)

  '    'Redraw High Limit Lines using passed in Abscissa Value
  '    CalcLimitLinesFromMpc(dtHighLimit, LimitArrayX, LimitArrayY)
  '    'Lookup Low Limit plot index
  '    intPlotIndex = GetPlotIndex(strGraphName, PlotNameEnum.HighLimit.ToString)
  '    'Plot Low Limit data points
  '    gGraphsCollection(strGraphName).Plots(intPlotIndex).PlotXY(LimitArrayX, LimitArrayY)

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub ResizeArraysToMatch(ByRef array1() As Double, ByRef array2() As Double)
    Try
      If array1.Length > array2.Length Then
        Array.Resize(array1, array2.Length)
      Else
        Array.Resize(array2, array1.Length)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveAcquiredDataRecord(ByVal intMeasurementNumber As Integer)
    Try

      Dim byteArray((gAdrArray.Length * 4) - 1) As Byte

      'Buffer.BlockCopy uses raw memory addresses, not array indexes, to copy array data. 
      'It only works on arrays of primitives .
      System.Buffer.BlockCopy(gAdrArray, 0, byteArray, 0, byteArray.Length)

      Call gDatabase.InsertAcquiredDataRecord(byteArray, gControlAndInterface.SeriesID, _
                                              gDeviceInProcess.EncodedSerialNumber, intMeasurementNumber)
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveLotMetricStatsToDatabase(ByVal arlMetricStats As ArrayList)
    Dim strTempXML As String
    Dim strXML As String
    Dim strStatistics As String
    Try
      'Build XML strings that are > 8000 chars and write them to database
      strTempXML = ""
      For Each strStatistics In arlMetricStats
        If strTempXML.Length + strStatistics.Length <= 7950 Then
          strTempXML = strTempXML & strStatistics
        Else
          strXML = "<Table>" & strTempXML & "</Table>"
          'Update database
          Call gDatabase.UpdateMultipleLotMetricData(strXML)
          strTempXML = strStatistics
        End If
      Next
      strXML = "<Table>" & strTempXML & "</Table>"
      'Update database
      Call gDatabase.UpdateMultipleLotMetricData(strXML)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ShiftDataArrayValuesByOffset(ByVal dataArray As Double(), ByVal dblOffset As Double)
    Dim intLoop As Integer

    Try

      For intLoop = 0 To dataArray.Length - 1
        dataArray(intLoop) = dataArray(intLoop) + dblOffset
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ScaleForceDataNewtons(ByVal dblVoltageArray() As Double, ByRef dblForceArray() As Double)
    Dim intLoop As Integer
    Dim dblMultiplier As Double
    Dim dblOffset As Double

    Try
      ReDim dblForceArray(dblVoltageArray.Length - 1)

      dblMultiplier = gdblForceAmpGain

      If gintForceAmpGainUnits = ForceAmpGainUnitsEnum.lbfPerVolt Then
        dblMultiplier = dblMultiplier * gConverter.NewtonsPerLbf
      End If

      dblMultiplier = dblMultiplier * gAmad.ForceCosineMultiplier
      dblOffset = gdblForceAmpOffset + gAmad.ForceCorrectionOffset

      For intLoop = 0 To (dblVoltageArray.Length - 1)
        dblForceArray(intLoop) = dblVoltageArray(intLoop) * dblMultiplier + dblOffset
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateAlphaTestResultsDataTable(ByVal metmetric As Metric) ' 2.0.1.0 TER
    Dim drResults As DataRow

    Try
      'save to datatable
      drResults = gdtAlphaTestResultsTable.NewRow
      drResults("MetricID") = metmetric.FunctionMetricID
      drResults("MetricValue") = metmetric.AlphaMetricValue
      gdtAlphaTestResultsTable.Rows.Add(drResults)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateMetricStatistics(ByVal metMetric As Metric)

    Try
      'increment sample size
      metMetric.Statistics.SampleSize = metMetric.Statistics.SampleSize + 1
      'add to sum
      metMetric.Statistics.SumMetricVal = metMetric.Statistics.SumMetricVal + metMetric.MetricValue
      'add to sum squared
      metMetric.Statistics.SumMetricValSqrd = _
        metMetric.Statistics.SumMetricValSqrd + metMetric.MetricValue ^ 2
      'calculate average
      metMetric.Statistics.Avg = metMetric.Statistics.SumMetricVal / metMetric.Statistics.SampleSize
      'calculate std dev.
      If ((metMetric.Statistics.SumMetricValSqrd _
          - metMetric.Statistics.SumMetricVal ^ 2 / metMetric.Statistics.SampleSize) > 0.0001) Then
        metMetric.Statistics.StDev = Math.Sqrt((metMetric.Statistics.SumMetricValSqrd _
          - metMetric.Statistics.SumMetricVal ^ 2 / metMetric.Statistics.SampleSize) _
          / (metMetric.Statistics.SampleSize - 1))
      Else
        metMetric.Statistics.StDev = 0.0
      End If
      'find max
      If metMetric.MetricValue > metMetric.Statistics.Max Then
        metMetric.Statistics.Max = metMetric.MetricValue
      End If
      'find min
      If metMetric.MetricValue < metMetric.Statistics.Min Then
        metMetric.Statistics.Min = metMetric.MetricValue
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateProgResultsDataTable(ByVal metmetric As Metric)
    Dim drResults As DataRow

    Try
      'save to datatable
      drResults = gdtProgResultsTable.NewRow
      drResults("MetricID") = metmetric.FunctionMetricID
      drResults("MetricValue") = metmetric.MetricValue
      gdtProgResultsTable.Rows.Add(drResults)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateTestFailuresDataTable(ByVal metmetric As Metric)
    Dim drResults As DataRow

    Try
      'save to datatable
      drResults = gdtTestFailuresTable.NewRow
      drResults("MetricID") = metmetric.FunctionMetricID
      gdtTestFailuresTable.Rows.Add(drResults)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateTestResultsDataTable(ByVal metmetric As Metric)
    Dim drResults As DataRow

    Try
      'save to datatable
      drResults = gdtTestResultsTable.NewRow
      drResults("MetricID") = metmetric.FunctionMetricID
      drResults("MetricValue") = metmetric.MetricValue
      gdtTestResultsTable.Rows.Add(drResults)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

#End Region





End Module
