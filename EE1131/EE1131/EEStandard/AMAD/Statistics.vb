Option Explicit On
Option Strict Off

Imports system.xml.Serialization
Imports System.IO
Imports System.Text
Imports System.Xml

Public Class Statistics

  Private mMetricID As Guid
  Private mAvg As Double
  Private mCp As Double
  Private mCpk As Double
  Private mFailHighCount As Int16
  Private mFailLowCount As Int16
  Private mMin As Double
  Private mMax As Double
  Private mStDev As Double
  Private mSumMetricVal As Double
  Private mSumMetricValSqrd As Double
  Private mSampleSize As Int16

  Public Sub New()
    Me.mAvg = 0
    Me.mCp = Double.NaN
    Me.mCpk = Double.NaN
    Me.mFailHighCount = 0
    Me.mFailLowCount = 0
    Me.mMin = Double.MaxValue
    Me.mMax = Double.MinValue
    Me.mStDev = 0
    Me.mSumMetricVal = 0
    Me.mSumMetricValSqrd = 0
    Me.mSampleSize = 0
  End Sub

  Public Property MetricID() As Guid
    Get
      Return Me.mMetricID
    End Get
    Set(ByVal value As Guid)
      Me.mMetricID = value
    End Set
  End Property

  Public Property Avg() As Double
    Get
      Return Me.mAvg
    End Get
    Set(ByVal value As Double)
      Me.mAvg = value
    End Set
  End Property

  Public Property Cp() As Double
    Get
      Return Me.mCp
    End Get
    Set(ByVal value As Double)
      Me.mCp = value
    End Set
  End Property

  Public Property Cpk() As Double
    Get
      Return Me.mCpk
    End Get
    Set(ByVal value As Double)
      Me.mCpk = value
    End Set
  End Property

  Public Property FailHighCount() As Int16
    Get
      Return Me.mFailHighCount
    End Get
    Set(ByVal value As Int16)
      Me.mFailHighCount = value
    End Set
  End Property

  Public Property FailLowCount() As Int16
    Get
      Return Me.mFailLowCount
    End Get
    Set(ByVal value As Int16)
      Me.mFailLowCount = value
    End Set
  End Property

  Public Property Min() As Double
    Get
      Return Me.mMin
    End Get
    Set(ByVal value As Double)
      Me.mMin = value
    End Set
  End Property

  Public Property Max() As Double
    Get
      Return Me.mMax
    End Get
    Set(ByVal value As Double)
      Me.mMax = value
    End Set
  End Property

  Public Property StDev() As Double
    Get
      Return Me.mStDev
    End Get
    Set(ByVal value As Double)
      Me.mStDev = value
    End Set
  End Property

  Public Property SumMetricVal() As Double
    Get
      Return Me.mSumMetricVal
    End Get
    Set(ByVal value As Double)
      Me.mSumMetricVal = value
    End Set
  End Property

  Public Property SumMetricValSqrd() As Double
    Get
      Return Me.mSumMetricValSqrd
    End Get
    Set(ByVal value As Double)
      Me.mSumMetricValSqrd = value
    End Set
  End Property

  Public Property SampleSize() As Int16
    Get
      Return Me.mSampleSize
    End Get
    Set(ByVal value As Int16)
      Me.mSampleSize = value
    End Set
  End Property

  Public Sub SaveToDb(ByVal db As Database)

    Dim serializer As XmlSerializer = New XmlSerializer(GetType(Statistics))
    Dim sw As New StringWriter
    Dim sb As StringBuilder
    Dim sxml As String
    Dim intStart As Int16
    Dim intLength As Int16
    Dim i As Int16

    serializer.Serialize(sw, Me)
    'Remove unwanted text from element tags
    intLength = sw.ToString.IndexOf("<Statistics")
    sb = sw.GetStringBuilder.Remove(0, intLength)
    intStart = sb.ToString.IndexOf(" xmlns:")
    intLength = sb.ToString.IndexOf(">")
    sb.Remove(intStart, intLength - intStart)
    sxml = sb.ToString
    'Debug.Print(sxml)

    'Use XMLDocument methods to remove elements with value of NaN (Not a Number)
    Dim doc As New XmlDocument
    Dim nodeMetric As XmlNode
    Dim nodeElement As XmlNode
    Dim nodeValue As XmlNode
    'Offset is used to adjust index of element nodes if they are removed
    Dim intElementOffset As Int16
    doc.LoadXml(sxml)
    For Each nodeMetric In doc.ChildNodes
      intElementOffset = 0
      For i = 0 To nodeMetric.ChildNodes.Count - 1
        nodeElement = nodeMetric.ChildNodes(i + intElementOffset)
        For Each nodeValue In nodeElement.ChildNodes
          'Debug.Print(nodeElement.Name & " " & nodeValue.Value)
          If nodeValue.Value = "NaN" Then
            nodeMetric.RemoveChild(nodeElement)
            intElementOffset -= 1
          End If
        Next
      Next
    Next

    'Save modified XML Document to a string for passing to stored procedure
    Dim sw2 As New StringWriter
    doc.Save(sw2)
    intLength = sw2.ToString.IndexOf("<Statistics")
    sb = sw2.GetStringBuilder.Remove(0, intLength)
    sxml = sb.ToString
    'Debug.Print(sxml)

    gDatabase.UpdateLotMetricData(sb.ToString, "")
    sw.Close()

  End Sub

  Public Function SerializeMetricStatsToString(ByVal db As Database) As String

    Dim serializer As XmlSerializer = New XmlSerializer(GetType(Statistics))
    Dim sw As New StringWriter
    Dim sb As StringBuilder
    Dim sxml As String
    Dim intStart As Int16
    Dim intLength As Int16
    Dim i As Int16

    serializer.Serialize(sw, Me)
    'Remove unwanted text from element tags
    intLength = sw.ToString.IndexOf("<Statistics")
    sb = sw.GetStringBuilder.Remove(0, intLength)
    intStart = sb.ToString.IndexOf(" xmlns:")
    intLength = sb.ToString.IndexOf(">")
    sb.Remove(intStart, intLength - intStart)
    sxml = sb.ToString
    'Debug.Print(sxml)

    'Use XMLDocument methods to remove elements with value of NaN (Not a Number)
    Dim doc As New XmlDocument
    Dim nodeMetric As XmlNode
    Dim nodeElement As XmlNode
    Dim nodeValue As XmlNode
    'Offset is used to adjust index of element nodes if they are removed
    Dim intElementOffset As Int16
    doc.LoadXml(sxml)
    For Each nodeMetric In doc.ChildNodes
      intElementOffset = 0
      For i = 0 To nodeMetric.ChildNodes.Count - 1
        nodeElement = nodeMetric.ChildNodes(i + intElementOffset)
        For Each nodeValue In nodeElement.ChildNodes
          'Debug.Print(nodeElement.Name & " " & nodeValue.Value)
          If nodeValue.Value = "NaN" Then
            nodeMetric.RemoveChild(nodeElement)
            intElementOffset -= 1
          End If
        Next
      Next
    Next

    'Save modified XML Document to a string for passing to stored procedure
    Dim sw2 As New StringWriter
    doc.Save(sw2)
    intLength = sw2.ToString.IndexOf("<Statistics")
    sb = sw2.GetStringBuilder.Remove(0, intLength)
    sxml = sb.ToString
    'Debug.Print(sxml)

    Return sb.ToString
    sw.Close()
  End Function

End Class
