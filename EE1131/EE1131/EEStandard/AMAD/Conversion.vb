Option Explicit On
Option Strict Off

Public Class Conversion

#Region "Private Member Variables =================================================="
  Private mAnomaly As clsAnomaly
  Private mStackTrace As New clsStackTrace
#End Region

#Region "Properties =================================================="
  Public ReadOnly Property InchesPerMillimeter() As Double
    Get
      Return (1 / MillimetersPerInch)
    End Get
  End Property

  Public ReadOnly Property InchesPerThou() As Double
    Get
      Return (1 / ThouPerInch)
    End Get
  End Property

  Public ReadOnly Property MetersPerInch() As Double
    Get
      Return MillimetersPerInch * Me.MetersPerMillimeters
    End Get
  End Property

  Public ReadOnly Property MetersPerThou() As Double
    Get
      Return Me.MetersPerInch * Me.InchesPerThou
    End Get
  End Property

  Public ReadOnly Property MetersPerMillimeters() As Double
    Get
      Return (1 / MillimetersPerMeters)
    End Get
  End Property

  Public ReadOnly Property MillimetersPerInch() As Double
    Get
      Return 25.4
    End Get
  End Property

  Public ReadOnly Property MillimetersPerMeters() As Double
    Get
      Return 1000
    End Get
  End Property

  Public ReadOnly Property MillimeterPerThou() As Double
    Get
      Return 1 / ThouPerMillimeter
    End Get
  End Property

  Public ReadOnly Property NewtonsPerLbf()
    Get
      Return 4.44822162
    End Get
  End Property

  Public ReadOnly Property Nmm_per_InOz() As Double
    Get
      Return 7.061552
    End Get
  End Property

  Public ReadOnly Property ThouPerInch() As Double
    Get
      Return 1000
    End Get
  End Property

  Public ReadOnly Property ThouPerMillimeter() As Double
    Get
      Return Me.ThouPerInch * Me.InchesPerMillimeter
    End Get
  End Property

#End Region

#Region "Public Methods =================================================="

  Public Function BinaryStringToDecimal(ByVal strBinary As String) As Integer
    Dim intValue As Integer
    Dim intLoop As Integer = 0
    Try
      For intLoop = 1 To 8
        intValue = intValue + (Val(Mid(strBinary, intLoop, 1)) * 2 ^ (8 - intLoop))
      Next

      Return intValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    End Try
  End Function

  Public Function DecimalToBinary(ByVal DecimalNum As Long) As String
    Dim strResult As String
    Dim lngNumber As Long
    Dim intLoop As Integer = 0
    Try
      lngNumber = DecimalNum

      strResult = Trim(Str(lngNumber Mod 2))
      lngNumber = lngNumber \ 2

      Do While lngNumber <> 0
        strResult = Trim(Str(lngNumber Mod 2)) & strResult
        lngNumber = lngNumber \ 2
      Loop
      If strResult.Length < 8 Then
        For intLoop = 1 To 8 - strResult.Length
          strResult = "0" & strResult
        Next
      End If
      Return strResult

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    End Try
  End Function

  Public Function Date2Julian(ByVal vDate As Date) As String
    Dim strTemp As String = ""

    'Date2Julian = CLng(Format(Year(vDate), "0000") _
    '             + Format(DateDiff("d", CDate("01/01/" _
    '             + Format(Year(vDate), "0000")), vDate) _
    '             + 1, "000"))


    strTemp = Format(DateDiff("d", CDate("01/01/" + Format(Year(vDate), "0000")), vDate) + 1, "000")
    strTemp = Format(vDate, "yy") & strTemp

    Return strTemp

  End Function

#End Region

#Region "Private Methods =================================================="

#End Region



End Class
