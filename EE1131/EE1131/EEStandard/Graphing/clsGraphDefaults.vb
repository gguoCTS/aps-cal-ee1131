
Imports System.Configuration


Public Class clsGraphDefaults
  Inherits ConfigurationSection

  'Public Shared Function GetConfig() As clsGraphDefaults
  '  Return ConfigurationManager.GetSection("graphDefaults")
  'End Function

  <ConfigurationProperty("CaptionBackColor")> _
  Public ReadOnly Property CaptionBackColor() As String
    Get
      Return CType(Me("CaptionBackColor"), String)
    End Get
  End Property

  <ConfigurationProperty("CaptionForeColor")> _
  Public ReadOnly Property CaptionForeColor() As String
    Get
      Return CType(Me("CaptionForeColor"), String)
    End Get
  End Property

  <ConfigurationProperty("LocationX")> _
  Public ReadOnly Property LocationX() As String
    Get
      Return CType(Me("LocationX"), String)
    End Get
  End Property

  <ConfigurationProperty("LocationY")> _
  Public ReadOnly Property LocationY() As String
    Get
      Return CType(Me("LocationY"), String)
    End Get
  End Property

  <ConfigurationProperty("PlotAreaColor")> _
  Public ReadOnly Property PlotAreaColor() As String
    Get
      Return CType(Me("PlotAreaColor"), String)
    End Get
  End Property

  <ConfigurationProperty("Width")> _
  Public ReadOnly Property Width() As String
    Get
      Return CType(Me("Width"), String)
    End Get
  End Property

  <ConfigurationProperty("Height")> _
  Public ReadOnly Property Height() As String
    Get
      Return CType(Me("Height"), String)
    End Get
  End Property

  <ConfigurationProperty("XAxis")> _
  Public ReadOnly Property XAxis() As XAxisElement
    Get
      Return CType(Me("XAxis"), XAxisElement)
    End Get
    'Set(ByVal value As XAxisElement)
    '  Me("XAxis") = value
    'End Set
  End Property

  <ConfigurationProperty("YAxis")> _
  Public ReadOnly Property YAxis() As YAxisElement
    Get
      Return CType(Me("YAxis"), YAxisElement)
    End Get
  End Property

  <ConfigurationProperty("Legend")> _
  Public ReadOnly Property Legend() As LegendElement
    Get
      Return CType(Me("Legend"), LegendElement)
    End Get
  End Property

End Class




Public Class XAxisElement
  Inherits ConfigurationElement

  <ConfigurationProperty("AutoMinorDivisionFrequency")> _
  Public ReadOnly Property AutoMinorDivisionFrequency() As Integer
    Get
      Return CType(Me("AutoMinorDivisionFrequency"), Integer)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridColor")> _
  Public ReadOnly Property MajorDivisionsGridColor() As String
    Get
      Return CType(Me("MajorDivisionsGridColor"), String)
    End Get
  End Property

  <ConfigurationProperty("AutoSpacing")> _
  Public ReadOnly Property AutoSpacing() As Boolean
    Get
      Return CType(Me("AutoSpacing"), Boolean)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsBase")> _
  Public ReadOnly Property MajorDivisionsBase() As Integer
    Get
      Return CType(Me("MajorDivisionsBase"), Integer)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridLineStyle")> _
  Public ReadOnly Property MajorDivisionsGridLineStyle() As String
    Get
      Return CType(Me("MajorDivisionsGridLineStyle"), String)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridVisible")> _
  Public ReadOnly Property MajorDivisionsGridVisible() As Boolean
    Get
      Return CType(Me("MajorDivisionsGridVisible"), Boolean)
    End Get
  End Property
End Class


Public Class YAxisElement
  Inherits ConfigurationElement

  <ConfigurationProperty("AutoMinorDivisionFrequency")> _
  Public ReadOnly Property AutoMinorDivisionFrequency() As Integer
    Get
      Return CType(Me("AutoMinorDivisionFrequency"), Integer)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridColor")> _
  Public ReadOnly Property MajorDivisionsGridColor() As String
    Get
      Return CType(Me("MajorDivisionsGridColor"), String)
    End Get
  End Property

  <ConfigurationProperty("AutoSpacing")> _
  Public ReadOnly Property AutoSpacing() As Boolean
    Get
      Return CType(Me("AutoSpacing"), Boolean)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsBase")> _
  Public ReadOnly Property MajorDivisionsBase() As Integer
    Get
      Return CType(Me("MajorDivisionsBase"), Integer)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridLineStyle")> _
  Public ReadOnly Property MajorDivisionsGridLineStyle() As String
    Get
      Return CType(Me("MajorDivisionsGridLineStyle"), String)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridVisible")> _
  Public ReadOnly Property MajorDivisionsGridVisible() As Boolean
    Get
      Return CType(Me("MajorDivisionsGridVisible"), Boolean)
    End Get
  End Property

  <ConfigurationProperty("AxisMode")> _
  Public ReadOnly Property AxisMode() As String
    Get
      Return CType(Me("AxisMode"), String)
    End Get
  End Property

End Class


Public Class LegendElement
  Inherits ConfigurationElement

  <ConfigurationProperty("LocationX")> _
  Public ReadOnly Property LocationX() As Integer
    Get
      Return CType(Me("LocationX"), Integer)
    End Get
  End Property

  <ConfigurationProperty("LocationY")> _
  Public ReadOnly Property LocationY() As Integer
    Get
      Return CType(Me("LocationY"), Integer)
    End Get
  End Property

  <ConfigurationProperty("Width")> _
  Public ReadOnly Property Width() As Integer
    Get
      Return CType(Me("Width"), Integer)
    End Get
  End Property

  <ConfigurationProperty("Height")> _
  Public ReadOnly Property Height() As Integer
    Get
      Return CType(Me("Height"), Integer)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridLineStyle")> _
  Public ReadOnly Property MajorDivisionsGridLineStyle() As String
    Get
      Return CType(Me("MajorDivisionsGridLineStyle"), String)
    End Get
  End Property

  <ConfigurationProperty("MajorDivisionsGridVisible")> _
  Public ReadOnly Property MajorDivisionsGridVisible() As Boolean
    Get
      Return CType(Me("MajorDivisionsGridVisible"), Boolean)
    End Get
  End Property

  <ConfigurationProperty("Visible")> _
  Public ReadOnly Property Visible() As Boolean
    Get
      Return CType(Me("Visible"), Boolean)
    End Get
  End Property

  <ConfigurationProperty("Border")> _
  Public ReadOnly Property Border() As String
    Get
      Return CType(Me("Border"), String)
    End Get
  End Property

End Class

