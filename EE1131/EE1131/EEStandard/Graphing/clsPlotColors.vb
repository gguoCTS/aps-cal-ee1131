Public Class clsPlotColors

  Private mPlotColors(57) As Color

  Public ReadOnly Property PlotColors() As Color()
    Get
      Return mPlotColors
    End Get
  End Property

  Public Sub New()
    Try
      mPlotColors(0) = Color.Blue()
      mPlotColors(1) = Color.BlueViolet()
      mPlotColors(2) = Color.Brown()
      mPlotColors(3) = Color.CadetBlue()
      mPlotColors(4) = Color.Chocolate()
      mPlotColors(5) = Color.Coral()
      mPlotColors(6) = Color.CornflowerBlue()
      mPlotColors(7) = Color.Crimson()
      mPlotColors(8) = Color.DarkBlue()
      mPlotColors(9) = Color.DarkCyan()
      mPlotColors(10) = Color.DarkGoldenrod()
      mPlotColors(11) = Color.DarkGreen()
      mPlotColors(12) = Color.DarkKhaki()
      mPlotColors(13) = Color.DarkMagenta()
      mPlotColors(14) = Color.DarkOliveGreen()
      mPlotColors(15) = Color.DarkOrange()
      mPlotColors(16) = Color.DarkOrchid()
      mPlotColors(17) = Color.DarkRed()
      mPlotColors(18) = Color.DarkSalmon()
      mPlotColors(19) = Color.DarkSeaGreen()
      mPlotColors(20) = Color.DarkSlateBlue()
      mPlotColors(21) = Color.DarkSlateGray()
      mPlotColors(22) = Color.DarkTurquoise()
      mPlotColors(23) = Color.DarkViolet()
      mPlotColors(24) = Color.DeepPink()
      mPlotColors(25) = Color.DeepSkyBlue()
      mPlotColors(26) = Color.DodgerBlue()
      mPlotColors(27) = Color.Firebrick()
      mPlotColors(28) = Color.ForestGreen()
      mPlotColors(29) = Color.Fuchsia()
      mPlotColors(30) = Color.Gold()
      mPlotColors(31) = Color.Goldenrod()
      mPlotColors(32) = Color.Green()
      mPlotColors(33) = Color.HotPink()
      mPlotColors(34) = Color.IndianRed()
      mPlotColors(35) = Color.Indigo()
      mPlotColors(36) = Color.Maroon()
      mPlotColors(37) = Color.MediumBlue()
      mPlotColors(38) = Color.MediumSeaGreen()
      mPlotColors(39) = Color.MediumSlateBlue()
      mPlotColors(40) = Color.MediumVioletRed()
      mPlotColors(41) = Color.MidnightBlue()
      mPlotColors(42) = Color.Navy()
      mPlotColors(43) = Color.Olive()
      mPlotColors(44) = Color.OliveDrab()
      mPlotColors(45) = Color.Orange()
      mPlotColors(46) = Color.OrangeRed()
      mPlotColors(47) = Color.Peru()
      mPlotColors(48) = Color.Purple()
      mPlotColors(49) = Color.Red()
      mPlotColors(50) = Color.RoyalBlue()
      mPlotColors(51) = Color.SaddleBrown()
      mPlotColors(52) = Color.SeaGreen()
      mPlotColors(53) = Color.Sienna()
      mPlotColors(54) = Color.SlateBlue()
      mPlotColors(55) = Color.SteelBlue()
      mPlotColors(56) = Color.Teal()
      mPlotColors(57) = Color.Tomato()

    Catch ex As Exception
      MsgBox(ex.Message, MsgBoxStyle.OkOnly, "Error in PlotDefaults_New")

    End Try
  End Sub




End Class
