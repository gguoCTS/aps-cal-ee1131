﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCANTestPanel
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.btnCallCANFunction = New System.Windows.Forms.Button()
    Me.cboCANFunctions = New System.Windows.Forms.ComboBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.dgvDynamicWriteByteValues = New System.Windows.Forms.DataGridView()
    Me.Values = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.txtCANFunctionResponse = New System.Windows.Forms.TextBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.btnTestGetSNMultipleTrials = New System.Windows.Forms.Button()
    Me.btnReadEEProm = New System.Windows.Forms.Button()
    Me.btnLongTest = New System.Windows.Forms.Button()
    Me.chkLogCANMessages = New System.Windows.Forms.CheckBox()
    Me.btnResetEEProm = New System.Windows.Forms.Button()
    Me.btnRestartCanActuator = New System.Windows.Forms.Button()
    CType(Me.dgvDynamicWriteByteValues, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'btnCallCANFunction
    '
    Me.btnCallCANFunction.Location = New System.Drawing.Point(263, 99)
    Me.btnCallCANFunction.Name = "btnCallCANFunction"
    Me.btnCallCANFunction.Size = New System.Drawing.Size(116, 23)
    Me.btnCallCANFunction.TabIndex = 0
    Me.btnCallCANFunction.Text = "Call CAN Function"
    Me.btnCallCANFunction.UseVisualStyleBackColor = True
    '
    'cboCANFunctions
    '
    Me.cboCANFunctions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboCANFunctions.FormattingEnabled = True
    Me.cboCANFunctions.Location = New System.Drawing.Point(22, 46)
    Me.cboCANFunctions.Name = "cboCANFunctions"
    Me.cboCANFunctions.Size = New System.Drawing.Size(235, 21)
    Me.cboCANFunctions.Sorted = True
    Me.cboCANFunctions.TabIndex = 4
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(19, 30)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(106, 13)
    Me.Label1.TabIndex = 5
    Me.Label1.Text = "Select CAN Function"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(22, 83)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(135, 13)
    Me.Label2.TabIndex = 7
    Me.Label2.Text = "Dynamic Write Byte Values"
    '
    'dgvDynamicWriteByteValues
    '
    Me.dgvDynamicWriteByteValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
    Me.dgvDynamicWriteByteValues.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Values})
    Me.dgvDynamicWriteByteValues.Location = New System.Drawing.Point(22, 99)
    Me.dgvDynamicWriteByteValues.Name = "dgvDynamicWriteByteValues"
    Me.dgvDynamicWriteByteValues.Size = New System.Drawing.Size(235, 150)
    Me.dgvDynamicWriteByteValues.TabIndex = 8
    '
    'Values
    '
    Me.Values.HeaderText = "Values"
    Me.Values.Name = "Values"
    '
    'txtCANFunctionResponse
    '
    Me.txtCANFunctionResponse.Location = New System.Drawing.Point(385, 101)
    Me.txtCANFunctionResponse.Name = "txtCANFunctionResponse"
    Me.txtCANFunctionResponse.Size = New System.Drawing.Size(193, 20)
    Me.txtCANFunctionResponse.TabIndex = 9
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(382, 85)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(124, 13)
    Me.Label3.TabIndex = 10
    Me.Label3.Text = "CAN Function Response"
    '
    'btnTestGetSNMultipleTrials
    '
    Me.btnTestGetSNMultipleTrials.Location = New System.Drawing.Point(400, 156)
    Me.btnTestGetSNMultipleTrials.Name = "btnTestGetSNMultipleTrials"
    Me.btnTestGetSNMultipleTrials.Size = New System.Drawing.Size(157, 23)
    Me.btnTestGetSNMultipleTrials.TabIndex = 13
    Me.btnTestGetSNMultipleTrials.Text = "TestGetSnMultipleTrials"
    Me.btnTestGetSNMultipleTrials.UseVisualStyleBackColor = True
    '
    'btnReadEEProm
    '
    Me.btnReadEEProm.Location = New System.Drawing.Point(400, 185)
    Me.btnReadEEProm.Name = "btnReadEEProm"
    Me.btnReadEEProm.Size = New System.Drawing.Size(157, 23)
    Me.btnReadEEProm.TabIndex = 14
    Me.btnReadEEProm.Text = "Read/Write EEProm"
    Me.btnReadEEProm.UseVisualStyleBackColor = True
    '
    'btnLongTest
    '
    Me.btnLongTest.Location = New System.Drawing.Point(400, 214)
    Me.btnLongTest.Name = "btnLongTest"
    Me.btnLongTest.Size = New System.Drawing.Size(157, 23)
    Me.btnLongTest.TabIndex = 15
    Me.btnLongTest.Text = "LongTest"
    Me.btnLongTest.UseVisualStyleBackColor = True
    '
    'chkLogCANMessages
    '
    Me.chkLogCANMessages.AutoSize = True
    Me.chkLogCANMessages.Location = New System.Drawing.Point(275, 48)
    Me.chkLogCANMessages.Name = "chkLogCANMessages"
    Me.chkLogCANMessages.Size = New System.Drawing.Size(120, 17)
    Me.chkLogCANMessages.TabIndex = 16
    Me.chkLogCANMessages.Text = "Log CAN Messages"
    Me.chkLogCANMessages.UseVisualStyleBackColor = True
    '
    'btnResetEEProm
    '
    Me.btnResetEEProm.Location = New System.Drawing.Point(400, 243)
    Me.btnResetEEProm.Name = "btnResetEEProm"
    Me.btnResetEEProm.Size = New System.Drawing.Size(157, 23)
    Me.btnResetEEProm.TabIndex = 17
    Me.btnResetEEProm.Text = "Reset EEProm"
    Me.btnResetEEProm.UseVisualStyleBackColor = True
    '
    'btnRestartCanActuator
    '
    Me.btnRestartCanActuator.Location = New System.Drawing.Point(400, 127)
    Me.btnRestartCanActuator.Name = "btnRestartCanActuator"
    Me.btnRestartCanActuator.Size = New System.Drawing.Size(157, 23)
    Me.btnRestartCanActuator.TabIndex = 18
    Me.btnRestartCanActuator.Text = "Restart CAN Actuator"
    Me.btnRestartCanActuator.UseVisualStyleBackColor = True
    '
    'frmCANTestPanel
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(633, 381)
    Me.Controls.Add(Me.btnRestartCanActuator)
    Me.Controls.Add(Me.btnResetEEProm)
    Me.Controls.Add(Me.chkLogCANMessages)
    Me.Controls.Add(Me.btnLongTest)
    Me.Controls.Add(Me.btnReadEEProm)
    Me.Controls.Add(Me.btnTestGetSNMultipleTrials)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.txtCANFunctionResponse)
    Me.Controls.Add(Me.dgvDynamicWriteByteValues)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.cboCANFunctions)
    Me.Controls.Add(Me.btnCallCANFunction)
    Me.Name = "frmCANTestPanel"
    Me.Text = "CAN Test Panel"
    CType(Me.dgvDynamicWriteByteValues, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnCallCANFunction As System.Windows.Forms.Button
  Friend WithEvents cboCANFunctions As System.Windows.Forms.ComboBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents dgvDynamicWriteByteValues As System.Windows.Forms.DataGridView
  Friend WithEvents Values As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents txtCANFunctionResponse As System.Windows.Forms.TextBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents btnTestGetSNMultipleTrials As System.Windows.Forms.Button
  Friend WithEvents btnReadEEProm As System.Windows.Forms.Button
  Friend WithEvents btnLongTest As System.Windows.Forms.Button
  Friend WithEvents chkLogCANMessages As System.Windows.Forms.CheckBox
  Friend WithEvents btnResetEEProm As System.Windows.Forms.Button
  Friend WithEvents btnRestartCanActuator As System.Windows.Forms.Button

End Class
