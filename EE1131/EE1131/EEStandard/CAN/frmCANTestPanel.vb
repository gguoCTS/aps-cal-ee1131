﻿Imports System.IO
Imports System.Reflection
Imports System.Linq

Public Class frmCANTestPanel

  Private mStopWatch As New clsStopWatch

  Private Sub frmCANTestPanel_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    Dim CANMessageFunction As clsCANActuator.clsCANMessage.MessageFunctions
    Try

      If gCanActuator Is Nothing Then
        'gCanActuator = New clsCANActuator("C:\EE's\Configuration\", "Lysander")
        gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)
        If gCanActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gCanActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      End If

      'gstrAssemblyName = My.Application.Info.AssemblyName

      'strFilePath = "C:\EE's\Configuration\"
      'strFileName = "CANToolMessageDefinitions.csv"

      'GetCANMessages(strFilePath, strFileName, strFilter)
      'If gAnomaly IsNot Nothing Then
      '  Throw New TsopAnomalyException
      'End If

      For Each CANMessageFunction In gCANActuator.CANMessages.Keys
        cboCANFunctions.Items.Add(CANMessageFunction.ToString)
      Next
      cboCANFunctions.Text = cboCANFunctions.Items(0)


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
    End Try
  End Sub

  Private Sub btnCallCANFunction_Click(sender As System.Object, e As System.EventArgs) Handles btnCallCANFunction.Click
    'Dim strReturnValue(0) As String
    'Dim lCANMessage As clsCANActuator.clsCANMessage
    Dim intDynamicWriteBytes() As Integer
    'Dim intdgvRow As Integer
    'Dim intIndex As Integer
    'Dim strFieldValues As String
    Dim strReturnValue As String = ""
    Dim blnResult As Boolean
    Dim strUpperByte As String = ""
    Dim strLowerByte As String = ""
    Try
      txtCANFunctionResponse.Text = " "
      txtCANFunctionResponse.Refresh()
      Application.DoEvents()

      Me.Cursor = Cursors.WaitCursor

      Select Case cboCANFunctions.SelectedItem
        Case "JumpToDefaultBootloader"
          blnResult = gCanActuator.JumpToDefaultBootloader(strReturnValue)
        Case "ReadCustomerBootloaderVersion"
          blnResult = gCanActuator.ReadCustomerBootloaderVersion(strReturnValue)
        Case "ReadDefaultBootloaderVersion"
          blnResult = gCanActuator.ReadDefaultBootloaderVersion(strReturnValue)
        Case "ReadHardwarePartNumberHighBytes"
          blnResult = gCanActuator.ReadHardwarePartNumberHighBytes(strReturnValue)
        Case "ReadHardwarePartNumberLowBytes"
          blnResult = gCanActuator.ReadHardwarePartNumberLowBytes(strReturnValue)
        Case "ReadProgramChecksum"
          blnResult = gCanActuator.ReadProgramChecksum(strReturnValue)
        Case "ReadSerialNumber"
          blnResult = gCanActuator.ReadSerialNumber(strReturnValue)
        Case "ReadSoftwareVersion"
          blnResult = gCanActuator.ReadApplicationSoftwareVersion(strReturnValue)
        Case "ReadSpecialMemoryLine1"
          blnResult = gCanActuator.ReadSpecialMemoryLine1(strUpperByte, strLowerByte)
          strReturnValue = strUpperByte & "/" & strLowerByte
        Case "ReadSpecialMemoryLine2"
          blnResult = gCanActuator.ReadSpecialMemoryLine2(strUpperByte, strLowerByte)
          strReturnValue = strUpperByte & "/" & strLowerByte
        Case "ReadVendorPCB"
          blnResult = gCanActuator.ReadVendorPCB(strReturnValue)
        Case "ResetSpecialMemoryLine1"
          blnResult = gCanActuator.ResetSpecialMemoryLine1(strReturnValue)
        Case "ResetSpecialMemoryLine2"
          blnResult = gCanActuator.ResetSpecialMemoryLine2(strReturnValue)
        Case "WriteSpecialMemoryLine1"
          ReDim intDynamicWriteBytes(0)
          intDynamicWriteBytes(0) = dgvDynamicWriteByteValues.Rows(0).Cells(0).Value
          blnResult = gCanActuator.WriteSpecialMemoryLine1(intDynamicWriteBytes, strReturnValue)
        Case "WriteSpecialMemoryLine2"
          ReDim intDynamicWriteBytes(0)
          intDynamicWriteBytes(0) = dgvDynamicWriteByteValues.Rows(0).Cells(0).Value
          blnResult = gCanActuator.WriteSpecialMemoryLine2(intDynamicWriteBytes, strReturnValue)
        Case "ReadPowerCycleCounts"
          blnResult = gCanActuator.ReadPowerCycleCounts(strReturnValue)
        Case "ReadRunTime"
          blnResult = gCanActuator.ReadRunTime(strReturnValue)
        Case "ReadSpan"
          blnResult = gCanActuator.ReadSpan(strReturnValue)
        Case "ResetEEPromAll"
          blnResult = gCanActuator.ResetEEPromAll(strReturnValue)
        Case "ResetEEPromPowerCycles"
          blnResult = gCanActuator.ResetEEPromPowerCycles(strReturnValue)
        Case "ResetEEPromRunTime"
          blnResult = gCanActuator.ResetEEPromRunTime(strReturnValue)
        Case "ResetEEPromSpan"
          blnResult = gCanActuator.ResetEEPromSpan(strReturnValue)
        Case "ReadFaultCount"
          blnResult = gCanActuator.ReadFaultCount(strReturnValue)
        Case "ResetActuator"
          blnResult = gCanActuator.ResetActuator()
        Case "DisableZenCorrection"
          blnResult = gCanActuator.DisableZenCorrection(strReturnValue)
        Case "DisablePositionCommandTimeout"
          blnResult = gCanActuator.DisablePositionCommandTimeout(strReturnValue)
        Case "ReadTempHWM"
          blnResult = gCanActuator.ReadTempHWM(strReturnValue)
        Case "ReadBurnInCycles"
          blnResult = gCanActuator.ReadBurnInCycles(strReturnValue)
        Case "ReadBurnInTime"
          blnResult = gCanActuator.ReadBurnInTime(strReturnValue)
        Case "ReadAPSType"
          blnResult = gCanActuator.ReadAPSType(strReturnValue)
        Case "WriteStatusUpdateTimeOverride"
          ReDim intDynamicWriteBytes(0)
          intDynamicWriteBytes(0) = dgvDynamicWriteByteValues.Rows(0).Cells(0).Value
          blnResult = gCanActuator.WriteStatusUpdateTimeOverride(intDynamicWriteBytes, strReturnValue)
        Case "APSCalibrate"
          ReDim intDynamicWriteBytes(0)
          intDynamicWriteBytes(0) = dgvDynamicWriteByteValues.Rows(0).Cells(0).Value
          blnResult = gCanActuator.APSCalibrate(intDynamicWriteBytes, strReturnValue)
        Case "ReadAPSCalibrateProgress"
          blnResult = gCanActuator.ReadAPSCalibrateProgress(strReturnValue)
        Case "ReadAPSCountValue"
          blnResult = gCanActuator.ReadAPSCountValue(strReturnValue)
        Case "WriteOverrideCommand"
          blnResult = gCanActuator.WriteOverrideCommand(strReturnValue)
        Case "ReadInternalTemperature"
          blnResult = gCanActuator.ReadInternalTemperature(strReturnValue)
        Case "ReadBatteryVoltage"
          blnResult = gCanActuator.ReadBatteryVoltage(strReturnValue)
        Case "ReadProgramChecksumInteger"
          blnResult = gCanActuator.ReadProgramChecksumInteger(strReturnValue)
        Case "CumminsCommandPosition"
          ReDim intDynamicWriteBytes(1)
          intDynamicWriteBytes(0) = dgvDynamicWriteByteValues.Rows(0).Cells(0).Value And (&HFF) 'Mask off Upper bytes
          intDynamicWriteBytes(1) = (dgvDynamicWriteByteValues.Rows(0).Cells(0).Value And &HFF00) / &H100 'Mask off lower bytes
          blnResult = gCanActuator.CumminsCommandPosition(intDynamicWriteBytes)
        Case "CumminsCommandOverride"
          blnResult = gCanActuator.CumminsCommandOverride()
        Case "ReadActuatorState"
          blnResult = gCanActuator.ReadActuatorState(strReturnValue)
        Case "ReadActuatorPositionStatus"
          blnResult = gCanActuator.ReadActuatorPositionStatus(strReturnValue)
        Case "ValveCalibrate"
          blnResult = gCanActuator.ValveCalibrate(strReturnValue)
        Case "CumminsClearStops"
          'blnResult = gCanActuator.CumminsClearStops(strReturnValue)
          blnResult = gCanActuator.CumminsClearStops()
      End Select
      If blnResult Then
        txtCANFunctionResponse.Text = strReturnValue
      Else
        txtCANFunctionResponse.Text = "Error"
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Public Function StreamCsvFileIntoDataTable(ByVal strFilePath As String, ByVal strFileName As String, _
                                           Optional ByVal strFilter As String = Nothing) As DataTable

    Dim dt As New System.Data.DataTable
    Dim blnFirstLine As Boolean = True
    Dim strFilterFieldValue As String
    Dim strFilterColumnName As String
    Dim intFilterColumn As Integer
    Dim strFile As String
    Dim cols() As String
    Dim data() As String
    Dim col As String
    Try

      strFilterColumnName = ""
      strFilterFieldValue = ""
      intFilterColumn = -1
      cols = Nothing
      data = Nothing

      If strFilter <> Nothing Then
        strFilterColumnName = (strFilter.Split("=")(0)).Trim
        strFilterFieldValue = (strFilter.Split("=")(1)).Trim
      End If

      strFile = strFilePath & strFileName

      If IO.File.Exists(strFile) Then
        Using sr As New StreamReader(strFile)
          While Not sr.EndOfStream
            If blnFirstLine Then
              blnFirstLine = False
              cols = sr.ReadLine.Split(",")
              For Each col In cols
                dt.Columns.Add(New DataColumn(col, GetType(String)))
              Next
              If strFilterColumnName <> "" Then
                intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
              End If
            Else
              data = sr.ReadLine.Split(",")
              If intFilterColumn <> -1 Then
                If data(intFilterColumn) = strFilterFieldValue Then
                  dt.Rows.Add(data.ToArray)
                End If
              Else
                dt.Rows.Add(data.ToArray)
              End If
            End If
          End While
        End Using
      End If
      Return dt

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function


  Private Sub btnReadEEProm_Click(sender As System.Object, e As System.EventArgs) Handles btnReadEEProm.Click
    Dim blnReturnValue As Boolean
    Dim strSerialNumber As String = ""
    Dim intloop As Integer
    Try
      Me.Cursor = Cursors.WaitCursor

      txtCANFunctionResponse.Text = ""

      For intLoop = 1 To 2

        'blnReturnValue = gCANActuator.ActuatorPresentOnBus()
        blnReturnValue = gCANActuator.ReadSerialNumber(strSerialNumber)
        txtCANFunctionResponse.Text = strSerialNumber

        blnReturnValue = gCANActuator.EnterBootloader()
        If gCANActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
          gCANActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        'Call gCANActuator.TestReadEEPromMultipleTrials()
        blnReturnValue = gCANActuator.ReadEEPromIntoIntelHexFormat()
        If gCANActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
          gCANActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        blnReturnValue = gCANActuator.WriteIntelHexFile()
        If gCANActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
          gCANActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        blnReturnValue = gCANActuator.StartApplication
        If gCANActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
          gCANActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        gCANActuator.ShutDown()
        If gCANActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
          gCANActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        gCANActuator = Nothing

        'gCanActuator = New clsCANActuator("C:\EE's\Configuration\", "Lysander")
        gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)
        If gCanActuator.Anomaly IsNot Nothing Then
          txtCANFunctionResponse.Text = "Error"
          gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gCanActuator.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        'gCANActuator.ReStartCAN()

      Next


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Private Structure TestResults
    Dim DateTime As DateTime
    Dim StationBit As Integer
    Dim InitialStationStatus As String
    Dim StationStarted As String
    Dim StationPassed As String
    Dim CustomerBootloaderVersion As String
    Dim DefaultBootloaderVersion As String
    Dim HardwarePartNumberHighBytes As String
    Dim HardwarePartNumberLowBytes As String
    Dim SerialNumber As String
    Dim PowerCycleCount As String
    Dim ApplicationSoftwareVersion As String
    Dim VendorPCB As String
    Dim EEPromRecords As String
    Dim CanError As String
  End Structure

  Private Sub btnLongTest_Click(sender As System.Object, e As System.EventArgs) Handles btnLongTest.Click
    Dim intDynamicWriteBytes() As Integer
    Dim strReturnValue As String = ""
    Dim blnResult As Boolean
    Dim intStations() As Integer
    Dim blnDone As Boolean
    Dim intStationIndex As Integer
    Dim TestStreamWriter As IO.StreamWriter = Nothing
    Dim fileName As String
    Dim Results As New TestResults
    Dim intTrialNumber As Integer
    Dim strUpperByte As String = ""
    Dim strLowerByte As String = ""
    Dim intRetryEEProm As Integer

    Me.Cursor = Cursors.WaitCursor

    fileName = "C:\EE's\ActuatorData\LongTest\TestResults.csv"
    TestStreamWriter = New IO.StreamWriter(fileName, True)

    TestStreamWriter.WriteLine(HeaderString())
    TestStreamWriter.Flush()

    intStationIndex = 0
    intStations = {2, 4, 5, 7, 9}
    ReDim intDynamicWriteBytes(0)
    intTrialNumber = 1
    intRetryEEProm = 0

    ''Enable CAN Message Logging on First Station
    'gCANActuator.CanMessageLoggingEnabled = True

    Do
      Application.DoEvents()

      intRetryEEProm = 0

      Results.DateTime = Now

      Results.StationBit = intStations(intStationIndex)

      blnResult = gCANActuator.ReadSpecialMemoryLine1(strUpperByte, strLowerByte)
      Results.InitialStationStatus = strUpperByte & "/" & strLowerByte

      intDynamicWriteBytes(0) = intStations(intStationIndex)

      blnResult = gCANActuator.WriteSpecialMemoryLine1(intDynamicWriteBytes, strReturnValue)
      blnResult = gCANActuator.ReadSpecialMemoryLine1(strUpperByte, strLowerByte)
      Results.StationStarted = strUpperByte & "/" & strLowerByte

      blnResult = gCANActuator.WriteSpecialMemoryLine2(intDynamicWriteBytes, strReturnValue)
      blnResult = gCANActuator.ReadSpecialMemoryLine2(strUpperByte, strLowerByte)
      Results.StationPassed = strUpperByte & "/" & strLowerByte

      blnResult = gCANActuator.ReadCustomerBootloaderVersion(Results.CustomerBootloaderVersion)

      blnResult = gCANActuator.ReadDefaultBootloaderVersion(Results.DefaultBootloaderVersion)

      blnResult = gCANActuator.ReadHardwarePartNumberHighBytes(Results.HardwarePartNumberHighBytes)

      blnResult = gCANActuator.ReadHardwarePartNumberLowBytes(Results.HardwarePartNumberLowBytes)

      blnResult = gCANActuator.ReadSerialNumber(Results.SerialNumber)

      blnResult = gCANActuator.ReadPowerCycleCounts(Results.PowerCycleCount)

      blnResult = gCANActuator.ReadApplicationSoftwareVersion(Results.ApplicationSoftwareVersion)

      blnResult = gCANActuator.ReadVendorPCB(Results.VendorPCB)

      blnResult = gCANActuator.EnterBootloader()

      blnResult = gCANActuator.ReadEEPromIntoIntelHexFormat()
      Results.EEPromRecords = gCANActuator.IntelHexFile.Count
      If gCANActuator.IntelHexFile.Count <> 162 Then
        If gCANActuator.Anomaly IsNot Nothing Then
          Results.CanError = "Too Few EEProm Records. Error: " & gCANActuator.Anomaly.AnomalyExceptionMessage & " " & gCANActuator.Anomaly.AnomalyMessageText
        Else
          Results.CanError = "Too Few EEProm Records"
          'Do
          '  intRetryEEProm += 1
          '  Console.WriteLine("Retry EEProm Read Count: " & intRetryEEProm)
          '  blnResult = gCANActuator.ReadEEPromIntoIntelHexFormat()
          '  Results.EEPromRecords = gCANActuator.IntelHexFile.Count
          'Loop Until intRetryEEProm = 5
          'End If
          'Else
        End If
      End If

      blnResult = gCANActuator.StartApplication
      mStopWatch.DelayTime(3500)

      TestStreamWriter.WriteLine(ResultsToString(Results))
      TestStreamWriter.Flush()

      'gCANActuator.CanMessageLoggingEnabled = False

      'Shut down and re-instantiate CAN Actuator Object
      gCANActuator.ShutDown()
      gCANActuator = Nothing
      'gCanActuator = New clsCANActuator("C:\EE's\Configuration\", "Lysander")
      gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)


      'If anomaly, reset CAN and start over
      If gCANActuator.Anomaly IsNot Nothing Then
        gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
        gCANActuator.Anomaly = Nothing

        'Store Error in Results
        Results.CanError = Results.CanError & "Anomaly " & gAnomaly.AnomalyNumber & " Occured in Procedure:, " & gAnomaly.ModuleName & "." & gAnomaly.ProcedureName
        If gAnomaly.AnomalyExceptionMessage <> String.Empty Then
          Results.CanError = Results.CanError & ", " & gAnomaly.AnomalyExceptionMessage
        End If
        If gAnomaly.AnomalyMessageText <> String.Empty Then
          Results.CanError = Results.CanError & ", " & gAnomaly.AnomalyMessageText
        End If

        'Clear Anomaly
        gCANActuator.Anomaly = Nothing

        'Set to last station so next loop will start at first station
        intStationIndex = 4

        'Restart CAN Interface
        gCANActuator.ReStartCAN()

      Else
        Results.CanError = ""
      End If

      If intStationIndex = 4 Then
        blnResult = gCANActuator.ResetSpecialMemoryLine1(strReturnValue)
        blnResult = gCANActuator.ResetSpecialMemoryLine2(strReturnValue)
        Console.WriteLine("Trial " & intTrialNumber)
        intStationIndex = 0
        intTrialNumber += 1

        ''Enable CAN Message Logging on First Station
        'gCANActuator.CanMessageLoggingEnabled = True

      Else
        intStationIndex += 1

        ''First Station Done, disable CAN Message Logging
        'gCANActuator.CanMessageLoggingEnabled = False

      End If
    Loop Until blnDone

    Me.Cursor = Cursors.Default
    TestStreamWriter.Close()

  End Sub

  Private Function ResultsToString(ByRef Results As TestResults) As String
    Dim strReturnValue As String

    strReturnValue = Results.DateTime & ", " _
                   & Results.StationBit & ", " _
                   & Results.InitialStationStatus & ", " _
                   & Results.StationStarted & ", " _
                   & Results.StationPassed & ", " _
                   & Results.CustomerBootloaderVersion & ", " _
                   & Results.DefaultBootloaderVersion & ", " _
                   & Results.HardwarePartNumberHighBytes & ", " _
                   & Results.HardwarePartNumberLowBytes & ", " _
                   & Results.SerialNumber & ", " _
                   & Results.PowerCycleCount & ", " _
                   & Results.ApplicationSoftwareVersion & ", " _
                   & Results.VendorPCB & ", " _
                   & Results.EEPromRecords & ", " _
                   & Results.CanError & ", "


    Return strReturnValue

  End Function

  Private Function HeaderString() As String
    Dim strReturnValue As String

    strReturnValue = "DateTime" & ", " _
                   & "StationBit" & ", " _
                   & "InitialStationStatus" & ", " _
                   & "StationStarted" & ", " _
                   & "StationPassed" & ", " _
                   & "CustomerBootloaderVersion" & ", " _
                   & "DefaultBootloaderVersion" & ", " _
                   & "HardwarePartNumberHighBytes" & ", " _
                   & "HardwarePartNumberLowBytes" & ", " _
                   & "SerialNumber" & ", " _
                   & "PowerCycleCount" & ", " _
                   & "ApplicationSoftwareVersion" & ", " _
                   & "VendorPCB" & ", " _
                   & "EEPromRecords" & ", " _
                   & "CANError" & ", "


    Return strReturnValue

  End Function

  Private Sub chkLogCANMessages_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkLogCANMessages.CheckedChanged

    gCANActuator.CanMessageLoggingEnabled = chkLogCANMessages.Checked

  End Sub

  'Private Sub btnResetEEProm_Click(sender As System.Object, e As System.EventArgs) Handles btnResetEEProm.Click
  '  Dim strReturnValue As String = ""
  '  Try
  '    Me.Cursor = Cursors.WaitCursor
  '    txtCANFunctionResponse.Text = ""

  '    Call gCANActuator.ResetEEProm(strReturnValue)
  '    txtCANFunctionResponse.Text = strReturnValue

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    Call DisplayAnomaly()
  '    gAnomaly = Nothing
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Call DisplayAnomaly()
  '    gAnomaly = Nothing
  '  Finally
  '    Me.Cursor = Cursors.Default
  '  End Try
  'End Sub

  Private Sub btnRestartCanActuator_Click(sender As System.Object, e As System.EventArgs) Handles btnRestartCanActuator.Click
    Try
      txtCANFunctionResponse.Text = ""

      Me.Cursor = Cursors.WaitCursor
      gCANActuator.ShutDown()
      If gCANActuator.Anomaly IsNot Nothing Then
        txtCANFunctionResponse.Text = "Error"
        gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
        gCANActuator.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gCANActuator = Nothing

      'gCanActuator = New clsCANActuator("C:\EE's\Configuration\", "Lysander")
      gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)

      If gCANActuator.Anomaly IsNot Nothing Then
        txtCANFunctionResponse.Text = "Error"
        gAnomaly = New clsDbAnomaly(gCANActuator.Anomaly, gDatabase)
        gCANActuator.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      txtCANFunctionResponse.Text = "Success"

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      gAnomaly = Nothing
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Private Sub cboCANFunctions_Click(sender As Object, e As EventArgs) Handles cboCANFunctions.Click
    txtCANFunctionResponse.Text = " "
    txtCANFunctionResponse.Refresh()
    Application.DoEvents()
  End Sub
End Class
