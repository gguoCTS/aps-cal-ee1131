Option Explicit On
Option Strict Off
Public Class MpcValue

    Private mAbscissa1 As Double
    Private mAbscissa2 As Double
    Private mOrdinate1 As Double
    Private mOrdinate2 As Double

    Public Sub New()
        Me.mAbscissa1 = Double.NaN
        Me.mAbscissa2 = Double.NaN
        Me.mOrdinate1 = Double.NaN
        Me.mOrdinate2 = Double.NaN
    End Sub

    Public Sub New(ByVal abscissa1 As Double, ByVal abscissa2 As Double, _
                    ByVal ordinate1 As Double, ByVal ordinate2 As Double)
        Me.mAbscissa1 = abscissa1
        Me.mAbscissa2 = abscissa2
        Me.mOrdinate1 = ordinate1
        Me.mOrdinate2 = ordinate2
    End Sub

    Public Property Abscissa1() As Double
        Get
            Return Me.mAbscissa1
        End Get
        Set(ByVal value As Double)
            Me.mAbscissa1 = value
        End Set
    End Property

    Public Property Abscissa2() As Double
        Get
            Return Me.mAbscissa2
        End Get
        Set(ByVal value As Double)
            Me.mAbscissa2 = value
        End Set
    End Property

    Public Property Ordinate1() As Double
        Get
            Return Me.mOrdinate1
        End Get
        Set(ByVal value As Double)
            Me.mOrdinate1 = value
        End Set
    End Property

    Public Property Ordinate2() As Double
        Get
            Return Me.mOrdinate2
        End Get
        Set(ByVal value As Double)
            Me.mOrdinate2 = value
        End Set
    End Property

End Class
