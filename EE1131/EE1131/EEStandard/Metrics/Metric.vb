Option Explicit On
Option Strict Off

Public Class Metric

  Private mAlphaMetricValue As String
  Private mDomainUnitsOfMeasure As String
  Private mEnabled As Boolean = True      'default to enabled
  Private mFailHighCount As Int16
  Private mFailLowCount As Int16
  Private mFunctionMetricID As Guid
  Private mstrGraphName As String
  Private mlocation As Double = 0
  Private mMethodArgsTable As DataTable
  Private mMethodName As String
  Private mMetricName As String
  Private mMetricType As MetricTypeEnum
  Private mMetricValue As Double
  Private mMPC As New MetricPerformanceCriteria
  Private mPassFailDeterminationType As PassFailDeterminationTypeEnum
  Private mPassFailStatus As PassFailStatusEnum
  Private mblnProcessed As Boolean
  Private mblnReportOnly As Boolean
  Private mintRoundingDigits As Integer = -1
  Private mStatistics As New Statistics
  Private mUnitsOfMeasure As String


  Public Enum PassFailDeterminationTypeEnum As Integer
    None = 0
    SingleRegion = 1
    PercentTolerance = 2
    SingleRegionHigh = 3
    SingleRegionLow = 4
    ProgSingleRegionLookupPropertyMPC = 5
    SingleRegionGreaterThanLowLimit = 6
    CompareAlphaMPC = 7
  End Enum

  Public Enum MetricTypeEnum As Integer
    FunctionalTest = 1
    Programming = 2
    Alpha = 3 ' 2.0.1.0 TER
    GraphOnly = 4 ' 3.1.0.3 TER
  End Enum

  Public Enum PassFailStatusEnum As Integer
    Good = 1
    Reject = 2
    ReportOnly = 3
    NoData = 4
  End Enum

  Public Property AlphaMetricValue() As String
    Get
      Return Me.mAlphaMetricValue
    End Get
    Set(ByVal value As String)
      Me.mAlphaMetricValue = value
    End Set
  End Property

  Public Property DomainUnitsOfMeasure() As String
    Get
      Return Me.mDomainUnitsOfMeasure
    End Get
    Set(ByVal value As String)
      Me.mDomainUnitsOfMeasure = value
    End Set
  End Property


  Public Property Enabled() As Boolean
    Get
      Return Me.mEnabled
    End Get
    Set(ByVal value As Boolean)
      Me.mEnabled = value
    End Set
  End Property

  Public Property FunctionMetricID() As Guid
    Get
      Return Me.mFunctionMetricID
    End Get
    Set(ByVal value As Guid)
      Me.mFunctionMetricID = value
    End Set
  End Property

  Public Property GraphName() As String
    Get
      Return Me.mstrGraphName
    End Get
    Set(ByVal value As String)
      Me.mstrGraphName = value
    End Set
  End Property

  Public Property MethodArgsDataTable() As DataTable
    Get
      Return Me.mMethodArgsTable
    End Get
    Set(ByVal value As DataTable)
      Me.mMethodArgsTable = value
    End Set
  End Property

  Public Property MethodName() As String
    Get
      Return Me.mMethodName
    End Get
    Set(ByVal value As String)
      Me.mMethodName = value
    End Set
  End Property

  Public Property MetricLocation() As Double
    Get
      Return Me.mlocation
    End Get
    Set(ByVal value As Double)
      Me.mlocation = value
    End Set
  End Property

  Public Property MetricName() As String
    Get
      Return Me.mMetricName
    End Get
    Set(ByVal value As String)
      Me.mMetricName = value
    End Set
  End Property

  Public Property MetricType() As MetricTypeEnum
    Get
      Return Me.mMetricType
    End Get
    Set(ByVal value As MetricTypeEnum)
      Me.mMetricType = value
    End Set
  End Property

  Public Property MetricValue() As Double
    Get
      Return Me.mMetricValue
    End Get
    Set(ByVal value As Double)
      Me.mMetricValue = value
    End Set
  End Property

  Public Property MPC() As MetricPerformanceCriteria
    Get
      Return Me.mMPC
    End Get
    Set(ByVal value As MetricPerformanceCriteria)
      Me.mMPC = value
    End Set
  End Property

  Public Property PassFailStatus() As PassFailStatusEnum
    Get
      Return Me.mPassFailStatus
    End Get
    Set(ByVal value As PassFailStatusEnum)
      Me.mPassFailStatus = value
    End Set
  End Property

  Public Property PassFailDeterminationType() As PassFailDeterminationTypeEnum
    Get
      Return Me.mPassFailDeterminationType
    End Get
    Set(ByVal value As PassFailDeterminationTypeEnum)
      Me.mPassFailDeterminationType = value
    End Set
  End Property

  Public Property Processed() As Boolean
    Get
      Return Me.mblnProcessed
    End Get
    Set(ByVal value As Boolean)
      Me.mblnProcessed = value
    End Set
  End Property

  Public Property ReportOnly() As Boolean
    Get
      Return Me.mblnReportOnly
    End Get
    Set(ByVal value As Boolean)
      Me.mblnReportOnly = value
    End Set
  End Property

  Public Property RoundingDigits As Integer
    Get
      Return mintRoundingDigits
    End Get
    Set(value As Integer)
      mintRoundingDigits = value
    End Set
  End Property

  Public Property Statistics() As Statistics
    Get
      Return Me.mStatistics
    End Get
    Set(ByVal value As Statistics)
      Me.mStatistics = value
    End Set
  End Property

  Public Property UnitsOfMeasure() As String
    Get
      Return Me.mUnitsOfMeasure
    End Get
    Set(ByVal value As String)
      Me.mUnitsOfMeasure = value
    End Set
  End Property


  Public Sub New()

  End Sub
End Class
