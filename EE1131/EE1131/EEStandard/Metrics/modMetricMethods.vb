Imports System.Reflection

Module modMetricMethods
  Dim gTestResult As Boolean
  'Public Methods As MetricMethods
  Public Sub CalcAbsoluteLinearityMetric(ByVal guidMetricID As Guid) '2.0.1.0 TER
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim intMaxIndex As Integer
    Dim intMinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Call CalcLinAbsolute(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.IdealIndex1Value, gAmad.IdealIndex1Location, gAmad.IdealSlope, gAmad.EvaluationInterval, gDataArrays.AbsoluteLinearityArray)

      Call FindMaxWithMPC(gDataArrays.AbsoluteLinearityArray, intMaxIndex, guidMetricID)
      Call FindMinWithMPC(gDataArrays.AbsoluteLinearityArray, intMinIndex, guidMetricID)

      If Math.Abs(gDataArrays.AbsoluteLinearityArray(intMaxIndex)) > Math.Abs(gDataArrays.AbsoluteLinearityArray(intMinIndex)) Then
        metMetric.MetricValue = gDataArrays.AbsoluteLinearityArray(intMaxIndex)
        metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      Else
        metMetric.MetricValue = gDataArrays.AbsoluteLinearityArray(intMinIndex)
        metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcAbsoluteLinearityMetricPercentTolerance(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable.Copy
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable.Copy
      dtIdeal = metMetric.MPC.IdealValueRegionsTable.Copy

      Call CalcLinAbsolute(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.IdealIndex1Value, gAmad.IdealIndex1Location, gAmad.IdealSlope, gAmad.EvaluationInterval, gDataArrays.AbsoluteLinearityArray)
      ReDim PercentTolArray(gDataArrays.AbsoluteLinearityArray.GetUpperBound(0))
      Call CalcPercentTolWithIdeal(gDataArrays.AbsoluteLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, MaxIndex)

      metMetric.MetricValue = gDataArrays.AbsoluteLinearityArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcAbsoluteLinearityPerTolMetricSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMPCMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metMetricMPC As Metric
    Dim MaxIndex As Integer
    Dim dblIdealIndexValue As Double
    Dim dblIdealIndexLocation As Double
    Dim dblIdealSlope As Double
    Dim DutOutputArray() As Double
    Dim AbsoluteLinearityArray() As Double

    Try

      DutOutputArray = Nothing
      AbsoluteLinearityArray = Nothing
      'PercentTolArray = Nothing

      Select Case intOutputNumber
        Case 1
          dblIdealIndexValue = gAmad.IdealIndex1Output1
          dblIdealIndexLocation = gAmad.IdealIndex1Output1Location
          dblIdealSlope = gAmad.IdealSlopeOutput1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          dblIdealIndexValue = gAmad.IdealIndex1Output2
          dblIdealIndexLocation = gAmad.IdealIndex1Output2Location
          dblIdealSlope = gAmad.IdealSlopeOutput2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metMetric = gMetrics(guidMetricID)
      metMetricMPC = gMetrics(guidMPCMetricID)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable.Copy
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable.Copy
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable.Copy

      Call CalcLinAbsolute(DutOutputArray, gAmad.EvaluationStart, gAmad.EvaluationStop, dblIdealIndexValue, dblIdealIndexLocation, dblIdealSlope, gAmad.EvaluationInterval, AbsoluteLinearityArray)
      Select Case intOutputNumber
        Case 1
          gDataArrays.AbsoluteLinearityDutOut1Array = AbsoluteLinearityArray
        Case 2
          gDataArrays.AbsoluteLinearityDutOut2Array = AbsoluteLinearityArray
        Case Else
      End Select

      ReDim PercentTolArray(AbsoluteLinearityArray.Length - 1)

      Call CalcPercentTolWithIdeal(AbsoluteLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      Call FindMax(PercentTolArray, MaxIndex)

      metMetric.MetricValue = PercentTolArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcLinearityPerTolMetricSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMPCMetricID As Guid, ByVal guidRPMPCMetricID As Guid, ByVal guidI1MPCMetricID As Guid, ByVal guidI2MPCMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metMetricMPC As Metric
    Dim metMetricRPMPC As Metric
    Dim metMetricI1MPC As Metric
    Dim metMetricI2MPC As Metric
    Dim MaxIndex As Integer
    Dim dblIdealIndexValue As Double
    Dim dblIdealIndexLocation As Double
    Dim dblIdealSlope As Double
    Dim DutOutputArray() As Double
    Dim DutEncoderArray() As Double
    Dim AbsoluteLinearityArray() As Double
    Dim AbsoluteLinearityArray2() As Double
    Dim AbsoluteLinearityArray3() As Double
    Dim lintRPPos As Integer
    Dim lsngRPPos As Single
    Dim t As Integer

    Try

      DutOutputArray = Nothing
      DutEncoderArray = Nothing
      AbsoluteLinearityArray = Nothing
      AbsoluteLinearityArray2 = Nothing
      AbsoluteLinearityArray3 = Nothing
      'PercentTolArray = Nothing

      Select Case intOutputNumber
        Case 1
          dblIdealIndexValue = gAmad.IdealIndex1Output1
          dblIdealIndexLocation = gAmad.IdealIndex1Output1Location
          dblIdealSlope = gAmad.IdealSlopeOutput1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          dblIdealIndexValue = gAmad.IdealIndex1Output2
          dblIdealIndexLocation = gAmad.IdealIndex1Output2Location
          dblIdealSlope = gAmad.IdealSlopeOutput2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metMetric = gMetrics(guidMetricID)
      metMetricMPC = gMetrics(guidMPCMetricID)
      metMetricRPMPC = gMetrics(guidRPMPCMetricID)
      metMetricI1MPC = gMetrics(guidI1MPCMetricID)
      metMetricI2MPC = gMetrics(guidI2MPCMetricID)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable.Copy
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable.Copy
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable.Copy

      If metMetricRPMPC.MetricValue <> 0 Then
        ReDim AbsoluteLinearityArray(CInt(Math.Abs(gAmad.EvaluationStop - gAmad.EvaluationStart) / gAmad.EvaluationInterval))

        DutEncoderArray = gDataArrays.XPositionArray

        For t = 0 To DutEncoderArray.Length - 1
          If DutEncoderArray(t) > metMetricRPMPC.MetricValue Then
            lintRPPos = t
            lsngRPPos = DutEncoderArray(t)
            Exit For
          End If
        Next t

        Call CalcLinAbsolute(DutOutputArray, gAmad.EvaluationStart, lsngRPPos, dblIdealIndexValue, dblIdealIndexLocation, 0, gAmad.EvaluationInterval, AbsoluteLinearityArray2)
        'Call CalcLinAbsolute(DutOutputArray, lsngRPPos, gAmad.EvaluationStop, dblIdealIndexValue, lsngRPPos, dblIdealSlope, gAmad.EvaluationInterval, AbsoluteLinearityArray3)
        Call CalcLinTwoPoint(DutOutputArray, lsngRPPos, gAmad.EvaluationStop, metMetricI1MPC.MetricValue, metMetricRPMPC.MetricValue, metMetricI2MPC.MetricValue, metMetricI2MPC.MetricLocation, gAmad.EvaluationInterval, AbsoluteLinearityArray3)

        For t = 0 To AbsoluteLinearityArray.Length - 1
          If t < lintRPPos Then
            AbsoluteLinearityArray(t) = AbsoluteLinearityArray2(t)
          Else
            AbsoluteLinearityArray(t) = AbsoluteLinearityArray3(t - lintRPPos)
          End If
        Next t

      Else
        Call CalcLinAbsolute(DutOutputArray, gAmad.EvaluationStart, gAmad.EvaluationStop, dblIdealIndexValue, dblIdealIndexLocation, dblIdealSlope, gAmad.EvaluationInterval, AbsoluteLinearityArray)
      End If

      Select Case intOutputNumber
        Case 1
          gDataArrays.AbsoluteLinearityDutOut1Array = AbsoluteLinearityArray
        Case 2
          gDataArrays.AbsoluteLinearityDutOut2Array = AbsoluteLinearityArray
        Case Else
      End Select

      ReDim PercentTolArray(AbsoluteLinearityArray.Length - 1)

      Call CalcPercentTolWithIdeal(AbsoluteLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      Call FindMax(PercentTolArray, MaxIndex)

      metMetric.MetricValue = PercentTolArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub CalcAbsoluteLinearityPerTolMetricWithDynamicEvalRangeAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMPCMetricID As Guid)
  '  Dim dtHighLimit As New DataTable
  '  Dim dtLowLimit As New DataTable
  '  Dim dtIdeal As New DataTable
  '  Dim PercentTolArray() As Double
  '  Dim metMetric As Metric
  '  Dim metMetricMPC As Metric
  '  Dim MaxIndex As Integer
  '  Dim dblIdealIndexValue As Double
  '  Dim dblIdealIndexLocation As Double
  '  Dim dblIdealSlope As Double
  '  Dim DutOutputArray() As Double
  '  Dim AbsoluteLinearityArray() As Double

  '  Try

  '    'Modify Per Tol Metric MPC Ideal with new Abscissa value
  '    Call ModifyMpcIdealAbscissasWithEvalRange(guidMetricID)

  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissasWithEvalRange(guidMPCMetricID)

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMPCMetricID)

  '    DutOutputArray = Nothing
  '    AbsoluteLinearityArray = Nothing

  '    dblIdealIndexValue = gAmad.IdealIndex1Value
  '    dblIdealIndexLocation = gAmad.IdealIndex1Location
  '    dblIdealSlope = gAmad.IdealSlope
  '    DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray

  '    metMetric = gMetrics(guidMetricID)
  '    metMetricMPC = gMetrics(guidMPCMetricID)

  '    dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable.Copy
  '    dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable.Copy
  '    dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable.Copy

  '    Call CalcLinAbsolute(DutOutputArray, gAmad.EvaluationStart, gAmad.EvaluationStop, dblIdealIndexValue, dblIdealIndexLocation, dblIdealSlope, gAmad.EvaluationInterval, AbsoluteLinearityArray)

  '    gDataArrays.AbsoluteLinearityArray = AbsoluteLinearityArray

  '    ReDim PercentTolArray(AbsoluteLinearityArray.Length - 1)

  '    Call CalcPercentTolWithIdeal(AbsoluteLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
  '    If gAnomaly IsNot Nothing Then
  '      Throw New TsopAnomalyException
  '    End If
  '    Call FindMax(PercentTolArray, MaxIndex)

  '    metMetric.MetricValue = PercentTolArray(MaxIndex)
  '    metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcAbsoluteLinearityMaxSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim MaxIndex As Integer
    Dim AbsoluteLinearityArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      AbsoluteLinearityArray = Nothing

      Select Case intOutputNumber
        Case 1
          AbsoluteLinearityArray = gDataArrays.AbsoluteLinearityDutOut1Array
        Case 2
          AbsoluteLinearityArray = gDataArrays.AbsoluteLinearityDutOut2Array
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMaxWithMpcAllRegions(AbsoluteLinearityArray, MaxIndex, guidMpcMetricID)

      If MaxIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = AbsoluteLinearityArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcAbsoluteLinearityMinSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim MinIndex As Integer
    Dim AbsoluteLinearityArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      AbsoluteLinearityArray = Nothing

      Select Case intOutputNumber
        Case 1
          AbsoluteLinearityArray = gDataArrays.AbsoluteLinearityDutOut1Array
        Case 2
          AbsoluteLinearityArray = gDataArrays.AbsoluteLinearityDutOut2Array
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMinWithMpcAllRegions(AbsoluteLinearityArray, MinIndex, guidMpcMetricID)

      If MinIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = AbsoluteLinearityArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub CalcAbsLinearityMetricPercentTolDynamicAbscissa(ByVal guidMetricID As Guid, ByVal abscissaRegionNumber As Integer, ByVal abscissaNumber As Integer, ByVal abscissaValue As Double)
  '  ' 3.1.0.2 TER new routine
  '  Dim dtHighLimit As New DataTable
  '  Dim dtLowLimit As New DataTable
  '  Dim dtIdeal As New DataTable
  '  Dim PercentTolArray() As Double
  '  Dim metMetric As Metric
  '  Dim MaxIndex As Integer

  '  Try

  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissa(guidMetricID, abscissaRegionNumber, abscissaNumber, abscissaValue)

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMetricID)

  '    metMetric = gMetrics(guidMetricID)
  '    dtHighLimit = metMetric.MPC.HighLimitRegionsTable
  '    dtLowLimit = metMetric.MPC.LowLimitRegionsTable
  '    dtIdeal = metMetric.MPC.IdealValueRegionsTable

  '    Call CalcLinAbsolute(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.IdealIndex1Value, gAmad.IdealIndex1Location, gAmad.IdealSlope, gAmad.EvaluationInterval, gDataArrays.AbsoluteLinearityArray)
  '    ReDim PercentTolArray(gDataArrays.AbsoluteLinearityArray.GetUpperBound(0))
  '    Call CalcPercentTolWithIdeal(gDataArrays.AbsoluteLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
  '    Call FindMax(PercentTolArray, MaxIndex)

  '    'metMetric.MetricValue = gDataArrays.AbsoluteLinearityArray(MaxIndex)' 3.1.0.2 TER
  '    metMetric.MetricValue = PercentTolArray(MaxIndex) ' 3.1.0.2 TER
  '    metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcAbsoluteLinearityPeakNegative(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid)
    ' 3.1.0.2 TER
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMinNegativeWithMpcAllRegions(gDataArrays.AbsoluteLinearityArray, MinIndex, guidMpcMetricID)

      If MinIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = gDataArrays.AbsoluteLinearityArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcAbsoluteLinearityPeakPositive(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid)
    ' 3.1.0.2 TER
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxPositiveWithMpcAllRegions(gDataArrays.AbsoluteLinearityArray, MaxIndex, guidMpcMetricID)

      If MaxIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = gDataArrays.AbsoluteLinearityArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcDutShaftTravelMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim dblShaftTravel As Double
    Try

      metMetric = gMetrics(guidMetricID)

      Call FindDutShaftTravel(gDataArrays.ForwardForceValueArray, gDataArrays.ForwardEncoderPositionArray, dblShaftTravel)

      metMetric.MetricValue = dblShaftTravel

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcENRMetricMax(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intMaxIndex As Integer
    Dim intLimitCounter As Int16 = 0

    Try
      metMetric = gMetrics(guidMetricID)
      Call FindMax(gDataArrays.ForwardEvaluatedENRArray, intMaxIndex)

      metMetric.MetricValue = gDataArrays.ForwardEvaluatedENRArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub CalcENRMetricMaxDynamicAbscissa(ByVal guidMetricID As Guid, ByVal abscissaRegionNumber As Integer, ByVal abscissaNumber As Integer, ByVal abscissaValue As Double)
  '  ' 3.1.0.2 TER
  '  Dim metMetric As Metric
  '  Dim intMaxIndex As Integer
  '  Dim intLimitCounter As Int16 = 0

  '  Try
  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissa(guidMetricID, abscissaRegionNumber, abscissaNumber, abscissaValue)
  '    If gAnomaly IsNot Nothing Then
  '      Throw New TsopAnomalyException
  '    End If

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMetricID)
  '    If gAnomaly IsNot Nothing Then
  '      Throw New TsopAnomalyException
  '    End If

  '    metMetric = gMetrics(guidMetricID)
  '    Call FindMaxWithMpcAllRegions(gDataArrays.ForwardEvaluatedENRArray, intMaxIndex, guidMetricID)
  '    If gAnomaly IsNot Nothing Then
  '      Throw New TsopAnomalyException
  '    End If

  '    metMetric.MetricValue = gDataArrays.ForwardEvaluatedENRArray(intMaxIndex)
  '    metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcForceAvgMetric(ByVal guidAvgMetric1ID As Guid, ByVal guidMetric1ID As Guid, ByVal guidMetric2ID As Guid)
    Dim metMetricAvg As Metric
    Dim metMetric1 As Metric
    Dim metMetric2 As Metric
    Try

      metMetricAvg = gMetrics(guidAvgMetric1ID)
      metMetric1 = gMetrics(guidMetric1ID)
      metMetric2 = gMetrics(guidMetric2ID)

      metMetricAvg.MetricValue = (metMetric1.MetricValue + metMetric2.MetricValue) / 2
      metMetricAvg.MetricLocation = metMetric1.MetricLocation

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcForceValueAtIndexForward(ByVal guidMetricID As Guid, ByVal IndexPosition As Double)
    Dim metMetric As Metric
    Dim intValueIndex As Integer
    Dim dblForceValue As Double
    Try

      metMetric = gMetrics(guidMetricID)
      intValueIndex = CInt((IndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      dblForceValue = gDataArrays.ForwardPositionBasedForceArray(intValueIndex)

      metMetric.MetricValue = dblForceValue
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcForceValueAtIndexReverse(ByVal guidMetricID As Guid, ByVal IndexPosition As Double)
    Dim metMetric As Metric
    Dim intValueIndex As Integer
    Dim dblForceValue As Double
    Try

      metMetric = gMetrics(guidMetricID)
      intValueIndex = CInt((IndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      dblForceValue = gDataArrays.ReversePositionBasedForceArray(intValueIndex)

      metMetric.MetricValue = dblForceValue
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresisMetricSelectOutput(ByVal guidMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim MaxIndex As Integer
    Dim MinIndex As Integer
    Dim FwdDutArray() As Double
    Dim RevDutArray() As Double
    Dim HysteresisArray() As Double

    Try

      FwdDutArray = Nothing
      RevDutArray = Nothing
      HysteresisArray = Nothing

      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Select Case intOutputNumber
        Case 1
          FwdDutArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
          RevDutArray = gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray
        Case 2
          FwdDutArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
          RevDutArray = gDataArrays.ReverseDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call CalcHysteresis(FwdDutArray, RevDutArray, HysteresisArray)
      Select Case intOutputNumber
        Case 1
          gDataArrays.HysteresisDutOut1Array = HysteresisArray
        Case 2
          gDataArrays.HysteresisDutOut2Array = HysteresisArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException

      End Select
      Call FindMaxWithMPC(HysteresisArray, MaxIndex, guidMetricID)
      Call FindMinWithMPC(HysteresisArray, MinIndex, guidMetricID)

      If Math.Abs(HysteresisArray(MaxIndex)) > Math.Abs(HysteresisArray(MinIndex)) Then
        metMetric.MetricValue = HysteresisArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      Else
        metMetric.MetricValue = HysteresisArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresisMetric(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    'Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim MaxIndex As Integer
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Call CalcHysteresis(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray, gDataArrays.HysteresisArray)

      Call FindMaxWithMPC(gDataArrays.HysteresisArray, MaxIndex, guidMetricID)
      Call FindMinWithMPC(gDataArrays.HysteresisArray, MinIndex, guidMetricID)

      If Math.Abs(gDataArrays.HysteresisArray(MaxIndex)) > Math.Abs(gDataArrays.HysteresisArray(MinIndex)) Then
        metMetric.MetricValue = gDataArrays.HysteresisArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      Else
        metMetric.MetricValue = gDataArrays.HysteresisArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub CalcHysteresisMetricDynamicEvalRange(ByVal guidMetricID As Guid)
  '  Dim dtHighLimit As New DataTable
  '  Dim dtLowLimit As New DataTable
  '  Dim dtIdeal As New DataTable
  '  'Dim PercentTolArray() As Double
  '  Dim metMetric As Metric
  '  Dim MaxIndex As Integer
  '  Dim MinIndex As Integer

  '  Try

  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissasWithEvalRange(guidMetricID)

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMetricID)

  '    metMetric = gMetrics(guidMetricID)

  '    dtHighLimit = metMetric.MPC.HighLimitRegionsTable
  '    dtLowLimit = metMetric.MPC.LowLimitRegionsTable
  '    dtIdeal = metMetric.MPC.IdealValueRegionsTable

  '    Call CalcHysteresis(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray, gDataArrays.HysteresisArray)

  '    Call FindMaxWithMPC(gDataArrays.HysteresisArray, MaxIndex, guidMetricID)
  '    Call FindMinWithMPC(gDataArrays.HysteresisArray, MinIndex, guidMetricID)

  '    If Math.Abs(gDataArrays.HysteresisArray(MaxIndex)) > Math.Abs(gDataArrays.HysteresisArray(MinIndex)) Then
  '      metMetric.MetricValue = gDataArrays.HysteresisArray(MaxIndex)
  '      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
  '    Else
  '      metMetric.MetricValue = gDataArrays.HysteresisArray(MinIndex)
  '      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
  '    End If

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcHysteresisPeakNegative(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid)
    ' 3.1.0.2 TER
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMinNegativeWithMpcAllRegions(gDataArrays.HysteresisArray, MinIndex, guidMpcMetricID)

      If MinIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = gDataArrays.HysteresisArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresisPeakPositive(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid)
    ' 3.1.0.2 TER
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxPositiveWithMpcAllRegions(gDataArrays.HysteresisArray, MaxIndex, guidMpcMetricID)

      If MaxIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = gDataArrays.HysteresisArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub CalcHysteresisMetricDynamicAbscissa(ByVal guidMetricID As Guid, ByVal abscissaRegionNumber As Integer, ByVal abscissaNumber As Integer, ByVal abscissaValue As Double)
  '  ' 3.1.0.2 TER
  '  Dim dtHighLimit As New DataTable
  '  Dim dtLowLimit As New DataTable
  '  Dim dtIdeal As New DataTable
  '  Dim metMetric As Metric
  '  Dim MaxIndex As Integer
  '  Dim MinIndex As Integer

  '  Try
  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissa(guidMetricID, abscissaRegionNumber, abscissaNumber, abscissaValue)

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMetricID)

  '    metMetric = gMetrics(guidMetricID)

  '    dtHighLimit = metMetric.MPC.HighLimitRegionsTable
  '    dtLowLimit = metMetric.MPC.LowLimitRegionsTable
  '    dtIdeal = metMetric.MPC.IdealValueRegionsTable

  '    Call CalcHysteresis(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray, gDataArrays.HysteresisArray)

  '    Call FindMaxWithMPC(gDataArrays.HysteresisArray, MaxIndex, guidMetricID)
  '    Call FindMinWithMPC(gDataArrays.HysteresisArray, MinIndex, guidMetricID)

  '    If Math.Abs(gDataArrays.HysteresisArray(MaxIndex)) > Math.Abs(gDataArrays.HysteresisArray(MinIndex)) Then
  '      metMetric.MetricValue = gDataArrays.HysteresisArray(MaxIndex)
  '      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
  '    Else
  '      metMetric.MetricValue = gDataArrays.HysteresisArray(MinIndex)
  '      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
  '    End If

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcHysteresisMetricMax(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxWithMPC(gDataArrays.HysteresisArray, MaxIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.HysteresisArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresisMetricMin(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      Call FindMinWithMPC(gDataArrays.HysteresisArray, MinIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.HysteresisArray(MinIndex)
      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresisMetricPercentTolerance_notUsed(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    'Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim MaxIndex As Integer
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Call CalcHysteresis(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray, gDataArrays.HysteresisArray)
      'Call CalcHysteresis(gDataArrays.ForwardPositionBasedPercentVrefArray, gDataArrays.ReversePositionBasedPercentVrefArray, gDataArrays.XPositionArray)

      'ReDim PercentTolArray(gDataArrays.HysteresisArray.GetUpperBound(0))

      'Call CalcPercentTolWithIdeal(gDataArrays.HysteresisArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMaxWithMPC(gDataArrays.HysteresisArray, MaxIndex, guidMetricID)
      Call FindMinWithMPC(gDataArrays.HysteresisArray, MinIndex, guidMetricID)

      If Math.Abs(gDataArrays.HysteresisArray(MaxIndex)) > Math.Abs(gDataArrays.HysteresisArray(MinIndex)) Then
        metMetric.MetricValue = gDataArrays.HysteresisArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      Else
        metMetric.MetricValue = gDataArrays.HysteresisArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcHysteresisMetricPercentTolerance(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Call CalcHysteresis(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray, gDataArrays.HysteresisArray)

      ReDim PercentTolArray(gDataArrays.HysteresisArray.GetUpperBound(0))

      Call CalcPercentTolWithIdeal(gDataArrays.HysteresisArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, MaxIndex)

      metMetric.MetricValue = PercentTolArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcI1I2Length(ByVal guidMetricLocation1ID As Guid, ByVal guidMetricLocation2ID As Guid, ByVal guidMetricValueID As Guid)
    Dim metMetricLocation1
    Dim metMetricLocation2
    Dim metMetric As Metric

    Try
      metMetricLocation1 = gMetrics(guidMetricLocation1ID)
      metMetricLocation2 = gMetrics(guidMetricLocation2ID)
      metMetric = gMetrics(guidMetricValueID)

      metMetric.MetricValue = metMetricLocation2.MetricValue - metMetricLocation1.MetricValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcIndLinearityMetricMax(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxWithMPC(gDataArrays.IndLinearityArray, MaxIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.IndLinearityArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcIndLinearityMetricMin(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMinWithMPC(gDataArrays.IndLinearityArray, MinIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.IndLinearityArray(MinIndex)
      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  'Public Sub CalcIndLinearityMetric(ByVal guidMetricID As Guid)
  '  Dim dtHighLimit As New DataTable
  '  Dim dtLowLimit As New DataTable
  '  Dim dtIdeal As New DataTable
  '  Dim metMetric As Metric
  '  Dim intMaxIndex As Integer
  '  Dim intMinIndex As Integer

  '  Try
  '    metMetric = gMetrics(guidMetricID)

  '    dtHighLimit = metMetric.MPC.HighLimitRegionsTable
  '    dtLowLimit = metMetric.MPC.LowLimitRegionsTable
  '    dtIdeal = metMetric.MPC.IdealValueRegionsTable

  '    Call CalcIndependentLinearity(gDataArrays.ForwardPositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, False, gDataArrays.IndLinearityArray)

  '    Call FindMax(gDataArrays.IndLinearityArray, intMaxIndex)
  '    Call FindMin(gDataArrays.IndLinearityArray, intMinIndex)

  '    If Math.Abs(gDataArrays.IndLinearityArray(intMaxIndex)) > Math.Abs(gDataArrays.IndLinearityArray(intMinIndex)) Then
  '      metMetric.MetricValue = gDataArrays.IndLinearityArray(intMaxIndex)
  '      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
  '    Else
  '      metMetric.MetricValue = gDataArrays.IndLinearityArray(intMinIndex)
  '      metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
  '    End If

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub CalcIndLinearityMetric(ByVal guidMetricID As Guid) ' 3.0.0.0 TER
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim intMaxIndex As Integer
    Dim intMinIndex As Integer
    Dim dblILStart As Double
    Dim dblILStop As Double
    Dim intRows As Integer
    Dim dblLeastSquaresRegionStart As Double
    Dim dblLeastSquaresRegionStop As Double

    Try
      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      dblILStart = dtHighLimit.Rows(0).Item("Abscissa1")
      intRows = dtHighLimit.Rows.Count
      dblILStop = dtHighLimit.Rows(intRows - 1).Item("Abscissa2")

      'This was done for 586 CZ
      dblLeastSquaresRegionStart = dblILStart - dblILStop
      dblLeastSquaresRegionStop = dblILStop - dblILStop

      Call CalcIndependentLinearity(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, dblLeastSquaresRegionStart, dblLeastSquaresRegionStop, gAmad.EvaluationInterval, False, gDataArrays.IndLinearityArray)

      Call FindMax(gDataArrays.IndLinearityArray, intMaxIndex)
      Call FindMin(gDataArrays.IndLinearityArray, intMinIndex)

      If Math.Abs(gDataArrays.IndLinearityArray(intMaxIndex)) > Math.Abs(gDataArrays.IndLinearityArray(intMinIndex)) Then
        metMetric.MetricValue = gDataArrays.IndLinearityArray(intMaxIndex)
        metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      Else
        metMetric.MetricValue = gDataArrays.IndLinearityArray(intMinIndex)
        metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcIndLinearityMetricPercentTolerance(ByVal guidMetricID As Guid, ByVal guidMetricIDforMPC As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metMetricMPC As Metric
    Dim intMaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      metMetricMPC = gMetrics(guidMetricIDforMPC)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable

      ReDim PercentTolArray(gDataArrays.IndLinearityArray.GetUpperBound(0))

      Call CalcPercentTolWithIdeal(gDataArrays.IndLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, intMaxIndex)

      metMetric.MetricValue = PercentTolArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcIndLinearityTrend(ByVal metricID As Guid)
    Dim lsngLinTrendMaxValue As Single
    Dim lsngLinTrendMinValue As Single
    Dim lintLinTrendMaxLoc As Integer
    Dim lintLinTrendMinLoc As Integer
    Dim intLoop As Integer
    Dim metMetric As Metric
    Dim dblRegionStart As Double
    Dim dblRegionStop As Double

    Try
      metMetric = gMetrics(metricID)

      dblRegionStart = metMetric.MPC.IdealValueRegionsTable.Rows(0).Item("Abscissa1") - gAmad.EvaluationStart
      dblRegionStop = metMetric.MPC.IdealValueRegionsTable.Rows(0).Item("Abscissa2") - gAmad.EvaluationStart

      '*** Calculate Linearity Trend Information ***
      lsngLinTrendMaxValue = Single.MinValue '-100000   'Initialize to make sure we catch
      lsngLinTrendMinValue = Single.MaxValue '100000    'the linearity data

      For intLoop = dblRegionStart To dblRegionStop
        If gDataArrays.IndLinearityArray(intLoop) > lsngLinTrendMaxValue Then
          lsngLinTrendMaxValue = gDataArrays.IndLinearityArray(intLoop)
          lintLinTrendMaxLoc = intLoop
        End If
        If gDataArrays.IndLinearityArray(intLoop) < lsngLinTrendMinValue Then
          lsngLinTrendMinValue = gDataArrays.IndLinearityArray(intLoop)
          lintLinTrendMinLoc = intLoop
        End If
      Next

      'Define the linearity trend (one half the magnitude of the difference between the 2 peaks)
      'The linearity trend is signed positive if the max occurs farther into the scan than the
      'min, and signed negative if the min occurs farther into the scan than the max.
      'NOTE Because scan locations are referenced to ScanEnd, calculate the trend
      '     as follows:
      '       When lintLinTrendMaxLoc > lintLinTrendMinLoc...
      '            The min occurred after the max (w.r.t. to the forward scan).
      '       When lintLinTrendMaxLoc < lintLinTrendMinLoc...
      '            The max occurred after the min (w.r.t. to the forward scan).

      If lintLinTrendMaxLoc > lintLinTrendMinLoc Then     'Negative Trend
        metMetric.MetricValue = (lsngLinTrendMinValue - lsngLinTrendMaxValue) / 2
      Else                                                'Positive Trend
        metMetric.MetricValue = (lsngLinTrendMaxValue - lsngLinTrendMinValue) / 2
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMaxOutputMetricSelectOutput(ByVal guidMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim intMaxIndex As Integer
    Dim DutOutputArray() As Double

    Try
      DutOutputArray = Nothing

      metMetric = gMetrics(guidMetricID)

      Select Case intOutputNumber
        Case 1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      'Call FindArrayMaxBetweenPositions(DutOutputArray, gAmad.EvaluationStart, gAmad.EvaluationStop, intMaxIndex)
      Call FindMaxWithMpcAllRegions(DutOutputArray, intMaxIndex, guidMetricID)

      metMetric.MetricValue = DutOutputArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMicrogradientMetric(ByVal guidMetricID As Guid) '2.0.1.0 TER
    Dim metMetric As Metric
    Dim MaxIndex1 As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call CalcMicrogradient(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.IdealSlope, gAmad.EvaluationInterval, gDataArrays.MicrogradientArray)

      FindAbsoluteMaxWithMPC(gDataArrays.MicrogradientArray, MaxIndex1, guidMetricID)

      metMetric.MetricValue = (gDataArrays.MicrogradientArray(MaxIndex1))
      metMetric.MetricLocation = MaxIndex1 * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  'Public Sub CalcMicrogradientMetricDynamicAbscissa(ByVal guidMetricID As Guid, ByVal abscissaRegionNumber As Integer, ByVal abscissaNumber As Integer, ByVal abscissaValue As Double)
  '  ' 3.1.0.2 TER
  '  Dim metMetric As Metric
  '  Dim MaxIndex As Integer

  '  Try
  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissa(guidMetricID, abscissaRegionNumber, abscissaNumber, abscissaValue)

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMetricID)

  '    metMetric = gMetrics(guidMetricID)

  '    Call CalcMicrogradient(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.IdealSlope, gAmad.EvaluationInterval, gDataArrays.MicrogradientArray)

  '    FindAbsoluteMaxWithMPC(gDataArrays.MicrogradientArray, MaxIndex, guidMetricID)

  '    metMetric.MetricValue = (gDataArrays.MicrogradientArray(MaxIndex))
  '    metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try

  'End Sub

  'Public Sub CalcMicrogradientMetricDynamicEvalRange(ByVal guidMetricID As Guid)
  '  ' 3.1.0.2 TER
  '  Dim metMetric As Metric
  '  Dim MaxIndex As Integer

  '  Try
  '    'Modify MPC with new Abscissa value
  '    Call ModifyMpcAbscissasWithEvalRange(guidMetricID)

  '    'Redraw Limit Lines using new Abscissa value
  '    Call RedrawLimitLines(guidMetricID)

  '    metMetric = gMetrics(guidMetricID)

  '    Call CalcMicrogradient(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.IdealSlope, gAmad.EvaluationInterval, gDataArrays.MicrogradientArray)

  '    FindAbsoluteMaxWithMPC(gDataArrays.MicrogradientArray, MaxIndex, guidMetricID)

  '    metMetric.MetricValue = (gDataArrays.MicrogradientArray(MaxIndex))
  '    metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try

  'End Sub

  Public Sub CalcMicrogradientPeakNegative(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid)
    ' 3.1.0.2 TER
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMinNegativeWithMpcAllRegions(gDataArrays.MicrogradientArray, MinIndex, guidMpcMetricID)

      If MinIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = gDataArrays.MicrogradientArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMicrogradientPeakPositive(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid)
    ' 3.1.0.2 TER
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxPositiveWithMpcAllRegions(gDataArrays.MicrogradientArray, MaxIndex, guidMpcMetricID)

      If MaxIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = gDataArrays.MicrogradientArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMicrogradientPerTolMetricSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMPCMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metMetricMPC As Metric
    Dim MaxIndex As Integer
    Dim dblIdealSlope As Double
    Dim DutOutputArray() As Double
    Dim MicrogradientArray() As Double

    Try

      DutOutputArray = Nothing
      MicrogradientArray = Nothing

      Select Case intOutputNumber
        Case 1
          dblIdealSlope = gAmad.IdealSlopeOutput1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          dblIdealSlope = gAmad.IdealSlopeOutput2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metMetric = gMetrics(guidMetricID)
      metMetricMPC = gMetrics(guidMPCMetricID)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable.Copy
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable.Copy
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable.Copy

      Call CalcMicrogradient(DutOutputArray, gAmad.EvaluationStart, gAmad.EvaluationStop, dblIdealSlope, gAmad.EvaluationInterval, MicrogradientArray)
      Select Case intOutputNumber
        Case 1
          gDataArrays.MicrogradientDutOut1Array = MicrogradientArray
        Case 2
          gDataArrays.MicrogradientDutOut2Array = MicrogradientArray
      End Select

      ReDim PercentTolArray(MicrogradientArray.GetUpperBound(0))
      Call CalcPercentTolWithIdeal(MicrogradientArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, MaxIndex)

      metMetric.MetricValue = PercentTolArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMicrogradientMaxSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim MaxIndex As Integer
    Dim MicrogradientArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      MicrogradientArray = Nothing

      Select Case intOutputNumber
        Case 1
          MicrogradientArray = gDataArrays.MicrogradientDutOut1Array
        Case 2
          MicrogradientArray = gDataArrays.MicrogradientDutOut2Array
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMaxWithMpcAllRegions(MicrogradientArray, MaxIndex, guidMpcMetricID)

      If MaxIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = MicrogradientArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMicrogradientMinSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim MinIndex As Integer
    Dim MicrogradientArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      MicrogradientArray = Nothing

      Select Case intOutputNumber
        Case 1
          MicrogradientArray = gDataArrays.MicrogradientDutOut1Array
        Case 2
          MicrogradientArray = gDataArrays.MicrogradientDutOut2Array
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMinWithMpcAllRegions(MicrogradientArray, MinIndex, guidMpcMetricID)

      If MinIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = MicrogradientArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcMinOutputMetricSelectOutput(ByVal guidMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim intMinIndex As Integer
    Dim DutOutputArray() As Double

    Try
      DutOutputArray = Nothing

      metMetric = gMetrics(guidMetricID)

      Select Case intOutputNumber
        Case 1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMinWithMpcAllRegions(DutOutputArray, intMinIndex, guidMetricID)

      metMetric.MetricValue = DutOutputArray(intMinIndex)
      metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcOutputChangeMetric(ByVal guidMetricID As Guid, ByVal guidMetricV1ID As Guid, ByVal guidMetricV2ID As Guid)
    Dim metMetric As Metric
    Dim metMetricV1 As Metric
    Dim metMetricV2 As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      metMetricV1 = gMetrics(guidMetricV1ID)
      metMetricV2 = gMetrics(guidMetricV2ID)

      metMetric.MetricValue = metMetricV2.MetricValue - metMetricV1.MetricValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcOverallResistanceMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)

      metMetric.MetricValue = gAmad.OverallResistance

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcOverallResistanceMetricSelectOutput(ByVal guidMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim dblOverallResistance As Double
    Try
      metMetric = gMetrics(guidMetricID)

      Select Case intOutputNumber
        Case 1
          dblOverallResistance = gAmad.MeasuredOverallResOutput1
        Case 2
          dblOverallResistance = gAmad.MeasuredOverallResOutput2
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metMetric.MetricValue = dblOverallResistance

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPedalReturnMetric(ByVal guidMetricID As Guid, ByVal intMeasurementNumber As Integer)
    ' 2.0.1.0 TER

    Dim metMetric As Metric
    Dim dblPedalReturnTime As Double
    Try
      metMetric = gMetrics(guidMetricID)

      dblPedalReturnTime = gPedalReturnProperties(intMeasurementNumber - 1).ReturnTime * 1000

      metMetric.MetricValue = dblPedalReturnTime

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPedalReturnMetricAverage(ByVal guidMetricID As Guid)
    ' 2.0.1.0 TER

    Dim metMetric As Metric
    Dim intLoop As Integer
    Dim intCount As Integer
    Dim dblSum As Double
    Dim dblAverage As Double
    Try
      metMetric = gMetrics(guidMetricID)

      dblSum = 0
      intCount = 0
      For intLoop = 0 To gPedalReturnProperties.Length - 1
        dblSum = dblSum + gPedalReturnProperties(intLoop).ReturnTime * 1000
        intCount += 1
      Next

      dblAverage = dblSum / intCount

      metMetric.MetricValue = dblAverage

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPedalReturnMetricMax(ByVal guidMetricID As Guid)
    ' 2.0.1.0 TER
    Dim metMetric As Metric
    Dim intLoop As Integer
    Dim intMaxMeasurement As Integer
    Dim dblMax As Double
    Dim strArrayName As String
    Dim dblArray() As Double
    Dim myType As Type
    Dim myProperty As PropertyInfo

    Try
      myType = GetType(DataArrays)

      metMetric = gMetrics(guidMetricID)

      dblMax = Double.MinValue
      For intLoop = 0 To gPedalReturnProperties.Length - 1
        If gPedalReturnProperties(intLoop).ReturnTime * 1000 > dblMax Then
          dblMax = gPedalReturnProperties(intLoop).ReturnTime * 1000
          intMaxMeasurement = intLoop + 1
        End If
      Next
      gAmad.MeasurementNumberMax = intMaxMeasurement

      metMetric.MetricValue = dblMax

      strArrayName = "PedalReturnDut1VoltageArray" & CStr(intMaxMeasurement)
      myProperty = myType.GetProperty(strArrayName)
      dblArray = myProperty.GetValue(gDataArrays, Nothing)
      gDataArrays.PedalReturnDut1VoltageArrayMax = dblArray.Clone

      strArrayName = "PedalReturnDut2VoltageArray" & CStr(intMaxMeasurement)
      myProperty = myType.GetProperty(strArrayName)
      dblArray = myProperty.GetValue(gDataArrays, Nothing)
      gDataArrays.PedalReturnDut2VoltageArrayMax = dblArray.Clone

      strArrayName = "PedalReturnTimeArray" & CStr(intMaxMeasurement)
      myProperty = myType.GetProperty(strArrayName)
      dblArray = myProperty.GetValue(gDataArrays, Nothing)
      gDataArrays.PedalReturnTimeArrayMax = dblArray.Clone

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPedalReturnMetricStartTime(ByVal guidMetricID As Guid, ByVal intMeasurementNumber As Integer)
    ' 2.0.1.0 TER

    Dim metMetric As Metric
    Dim dblStartTime As Double
    Try
      metMetric = gMetrics(guidMetricID)

      dblStartTime = gPedalReturnProperties(intMeasurementNumber - 1).StartTime * 1000

      metMetric.MetricValue = dblStartTime

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPedalReturnMetricEndTime(ByVal guidMetricID As Guid, ByVal intMeasurementNumber As Integer)
    ' 2.0.1.0 TER

    Dim metMetric As Metric
    Dim dblEndTime As Double
    Try
      metMetric = gMetrics(guidMetricID)

      dblEndTime = gPedalReturnProperties(intMeasurementNumber - 1).EndTime * 1000

      metMetric.MetricValue = dblEndTime

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPercentTolWithIdealnew(ByVal linDevArray() As Double, ByVal highRegionsTable As DataTable, _
    ByVal lowRegionsTable As DataTable, ByVal IdealRegionsTable As DataTable, ByRef percentTolArray() As Double)
    Dim i As Int16
    Dim intRow As Int16
    Dim rowHigh As DataRow
    Dim rowLow As DataRow
    Dim rowIdeal As DataRow
    Dim intRowCount As Int16 = highRegionsTable.Rows.Count
    Dim HighLimit As Single
    Dim LowLimit As Single
    Dim Ideal As Single
    Dim intLimitShape As Int16

    'General Algorithm:
    'For each data point that is within the evaluation region defined by the MPC domain coordinates (Abscissas)
    'Calculate the High Tolerance, Low Tolerance, and Ideal Values for each data point
    'Determine if the data point value is above or below Ideal
    'If Above Ideal, calculate percentage of High Tolerance and store in percentTolArray at that data point's index
    'If Below Ideal, calculate percentage of Low Tolerance and store in percentTolArray at that data point's index

    Try
      rowHigh = highRegionsTable.Rows(0)
      intLimitShape = rowHigh("LimitShape")

      For i = 0 To linDevArray.GetUpperBound(0)
        If intLimitShape = MetricPerformanceCriteria.LimitShape.Point Then
          rowHigh = highRegionsTable.Rows(0)
          rowLow = lowRegionsTable.Rows(0)
          rowIdeal = IdealRegionsTable.Rows(0)
          HighLimit = rowHigh("Slope") * (i * gAmad.EvaluationInterval) + rowHigh("YIntercept")
          LowLimit = rowLow("Slope") * (i * gAmad.EvaluationInterval) + rowLow("YIntercept")
          Ideal = rowIdeal("Slope") * (i * gAmad.EvaluationInterval) + rowIdeal("YIntercept")
          If linDevArray(i) > Ideal Then
            percentTolArray(i) = (linDevArray(i)) / HighLimit * 100
          ElseIf linDevArray(i) < Ideal Then
            percentTolArray(i) = (linDevArray(i)) / LowLimit * 100
          Else
            percentTolArray(i) = 0
          End If
        Else
          For intRow = 0 To highRegionsTable.Rows.Count - 1
            rowHigh = highRegionsTable.Rows(intRow)
            rowLow = lowRegionsTable.Rows(intRow)
            rowIdeal = IdealRegionsTable.Rows(intRow)
            If (i * gAmad.EvaluationInterval) >= rowHigh("Abscissa1") And (i * gAmad.EvaluationInterval) <= rowHigh("Abscissa2") Then
              HighLimit = rowHigh("Slope") * (i * gAmad.EvaluationInterval) + rowHigh("YIntercept")
              LowLimit = rowLow("Slope") * (i * gAmad.EvaluationInterval) + rowLow("YIntercept")
              Ideal = rowIdeal("Slope") * (i * gAmad.EvaluationInterval) + rowIdeal("YIntercept")
              If linDevArray(i) > Ideal Then
                percentTolArray(i) = (linDevArray(i)) / HighLimit * 100
              ElseIf linDevArray(i) < Ideal Then
                percentTolArray(i) = (linDevArray(i)) / LowLimit * 100
              Else
                percentTolArray(i) = 0
              End If
            End If
          Next
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcPercentTolWithIdeal(ByVal linDevArray() As Double, ByVal highRegionsTable As DataTable, _
  ByVal lowRegionsTable As DataTable, ByVal IdealRegionsTable As DataTable, ByRef percentTolArray() As Double)
    Dim i As Int16
    Dim sngPosition As Single
    Dim intRow As Int16
    Dim rowHigh As DataRow
    Dim rowLow As DataRow
    Dim rowIdeal As DataRow
    Dim intRowCount As Int16 = highRegionsTable.Rows.Count
    Dim HighLimit As Single
    Dim LowLimit As Single
    Dim Ideal As Single
    Dim intLimitShape As Int16

    'General Algorithm:
    'For each data point that is within the evaluation region defined by the MPC domain coordinates (Abscissas)
    'Calculate the High Tolerance, Low Tolerance, and Ideal Values for each data point
    'Determine if the data point value is above or below Ideal
    'If Above Ideal, calculate percentage of High Tolerance and store in percentTolArray at that data point's index
    'If Below Ideal, calculate percentage of Low Tolerance and store in percentTolArray at that data point's index

    Try
      rowHigh = highRegionsTable.Rows(0)
      intLimitShape = rowHigh("LimitShape")

      For i = 0 To linDevArray.GetUpperBound(0)
        If intLimitShape = MetricPerformanceCriteria.LimitShape.Point Then
          rowHigh = highRegionsTable.Rows(0)
          rowLow = lowRegionsTable.Rows(0)
          rowIdeal = IdealRegionsTable.Rows(0)
          If rowIdeal("YIntercept") IsNot DBNull.Value And rowLow("YIntercept") IsNot DBNull.Value And rowHigh("YIntercept") IsNot DBNull.Value Then
            HighLimit = rowHigh("Slope") * (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) + rowHigh("")
            LowLimit = rowLow("Slope") * (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) + rowLow("YIntercept")
            Ideal = rowIdeal("Slope") * (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) + rowIdeal("YIntercept")
            If linDevArray(i) > Ideal Then
              percentTolArray(i) = (linDevArray(i)) / HighLimit * 100
            ElseIf linDevArray(i) < Ideal Then
              percentTolArray(i) = (linDevArray(i)) / LowLimit * 100
            Else
              percentTolArray(i) = 0
            End If
          End If
        Else
          For intRow = 0 To highRegionsTable.Rows.Count - 1
            rowHigh = highRegionsTable.Rows(intRow)
            rowLow = lowRegionsTable.Rows(intRow)
            rowIdeal = IdealRegionsTable.Rows(intRow)
            sngPosition = (i * gAmad.EvaluationInterval + gAmad.EvaluationStart)
            If sngPosition >= rowHigh("Abscissa1") And sngPosition <= rowHigh("Abscissa2") Then
              HighLimit = rowHigh("Slope") * sngPosition + rowHigh("YIntercept")
              LowLimit = rowLow("Slope") * sngPosition + rowLow("YIntercept")
              Ideal = rowIdeal("Slope") * sngPosition + rowIdeal("YIntercept")
              If linDevArray(i) > Ideal Then
                percentTolArray(i) = (linDevArray(i)) / HighLimit * 100
              ElseIf linDevArray(i) < Ideal Then
                percentTolArray(i) = (linDevArray(i)) / LowLimit * 100
              Else
                percentTolArray(i) = 0
              End If
            End If
          Next
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcRepeatabilityMetric(ByVal guidMetricID As Guid) ' 2.0.1.0 TER
    Dim metMetric As Metric
    Dim MaxIndexForward As Integer
    Dim MaxIndexReverse As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      'Calc Forward Repeatability
      Call CalcRepeatability(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ForwardPositionBasedPercentVrefArray2, gDataArrays.ForwardRepeatabilityValueArray)
      'Calc Reverse Repeatability
      Call CalcRepeatability(gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReversePositionBasedPercentVrefArray2, gDataArrays.ReverseRepeatabilityValueArray)

      'Find Forward Max
      FindAbsoluteMaxWithMPC(gDataArrays.ForwardRepeatabilityValueArray, MaxIndexForward, guidMetricID)
      'Find Reverse Max
      FindAbsoluteMaxWithMPC(gDataArrays.ReverseRepeatabilityValueArray, MaxIndexReverse, guidMetricID)

      'Find larger between forward and reverse
      If Math.Abs(gDataArrays.ReverseRepeatabilityValueArray(MaxIndexReverse)) > Math.Abs(gDataArrays.ForwardRepeatabilityValueArray(MaxIndexForward)) Then
        metMetric.MetricValue = gDataArrays.ReverseRepeatabilityValueArray(MaxIndexReverse)
        metMetric.MetricLocation = MaxIndexReverse * gAmad.EvaluationInterval + gAmad.EvaluationStart
      Else
        metMetric.MetricValue = gDataArrays.ForwardRepeatabilityValueArray(MaxIndexForward)
        metMetric.MetricLocation = MaxIndexForward * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CalcRisePointMetric(ByVal guidMetricID As Guid, ByVal guidIndex1ID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim metIndex1Metric As Metric
    'Dim StartLoc As Single
    Dim lsngRPLineStartVal As Single        'Rising Point Line Start Value   
    Dim lsngRPLineStartLoc As Single        'Rising Point Line Start Location
    Dim lsngRPLineStopVal As Single         'Rising Point Line Start Value
    Dim lsngRPLineStopLoc As Single         'Rising Point Line Start Location 
    Dim lsngRPVal As Single                 'Rising Point Value
    Dim lsngRPLoc As Single                 'Rising Point Location 

    Try
      metMetric = gMetrics(guidMetricID)
      metIndex1Metric = gMetrics(guidIndex1ID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      'Rising Point 
      'If lintChanNum = CHAN0 Then
      'Calculate the location of the startpoint of line representing the linear portion of the output (Line 2)
      Call CalcRefPointByVal(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.RisePointStartLoc, lsngRPLineStartVal, lsngRPLineStartLoc)
      'Calculate the location of the endpoint of line representing the linear portion of the output (Line 2)
      Call CalcRefPointByVal(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.RisePointStopLoc, lsngRPLineStopVal, lsngRPLineStopLoc)
      'If the Line 2 Start = Line 2 Stop, errors will occur calculating Rising Point
      If lsngRPLineStartLoc = lsngRPLineStopLoc Then
        'Set Rising Point = 0
        lsngRPVal = 0
        lsngRPLoc = 0
      Else
        'Calculate Rising Point
        Call CalcRisingPoint(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, metIndex1Metric.MetricValue, lsngRPLineStartVal, lsngRPLineStartLoc, lsngRPLineStopVal, lsngRPLineStopLoc, lsngRPVal, lsngRPLoc)
      End If
      'End If

      metMetric.MetricValue = lsngRPLoc
      metMetric.MetricLocation = lsngRPVal

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcRisePointPositionMetric(ByVal guidMetricID As Guid, ByVal guidIndex1ID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim metIndex1Metric As Metric
    'Dim StartLoc As Single
    Dim lsngRPLineStartVal As Single        'Rising Point Line Start Value   
    Dim lsngRPLineStartLoc As Single        'Rising Point Line Start Location
    Dim lsngRPLineStopVal As Single         'Rising Point Line Start Value
    Dim lsngRPLineStopLoc As Single         'Rising Point Line Start Location 
    Dim lsngRPVal As Single                 'Rising Point Value
    Dim lsngRPLoc As Single                 'Rising Point Location 

    Try
      metMetric = gMetrics(guidMetricID)
      metIndex1Metric = gMetrics(guidIndex1ID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      'Rising Point 
      'If lintChanNum = CHAN0 Then
      'Calculate the location of the startpoint of line representing the linear portion of the output (Line 2)
      Call CalcRefPointByVal(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.RisePointStartPercentVRef, lsngRPLineStartVal, lsngRPLineStartLoc)
      'Calculate the location of the endpoint of line representing the linear portion of the output (Line 2)
      Call CalcRefPointByVal(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.RisePointStopPercentVRef, lsngRPLineStopVal, lsngRPLineStopLoc)
      'If the Line 2 Start = Line 2 Stop, errors will occur calculating Rising Point
      If lsngRPLineStartLoc = lsngRPLineStopLoc Then
        'Set Rising Point = 0
        lsngRPVal = 0
        lsngRPLoc = 0
      Else
        'Calculate Rising Point
        Call CalcRisingPoint(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, metIndex1Metric.MetricValue, lsngRPLineStartVal, lsngRPLineStartLoc, lsngRPLineStopVal, lsngRPLineStopLoc, lsngRPVal, lsngRPLoc)
      End If
      'End If

      metMetric.MetricValue = lsngRPLoc
      metMetric.MetricLocation = lsngRPVal

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub CalcFwdRepeatabilityMetric(ByVal guidMetricID As Guid) ' 2.0.1.0 TER
    Dim metMetric As Metric
    Dim MaxIndexForward As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      'Calc Forward Repeatability
      Call CalcRepeatability(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ForwardPositionBasedPercentVrefArray2, gDataArrays.ForwardRepeatabilityValueArray)
      gDataArrays.ReverseRepeatabilityValueArray = gDataArrays.ForwardRepeatabilityValueArray

      'Find Forward Max
      FindAbsoluteMaxWithMPC(gDataArrays.ForwardRepeatabilityValueArray, MaxIndexForward, guidMetricID)

      metMetric.MetricValue = gDataArrays.ForwardRepeatabilityValueArray(MaxIndexForward)
      metMetric.MetricLocation = MaxIndexForward * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CalcRevTorqueMetricMax(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intMaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMax(gDataArrays.InvReverseEvaluatedTorqueArray, intMaxIndex)

      metMetric.MetricValue = gDataArrays.InvReverseEvaluatedTorqueArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcRevTorqueMetricMin(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intMinIndex As Integer
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable

    Try
      metMetric = gMetrics(guidMetricID)
      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable

      Call FindMin(gDataArrays.InvReverseEvaluatedTorqueArray, intMinIndex)

      metMetric.MetricValue = gDataArrays.InvReverseEvaluatedTorqueArray(intMinIndex)
      metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSlopeDeviationMetricMax(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxWithMPC(gDataArrays.SlopeDeviationArray, MaxIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.SlopeDeviationArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSlopeDeviationMetricMin(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      Call FindMinWithMPC(gDataArrays.SlopeDeviationArray, MinIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.SlopeDeviationArray(MinIndex)
      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CalcSlopeDeviationMetricPercentTolerance(ByVal guidMetricID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    'Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim intMaxIndex As Integer
    Dim intMinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Call CalcSlopeDeviation(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.SlopeDevInterval, gAmad.SlopeDevIncrement, gAmad.IdealSlope, False, gDataArrays.SlopeDeviationArray)

      'ReDim PercentTolArray(gDataArrays.SlopeDeviationArray.GetUpperBound(0))

      'Call CalcPercentTolWithIdeal(gDataArrays.SlopeDeviationArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMaxWithMPC(gDataArrays.SlopeDeviationArray, intMaxIndex, guidMetricID)
      Call FindMinWithMPC(gDataArrays.SlopeDeviationArray, intMinIndex, guidMetricID)

      If Math.Abs(gDataArrays.SlopeDeviationArray(intMaxIndex)) > Math.Abs(gDataArrays.SlopeDeviationArray(intMinIndex)) Then
        metMetric.MetricValue = gDataArrays.SlopeDeviationArray(intMaxIndex)
        metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      Else
        metMetric.MetricValue = gDataArrays.SlopeDeviationArray(intMinIndex)
        metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSngPointLinPerTolMetricSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidIndexMetricID As Guid, ByVal guidMetricIDforMPC As Guid, ByVal intOutputNumber As Integer)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim metIndexMetric As Metric
    Dim metMetricMPC As Metric
    Dim intMaxIndex As Integer
    Dim DutOutputArray() As Double
    Dim PercentTolArray() As Double
    Dim SinglePointLinearity() As Double
    Dim dblIdealSlope As Double
    Try
      DutOutputArray = Nothing
      SinglePointLinearity = Nothing

      metMetric = gMetrics(guidMetricID)
      metIndexMetric = gMetrics(guidIndexMetricID)
      metMetricMPC = gMetrics(guidMetricIDforMPC)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable

      Select Case intOutputNumber
        Case 1
          dblIdealSlope = gAmad.IdealSlopeOutput1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          dblIdealSlope = gAmad.IdealSlopeOutput2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call CalcLinSngPoint(DutOutputArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndexMetric.MetricValue, metIndexMetric.MetricLocation, gAmad.EvaluationInterval, dblIdealSlope, SinglePointLinearity)
      Select Case intOutputNumber
        Case 1
          gDataArrays.SinglePointLinearityDutOut1Array = SinglePointLinearity
        Case 2
          gDataArrays.SinglePointLinearityDutOut2Array = SinglePointLinearity
      End Select

      ReDim PercentTolArray(SinglePointLinearity.GetUpperBound(0))
      Call CalcPercentTolWithIdeal(SinglePointLinearity, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      Call FindMax(PercentTolArray, intMaxIndex)

      metMetric.MetricValue = PercentTolArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSinglePointLinearityMaxSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim MaxIndex As Integer
    Dim SinglePointLinearityArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      SinglePointLinearityArray = Nothing

      Select Case intOutputNumber
        Case 1
          SinglePointLinearityArray = gDataArrays.SinglePointLinearityDutOut1Array
        Case 2
          SinglePointLinearityArray = gDataArrays.SinglePointLinearityDutOut2Array
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMaxWithMpcAllRegions(SinglePointLinearityArray, MaxIndex, guidMpcMetricID)

      If MaxIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = SinglePointLinearityArray(MaxIndex)
        metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSinglePointLinearityMinSelectOutputAndOtherMpc(ByVal guidMetricID As Guid, ByVal guidMpcMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim MinIndex As Integer
    Dim SinglePointLinearityArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      SinglePointLinearityArray = Nothing

      Select Case intOutputNumber
        Case 1
          SinglePointLinearityArray = gDataArrays.SinglePointLinearityDutOut1Array
        Case 2
          SinglePointLinearityArray = gDataArrays.SinglePointLinearityDutOut2Array
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      Call FindMinWithMpcAllRegions(SinglePointLinearityArray, MinIndex, guidMpcMetricID)

      If MinIndex = -1 Then
        metMetric.MetricValue = 0
        metMetric.MetricLocation = 0
      Else
        metMetric.MetricValue = SinglePointLinearityArray(MinIndex)
        metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSngPointLinearityMetricMax(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMaxWithMPC(gDataArrays.SinglePointLinearityArray, MaxIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.SinglePointLinearityArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSngPointLinearityMetricMin(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      Call FindMinWithMPC(gDataArrays.SinglePointLinearityArray, MinIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.SinglePointLinearityArray(MinIndex)
      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CalcSngPointLinearityMetric(ByVal guidMetricID As Guid, ByVal guidIndex1ID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim metIndex1Metric As Metric
    Dim intMaxIndex As Integer
    Dim intMinIndex As Integer

    Try

      metMetric = gMetrics(guidMetricID)
      metIndex1Metric = gMetrics(guidIndex1ID)
      'metIndex2Metric = gMetrics(guidIndex2ID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      'Call CalcLinSngPoint(gDataArrays.ForwardPositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndex1Metric.MetricValue, metIndex1Metric.MetricLocation - gdblXArrayOffsetPara, gAmad.EvaluationInterval, gAmad.IdealSlope, gDataArrays.SinglePointLinearityArray)
      Call CalcLinSngPoint(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndex1Metric.MetricValue, metIndex1Metric.MetricLocation, gAmad.EvaluationInterval, gAmad.IdealSlope, gDataArrays.SinglePointLinearityArray)

      Call FindMax(gDataArrays.SinglePointLinearityArray, intMaxIndex)
      Call FindMin(gDataArrays.SinglePointLinearityArray, intMinIndex)

      If Math.Abs(gDataArrays.SinglePointLinearityArray(intMaxIndex)) > Math.Abs(gDataArrays.SinglePointLinearityArray(intMinIndex)) Then
        metMetric.MetricValue = gDataArrays.SinglePointLinearityArray(intMaxIndex)
        metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      Else
        metMetric.MetricValue = gDataArrays.SinglePointLinearityArray(intMinIndex)
        metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSngPointLinearityMetricPercentTolerance(ByVal guidMetricID As Guid, ByVal guidMetricIDforMPC As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metMetricMPC As Metric
    Dim intMaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      metMetricMPC = gMetrics(guidMetricIDforMPC)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable

      ReDim PercentTolArray(gDataArrays.SinglePointLinearityArray.GetUpperBound(0))

      Call CalcPercentTolWithIdeal(gDataArrays.SinglePointLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, intMaxIndex)

      metMetric.MetricValue = PercentTolArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSpringRateBetweenIndexesMetric(ByVal guidMetricID As Guid, ByVal dblIndex1 As Double, ByVal dblIndex2 As Double)
    Dim metMetric As Metric
    Dim intIndex1 As Integer
    Dim intIndex2 As Integer
    Dim dblForce1 As Double
    Dim dblForce2 As Double
    Try

      metMetric = gMetrics(guidMetricID)
      intIndex1 = CInt((dblIndex1 - gAmad.EvaluationStart) / gAmad.EvaluationInterval)
      intIndex2 = CInt((dblIndex2 - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      dblForce1 = gDataArrays.ForwardPositionBasedForceArray(intIndex1)
      dblForce2 = gDataArrays.ForwardPositionBasedForceArray(intIndex2)

      metMetric.MetricValue = (dblForce2 - dblForce1) / (dblIndex2 - dblIndex1)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcSpringRateEvalRegionMetric(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intIndex1 As Integer
    Dim intIndex2 As Integer
    Dim dblForce1 As Double
    Dim dblForce2 As Double
    Try

      metMetric = gMetrics(guidMetricID)
      intIndex1 = CInt((gAmad.EvaluationStart - gAmad.EvaluationStart) / gAmad.EvaluationInterval)
      intIndex2 = CInt((gAmad.EvaluationStop - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      dblForce1 = gDataArrays.ForwardPositionBasedForceArray(intIndex1)
      dblForce2 = gDataArrays.ForwardPositionBasedForceArray(intIndex2)

      metMetric.MetricValue = (dblForce2 - dblForce1) / (gAmad.EvaluationStop - gAmad.EvaluationStart)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub



  Public Sub CalcTorqueMetricMax(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intMaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMax(gDataArrays.ForwardEvaluatedTorqueArray, intMaxIndex)

      metMetric.MetricValue = gDataArrays.ForwardEvaluatedTorqueArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcTorqueMetricMin(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      Call FindMin(gDataArrays.ForwardEvaluatedTorqueArray, MinIndex)

      metMetric.MetricValue = gDataArrays.ForwardEvaluatedTorqueArray(MinIndex)
      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcTwoPointLinearityMetricMax(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)

      'Call FindMax(gDataArrays.TwoPointLinearityArray, MaxIndex)
      Call FindMaxWithMPC(gDataArrays.TwoPointLinearityArray, MaxIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.TwoPointLinearityArray(MaxIndex)
      metMetric.MetricLocation = MaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcTwoPointLinearityMetricMin(ByVal guidMetricID As Guid, ByVal guidMetricIDMPC As Guid)
    Dim metMetric As Metric
    Dim MinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      'Call FindMin(gDataArrays.TwoPointLinearityArray, MinIndex)
      Call FindMinWithMPC(gDataArrays.TwoPointLinearityArray, MinIndex, guidMetricIDMPC)

      metMetric.MetricValue = gDataArrays.TwoPointLinearityArray(MinIndex)
      metMetric.MetricLocation = MinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CalcTwoPointLinearityMetric(ByVal guidMetricID As Guid, ByVal guidIndex1ID As Guid, ByVal guidIndex2ID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric
    Dim metIndex1Metric As Metric
    Dim metIndex2Metric As Metric
    Dim intMaxIndex As Integer
    Dim intMinIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      metIndex1Metric = gMetrics(guidIndex1ID)
      metIndex2Metric = gMetrics(guidIndex2ID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      Call CalcLinTwoPoint(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndex1Metric.MetricValue, metIndex1Metric.MetricLocation, metIndex2Metric.MetricValue, metIndex2Metric.MetricLocation, gAmad.EvaluationInterval, gDataArrays.TwoPointLinearityArray)

      Call FindMax(gDataArrays.TwoPointLinearityArray, intMaxIndex)
      Call FindMin(gDataArrays.TwoPointLinearityArray, intMinIndex)

      If Math.Abs(gDataArrays.TwoPointLinearityArray(intMaxIndex)) > Math.Abs(gDataArrays.TwoPointLinearityArray(intMinIndex)) Then
        metMetric.MetricValue = gDataArrays.TwoPointLinearityArray(intMaxIndex)
        metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      Else
        metMetric.MetricValue = gDataArrays.TwoPointLinearityArray(intMinIndex)
        metMetric.MetricLocation = intMinIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcTwoPointLinearityMetricPercentTolerance2(ByVal guidMetricID As Guid, ByVal guidMetricIDforMPC As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metMetricMPC As Metric
    Dim intMaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      metMetricMPC = gMetrics(guidMetricIDforMPC)

      dtHighLimit = metMetricMPC.MPC.HighLimitRegionsTable
      dtLowLimit = metMetricMPC.MPC.LowLimitRegionsTable
      dtIdeal = metMetricMPC.MPC.IdealValueRegionsTable

      ReDim PercentTolArray(gDataArrays.TwoPointLinearityArray.GetUpperBound(0))

      Call CalcPercentTolWithIdeal(gDataArrays.TwoPointLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, intMaxIndex)

      metMetric.MetricValue = PercentTolArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcTwoPointLinearityMetricPercentTolerance(ByVal guidMetricID As Guid, ByVal guidIndex1ID As Guid, ByVal guidIndex2ID As Guid)
    Dim dtHighLimit As New DataTable
    Dim dtLowLimit As New DataTable
    Dim dtIdeal As New DataTable
    Dim PercentTolArray() As Double
    Dim metMetric As Metric
    Dim metIndex1Metric As Metric
    Dim metIndex2Metric As Metric
    Dim intMaxIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      metIndex1Metric = gMetrics(guidIndex1ID)
      metIndex2Metric = gMetrics(guidIndex2ID)

      dtHighLimit = metMetric.MPC.HighLimitRegionsTable
      dtLowLimit = metMetric.MPC.LowLimitRegionsTable
      dtIdeal = metMetric.MPC.IdealValueRegionsTable

      'Call CalcLinTwoPoint(gDataArrays.ForwardPositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndex1Metric.MetricValue, metIndex1Metric.MetricLocation, metIndex2Metric.MetricValue, metIndex2Metric.MetricLocation, gAmad.EvaluationInterval, gDataArrays.TwoPointLinearityArray)
      'Call CalcLinTwoPoint(gDataArrays.ForwardPositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndex1Metric.MetricValue, metIndex1Metric.MetricLocation - gdblXArrayOffsetPara, metIndex2Metric.MetricValue, metIndex2Metric.MetricLocation - gdblXArrayOffsetPara, gAmad.EvaluationInterval, gDataArrays.TwoPointLinearityArray)
      Call CalcLinTwoPoint(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, metIndex1Metric.MetricValue, metIndex1Metric.MetricLocation, metIndex2Metric.MetricValue, metIndex2Metric.MetricLocation, gAmad.EvaluationInterval, gDataArrays.TwoPointLinearityArray)

      ReDim PercentTolArray(gDataArrays.TwoPointLinearityArray.GetUpperBound(0))

      Call CalcPercentTolWithIdeal(gDataArrays.TwoPointLinearityArray, dtHighLimit, dtLowLimit, dtIdeal, PercentTolArray)
      Call FindMax(PercentTolArray, intMaxIndex)

      metMetric.MetricValue = PercentTolArray(intMaxIndex)
      metMetric.MetricLocation = intMaxIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindForceMax(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intLoop As Integer
    Dim dblMaxForce As Double
    Dim intValueIndex As Integer
    Try

      metMetric = gMetrics(guidMetricID)
      dblMaxForce = Double.MinValue

      For intLoop = 0 To gDataArrays.ForwardPositionBasedForceArray.Length - 1
        If gDataArrays.ForwardPositionBasedForceArray(intLoop) > dblMaxForce Then
          dblMaxForce = gDataArrays.ForwardPositionBasedForceArray(intLoop)
          intValueIndex = intLoop
        End If
      Next

      metMetric.MetricValue = dblMaxForce
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindForceMin(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Dim intLoop As Integer
    Dim dblMinForce As Double
    Dim intValueIndex As Integer
    Try

      metMetric = gMetrics(guidMetricID)
      dblMinForce = Double.MaxValue

      For intLoop = 0 To gDataArrays.ForwardPositionBasedForceArray.Length - 1
        If gDataArrays.ForwardPositionBasedForceArray(intLoop) < dblMinForce Then
          dblMinForce = gDataArrays.ForwardPositionBasedForceArray(intLoop)
          intValueIndex = intLoop
        End If
      Next

      metMetric.MetricValue = dblMinForce
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindFwdForceMetricValueAtLocation(ByVal guidMetricID As Guid, ByVal IndexPosition As Double)
    Dim metMetric As Metric
    Dim intValueIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      intValueIndex = CInt((IndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      metMetric.MetricValue = gDataArrays.ForwardPositionBasedForceArray(intValueIndex)
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindRevForceMetricValueAtLocation(ByVal guidMetricID As Guid, ByVal IndexPosition As Double)
    Dim metMetric As Metric
    Dim intValueIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      intValueIndex = CInt((IndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      metMetric.MetricValue = gDataArrays.ReversePositionBasedForceArray(intValueIndex)
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMinWithMPC(ByVal DataArray() As Double, ByRef MinIndex As Integer, ByVal guidMetricID As Guid)
    Dim i As Integer
    Dim MinValue As Double = Double.MaxValue
    Dim rowIdeal As DataRow
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      rowIdeal = dtIdeal.Rows(0)

      For i = 0 To DataArray.Length - 1
        If (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) >= rowIdeal("Abscissa1") And (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) <= rowIdeal("Abscissa2") Then
          If DataArray(i) < MinValue Then
            MinValue = DataArray(i)
            MinIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMaxWithMPC(ByVal DataArray() As Double, ByRef MaxIndex As Integer, ByVal guidMetricID As Guid)
    Dim i As Double
    Dim MaxValue As Double = Double.MinValue
    Dim rowIdeal As DataRow
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      rowIdeal = dtIdeal.Rows(0)

      For i = 0 To DataArray.Length - 1
        If (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) >= rowIdeal("Abscissa1") And (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) <= rowIdeal("Abscissa2") Then
          If DataArray(i) > MaxValue Then
            MaxValue = DataArray(i)
            MaxIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMinWithMpcAllRegions(ByVal DataArray() As Double, ByRef MinIndex As Integer, ByVal guidMetricID As Guid)
    ' 3.1.0.2 TER
    Dim i As Integer
    Dim MinValue As Double = Double.MaxValue
    Dim rowFirstRegion As DataRow
    Dim rowLastRegion As DataRow
    Dim dtLimits As New DataTable
    Dim metMetric As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      If metMetric.MPC.IdealValueRegionsTable.Rows(0).Item("Abscissa1") IsNot DBNull.Value Then
        dtLimits = metMetric.MPC.IdealValueRegionsTable
      ElseIf metMetric.MPC.LowLimitRegionsTable.Rows(0).Item("Abscissa1") IsNot DBNull.Value Then
        dtLimits = metMetric.MPC.LowLimitRegionsTable
      ElseIf metMetric.MPC.HighLimitRegionsTable.Rows(0).Item("Abscissa1") IsNot DBNull.Value Then
        dtLimits = metMetric.MPC.HighLimitRegionsTable
      End If
      rowFirstRegion = dtLimits.Rows(0)
      rowLastRegion = dtLimits.Rows(dtLimits.Rows.Count - 1)

      For i = 0 To DataArray.Length - 1
        If (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) >= rowFirstRegion("Abscissa1") And (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) <= rowLastRegion("Abscissa2") Then
          If DataArray(i) < MinValue Then
            MinValue = DataArray(i)
            MinIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMaxWithMpcAllRegions(ByVal DataArray() As Double, ByRef MaxIndex As Integer, ByVal guidMetricID As Guid)
    ' 3.1.0.2 TER
    Dim i As Double
    Dim MaxValue As Double = Double.MinValue
    Dim rowFirstRegion As DataRow
    Dim rowLastRegion As DataRow
    Dim dtLimits As New DataTable
    Dim metMetric As Metric
    Dim sngPosition As Single

    Try
      metMetric = gMetrics(guidMetricID)
      If metMetric.MPC.IdealValueRegionsTable.Rows(0).Item("Abscissa1") IsNot DBNull.Value Then
        dtLimits = metMetric.MPC.IdealValueRegionsTable
      ElseIf metMetric.MPC.HighLimitRegionsTable.Rows(0).Item("Abscissa1") IsNot DBNull.Value Then
        dtLimits = metMetric.MPC.HighLimitRegionsTable
      ElseIf metMetric.MPC.LowLimitRegionsTable.Rows(0).Item("Abscissa1") IsNot DBNull.Value Then
        dtLimits = metMetric.MPC.LowLimitRegionsTable
      End If
      rowFirstRegion = dtLimits.Rows(0)
      rowLastRegion = dtLimits.Rows(dtLimits.Rows.Count - 1)

      For i = 0 To DataArray.Length - 1
        sngPosition = i * gAmad.EvaluationInterval + gAmad.EvaluationStart
        If sngPosition >= rowFirstRegion("Abscissa1") And sngPosition <= rowLastRegion("Abscissa2") Then
          If DataArray(i) > MaxValue Then
            MaxValue = DataArray(i)
            MaxIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMinNegativeWithMpcAllRegions(ByVal DataArray() As Double, ByRef MinIndex As Integer, ByVal guidMetricID As Guid)
    ' 3.1.0.2 TER
    Dim i As Integer
    Dim MinValue As Double
    Dim rowFirstRegion As DataRow
    Dim rowLastRegion As DataRow
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      rowFirstRegion = dtIdeal.Rows(0)
      rowLastRegion = dtIdeal.Rows(dtIdeal.Rows.Count - 1)

      MinValue = 0
      MinIndex = -1
      For i = 0 To DataArray.Length - 1
        If (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) >= rowFirstRegion("Abscissa1") And (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) <= rowLastRegion("Abscissa2") Then
          If DataArray(i) < MinValue Then
            MinValue = DataArray(i)
            MinIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMaxPositiveWithMpcAllRegions(ByVal DataArray() As Double, ByRef MaxIndex As Integer, ByVal guidMetricID As Guid)
    ' 3.1.0.2 TER
    Dim i As Double
    Dim MaxValue As Double
    Dim rowFirstRegion As DataRow
    Dim rowLastRegion As DataRow
    Dim dtIdeal As New DataTable
    Dim metMetric As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      rowFirstRegion = dtIdeal.Rows(0)
      rowLastRegion = dtIdeal.Rows(dtIdeal.Rows.Count - 1)

      MaxValue = 0
      MaxIndex = -1
      For i = 0 To DataArray.Length - 1
        If (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) >= rowFirstRegion("Abscissa1") And (i * gAmad.EvaluationInterval + gAmad.EvaluationStart) <= rowLastRegion("Abscissa2") Then
          If DataArray(i) > MaxValue Then
            MaxValue = DataArray(i)
            MaxIndex = i
          End If
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindLocationMetricFromOutputValueProperty(ByVal guidMetricID As Guid, ByVal intOutputNumber As Integer, ByVal dblOutputValue As Double)
    Dim intElementCount As Integer
    Dim intValueIndex As Integer
    Dim dblValueDifference As Double = Double.MaxValue
    Dim metMetric As Metric
    'Dim ResultsRow As DataRow
    'Dim dtIdeal As New DataTable
    'Dim ideal As Double
    'Dim rowideal As DataRow
    Dim DutOutputArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      Select Case intOutputNumber
        Case 1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      'Find closest array value to ideal value
      For intElementCount = 0 To DutOutputArray.Length - 1
        If Math.Abs(DutOutputArray(intElementCount) - dblOutputValue) < dblValueDifference Then
          dblValueDifference = Math.Abs(DutOutputArray(intElementCount) - dblOutputValue)
          intValueIndex = intElementCount
        End If
      Next

      metMetric.MetricValue = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart
      metMetric.MetricLocation = DutOutputArray(intValueIndex)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMax(ByVal DataArray() As Double, ByRef MaxIndex As Integer)
    Dim i As Integer
    Dim MaxValue As Double = Double.MinValue
    Try
      'look for max of values
      For i = 0 To DataArray.Length - 1
        If DataArray(i) > MaxValue Then
          MaxValue = DataArray(i)
          MaxIndex = i
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMetricLocationAtValue(ByVal guidMetricID As Guid, ByVal DataArray() As Double)
    Dim intElementCount As Integer
    Dim intValueIndex As Integer
    Dim dblValueDifference As Double = Double.MaxValue
    Dim metMetric As Metric
    'Dim ResultsRow As DataRow
    Dim dtIdeal As New DataTable
    Dim ideal As Double
    Dim rowideal As DataRow

    Try
      metMetric = gMetrics(guidMetricID)
      dtIdeal = metMetric.MPC.IdealValueRegionsTable
      rowideal = dtIdeal.Rows(0)
      ideal = rowideal("Slope") * metMetric.MetricLocation + rowideal("YIntercept")

      'Find closest array value to ideal value
      For intElementCount = 0 To DataArray.Length - 1
        If Math.Abs(DataArray(intElementCount) - ideal) < dblValueDifference Then
          dblValueDifference = Math.Abs(DataArray(intElementCount) - ideal)
          intValueIndex = intElementCount
        End If
      Next

      metMetric.MetricValue = DataArray(intValueIndex)
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart '+ gdblXArrayOffsetPara

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMetricValueAtLocation(ByVal guidMetricID As Guid, ByVal IndexPosition As Double)
    Dim metMetric As Metric
    Dim intValueIndex As Integer

    Try
      metMetric = gMetrics(guidMetricID)
      'intValueIndex = CInt((IndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)
      'intValueIndex = CInt((IndexPosition - gdblXArrayOffsetPara - gAmad.EvaluationStart) / gAmad.EvaluationInterval)
      intValueIndex = CInt((IndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      metMetric.MetricValue = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray(intValueIndex)
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMetricValueAtLocationSelectOutput(ByVal guidMetricID As Guid, ByVal sngIndexPosition As Single, ByVal intOutputNumber As Integer)
    Dim metMetric As Metric
    Dim intValueIndex As Integer
    Dim DutOutputArray() As Double

    Try
      metMetric = gMetrics(guidMetricID)

      intValueIndex = CInt((sngIndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      Select Case intOutputNumber
        Case 1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metMetric.MetricValue = DutOutputArray(intValueIndex)
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMetricValueAtAbscissaLocationSelectOutput(ByVal guidMetricID As Guid, ByVal intOutputNumber As Integer, _
                                                           ByVal intAbscissaRegion As Integer, ByVal intAbscissaNumber As Integer)

    Dim metMetric As Metric
    Dim intValueIndex As Integer
    Dim DutOutputArray() As Double
    Dim sngIndexPosition As Single
    Dim strAbscissaName As String
    Dim intRow As Integer
    Try

      metMetric = gMetrics(guidMetricID)

      strAbscissaName = "Abscissa" & CStr(intAbscissaNumber)
      intRow = intAbscissaRegion - 1

      If metMetric.MPC.IdealValueRegionsTable.Rows(intRow).Item(strAbscissaName) IsNot DBNull.Value Then
        sngIndexPosition = metMetric.MPC.IdealValueRegionsTable.Rows(intRow).Item(strAbscissaName)
      ElseIf metMetric.MPC.HighLimitRegionsTable.Rows(intRow).Item(strAbscissaName) IsNot DBNull.Value Then
        sngIndexPosition = metMetric.MPC.HighLimitRegionsTable.Rows(intRow).Item(strAbscissaName)
      ElseIf metMetric.MPC.LowLimitRegionsTable.Rows(intRow).Item(strAbscissaName) IsNot DBNull.Value Then
        sngIndexPosition = metMetric.MPC.LowLimitRegionsTable.Rows(intRow).Item(strAbscissaName)
      Else
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid Abscissa or Region Specified", gDatabase)
        Throw New TsopAnomalyException
      End If

      intValueIndex = CInt((sngIndexPosition - gAmad.EvaluationStart) / gAmad.EvaluationInterval)
      Select Case intOutputNumber
        Case 1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metMetric.MetricValue = DutOutputArray(intValueIndex)
      metMetric.MetricLocation = intValueIndex * gAmad.EvaluationInterval + gAmad.EvaluationStart

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub FindMin(ByVal DataArray() As Double, ByRef MinIndex As Integer)
    Dim i As Integer
    Dim MinValue As Double = Double.MaxValue
    Try

      'look for max of values
      For i = 0 To DataArray.Length - 1
        If DataArray(i) < MinValue Then
          MinValue = DataArray(i)
          MinIndex = i
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function FindValueAtLocation(ByVal ValueArray() As Double, ByVal LocationArray() As Double, ByVal location As Double, ByVal AveragingRangeHigh As Double, ByVal AveragingRangeLow As Double) As Double
    Dim Value As Double
    Dim i As Integer
    Dim NumValues As Integer
    Dim ldblSum As Double
    Try
      For i = 0 To LocationArray.Length - 1
        'account for encoder increments of 0.01
        If (Math.Round(LocationArray(i), 2, MidpointRounding.AwayFromZero) <= Math.Round((location + AveragingRangeHigh), 2, MidpointRounding.AwayFromZero)) And (Math.Round(LocationArray(i), 2, MidpointRounding.AwayFromZero) >= Math.Round((location - AveragingRangeLow), 2, MidpointRounding.AwayFromZero)) Then
          NumValues = NumValues + 1
          ldblSum = ldblSum + ValueArray(i)
        End If
      Next
      Value = ldblSum / NumValues
      Return Value

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Function

  Public Sub FindValueMetricWithLocationMetricSelectOutput(ByVal guidValueMetricID As Guid, ByVal guidLocationMetricID As Guid, ByVal intOutputNumber As Integer)
    Dim metValueMetric As Metric
    Dim metLocationMetric As Metric
    Dim intValueIndex As Integer
    Dim DutOutputArray() As Double

    Try
      metValueMetric = gMetrics(guidValueMetricID)
      metLocationMetric = gMetrics(guidLocationMetricID)

      intValueIndex = CInt((metLocationMetric.MetricValue - gAmad.EvaluationStart) / gAmad.EvaluationInterval)

      Select Case intOutputNumber
        Case 1
          DutOutputArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        Case 2
          DutOutputArray = gDataArrays.ForwardDUTOut2PositionBasedPercentVrefArray
        Case Else
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Invalid DUT Output Number", gDatabase)
          Throw New TsopAnomalyException
      End Select

      metValueMetric.MetricValue = DutOutputArray(intValueIndex)
      metValueMetric.MetricLocation = metLocationMetric.MetricValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetTestStatus(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = gTestResult

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub
  Public Sub LoadAlphaMetricFromIntegerProperty(ByVal guidMetricID As Guid, ByVal ParameterValue As Integer)
    ' 2.0.1.0 TER
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = CStr(ParameterValue)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadAlphaMetricFromProperty(ByVal guidMetricID As Guid, ByVal ParameterValue As String)
    ' 2.0.1.0 TER
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.AlphaMetricValue = ParameterValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadLocationMetricFromValueMetric(ByVal guidMetricLocationID As Guid, ByVal guidMetricValueID As Guid)
    Dim metMetricLocation As Metric
    Dim metMetricValue As Metric

    Try
      metMetricLocation = gMetrics(guidMetricLocationID)
      metMetricValue = gMetrics(guidMetricValueID)
      metMetricLocation.MetricValue = metMetricValue.MetricLocation

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadLocationMetricFromValueMetricPassFail(ByVal guidMetricLocationID As Guid, ByVal guidMetricValueID As Guid)
    Dim metMetricLocation
    Dim metMetricValue

    Try
      metMetricLocation = gMetrics(guidMetricLocationID)
      metMetricValue = gMetrics(guidMetricValueID)

      metMetricLocation.MetricValue = metMetricValue.MetricLocation

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadMetricFromProperty(ByVal guidMetricID As Guid, ByVal ParameterValue As Double)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = ParameterValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadMetricFromPropertyPassFail(ByVal guidMetricID As Guid, ByVal ParameterValue As Double)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = ParameterValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadPeakForceMetricFromMeasuredValue(ByVal guidMetricID As Guid)
    Dim metMetric As Metric
    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = gAmad.PeakForce

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadAlphaMetricFromSetupInfo(ByVal guidMetricID As Guid, ByVal strSetupInfoKey As String)
    ' 2.0.1.0 TER
    Dim metMetric As Metric
    Dim strMetricValue As String
    Try
      metMetric = gMetrics(guidMetricID)
      strMetricValue = gSetupInfoControls(strSetupInfoKey).Text

      metMetric.AlphaMetricValue = strMetricValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LoadValueMetricFromLocationMetric(ByVal guidValueMetricID As Guid, ByVal guidLocationMetricID As Guid)
    Dim metValueMetric As Metric
    Dim metLocationMetric As Metric

    Try
      metValueMetric = gMetrics(guidValueMetricID)
      metLocationMetric = gMetrics(guidLocationMetricID)

      metValueMetric.MetricValue = metLocationMetric.MetricLocation
      metValueMetric.MetricLocation = metLocationMetric.MetricValue

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ReturnDataFromPositionBasedArray(ByVal guidMetricID As Guid, ByVal index As Integer, ByVal DataArray() As Double)
    Dim metMetric As Metric

    Try
      metMetric = gMetrics(guidMetricID)
      metMetric.MetricValue = DataArray(index)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


End Module
