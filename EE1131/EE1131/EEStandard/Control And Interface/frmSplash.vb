Imports System.Configuration
Public NotInheritable Class frmSplash

  Private Sub SplashScreen1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    Me.Cursor = Cursors.WaitCursor
    Me.Left = (Screen.PrimaryScreen.Bounds.Width - Me.Width) / 2
    Me.Top = (Screen.PrimaryScreen.Bounds.Height - Me.Height) / 6


    lblEENumber.Text = ConfigurationManager.AppSettings("TsopName")

    Dim CAN_ACT_AssemblyName As String = "CAN Actuator"  'This is the name of the DLL to search for
    'Searches the loaded assemblies for Referenced DLL
    'If found it extracts the version from the full name.
    For i As Long = 0 To My.Application.Info.LoadedAssemblies.Count - 1
      If Mid(My.Application.Info.LoadedAssemblies.Item(i).FullName, 1, 12) = CAN_ACT_AssemblyName Then
        Dim v() As String = Split(My.Application.Info.LoadedAssemblies.Item(i).GetName.ToString, ",", )
        lblCAN_ACT_DLLver.Text = CAN_ACT_AssemblyName & " DLL" & v(1)
        Exit For
      Else
        lblCAN_ACT_DLLver.Text = ""
      End If
    Next

    'Application title
    If My.Application.Info.Title <> "" Then
      If Me.btnClose.Visible Then
        txtAppTitle.Text = Application.ProductName
      Else
        txtAppTitle.Text = "Loading " & Application.ProductName & "..."
      End If

    End If

    'Version info
    Dim strProgramVersion As String = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build & "." & My.Application.Info.Version.MinorRevision
    txtversion.Text = "TSOP version: " & strProgramVersion

    'Copyright info
    txtCopyright.Text = My.Application.Info.Copyright

  End Sub

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    Me.Close()
  End Sub

  Protected Overrides Sub Finalize()
    MyBase.Finalize()
  End Sub

End Class
