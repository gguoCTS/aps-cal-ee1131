
Imports System.Configuration


Module modAppInitialize

  'Private Const mStackTrace.CurrentClassName As String = "modAppInitialize"

  Public Sub AppInitialize()
    Try

      gstrAssemblyName = My.Application.Info.AssemblyName

      Call InitializeControlFlags()

      Call GetAppConfigSettings()
      If Not (gAnomaly Is Nothing) Then
        gAnomaly.AnomalyDbError = True
        Throw New TsopAnomalyException
      End If

      gstrTsopInitTaskListFileName = ConfigurationManager.AppSettings("TsopInitTaskListFileName")
      gdtTsopInitTaskList = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrTsopInitTaskListFileName, Nothing)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      'Instantiate database object
      gDatabase = New Database(gblnUseUnreleasedSets, gControlFlags.HasADRDatabase)
      If Not (gDatabase.Anomaly Is Nothing) Then
        gAnomaly = gDatabase.Anomaly
        gAnomaly.AnomalyDbError = True
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      'Get Lot Shift Definitions
      Call GetLotShiftDefinitions()
      If Not (gDatabase.Anomaly Is Nothing) Then
        gAnomaly = gDatabase.Anomaly
        gAnomaly.AnomalyDbError = True
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If
      'Call TempSetShiftsForDebug() 'debugging

      'Instantiate subprocess object
      gSubProcesses = gDatabase.GetTsopSubProcessesDictionary

      'Set subprocess to be "Initialization"
      gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.Initialization.ToString)

      'Add New StartupID Here
      gDatabase.NewStartupID()
      If Not (gDatabase.Anomaly Is Nothing) Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        gAnomaly.AnomalyDbError = True 'StartupID required to write anomaly to db
        Throw New TsopAnomalyException
      End If

      'Call modAppDataAcquisition.CalibrateDAQ()
      'If Not (gAnomaly Is Nothing) Then
      '  Throw New TsopAnomalyException
      'End If

      'TsopFunction Values Collection
      gTsopFunctionValues = gDatabase.GetTsopFunctionValuesDictionary()

      'TsopMode Values Collection
      gTsopModeValues = gDatabase.GetTsopModeValuesDictionary()

      'Create result data tables
      Call CreateResultDataTables()
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      'set start scan and program booleans
      'gblnStartScan = False
      gControlFlags.StartScan = False
      gControlFlags.ManualScan = False

      'gblnTorqueScan = False
      'gblnENRScan = False

      frmTsopMain.mnuOptionsOnPlcStartProgram.Checked = gControlFlags.OnPlcStartProgram
      frmTsopMain.mnuOptionsOnPlcStartTest.Checked = gControlFlags.OnPlcStartTest

      'If gMetricControlFlags(ControlFlagEnum.UsesPLC.ToString) = True Then
      '  frmTsopMain.mnuFunctionTest.Enabled = False
      'Else
      '  frmTsopMain.mnuFunctionTest.Enabled = True
      'End If

      ''motor location is unknown
      'gblnMotorLocationKnown = False

      gintGraphsPerPage = 4
      gintCurrentGraphPageNumber = 0
      gblnAntiTieDownTimeout = False

      gtmrLotShift.Interval = 1000
      AddHandler gtmrLotShift.Elapsed, AddressOf LotShiftTimerElapsed

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      If gDatabase Is Nothing Then
        gAnomaly.AnomalyDbError = True
      End If

    End Try
  End Sub

  Public Sub GetAppConfigSettings()
    Dim graphConfig As clsGraphDefaults

    Try

      graphConfig = ConfigurationManager.GetSection("graphDefaults")

      gblnUseUnreleasedSets = ConfigurationManager.AppSettings("UseUnreleasedSets")
      gstrActivityLogPath = ConfigurationManager.AppSettings("ActivityLogPath")
      gstrClassConfigFilePath = ConfigurationManager.AppSettings("ClassConfigFilePath")
      gstrDataFileBasePath = ConfigurationManager.AppSettings("DataFileBasePath")
      gstrDataFileSubFolderPath = ConfigurationManager.AppSettings("DataFileSubFolderPath")
      gstrProgFileSubFolderPath = ConfigurationManager.AppSettings("ProgFileSubFolderPath")
      'gstrPedalArmReturnDataFilePath = ConfigurationManager.AppSettings("PedalArmReturnDataFilePath") ' 2.0.1.0 TER
      gstrDefaultLotType = ConfigurationManager.AppSettings("DefaultLotType")
      gblnProductionSet = ConfigurationManager.AppSettings("ProductionSetDefault")
      gintOpenResistanceValue = ConfigurationManager.AppSettings("OpenResistanceValue")
      gintResistanceInSeriesWithOverallRes = ConfigurationManager.AppSettings("ResistanceInSeriesWithOverallRes")
      gintResistanceInSeriesWithSeriesRes = ConfigurationManager.AppSettings("ResistanceInSeriesWithSeriesRes")
      gintScanTimeoutMilliseconds = ConfigurationManager.AppSettings("ScanTimeoutMilliseconds")
      gstrPrintedReportDefinitionsFile = ConfigurationManager.AppSettings("PrintedReportDefinitions")
      gstrMenuDefinitionsFile = ConfigurationManager.AppSettings("MenuDefinitionsFile")
      gControlFlags.OnPlcStartProgram = ConfigurationManager.AppSettings("OnPlcStartProgram")
      gControlFlags.OnPlcStartTest = ConfigurationManager.AppSettings("OnPlcStartTest")
      gControlFlags.UsesPLC = ConfigurationManager.AppSettings("UsesPLC")
      gControlFlags.HasADRDatabase = ConfigurationManager.AppSettings("HasADRDatabase")
      gintVRefDivisor = ConfigurationManager.AppSettings("VRefDivisor") ' 2.0.1.0 TER
      gControlFlags.AutoRetestEnabled = ConfigurationManager.AppSettings("AutoRetestEnabled")
      gControlFlags.CyclePartEnabled = ConfigurationManager.AppSettings("CyclePartEnabled")
      gintNumCycles = ConfigurationManager.AppSettings("NumberOfCycles")

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

End Module
