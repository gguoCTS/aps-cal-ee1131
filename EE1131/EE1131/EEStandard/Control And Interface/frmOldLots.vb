Public Class frmOldLots

  Dim dtPreviousLotTypes As DataTable
  Dim dtPreviousLotNames As DataTable

  Private Sub frmOldLots_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    dtPreviousLotTypes = gDatabase.GetPreviousLotTypes(gDatabase.ProcessParameterID, gDatabase.TsopID)

    BindPreviousLotTypesCombo()
    BindPreviousLotNamesCombo()

    AddHandler cboPreviousLotTypes.SelectedValueChanged, AddressOf cboPreviousLotTypes_SelectedValueChanged
  End Sub

  Private Sub BindPreviousLotTypesCombo()
    Try
      cboPreviousLotTypes.DataSource = dtPreviousLotTypes
      cboPreviousLotTypes.ValueMember = dtPreviousLotTypes.Columns("LotTypeID").ToString
      cboPreviousLotTypes.DisplayMember = dtPreviousLotTypes.Columns("LotTypeName").ToString

    Catch ex As Exception
      MsgBox(ex.Message, MsgBoxStyle.Critical, "Error Found")
    End Try
  End Sub

  Private Sub BindPreviousLotNamesCombo()
    dtPreviousLotNames = gDatabase.GetPreviousLotNames(gDatabase.ProcessParameterID, gDatabase.TsopID, cboPreviousLotTypes.SelectedValue)

    Try
      cboPreviousLotNames.DataSource = dtPreviousLotNames
      cboPreviousLotNames.ValueMember = dtPreviousLotNames.Columns("LotID").ToString
      cboPreviousLotNames.DisplayMember = dtPreviousLotNames.Columns("LotRunName").ToString

    Catch ex As Exception
      MsgBox(ex.Message, MsgBoxStyle.Critical, "Error Found")
    End Try
  End Sub

  Private Sub cboPreviousLotTypes_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    BindPreviousLotNamesCombo()
  End Sub

  Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
    loadPreviousLot()
    Call frmTsopMain.PrintPreviousLot()
    gbPreviousLot = False
  End Sub

  Private Sub btnPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintPreview.Click
    loadPreviousLot()
    Call frmTsopMain.PrintPreviewPreviousLot()
    gbPreviousLot = False
  End Sub

  Private Sub loadPreviousLot()
    Dim mydgv As New DataGridView
    Dim mydgvr As New DataGridViewRow
    Dim intRow As Integer
    Dim intCol As Integer
    Dim intDayOfYear As Integer
    Dim intYear As Integer
    Dim lotID As Guid

    Try
      gbPreviousLot = True
      gPreviousTestLot = New Lot
      lotID = cboPreviousLotNames.SelectedValue

      'May need to query database for grid info for selected lot
      mydgv.ColumnCount = gDataGrids("FTStats").ColumnCount
      mydgv.RowCount = gDataGrids("FTStats").RowCount

      'gstrPreviousLotDateTime = dtPreviousLotNames.Rows(cboPreviousLotNames.SelectedIndex).Item(2).ToString
      gstrPreviousLotRun = cboPreviousLotNames.Text
      intDayOfYear = Mid(gstrPreviousLotRun, 1, 3)
      intYear = Mid(gstrPreviousLotRun, 4, 2) + 2000
      gstrPreviousLotDateTime = New DateTime(intYear, 1, 1).AddDays(intDayOfYear - 1)

      For intCol = 0 To mydgv.ColumnCount - 1
        mydgv.Columns(intCol).Width = gDataGrids("FTStats").Columns(intCol).Width
        mydgv.Columns(intCol).HeaderText = gDataGrids("FTStats").Columns(intCol).HeaderText
        If intCol = 0 Then
          For intRow = 0 To mydgv.RowCount - 1
            mydgv(intCol, intRow).Value = gDataGrids("FTStats")(intCol, intRow).Value
          Next
        End If
      Next

      gPreviousTestLot.LoadPreviousLotFromDb(gDatabase, "FunctionalTest", lotID)
      'gProgLot.LoadFromDb(gDatabase, "Programming")

      LoadPreviousLotMetricStatsFromDb(lotID, mydgv) 'lotid, dgv

      gdgvTableToPrint = mydgv

      gbPreviousLot = True

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    End Try
  End Sub
End Class