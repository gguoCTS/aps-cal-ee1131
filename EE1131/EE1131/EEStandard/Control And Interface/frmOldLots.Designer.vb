<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOldLots
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.Label1 = New System.Windows.Forms.Label
    Me.Label2 = New System.Windows.Forms.Label
    Me.cboPreviousLotTypes = New System.Windows.Forms.ComboBox
    Me.cboPreviousLotNames = New System.Windows.Forms.ComboBox
    Me.btnPrint = New System.Windows.Forms.Button
    Me.btnPrintPreview = New System.Windows.Forms.Button
    Me.SuspendLayout()
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(12, 9)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(49, 13)
    Me.Label1.TabIndex = 0
    Me.Label1.Text = "Lot Type"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(12, 64)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(53, 13)
    Me.Label2.TabIndex = 1
    Me.Label2.Text = "Lot Name"
    '
    'cboPreviousLotTypes
    '
    Me.cboPreviousLotTypes.FormattingEnabled = True
    Me.cboPreviousLotTypes.Location = New System.Drawing.Point(15, 25)
    Me.cboPreviousLotTypes.Name = "cboPreviousLotTypes"
    Me.cboPreviousLotTypes.Size = New System.Drawing.Size(231, 21)
    Me.cboPreviousLotTypes.TabIndex = 2
    '
    'cboPreviousLotNames
    '
    Me.cboPreviousLotNames.FormattingEnabled = True
    Me.cboPreviousLotNames.Location = New System.Drawing.Point(15, 80)
    Me.cboPreviousLotNames.Name = "cboPreviousLotNames"
    Me.cboPreviousLotNames.Size = New System.Drawing.Size(231, 21)
    Me.cboPreviousLotNames.TabIndex = 3
    '
    'btnPrint
    '
    Me.btnPrint.Location = New System.Drawing.Point(15, 128)
    Me.btnPrint.Name = "btnPrint"
    Me.btnPrint.Size = New System.Drawing.Size(75, 23)
    Me.btnPrint.TabIndex = 4
    Me.btnPrint.Text = "Print"
    Me.btnPrint.UseVisualStyleBackColor = True
    '
    'btnPrintPreview
    '
    Me.btnPrintPreview.Location = New System.Drawing.Point(147, 128)
    Me.btnPrintPreview.Name = "btnPrintPreview"
    Me.btnPrintPreview.Size = New System.Drawing.Size(99, 23)
    Me.btnPrintPreview.TabIndex = 4
    Me.btnPrintPreview.Text = "Print Preview"
    Me.btnPrintPreview.UseVisualStyleBackColor = True
    '
    'frmOldLots
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(267, 163)
    Me.Controls.Add(Me.btnPrintPreview)
    Me.Controls.Add(Me.btnPrint)
    Me.Controls.Add(Me.cboPreviousLotNames)
    Me.Controls.Add(Me.cboPreviousLotTypes)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.Label1)
    Me.Name = "frmOldLots"
    Me.Text = "frmOldLots"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents cboPreviousLotTypes As System.Windows.Forms.ComboBox
  Friend WithEvents cboPreviousLotNames As System.Windows.Forms.ComboBox
  Friend WithEvents btnPrint As System.Windows.Forms.Button
  Friend WithEvents btnPrintPreview As System.Windows.Forms.Button
End Class
