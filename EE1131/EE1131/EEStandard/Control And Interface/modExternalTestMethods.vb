Module modExternalTestMethods

  'Public gExternalTestMethods As Dictionary(Of Guid, String)
  'Public gRelationsToExternalTestMethods As Dictionary(Of Guid, String)

  Public gExternalTestMethodRelationships As Dictionary(Of String, Guid)

  Public mnuExternalTestMethodsBefore As New ToolStripMenuItem("Functional Test Occurs Before Test Method")
  Public mnuExternalTestMethodsBeforeComboBox As New ToolStripComboBox

  Public mnuExternalTestMethodsDuring As New ToolStripMenuItem("Functional Test Occurs During Test Method")
  Public mnuExternalTestMethodsDuringComboBox As New ToolStripComboBox
  Public mnuExternalTestMethodsDuringInterval As New ToolStripMenuItem("Interval/Duration = ")
  Public mnuExternalTestMethodsDuringIntervalTextBox As New ToolStripTextBox

  Public mnuExternalTestMethodsAfter As New ToolStripMenuItem("Functional Test Occurs After Test Method")
  Public mnuExternalTestMethodsAfterComboBox As New ToolStripComboBox

  Public mnuExternalTestMethodsNone As New ToolStripMenuItem("None")


End Module
