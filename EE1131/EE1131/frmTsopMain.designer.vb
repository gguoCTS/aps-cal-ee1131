<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTsopMain
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTsopMain))
    Me.AdditionalIOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.LinearEncoderMonitorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.cboParameterSets = New System.Windows.Forms.ComboBox()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.lblLotName = New System.Windows.Forms.Label()
    Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
    Me.grpTestSummary = New System.Windows.Forms.GroupBox()
    Me.txtTestLotYield = New System.Windows.Forms.TextBox()
    Me.Label18 = New System.Windows.Forms.Label()
    Me.lblTestCurrentYield = New System.Windows.Forms.Label()
    Me.txtTestCurrentYield = New System.Windows.Forms.TextBox()
    Me.Label16 = New System.Windows.Forms.Label()
    Me.txtTestCountTotalUnits = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.txtTestCountErrors = New System.Windows.Forms.TextBox()
    Me.Label14 = New System.Windows.Forms.Label()
    Me.txtTestCountUnitsRejected = New System.Windows.Forms.TextBox()
    Me.Label15 = New System.Windows.Forms.Label()
    Me.txtTestCountUnitsPassed = New System.Windows.Forms.TextBox()
    Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
    Me.tabResults = New System.Windows.Forms.TabControl()
    Me.TabPage1 = New System.Windows.Forms.TabPage()
    Me.GroupBox4 = New System.Windows.Forms.GroupBox()
    Me.lblLotType = New System.Windows.Forms.Label()
    Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
    Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
    Me.StatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
    Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
    Me.PrintGraphs = New System.Drawing.Printing.PrintDocument()
    Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
    Me.PrintTable = New System.Drawing.Printing.PrintDocument()
    Me.MenuStripMain = New System.Windows.Forms.MenuStrip()
    Me.mnuFunction = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionTest = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionProgram = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionProgramTest = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintTestResults = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintTestStatistics = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintProgResults = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintProgStatistics = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintGraphs = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreviousLot = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreview = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreviewTestResults = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreviewTestStatistics = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreviewProgResults = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreviewProgStatistics = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionPrintPreviewGraphs = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionAutoPrint = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionAutoPrintProgResults = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionAutoPrintTestResults = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionAutoPrintGraphs = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuFunctionExit = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuLot = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuLotType = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuLotNames = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuIncrementLotRun = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuTool = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuToolOPC = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuOptions = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuOptionsOnPLCStart = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuOptionsOnPlcStartProgram = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuOptionsOnPlcStartTest = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuOptionsDefaultTabPage = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuExternalTestMethods = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuHelp = New System.Windows.Forms.ToolStripMenuItem()
    Me.mnuHelpAbout = New System.Windows.Forms.ToolStripMenuItem()
    Me.btnAbortTest = New System.Windows.Forms.Button()
    Me.lblPassFailIndicator = New System.Windows.Forms.Label()
    Me.gbEncodedSerial = New System.Windows.Forms.GroupBox()
    Me.lblDefault = New System.Windows.Forms.Label()
    Me.lblEncodedSerialNumber = New System.Windows.Forms.Label()
    Me.lblVoltageValue = New System.Windows.Forms.Label()
    Me.lblVoltage = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.CartNumber = New System.Windows.Forms.TextBox()
    Me.PartsInCart = New System.Windows.Forms.TextBox()
    Me.SelectNewCart = New System.Windows.Forms.Button()
    Me.GroupBox1.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.FlowLayoutPanel1.SuspendLayout()
    Me.grpTestSummary.SuspendLayout()
    Me.tabResults.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.StatusStrip1.SuspendLayout()
    Me.MenuStripMain.SuspendLayout()
    Me.gbEncodedSerial.SuspendLayout()
    Me.SuspendLayout()
    '
    'AdditionalIOToolStripMenuItem
    '
    Me.AdditionalIOToolStripMenuItem.Name = "AdditionalIOToolStripMenuItem"
    Me.AdditionalIOToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
    Me.AdditionalIOToolStripMenuItem.Text = "Additional IO"
    '
    'LinearEncoderMonitorToolStripMenuItem
    '
    Me.LinearEncoderMonitorToolStripMenuItem.Name = "LinearEncoderMonitorToolStripMenuItem"
    Me.LinearEncoderMonitorToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
    Me.LinearEncoderMonitorToolStripMenuItem.Text = "Linear Encoder Monitor"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.cboParameterSets)
    Me.GroupBox1.Location = New System.Drawing.Point(0, 68)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(354, 41)
    Me.GroupBox1.TabIndex = 1
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Parameter Set"
    '
    'cboParameterSets
    '
    Me.cboParameterSets.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.cboParameterSets.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
    Me.cboParameterSets.FormattingEnabled = True
    Me.cboParameterSets.Location = New System.Drawing.Point(6, 16)
    Me.cboParameterSets.Name = "cboParameterSets"
    Me.cboParameterSets.Size = New System.Drawing.Size(342, 21)
    Me.cboParameterSets.TabIndex = 0
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.lblLotName)
    Me.GroupBox2.Location = New System.Drawing.Point(180, 29)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(174, 38)
    Me.GroupBox2.TabIndex = 2
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Lot Name.Run"
    '
    'lblLotName
    '
    Me.lblLotName.AutoSize = True
    Me.lblLotName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblLotName.Location = New System.Drawing.Point(27, 16)
    Me.lblLotName.Name = "lblLotName"
    Me.lblLotName.Size = New System.Drawing.Size(0, 13)
    Me.lblLotName.TabIndex = 16
    '
    'FlowLayoutPanel1
    '
    Me.FlowLayoutPanel1.Controls.Add(Me.grpTestSummary)
    Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 738)
    Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
    Me.FlowLayoutPanel1.Size = New System.Drawing.Size(490, 65)
    Me.FlowLayoutPanel1.TabIndex = 7
    '
    'grpTestSummary
    '
    Me.grpTestSummary.Controls.Add(Me.txtTestLotYield)
    Me.grpTestSummary.Controls.Add(Me.Label18)
    Me.grpTestSummary.Controls.Add(Me.lblTestCurrentYield)
    Me.grpTestSummary.Controls.Add(Me.txtTestCurrentYield)
    Me.grpTestSummary.Controls.Add(Me.Label16)
    Me.grpTestSummary.Controls.Add(Me.txtTestCountTotalUnits)
    Me.grpTestSummary.Controls.Add(Me.Label1)
    Me.grpTestSummary.Controls.Add(Me.txtTestCountErrors)
    Me.grpTestSummary.Controls.Add(Me.Label14)
    Me.grpTestSummary.Controls.Add(Me.txtTestCountUnitsRejected)
    Me.grpTestSummary.Controls.Add(Me.Label15)
    Me.grpTestSummary.Controls.Add(Me.txtTestCountUnitsPassed)
    Me.grpTestSummary.Location = New System.Drawing.Point(3, 3)
    Me.grpTestSummary.Name = "grpTestSummary"
    Me.grpTestSummary.Size = New System.Drawing.Size(473, 57)
    Me.grpTestSummary.TabIndex = 7
    Me.grpTestSummary.TabStop = False
    Me.grpTestSummary.Text = "Functional Test Summary"
    '
    'txtTestLotYield
    '
    Me.txtTestLotYield.AcceptsReturn = True
    Me.txtTestLotYield.Location = New System.Drawing.Point(405, 30)
    Me.txtTestLotYield.Name = "txtTestLotYield"
    Me.txtTestLotYield.Size = New System.Drawing.Size(60, 20)
    Me.txtTestLotYield.TabIndex = 19
    '
    'Label18
    '
    Me.Label18.AutoSize = True
    Me.Label18.Location = New System.Drawing.Point(402, 16)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(48, 13)
    Me.Label18.TabIndex = 18
    Me.Label18.Text = "Lot Yield"
    '
    'lblTestCurrentYield
    '
    Me.lblTestCurrentYield.AutoSize = True
    Me.lblTestCurrentYield.Location = New System.Drawing.Point(322, 16)
    Me.lblTestCurrentYield.Name = "lblTestCurrentYield"
    Me.lblTestCurrentYield.Size = New System.Drawing.Size(61, 13)
    Me.lblTestCurrentYield.TabIndex = 16
    Me.lblTestCurrentYield.Text = "0 Part Yield"
    '
    'txtTestCurrentYield
    '
    Me.txtTestCurrentYield.AcceptsReturn = True
    Me.txtTestCurrentYield.Cursor = System.Windows.Forms.Cursors.NoMove2D
    Me.txtTestCurrentYield.Location = New System.Drawing.Point(325, 30)
    Me.txtTestCurrentYield.Name = "txtTestCurrentYield"
    Me.txtTestCurrentYield.Size = New System.Drawing.Size(60, 20)
    Me.txtTestCurrentYield.TabIndex = 17
    '
    'Label16
    '
    Me.Label16.AutoSize = True
    Me.Label16.Location = New System.Drawing.Point(2, 16)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(58, 13)
    Me.Label16.TabIndex = 14
    Me.Label16.Text = "Total Units"
    '
    'txtTestCountTotalUnits
    '
    Me.txtTestCountTotalUnits.AcceptsReturn = True
    Me.txtTestCountTotalUnits.Location = New System.Drawing.Point(5, 30)
    Me.txtTestCountTotalUnits.Name = "txtTestCountTotalUnits"
    Me.txtTestCountTotalUnits.Size = New System.Drawing.Size(60, 20)
    Me.txtTestCountTotalUnits.TabIndex = 15
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(242, 16)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(34, 13)
    Me.Label1.TabIndex = 12
    Me.Label1.Text = "Errors"
    '
    'txtTestCountErrors
    '
    Me.txtTestCountErrors.AcceptsReturn = True
    Me.txtTestCountErrors.Location = New System.Drawing.Point(245, 30)
    Me.txtTestCountErrors.Name = "txtTestCountErrors"
    Me.txtTestCountErrors.Size = New System.Drawing.Size(60, 20)
    Me.txtTestCountErrors.TabIndex = 13
    '
    'Label14
    '
    Me.Label14.AutoSize = True
    Me.Label14.Location = New System.Drawing.Point(162, 16)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(77, 13)
    Me.Label14.TabIndex = 10
    Me.Label14.Text = "Units Rejected"
    '
    'txtTestCountUnitsRejected
    '
    Me.txtTestCountUnitsRejected.AcceptsReturn = True
    Me.txtTestCountUnitsRejected.Location = New System.Drawing.Point(165, 30)
    Me.txtTestCountUnitsRejected.Name = "txtTestCountUnitsRejected"
    Me.txtTestCountUnitsRejected.Size = New System.Drawing.Size(60, 20)
    Me.txtTestCountUnitsRejected.TabIndex = 11
    '
    'Label15
    '
    Me.Label15.AutoSize = True
    Me.Label15.Location = New System.Drawing.Point(82, 16)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(69, 13)
    Me.Label15.TabIndex = 8
    Me.Label15.Text = "Units Passed"
    '
    'txtTestCountUnitsPassed
    '
    Me.txtTestCountUnitsPassed.AcceptsReturn = True
    Me.txtTestCountUnitsPassed.Location = New System.Drawing.Point(85, 30)
    Me.txtTestCountUnitsPassed.Name = "txtTestCountUnitsPassed"
    Me.txtTestCountUnitsPassed.Size = New System.Drawing.Size(60, 20)
    Me.txtTestCountUnitsPassed.TabIndex = 9
    '
    'Column2
    '
    Me.Column2.HeaderText = "Column2"
    Me.Column2.Name = "Column2"
    '
    'Column1
    '
    Me.Column1.HeaderText = "Column1"
    Me.Column1.Name = "Column1"
    '
    'tabResults
    '
    Me.tabResults.Controls.Add(Me.TabPage1)
    Me.tabResults.Location = New System.Drawing.Point(0, 148)
    Me.tabResults.Name = "tabResults"
    Me.tabResults.SelectedIndex = 0
    Me.tabResults.Size = New System.Drawing.Size(828, 584)
    Me.tabResults.TabIndex = 0
    Me.tabResults.TabStop = False
    '
    'TabPage1
    '
    Me.TabPage1.Location = New System.Drawing.Point(4, 22)
    Me.TabPage1.Name = "TabPage1"
    Me.TabPage1.Size = New System.Drawing.Size(820, 558)
    Me.TabPage1.TabIndex = 0
    Me.TabPage1.Text = "TabPage1"
    Me.TabPage1.UseVisualStyleBackColor = True
    '
    'GroupBox4
    '
    Me.GroupBox4.Controls.Add(Me.lblLotType)
    Me.GroupBox4.Location = New System.Drawing.Point(0, 29)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(174, 38)
    Me.GroupBox4.TabIndex = 1
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "Lot Type"
    '
    'lblLotType
    '
    Me.lblLotType.AutoSize = True
    Me.lblLotType.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblLotType.Location = New System.Drawing.Point(26, 17)
    Me.lblLotType.Name = "lblLotType"
    Me.lblLotType.Size = New System.Drawing.Size(0, 13)
    Me.lblLotType.TabIndex = 1
    '
    'StatusStrip1
    '
    Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel1, Me.StatusLabel2})
    Me.StatusStrip1.Location = New System.Drawing.Point(0, 806)
    Me.StatusStrip1.Name = "StatusStrip1"
    Me.StatusStrip1.Size = New System.Drawing.Size(846, 29)
    Me.StatusStrip1.SizingGrip = False
    Me.StatusStrip1.TabIndex = 9
    Me.StatusStrip1.Text = "StatusStrip1"
    '
    'StatusLabel1
    '
    Me.StatusLabel1.AutoSize = False
    Me.StatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
    Me.StatusLabel1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.StatusLabel1.Name = "StatusLabel1"
    Me.StatusLabel1.Size = New System.Drawing.Size(300, 24)
    Me.StatusLabel1.Text = "StatusLabel1"
    Me.StatusLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'StatusLabel2
    '
    Me.StatusLabel2.AutoSize = False
    Me.StatusLabel2.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
    Me.StatusLabel2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.StatusLabel2.Name = "StatusLabel2"
    Me.StatusLabel2.Size = New System.Drawing.Size(500, 24)
    Me.StatusLabel2.Text = "StatusLabel2"
    Me.StatusLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'PrintPreviewDialog1
    '
    Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
    Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
    Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
    Me.PrintPreviewDialog1.Enabled = True
    Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
    Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
    Me.PrintPreviewDialog1.Visible = False
    '
    'PrintGraphs
    '
    Me.PrintGraphs.DocumentName = "Graphs"
    '
    'PrintDialog1
    '
    Me.PrintDialog1.UseEXDialog = True
    '
    'PrintTable
    '
    Me.PrintTable.DocumentName = "Results"
    '
    'MenuStripMain
    '
    Me.MenuStripMain.Enabled = False
    Me.MenuStripMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFunction, Me.mnuLot, Me.mnuTool, Me.mnuOptions, Me.mnuExternalTestMethods, Me.mnuHelp})
    Me.MenuStripMain.Location = New System.Drawing.Point(0, 0)
    Me.MenuStripMain.Name = "MenuStripMain"
    Me.MenuStripMain.Size = New System.Drawing.Size(846, 24)
    Me.MenuStripMain.TabIndex = 15
    Me.MenuStripMain.Text = "MenuStrip1"
    '
    'mnuFunction
    '
    Me.mnuFunction.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFunctionTest, Me.mnuFunctionProgram, Me.mnuFunctionProgramTest, Me.mnuFunctionPrint, Me.mnuFunctionPrintPreview, Me.mnuFunctionAutoPrint, Me.mnuFunctionExit})
    Me.mnuFunction.Name = "mnuFunction"
    Me.mnuFunction.Size = New System.Drawing.Size(66, 20)
    Me.mnuFunction.Text = "Function"
    '
    'mnuFunctionTest
    '
    Me.mnuFunctionTest.Name = "mnuFunctionTest"
    Me.mnuFunctionTest.ShortcutKeyDisplayString = "F1"
    Me.mnuFunctionTest.ShortcutKeys = System.Windows.Forms.Keys.F1
    Me.mnuFunctionTest.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionTest.Text = "Test"
    '
    'mnuFunctionProgram
    '
    Me.mnuFunctionProgram.Enabled = False
    Me.mnuFunctionProgram.Name = "mnuFunctionProgram"
    Me.mnuFunctionProgram.ShortcutKeyDisplayString = "F2"
    Me.mnuFunctionProgram.ShortcutKeys = System.Windows.Forms.Keys.F2
    Me.mnuFunctionProgram.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionProgram.Text = "Program"
    Me.mnuFunctionProgram.Visible = False
    '
    'mnuFunctionProgramTest
    '
    Me.mnuFunctionProgramTest.Name = "mnuFunctionProgramTest"
    Me.mnuFunctionProgramTest.ShortcutKeyDisplayString = "F3"
    Me.mnuFunctionProgramTest.ShortcutKeys = System.Windows.Forms.Keys.F3
    Me.mnuFunctionProgramTest.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionProgramTest.Text = "Program And Test"
    '
    'mnuFunctionPrint
    '
    Me.mnuFunctionPrint.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFunctionPrintTestResults, Me.mnuFunctionPrintTestStatistics, Me.mnuFunctionPrintProgResults, Me.mnuFunctionPrintProgStatistics, Me.mnuFunctionPrintGraphs, Me.mnuFunctionPrintPreviousLot})
    Me.mnuFunctionPrint.Name = "mnuFunctionPrint"
    Me.mnuFunctionPrint.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionPrint.Text = "Print"
    '
    'mnuFunctionPrintTestResults
    '
    Me.mnuFunctionPrintTestResults.Name = "mnuFunctionPrintTestResults"
    Me.mnuFunctionPrintTestResults.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintTestResults.Text = "Test Results"
    '
    'mnuFunctionPrintTestStatistics
    '
    Me.mnuFunctionPrintTestStatistics.Name = "mnuFunctionPrintTestStatistics"
    Me.mnuFunctionPrintTestStatistics.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintTestStatistics.Text = "Test Statistics"
    '
    'mnuFunctionPrintProgResults
    '
    Me.mnuFunctionPrintProgResults.Name = "mnuFunctionPrintProgResults"
    Me.mnuFunctionPrintProgResults.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintProgResults.Text = "Programming Results"
    '
    'mnuFunctionPrintProgStatistics
    '
    Me.mnuFunctionPrintProgStatistics.Name = "mnuFunctionPrintProgStatistics"
    Me.mnuFunctionPrintProgStatistics.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintProgStatistics.Text = "Programming Statistics"
    '
    'mnuFunctionPrintGraphs
    '
    Me.mnuFunctionPrintGraphs.Name = "mnuFunctionPrintGraphs"
    Me.mnuFunctionPrintGraphs.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintGraphs.Text = "Graphs"
    '
    'mnuFunctionPrintPreviousLot
    '
    Me.mnuFunctionPrintPreviousLot.Name = "mnuFunctionPrintPreviousLot"
    Me.mnuFunctionPrintPreviousLot.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintPreviousLot.Text = "Previous Lot"
    '
    'mnuFunctionPrintPreview
    '
    Me.mnuFunctionPrintPreview.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFunctionPrintPreviewTestResults, Me.mnuFunctionPrintPreviewTestStatistics, Me.mnuFunctionPrintPreviewProgResults, Me.mnuFunctionPrintPreviewProgStatistics, Me.mnuFunctionPrintPreviewGraphs})
    Me.mnuFunctionPrintPreview.Name = "mnuFunctionPrintPreview"
    Me.mnuFunctionPrintPreview.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionPrintPreview.Text = "Print Preview"
    '
    'mnuFunctionPrintPreviewTestResults
    '
    Me.mnuFunctionPrintPreviewTestResults.Name = "mnuFunctionPrintPreviewTestResults"
    Me.mnuFunctionPrintPreviewTestResults.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintPreviewTestResults.Text = "Test Results"
    '
    'mnuFunctionPrintPreviewTestStatistics
    '
    Me.mnuFunctionPrintPreviewTestStatistics.Name = "mnuFunctionPrintPreviewTestStatistics"
    Me.mnuFunctionPrintPreviewTestStatistics.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintPreviewTestStatistics.Text = "Test Statistics"
    '
    'mnuFunctionPrintPreviewProgResults
    '
    Me.mnuFunctionPrintPreviewProgResults.Name = "mnuFunctionPrintPreviewProgResults"
    Me.mnuFunctionPrintPreviewProgResults.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintPreviewProgResults.Text = "Programming Results"
    '
    'mnuFunctionPrintPreviewProgStatistics
    '
    Me.mnuFunctionPrintPreviewProgStatistics.Name = "mnuFunctionPrintPreviewProgStatistics"
    Me.mnuFunctionPrintPreviewProgStatistics.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintPreviewProgStatistics.Text = "Programming Statistics"
    '
    'mnuFunctionPrintPreviewGraphs
    '
    Me.mnuFunctionPrintPreviewGraphs.Name = "mnuFunctionPrintPreviewGraphs"
    Me.mnuFunctionPrintPreviewGraphs.Size = New System.Drawing.Size(197, 22)
    Me.mnuFunctionPrintPreviewGraphs.Text = "Graphs"
    '
    'mnuFunctionAutoPrint
    '
    Me.mnuFunctionAutoPrint.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuFunctionAutoPrintProgResults, Me.mnuFunctionAutoPrintTestResults, Me.mnuFunctionAutoPrintGraphs})
    Me.mnuFunctionAutoPrint.Name = "mnuFunctionAutoPrint"
    Me.mnuFunctionAutoPrint.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionAutoPrint.Text = "Auto Print"
    '
    'mnuFunctionAutoPrintProgResults
    '
    Me.mnuFunctionAutoPrintProgResults.CheckOnClick = True
    Me.mnuFunctionAutoPrintProgResults.Name = "mnuFunctionAutoPrintProgResults"
    Me.mnuFunctionAutoPrintProgResults.Size = New System.Drawing.Size(160, 22)
    Me.mnuFunctionAutoPrintProgResults.Text = "Program Results"
    '
    'mnuFunctionAutoPrintTestResults
    '
    Me.mnuFunctionAutoPrintTestResults.CheckOnClick = True
    Me.mnuFunctionAutoPrintTestResults.Name = "mnuFunctionAutoPrintTestResults"
    Me.mnuFunctionAutoPrintTestResults.Size = New System.Drawing.Size(160, 22)
    Me.mnuFunctionAutoPrintTestResults.Text = "Test Results"
    '
    'mnuFunctionAutoPrintGraphs
    '
    Me.mnuFunctionAutoPrintGraphs.CheckOnClick = True
    Me.mnuFunctionAutoPrintGraphs.Name = "mnuFunctionAutoPrintGraphs"
    Me.mnuFunctionAutoPrintGraphs.Size = New System.Drawing.Size(160, 22)
    Me.mnuFunctionAutoPrintGraphs.Text = "Graphs"
    '
    'mnuFunctionExit
    '
    Me.mnuFunctionExit.Name = "mnuFunctionExit"
    Me.mnuFunctionExit.Size = New System.Drawing.Size(189, 22)
    Me.mnuFunctionExit.Text = "Exit"
    '
    'mnuLot
    '
    Me.mnuLot.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuLotType, Me.mnuLotNames, Me.mnuIncrementLotRun})
    Me.mnuLot.Name = "mnuLot"
    Me.mnuLot.Size = New System.Drawing.Size(36, 20)
    Me.mnuLot.Text = "Lot"
    '
    'mnuLotType
    '
    Me.mnuLotType.Name = "mnuLotType"
    Me.mnuLotType.Size = New System.Drawing.Size(172, 22)
    Me.mnuLotType.Text = "Lot Type"
    '
    'mnuLotNames
    '
    Me.mnuLotNames.Name = "mnuLotNames"
    Me.mnuLotNames.Size = New System.Drawing.Size(172, 22)
    Me.mnuLotNames.Text = "Lot Names"
    '
    'mnuIncrementLotRun
    '
    Me.mnuIncrementLotRun.Name = "mnuIncrementLotRun"
    Me.mnuIncrementLotRun.Size = New System.Drawing.Size(172, 22)
    Me.mnuIncrementLotRun.Text = "Increment Lot Run"
    '
    'mnuTool
    '
    Me.mnuTool.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuToolOPC})
    Me.mnuTool.Name = "mnuTool"
    Me.mnuTool.Size = New System.Drawing.Size(43, 20)
    Me.mnuTool.Text = "Tool"
    '
    'mnuToolOPC
    '
    Me.mnuToolOPC.Name = "mnuToolOPC"
    Me.mnuToolOPC.Size = New System.Drawing.Size(98, 22)
    Me.mnuToolOPC.Text = "OPC"
    '
    'mnuOptions
    '
    Me.mnuOptions.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOptionsOnPLCStart, Me.mnuOptionsDefaultTabPage})
    Me.mnuOptions.Name = "mnuOptions"
    Me.mnuOptions.Size = New System.Drawing.Size(61, 20)
    Me.mnuOptions.Text = "Options"
    '
    'mnuOptionsOnPLCStart
    '
    Me.mnuOptionsOnPLCStart.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOptionsOnPlcStartProgram, Me.mnuOptionsOnPlcStartTest})
    Me.mnuOptionsOnPLCStart.Name = "mnuOptionsOnPLCStart"
    Me.mnuOptionsOnPLCStart.Size = New System.Drawing.Size(164, 22)
    Me.mnuOptionsOnPLCStart.Text = "On PLC Start"
    '
    'mnuOptionsOnPlcStartProgram
    '
    Me.mnuOptionsOnPlcStartProgram.Name = "mnuOptionsOnPlcStartProgram"
    Me.mnuOptionsOnPlcStartProgram.Size = New System.Drawing.Size(120, 22)
    Me.mnuOptionsOnPlcStartProgram.Text = "Program"
    '
    'mnuOptionsOnPlcStartTest
    '
    Me.mnuOptionsOnPlcStartTest.Name = "mnuOptionsOnPlcStartTest"
    Me.mnuOptionsOnPlcStartTest.Size = New System.Drawing.Size(120, 22)
    Me.mnuOptionsOnPlcStartTest.Text = "Test"
    '
    'mnuOptionsDefaultTabPage
    '
    Me.mnuOptionsDefaultTabPage.ForeColor = System.Drawing.SystemColors.ControlText
    Me.mnuOptionsDefaultTabPage.Name = "mnuOptionsDefaultTabPage"
    Me.mnuOptionsDefaultTabPage.Size = New System.Drawing.Size(164, 22)
    Me.mnuOptionsDefaultTabPage.Text = "Default Tab Page"
    '
    'mnuExternalTestMethods
    '
    Me.mnuExternalTestMethods.Enabled = False
    Me.mnuExternalTestMethods.Name = "mnuExternalTestMethods"
    Me.mnuExternalTestMethods.Size = New System.Drawing.Size(135, 20)
    Me.mnuExternalTestMethods.Text = "External Test Methods"
    '
    'mnuHelp
    '
    Me.mnuHelp.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuHelpAbout})
    Me.mnuHelp.Name = "mnuHelp"
    Me.mnuHelp.Size = New System.Drawing.Size(44, 20)
    Me.mnuHelp.Text = "Help"
    '
    'mnuHelpAbout
    '
    Me.mnuHelpAbout.Name = "mnuHelpAbout"
    Me.mnuHelpAbout.Size = New System.Drawing.Size(107, 22)
    Me.mnuHelpAbout.Text = "About"
    '
    'btnAbortTest
    '
    Me.btnAbortTest.Location = New System.Drawing.Point(748, 36)
    Me.btnAbortTest.Name = "btnAbortTest"
    Me.btnAbortTest.Size = New System.Drawing.Size(80, 23)
    Me.btnAbortTest.TabIndex = 16
    Me.btnAbortTest.Text = "Abort"
    Me.btnAbortTest.UseVisualStyleBackColor = True
    '
    'lblPassFailIndicator
    '
    Me.lblPassFailIndicator.BackColor = System.Drawing.Color.Red
    Me.lblPassFailIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblPassFailIndicator.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblPassFailIndicator.ForeColor = System.Drawing.Color.White
    Me.lblPassFailIndicator.Location = New System.Drawing.Point(619, 45)
    Me.lblPassFailIndicator.Name = "lblPassFailIndicator"
    Me.lblPassFailIndicator.Size = New System.Drawing.Size(109, 79)
    Me.lblPassFailIndicator.TabIndex = 29
    Me.lblPassFailIndicator.Text = "UNKNOWN"
    Me.lblPassFailIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'gbEncodedSerial
    '
    Me.gbEncodedSerial.Controls.Add(Me.lblDefault)
    Me.gbEncodedSerial.Controls.Add(Me.lblEncodedSerialNumber)
    Me.gbEncodedSerial.Location = New System.Drawing.Point(360, 29)
    Me.gbEncodedSerial.Name = "gbEncodedSerial"
    Me.gbEncodedSerial.Size = New System.Drawing.Size(174, 45)
    Me.gbEncodedSerial.TabIndex = 34
    Me.gbEncodedSerial.TabStop = False
    Me.gbEncodedSerial.Text = "Encoded Serial "
    '
    'lblDefault
    '
    Me.lblDefault.AutoSize = True
    Me.lblDefault.Location = New System.Drawing.Point(152, 46)
    Me.lblDefault.Name = "lblDefault"
    Me.lblDefault.Size = New System.Drawing.Size(0, 13)
    Me.lblDefault.TabIndex = 10
    '
    'lblEncodedSerialNumber
    '
    Me.lblEncodedSerialNumber.BackColor = System.Drawing.SystemColors.ButtonHighlight
    Me.lblEncodedSerialNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblEncodedSerialNumber.Location = New System.Drawing.Point(9, 17)
    Me.lblEncodedSerialNumber.Name = "lblEncodedSerialNumber"
    Me.lblEncodedSerialNumber.Size = New System.Drawing.Size(159, 20)
    Me.lblEncodedSerialNumber.TabIndex = 0
    Me.lblEncodedSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'lblVoltageValue
    '
    Me.lblVoltageValue.BackColor = System.Drawing.Color.White
    Me.lblVoltageValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblVoltageValue.Location = New System.Drawing.Point(456, 106)
    Me.lblVoltageValue.Name = "lblVoltageValue"
    Me.lblVoltageValue.Size = New System.Drawing.Size(56, 18)
    Me.lblVoltageValue.TabIndex = 36
    Me.lblVoltageValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblVoltage
    '
    Me.lblVoltage.AutoSize = True
    Me.lblVoltage.Location = New System.Drawing.Point(453, 91)
    Me.lblVoltage.Name = "lblVoltage"
    Me.lblVoltage.Size = New System.Drawing.Size(43, 13)
    Me.lblVoltage.TabIndex = 35
    Me.lblVoltage.Text = "Voltage"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(748, 70)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(36, 13)
    Me.Label2.TabIndex = 37
    Me.Label2.Text = "Cart #"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(751, 118)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(45, 13)
    Me.Label3.TabIndex = 38
    Me.Label3.Text = "Part Qty"
    '
    'CartNumber
    '
    Me.CartNumber.Location = New System.Drawing.Point(748, 85)
    Me.CartNumber.Name = "CartNumber"
    Me.CartNumber.Size = New System.Drawing.Size(72, 20)
    Me.CartNumber.TabIndex = 39
    '
    'PartsInCart
    '
    Me.PartsInCart.Location = New System.Drawing.Point(748, 134)
    Me.PartsInCart.Name = "PartsInCart"
    Me.PartsInCart.Size = New System.Drawing.Size(72, 20)
    Me.PartsInCart.TabIndex = 40
    '
    'SelectNewCart
    '
    Me.SelectNewCart.Location = New System.Drawing.Point(619, 136)
    Me.SelectNewCart.Name = "SelectNewCart"
    Me.SelectNewCart.Size = New System.Drawing.Size(108, 28)
    Me.SelectNewCart.TabIndex = 41
    Me.SelectNewCart.Text = "Select New Cart"
    Me.SelectNewCart.UseVisualStyleBackColor = True
    '
    'frmTsopMain
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
    Me.BackColor = System.Drawing.SystemColors.Control
    Me.ClientSize = New System.Drawing.Size(846, 835)
    Me.Controls.Add(Me.SelectNewCart)
    Me.Controls.Add(Me.PartsInCart)
    Me.Controls.Add(Me.CartNumber)
    Me.Controls.Add(Me.Label3)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.lblVoltageValue)
    Me.Controls.Add(Me.lblVoltage)
    Me.Controls.Add(Me.gbEncodedSerial)
    Me.Controls.Add(Me.lblPassFailIndicator)
    Me.Controls.Add(Me.MenuStripMain)
    Me.Controls.Add(Me.StatusStrip1)
    Me.Controls.Add(Me.btnAbortTest)
    Me.Controls.Add(Me.GroupBox2)
    Me.Controls.Add(Me.GroupBox4)
    Me.Controls.Add(Me.FlowLayoutPanel1)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.tabResults)
    Me.Cursor = System.Windows.Forms.Cursors.Default
    Me.MinimizeBox = False
    Me.MinimumSize = New System.Drawing.Size(800, 600)
    Me.Name = "frmTsopMain"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
    Me.Text = "EE1131 - 647 / 648 APS Cal Test System"
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.FlowLayoutPanel1.ResumeLayout(False)
    Me.grpTestSummary.ResumeLayout(False)
    Me.grpTestSummary.PerformLayout()
    Me.tabResults.ResumeLayout(False)
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox4.PerformLayout()
    Me.StatusStrip1.ResumeLayout(False)
    Me.StatusStrip1.PerformLayout()
    Me.MenuStripMain.ResumeLayout(False)
    Me.MenuStripMain.PerformLayout()
    Me.gbEncodedSerial.ResumeLayout(False)
    Me.gbEncodedSerial.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents cboParameterSets As System.Windows.Forms.ComboBox
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
  Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
  Friend WithEvents tabResults As System.Windows.Forms.TabControl
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
  Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
  Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
  Friend WithEvents PrintPreviewDialog1 As System.Windows.Forms.PrintPreviewDialog
  Friend WithEvents PrintGraphs As System.Drawing.Printing.PrintDocument
  Friend WithEvents PrintDialog1 As System.Windows.Forms.PrintDialog
  Friend WithEvents PrintTable As System.Drawing.Printing.PrintDocument
  Friend WithEvents AdditionalIOToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents LinearEncoderMonitorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents MenuStripMain As System.Windows.Forms.MenuStrip
  Friend WithEvents mnuFunction As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionTest As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionProgram As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionProgramTest As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintProgResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintProgStatistics As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintTestResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintTestStatistics As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintGraphs As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreview As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreviewProgResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreviewProgStatistics As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreviewTestResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreviewTestStatistics As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreviewGraphs As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionAutoPrint As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionAutoPrintProgResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionAutoPrintTestResults As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionAutoPrintGraphs As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionExit As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuTool As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuToolOPC As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuOptions As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuOptionsOnPLCStart As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuOptionsOnPlcStartProgram As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuOptionsOnPlcStartTest As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuExternalTestMethods As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuHelp As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuLot As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuHelpAbout As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuLotType As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents lblLotType As System.Windows.Forms.Label
  Friend WithEvents mnuLotNames As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents lblLotName As System.Windows.Forms.Label
  Friend WithEvents mnuIncrementLotRun As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents btnAbortTest As System.Windows.Forms.Button
  Friend WithEvents mnuOptionsDefaultTabPage As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents mnuFunctionPrintPreviousLot As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents StatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents lblPassFailIndicator As System.Windows.Forms.Label
  Friend WithEvents gbEncodedSerial As System.Windows.Forms.GroupBox
  Friend WithEvents lblDefault As System.Windows.Forms.Label
  Friend WithEvents lblEncodedSerialNumber As System.Windows.Forms.Label
  Friend WithEvents lblVoltageValue As System.Windows.Forms.Label
  Friend WithEvents lblVoltage As System.Windows.Forms.Label
  Friend WithEvents grpTestSummary As GroupBox
  Friend WithEvents txtTestLotYield As TextBox
  Friend WithEvents Label18 As Label
  Friend WithEvents lblTestCurrentYield As Label
  Friend WithEvents txtTestCurrentYield As TextBox
  Friend WithEvents Label16 As Label
  Friend WithEvents txtTestCountTotalUnits As TextBox
  Friend WithEvents Label1 As Label
  Friend WithEvents txtTestCountErrors As TextBox
  Friend WithEvents Label14 As Label
  Friend WithEvents txtTestCountUnitsRejected As TextBox
  Friend WithEvents Label15 As Label
  Friend WithEvents txtTestCountUnitsPassed As TextBox
  Friend WithEvents Label2 As Label
  Friend WithEvents Label3 As Label
  Friend WithEvents CartNumber As TextBox
  Friend WithEvents PartsInCart As TextBox
  Friend WithEvents SelectNewCart As Button
End Class
