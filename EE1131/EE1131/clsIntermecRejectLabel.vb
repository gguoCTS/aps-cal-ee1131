﻿Public Class clsIntermecRejectLabel

  Public Structure rejectLabelStruct
    Dim BOM As String
    Dim Voltage As String
    Dim Serial As String
    Dim PrintDateTime As String
    Dim ProcessName As String
    Dim Description As String
  End Structure

  Public rejectLabelStructure As rejectLabelStruct
  Private pd As New System.Drawing.Printing.PrintDocument
  Private strPrintData As String

  Public Sub PrintRejectLabel()

    Dim Description As String = ""
    Dim Description2 As String = ""
    Dim Description3 As String = ""
    Dim TempStr As String = ""
    Dim intLoop As Integer

    Const txtLen As Integer = 32

    If rejectLabelStructure.Description.Length > txtLen Then
      TempStr = rejectLabelStructure.Description
      Description = ""
      For intLoop = 1 To txtLen
        Description = Description & Mid(TempStr, intLoop, 1)
      Next
    Else
      Description = rejectLabelStructure.Description
    End If
    'Description = Description & " "
    If rejectLabelStructure.Description.Length > txtLen*2 Then
      TempStr = Mid(rejectLabelStructure.Description, txtLen+1, txtLen*2)
      Description2 = ""
      For intLoop = 1 To txtLen 
        Description2 = Description2 & Mid(TempStr, intLoop, 1)
      Next
    Else
      If rejectLabelStructure.Description.Length > txtLen  Then
        TempStr = Mid(rejectLabelStructure.Description, txtLen + 1, rejectLabelStructure.Description.Length)
        For intLoop = 1 To rejectLabelStructure.Description.Length - txtLen 
          Description2 = Description2 & Mid(TempStr, intLoop, 1)
        Next
      End If
    End If
    'Description2 = Description2 & " "
    If rejectLabelStructure.Description.Length > txtLen*2 + txtLen Then
      TempStr = Mid(rejectLabelStructure.Description, txtLen+1, txtLen*2 + txtLen)
      Description3 = ""
      For intLoop = 1 To txtLen 
        Description3 = Description3 & Mid(TempStr, intLoop, 1)
      Next
    Else
      If rejectLabelStructure.Description.Length > txtLen*2 Then
        TempStr = Mid(rejectLabelStructure.Description, txtLen*2+1, rejectLabelStructure.Description.Length)
        For intLoop = 1 To rejectLabelStructure.Description.Length - txtLen*2
          Description3 = Description3 & Mid(TempStr, intLoop, 1)
        Next
      End If
    End If
    'Description3 = Description3 & " "

    strPrintData = ""
    strPrintData = strPrintData & "<STX><ESC>P<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E0;F0<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E1;F1<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E2;F2<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E3;F3<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E4;F4<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E5;F5<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E6;F6<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>E7;F7<ETX>" & vbCrLf

    strPrintData = strPrintData & "<STX>H0;o270,005;f3;c33;h1;w1;d0,05;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H1;o270,150;f3;c33;h1;w1;d0,05;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H2;o270,300;f3;c33;h1;w1;d0,12;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H3;o210,200;f3;c33;h1;w1;d0,30;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H4;o210,005;f3;c33;h1;w1;d0,30;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H5;o150,005;f3;c31;h1;w1;d0,50;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H6;o110,005;f3;c31;h1;w1;d0,50;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>H7;o70,005;f3;c31;h1;w1;d0,50;<ETX>" & vbCrLf

    strPrintData = strPrintData & "<STX>R;<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E0<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E1<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E2<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E3<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E4<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E5<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E6<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ESC>E7<ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><CAN><ETX>" & vbCrLf

    strPrintData = strPrintData & "<STX>" & rejectLabelStructure.BOM & "<CR> <ETX> "  & vbCrLf
    strPrintData = strPrintData & "<STX>" & rejectLabelStructure.Voltage & "<CR> <ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>" & rejectLabelStructure.Serial & "<CR> <ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>" & rejectLabelStructure.PrintDateTime & "<CR><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>" & rejectLabelStructure.ProcessName & "<CR><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>" & Description & "<CR><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>" & Description2 & "<CR><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX>" & Description3 & "<CR><ETX>" & vbCrLf
    strPrintData = strPrintData & "<STX><ETB><FF><ETX>" & vbCrLf

    pd.PrinterSettings.PrinterName = "Reject Printer"
    'strPrintData = strPrintData & "<STX><FF><ETX>" & vbCrLf
    AddHandler pd.PrintPage, AddressOf pd_printpage

    pd.Print()
    RemoveHandler pd.PrintPage, AddressOf pd_printpage
  End Sub
  Private Sub pd_printpage(ByVal sender As Object, ByVal ev As System.Drawing.Printing.PrintPageEventArgs)
    Dim line As String = ""
    Dim printfont As Font
    Dim leftmargin As Integer
    Dim ypos As Integer
    printfont = New Font("Arial", 2)
    line = strPrintData ' & strPrintData2 & strPrintData
    ev.Graphics.DrawString(line, printfont, Brushes.Black, leftmargin, ypos, New StringFormat())

  End Sub
End Class
