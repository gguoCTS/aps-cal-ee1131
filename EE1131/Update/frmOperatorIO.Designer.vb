﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOperatorIO
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.lblButtonIndicator = New System.Windows.Forms.Label()
    Me.lblProx = New System.Windows.Forms.Label()
    Me.SuspendLayout()
    '
    'lblButtonIndicator
    '
    Me.lblButtonIndicator.BackColor = System.Drawing.Color.Lime
    Me.lblButtonIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblButtonIndicator.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblButtonIndicator.ForeColor = System.Drawing.Color.Black
    Me.lblButtonIndicator.Location = New System.Drawing.Point(84, 32)
    Me.lblButtonIndicator.Name = "lblButtonIndicator"
    Me.lblButtonIndicator.Size = New System.Drawing.Size(109, 79)
    Me.lblButtonIndicator.TabIndex = 30
    Me.lblButtonIndicator.Text = "UNKNOWN"
    Me.lblButtonIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblProx
    '
    Me.lblProx.BackColor = System.Drawing.Color.Lime
    Me.lblProx.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.lblProx.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblProx.ForeColor = System.Drawing.Color.Black
    Me.lblProx.Location = New System.Drawing.Point(84, 122)
    Me.lblProx.Name = "lblProx"
    Me.lblProx.Size = New System.Drawing.Size(109, 79)
    Me.lblProx.TabIndex = 30
    Me.lblProx.Text = "UNKNOWN"
    Me.lblProx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'frmOperatorIO
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(284, 262)
    Me.Controls.Add(Me.lblProx)
    Me.Controls.Add(Me.lblButtonIndicator)
    Me.Name = "frmOperatorIO"
    Me.Text = "frmOperatorIO"
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents lblButtonIndicator As System.Windows.Forms.Label
  Friend WithEvents lblProx As Label
End Class
